import random
import string


def generate_key(length):
    """
    Generates a random string of concatenated characters and digits of length
    provided by the `length` parameter.
    """
    return "".join(
        [random.choice(string.ascii_letters + string.digits) for _ in range(length)]
    )

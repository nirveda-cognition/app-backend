from factory.django import DjangoModelFactory


class CustomModelFactory(DjangoModelFactory):
    """
    Incoporates useful utils and behavior into the generic `DjangoModelFactory`.
    """

    @classmethod
    def create(cls, *args, **kwargs):
        print("DEBUG : at create function in CustomModelFactory.")
        created = super(CustomModelFactory, cls).create(*args, **kwargs)
        return cls.post_create(created, **kwargs)

    @classmethod
    def post_create(cls, model, **kwargs):
        return model

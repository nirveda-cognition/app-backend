import factory
import mock
from django.utils.text import slugify

from backend.app.authentication.models import NCGroup, Role
from backend.app.collection.models import (
    Collection,
    CollectionPointer,
    CollectionResult,
)
from backend.app.document.models import Document, DocumentStatus
from backend.app.organization.models import Organization, OrganizationType
from backend.app.task.models import CallbackRequest, Task, TaskResult
from backend.app.user.models import CustomUser

from .base import CustomModelFactory
from .fields import NowDateTimeField, PastDateTimeField
from .utils import generate_key


def ConstantTimeMixin(*fields):
    """
    If a model has an auto-now time related field, we cannot simply include
    this value in the factory kwargs since it will be overridden to the
    current time when the model is saved.

    If we use this mixin for the factory, then it will allow us to override
    the provided field so it can be explicitly provided in the factory
    arguments without an override from Django on model save.
    """

    class DynamicConstantTimeMixin(object):
        @classmethod
        def post_create(cls, model, **kwargs):
            update_kwargs = {}
            for field in fields:
                if field in kwargs:
                    update_kwargs[field] = kwargs[field]
            instance = super(DynamicConstantTimeMixin, cls).post_create(model, **kwargs)
            # Applying a direct update bypasses the auto time fields.
            model.__class__.objects.filter(pk=model.pk).update(**update_kwargs)
            instance.refresh_from_db()
            return instance

    return DynamicConstantTimeMixin


class TimestampedFactory(CustomModelFactory):
    created_at = PastDateTimeField()
    updated_at = PastDateTimeField()

    class Meta:
        abstract = True


class OrganizationTypeFactory(CustomModelFactory):
    """
    A DjangoModelFactory to create instances of `obj:OrganizationType`.
    """

    # We need to skip the first 3 organization types created as fixtures.
    id = factory.Sequence(lambda n: 4 + n)
    title = factory.Faker("name")
    description = factory.Faker("sentence")
    slug = factory.LazyAttribute(lambda o: slugify(o.title))

    class Meta:
        model = OrganizationType


class OrganizationFactory(TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:Organization`.
    """

    # We need to skip the first 3 organizations created as fixtures.
    id = factory.Sequence(lambda n: 4 + n)
    name = factory.Faker("name")
    description = factory.Faker("sentence")
    is_active = True
    api_access = True
    api_key = factory.LazyFunction(lambda: generate_key(12))
    api_url = factory.Faker("url")
    json_info = {}
    logo = factory.Faker("file_path")
    callback_whitelist = None
    type = factory.SubFactory(OrganizationTypeFactory)
    slug = factory.LazyAttribute(lambda o: slugify(o.name))

    class Meta:
        model = Organization


class UserFactory(TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:CustomUser`.

    The `obj:UserFactory` exposes certain convenience parameters for
    creating an `obj:CustomUser` instance with a certain roles/permissions:

    Creating User w Explicit Role

        - UserFactory(role="admin", ...)
            Creates a `obj:CustomUser` instance with ADMIN :obj:`Role`.
            The is_admin flag will default to True.

            Defaults:
                is_superuser = False
                is_admin = True

        - UserFactory(role="account_manager", ...)
            Creates a `obj:CustomUser` instance with ACCOUNT_MANAGER
            :obj:`Role`.

            Defaults:
                is_superuser = False
                is_admin = True

        - UserFactory(role="account_user", ...)
            Creates a `obj:CustomUser` instance with ACCOUNT_USER
            :obj:`Role`.

            Defaults:
                is_superuser = False
                is_admin = False

         - UserFactory(role="user", ...)
            Creates a `obj:CustomUser` instance with USER :obj:`Role`.

            Defaults:
                is_superuser = False
                is_admin = False

    Creating User w Explicit Permission Flags

        - UserFactory(admin=True, ...)
            Creates a `obj:CustomUser` instance with is_admin flag set to True,
            is_superuser/is_staff flags set to False.

            Defaults:
                role = ADMIN
                is_superuser = False

        - UserFactory(superuser=True, ...)
            Creates a `obj:CustomUser` is_superuser flag set to True.

            Defaults:
                role = ADMIN
                is_admin = False

        - UserFactory(staff=True)
            Creates a `obj:CustomUser` is_staff flag set to True.

            Defaults:
                role = USER
                is_admin = False
                is_superuser = False

    Note:
        The `obj:UserFactory` relies on a fixture being present that populates
        the roles in the test database.  If this fixture is removed, this
        factory will no longer be usable.
    """

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    is_staff = False
    email = factory.Faker("email")
    username = factory.SelfAttribute("email")
    is_admin = False
    is_superuser = False
    is_active = True
    role = factory.LazyFunction(lambda: Role.objects.get(name="User"))
    timezone = factory.Faker("timezone")
    language = "en"
    organization = factory.SubFactory(OrganizationFactory)

    class Meta:
        model = CustomUser

    class Params:
        superuser = factory.Trait(is_superuser=True, is_admin=False, is_staff=True)
        admin = factory.Trait(is_superuser=False, is_admin=True, is_staff=False)
        staff = factory.Trait(is_superuser=False, is_admin=False, is_staff=True)

    @classmethod
    def post_create(cls, model, **kwargs):
        if "created_at" in kwargs:
            model.created_at = kwargs["created_at"]
            model.save()
        return super(UserFactory, cls).post_create(model, **kwargs)

    @classmethod
    def create(cls, *args, **kwargs):
        print("DEBUG : at create function in UserFactory class")
        data = dict(*args, **kwargs)
        if "role" in data:
            if isinstance(data["role"], str):
                try:
                    data["role"] = Role.objects.get(name=data["role"])
                except Role.DoesNotExist:
                    raise Exception(
                        "When creating a user, if providing the role as a "
                        "string name, %s, the Role with the provided name must "
                        "exist.  Either create a new Role and provide that "
                        "role explicitly or provide a name that corresponds to "
                        "an already existing Role."
                    )
            elif isinstance(data["role"], int):
                try:
                    data["role"] = Role.objects.get(code=data["role"])
                except Role.DoesNotExist:
                    raise Exception(
                        "When creating a user, if providing the role as an "
                        "integer code, %s, the Role with the provided code "
                        "must exist.  Either create a new Role and provide "
                        "that role explicitly or provide a code that "
                        "corresponds to an already existing Role."
                    )
        return super(UserFactory, cls).create(*args, **kwargs)

    @factory.post_generation
    def groups(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for group in extracted:
                # pylint: disable=no-member
                self.groups.add(group)


class RoleFactory(TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:Role`.
    """

    # We need to skip the first 4 roles created as fixtures.
    id = factory.Sequence(lambda n: n + 5)
    name = factory.Faker("name")
    description = factory.Faker("sentence")
    code = factory.Sequence(lambda n: n + 5)

    class Meta:
        model = Role


class NCGroupFactory(TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:NCGroup`.
    """

    # We need to skip the first 5 groups created as fixtures.
    id = factory.Sequence(lambda n: 6 + n)
    name = factory.Faker("name")
    description = factory.Faker("sentence")
    organization = factory.SubFactory(OrganizationFactory)

    class Meta:
        model = NCGroup

    @factory.post_generation
    def roles(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for role in extracted:
                # pylint: disable=no-member
                self.roles.add(role)


class CollectionPointerFactory(TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:CollectionPointer`.
    """

    collection_head = 1

    class Meta:
        model = CollectionPointer

    @factory.post_generation
    def collections(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for collection in extracted:
                # pylint: disable=no-member
                self.collections.add(collection)

    @factory.post_generation
    def documents(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for document in extracted:
                # pylint: disable=no-member
                self.documents.add(document)


class CollectionFactory(TimestampedFactory, ConstantTimeMixin("status_changed_at")):
    """
    A DjangoModelFactory to create instances of `obj:Collection`.

    The `obj:CollectionFactory` exposes certain convenience parameters for
    creating an `obj:Collection` instance with a certain state or schema:

    Creating Collection w Schema:

        - CollectionFactory(invoice=True, ...)
            Creates a `obj:Collection` instance with the INVOICE schema.

        - CollectionFactory(loan=True, ...)
            Creates a `obj:Collection` instance with the LOAN schema.

        - CollectionFactory(expense=True, ...)
            Creates a `obj:Collection` instance with the EXPENSE schema.

    Creating Collection w State:

        - CollectionFactory(active=True, ...)
            Creates a `obj:Collection` instance with the ACTIVE state.

        - CollectionFactory(deleted=True, ...)
            Creates a `obj:Collection` instance with the DELETED state.
    """

    name = factory.Faker("name")
    uuid = factory.Faker("uuid4")
    status_changed_at = NowDateTimeField()
    state = Collection.STATE_ACTIVE
    collection_state = Collection.COLLECTION_STATE_DEFAULT
    trash = False  # Currently not used.
    schema = Collection.SCHEMA_INVOICE
    collection_pointer = factory.SubFactory(CollectionPointerFactory)
    organization = factory.SubFactory(OrganizationFactory)
    owner = factory.SubFactory(
        UserFactory,
        organization=factory.LazyAttribute(
            lambda user: user.factory_parent.organization
        ),
    )
    created_by = factory.SubFactory(
        UserFactory,
        organization=factory.LazyAttribute(
            lambda user: user.factory_parent.organization
        ),
    )

    class Meta:
        model = Collection

    class Params:
        active = factory.Trait(state=Collection.STATE_ACTIVE)
        trash = factory.Trait(state=Collection.STATE_TRASH)
        invoice = factory.Trait(schema=Collection.SCHEMA_INVOICE)
        loan = factory.Trait(schema=Collection.SCHEMA_LOAN)
        expense = factory.Trait(schema=Collection.SCHEMA_EXPENSE)

    @factory.post_generation
    def documents(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for document in extracted:
                # pylint: disable=no-member
                self.documents.add(document)

    @factory.post_generation
    def assigned_users(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for user in extracted:
                # pylint: disable=no-member
                self.assigned_users.add(user)

    @factory.post_generation
    def collections(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for collection in extracted:
                # pylint: disable=no-member
                self.collections.add(collection)

    @factory.post_generation
    def results(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for result in extracted:
                # pylint: disable=no-member
                self.results.add(result)


class CollectionResultFactory(CustomModelFactory):
    """
    A DjangoModelFactory to create instances of `obj:CollectionResult`.
    """

    time_created = NowDateTimeField()
    uuid = factory.Faker("uuid4")
    result = {}
    collection = factory.SubFactory(CollectionFactory)

    class Meta:
        model = CollectionResult

    @classmethod
    def post_create(cls, model, **kwargs):
        # Overridden to hold time_created constant if explicitly provided.
        if "time_created" in kwargs:
            hold_time = kwargs["time_created"]
            with mock.patch("django.utils.timezone.now") as mock_now:
                mock_now.return_value = hold_time
                model.save()
        return super(CollectionResultFactory, cls).post_create(model, **kwargs)


class DocumentStatusFactory(TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:DocumentStatus`.

    The `obj:DocumentStatusFactory` exposes certain convenience parameters for
    creating an `obj:DocumentStatus` instance with a certain state:

    Creating DocumentStatus w State:

        - DocumentStatusFactory(pending=True, ...)
            Creates a `obj:DocumentStatus` instance with the PENDING state.

        - DocumentStatusFactory(completed=True, ...)
            Creates a `obj:DocumentStatus` instance with the COMPLETED state.

        - DocumentStatusFactory(failure=True, ...)
            Creates a `obj:DocumentStatus` instance with the FAILURE state.

        - DocumentStatusFactory(new=True, ...)
            Creates a `obj:DocumentStatus` instance with the NEW state.

    Note:
        The common instances of `obj:DocumentStatus` are already populated
        in the test database with a fixture, so it is unlikely that this
        factory is needed outside of testing edge cases.
    """

    status = DocumentStatus.NEW
    description = factory.Faker("sentence")

    class Meta:
        model = DocumentStatus

    class Params:
        pending = factory.Trait(status=DocumentStatus.PENDING)
        completed = factory.Trait(status=DocumentStatus.COMPLETED)
        failure = factory.Trait(status=DocumentStatus.FAILURE)
        new = factory.Trait(status=DocumentStatus.NEW)


class DocumentFactory(
    ConstantTimeMixin("status_changed_at", "created_at", "updated_at"),
    TimestampedFactory,
):
    """
    A DjangoModelFactory to create instances of `obj:Document`.

    The `obj:DocumentFactory` exposes certain convenience parameters for
    creating an `obj:Document` instance with a certain state or schema:

    Creating Document w Schema:

        - DocumentFactory(invoice=True, ...)
            Creates a `obj:Document` instance with the INVOICE schema.

        - DocumentFactory(loan=True, ...)
            Creates a `obj:Document` instance with the LOAN schema.

        - DocumentFactory(expense=True, ...)
            Creates a `obj:Document` instance with the EXPENSE schema.

    Creating Document w State:

        - DocumentFactory(uploading=True, ...)
            Creates a `obj:Document` instance with the UPLOADING state.

        - DocumentFactory(uploaded=True, ...)
            Creates a `obj:Document` instance with the UPLOADED state.

        - DocumentFactory(deleted=True, ...)
            Creates a `obj:Document` instance with the DELETED state.

    For convenience, a `obj:Document` instance can be created with the
    `obj:DocumentFactory` with a provided document_status by specifying the
    document_status by it's string name.

    >>> document = DocumentFactory(document_status="Cleared", ...)

    Note:
        The `obj:DocumentFactory` relies on a fixture being present that
        populates the document statuses in the test database.  If this fixture
        is removed, this factory will no longer be usable.
    """

    name = factory.Faker("name")
    path = factory.Faker("file_path")
    uuid = factory.Faker("uuid4")
    document_status = factory.LazyFunction(
        lambda: DocumentStatus.objects.get(status=DocumentStatus.NEW)
    )
    aws_document_key = factory.LazyFunction(lambda: generate_key(82))
    size = factory.Faker("random_number")
    state = Document.STATE_UPLOADED
    trash = False  # Currently not used.
    content_type = "application/json"
    schema = Document.SCHEMA_INVOICE
    organization = factory.SubFactory(OrganizationFactory)
    owner = factory.SubFactory(
        UserFactory,
        organization=factory.LazyAttribute(
            lambda user: user.factory_parent.organization
        ),
    )
    creator = factory.SubFactory(
        UserFactory,
        organization=factory.LazyAttribute(
            lambda user: user.factory_parent.organization
        ),
    )

    class Meta:
        model = Document

    class Params:
        invoice = factory.Trait(schema=Document.SCHEMA_INVOICE)
        loan = factory.Trait(schema=Document.SCHEMA_LOAN)
        expense = factory.Trait(schema=Document.SCHEMA_EXPENSE)
        uploading = factory.Trait(state=Document.STATE_UPLOADING)
        uploaded = factory.Trait(state=Document.STATE_UPLOADED)
        trash = factory.Trait(state=Document.STATE_TRASH)

    @classmethod
    def create(cls, *args, **kwargs):
        print("DEBUG : at create function in DocumentFactory.")
        data = dict(*args, **kwargs)
        if "document_status" in data:
            if isinstance(data["document_status"], str):
                try:
                    data["document_status"] = DocumentStatus.objects.get(
                        name=data["document_status"]
                    )
                except DocumentStatus.DoesNotExist:
                    raise Exception(
                        "When creating a document, if providing the "
                        "document_status as a string name, %s, the "
                        "DocumentStatus with the provided name must "
                        "exist.  Either create a new DocumentStatus and "
                        "provide that instance explicitly or provide a name "
                        "that corresponds to an already existing "
                        "DocumentStatus."
                    )
        return super(DocumentFactory, cls).create(*args, **kwargs)

    @factory.post_generation
    def collections(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for collection in extracted:
                self.collection_list.add(collection)


class TaskFactory(TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:Task`.
    """

    client_request = {}
    request = {}
    response = {}
    uuid = factory.Faker("uuid4")
    ai_uuid = factory.Faker("uuid4")
    backend_id = factory.LazyFunction(lambda: generate_key(24))
    organization = factory.SubFactory(OrganizationFactory)

    class Meta:
        model = Task

    @factory.post_generation
    def results(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for result in extracted:
                self.results.add(result)


class TaskResultFactory(ConstantTimeMixin("created_at"), TimestampedFactory):
    """
    A DjangoModelFactory to create instances of `obj:TaskResult`.
    """

    uuid = factory.Faker("uuid4")
    data = {}
    new_data = {}
    document = factory.SubFactory(
        DocumentFactory,
        organization=factory.LazyAttribute(
            lambda doc: doc.factory_parent.task.organization
        ),
    )
    task = factory.SubFactory(
        TaskFactory,
        organization=factory.LazyAttribute(
            lambda ts: ts.factory_parent.document.organization
        ),
    )

    class Meta:
        model = TaskResult


class CallbackRequestFactory(CustomModelFactory):
    """
    A DjangoModelFactory to create instances of `obj:CallbackRequest`.
    """

    task = factory.SubFactory(TaskFactory)
    callback = factory.Faker("url")

    class Meta:
        model = CallbackRequest

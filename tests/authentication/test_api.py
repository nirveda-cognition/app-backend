from datetime import datetime, timedelta

import mock
import pytest
from django.test import override_settings
from rest_framework_simplejwt.tokens import RefreshToken

from backend.app.authentication.models import NCGroup, ResetUID, Role
from backend.lib.utils.dateutils import api_datetime_string


class MockedRefreshToken(RefreshToken):
    """
    A mocked form of `rest_framework_simplejwt.tokens.RefreshToken` for
    purposes of testing.

    The accessed attributes/methods of the instance are overridden to provide
    constant values that can be compared against in tests.
    """

    def __str__(self):
        return "abcdefghijklmnop=="

    @property
    def access_token(self):
        return "abcdefghijklmnopqrstuvwxzy=="


def test_login(user, nirveda, api_client, monkeypatch):
    # Mock the RefreshToken.for_user() method to return our MockedRefreshToken.
    monkeypatch.setattr(RefreshToken, "for_user", lambda user: MockedRefreshToken())

    user.set_password("testpassword123")
    user.save()
    response = api_client.post(
        "/v2/auth/login/", data={"email": user.email, "password": "testpassword123"}
    )
    assert response.status_code == 201
    assert response.json() == {
        "access_token": "abcdefghijklmnopqrstuvwxzy==",
        "refresh_token": "abcdefghijklmnop==",
        "user": {
            "id": user.pk,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "full_name": user.full_name,
            "email": user.email,
            "category": user.category,
            "username": user.username,
            "is_active": user.is_active,
            "groups": [],
            "is_admin": user.is_admin,
            "is_superuser": user.is_superuser,
            "is_staff": user.is_staff,
            "created_by": None,
            "updated_by": None,
            "date_joined": api_datetime_string(user.date_joined),
            "updated_at": api_datetime_string(user.updated_at),
            "created_at": api_datetime_string(user.created_at),
            "timezone": user.timezone,
            "language": user.language,
            "last_login": None,
            "role": {
                "id": user.role.pk,
                "name": user.role.name,
                "description": user.role.description,
                "code": user.role.code,
            },
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "slug": nirveda.slug,
                "description": nirveda.description,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "is_active": nirveda.is_active,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": nirveda.logo,
                "updated_at": api_datetime_string(nirveda.updated_at),
                "created_at": api_datetime_string(nirveda.created_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
        },
    }


def test_login_missing_password(user, api_client):
    response = api_client.post(
        "/v2/auth/login/",
        data={
            "email": user.email,
        },
    )
    assert response.status_code == 400


def test_login_missing_email(user, api_client):
    response = api_client.post(
        "/v2/auth/login/",
        data={
            "password": user.password,
        },
    )
    assert response.status_code == 400


def test_invalid_email(api_client):
    response = api_client.post(
        "/v2/auth/login/",
        data={
            "email": "userdoesnotexist@gmail.com",
            "password": "fake-password",
        },
    )
    assert response.status_code == 403
    assert response.json() == {
        "errors": {
            "__all__": [
                {
                    "message": "The provided username does not exist in our system.",  # noqa
                    "code": "email_does_not_exist",
                }
            ]
        }
    }


def test_invalid_password(user, api_client):
    response = api_client.post(
        "/v2/auth/login/",
        data={
            "email": user.email,
            "password": "fake-password",
        },
    )
    assert response.status_code == 403
    assert response.json() == {
        "errors": {
            "__all__": [
                {
                    "message": "The provided password is invalid.",
                    "code": "invalid_credentials",
                }
            ]
        }
    }


def test_logout(user, api_client):
    api_client.force_login(user)
    response = api_client.post("/v2/auth/logout/")
    assert response.status_code == 201

    authenticated_response = api_client.get("/v2/organizations/")
    assert authenticated_response.status_code == 403


def test_reset_password(user, nirveda, api_client):
    reset_uid = ResetUID.objects.create(
        token="token1234567",
        used=False,
        user=user,
    )
    api_client.credentials(HTTP_SOURCE="WebApp")
    response = api_client.post(
        "/v2/auth/reset-password/",
        data={
            "token": reset_uid.token,
            "password": "TestUserPassword4321$",
            "confirm": "TestUserPassword4321$",
        },
    )
    user.refresh_from_db()
    assert response.status_code == 201
    assert response.json() == {
        "user": {
            "id": user.pk,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "full_name": user.full_name,
            "email": user.email,
            "category": user.category,
            "username": user.username,
            "is_active": True,
            "is_admin": False,
            "is_superuser": False,
            "is_staff": False,
            "created_by": None,
            "updated_at": api_datetime_string(user.updated_at),
            "created_at": api_datetime_string(user.created_at),
            "date_joined": api_datetime_string(user.date_joined),
            "timezone": user.timezone,
            "language": user.language,
            "last_login": None,
            "updated_by": None,
            "groups": [],
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "description": nirveda.description,
                "slug": nirveda.slug,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": None,
                "is_active": nirveda.is_active,
                "created_at": api_datetime_string(nirveda.created_at),
                "updated_at": api_datetime_string(nirveda.updated_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
            "role": {
                "id": user.role.pk,
                "name": user.role.name,
                "description": user.role.description,
                "code": user.role.code,
            },
        }
    }


def test_reset_password_invalid_token(user, api_client):
    api_client.credentials(HTTP_SOURCE="WebApp")
    response = api_client.post(
        "/v2/auth/reset-password/",
        data={
            "token": "token1234567",
            "password": "TestUserPassword4321$",
            "confirm": "TestUserPassword4321$",
        },
    )
    assert response.status_code == 403
    assert response.json() == {
        "errors": {
            "__all__": [
                {"message": "The provided token is invalid.", "code": "invalid_token"}
            ]
        }
    }


@override_settings(PWD_RESET_LINK_EXPIRY_TIME_IN_HRS=48)
@pytest.mark.freeze_time("2020-01-01")
def test_reset_password_token_expired(user, api_client, freezer):
    reset_uid = ResetUID.objects.create(
        token="token1234567",
        used=False,
        user=user,
    )
    # Move Forward in Time 5 Hours + 5 Minutes, Just After Expiry
    future_date = datetime(2020, 1, 1) + timedelta(minutes=60 * 48 + 5)
    freezer.move_to(future_date)

    api_client.credentials(HTTP_SOURCE="WebApp")
    response = api_client.post(
        "/v2/auth/reset-password/",
        data={
            "token": reset_uid.token,
            "password": "TestUserPassword4321$",
            "confirm": "TestUserPassword4321$",
        },
    )

    assert response.status_code == 403
    assert response.json() == {
        "errors": {
            "__all__": [
                {
                    "message": "The password reset link has expired.",
                    "code": "password_reset_link_expired",
                }
            ]
        }
    }


@override_settings(
    RESET_PWD_UI_LINK="https://nirvedacognition.ai/changepassword",
    FROM_EMAIL="nirveda@gmail.com",
)
@pytest.mark.freeze_time("2020-01-01")
def test_forgot_password(user, api_client):
    api_client.credentials(HTTP_SOURCE="WebApp")

    # Mock the Email Generation
    # Note that we cannot mock the email sending to test the contents of the
    # email because the email contents are supplied as arguments in the
    # EmailMultiAlternatives __init__ method, not the send() method.  Mocking an
    # __init__ method properly is extremely difficult - so the best we can do
    # is mock the render_to_string method an d make sure the supplied arguments
    # are valid for the generated email.
    with mock.patch(
        "backend.app.authentication.utils.render_to_string"
    ) as mocked:  # noqa
        response = api_client.post(
            "/v2/auth/forgot-password/", data={"email": user.email}
        )

    assert response.status_code == 201

    assert ResetUID.objects.count() == 1
    reset_uid = ResetUID.objects.first()
    assert reset_uid.user == user

    # Test Email Was Sent
    assert mocked.called
    assert mocked.call_args[0][0] == "email/forgot_password.html"
    assert mocked.call_args[0][1] == {
        "PWD_RESET_LINK": (
            "https://nirvedacognition.ai/changepassword?token=%s" % reset_uid.token
        ),
        "from_email": "nirveda@gmail.com",
        "EMAIL": user.email,
        "year": 2020,
        "brand": user.organization.brand,
        "NAME": "%s %s" % (user.first_name, user.last_name),
    }


def test_get_group(api_client, create_group, create_user, nirveda):
    group = create_group(organization=nirveda)
    user = create_user(groups=[group], organization=nirveda)
    api_client.force_login(user)
    response = api_client.get(
        "/v2/organizations/%s/groups/%s/" % (nirveda.pk, group.pk)
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": group.pk,
        "name": group.name,
        "description": group.description,
        "roles": [],
        "organization": {
            "id": nirveda.pk,
            "name": nirveda.name,
            "description": None,
            "slug": nirveda.slug,
            "num_users": nirveda.num_users,
            "num_groups": nirveda.num_groups,
            "json_info": {"document-extractor": ["invoice"]},
            "logo": None,
            "is_active": nirveda.is_active,
            "created_at": api_datetime_string(nirveda.created_at),
            "updated_at": api_datetime_string(nirveda.updated_at),
            "type": {
                "id": nirveda.type.pk,
                "title": nirveda.type.title,
                "slug": nirveda.type.slug,
                "description": nirveda.type.description,
            },
        },
        "users": [
            {
                "id": user.pk,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "full_name": user.full_name,
                "email": user.email,
                "category": user.category,
                "username": user.email,
                "is_active": user.is_active,
                "is_admin": user.is_admin,
                "is_superuser": user.is_superuser,
                "is_staff": user.is_staff,
            }
        ],
    }


def test_get_group_user(api_client, create_group, create_user, nirveda):
    group = create_group(organization=nirveda)
    user = create_user(groups=[group], organization=nirveda)
    api_client.force_login(user)
    response = api_client.get(
        "/v2/organizations/%s/groups/%s/users/%s/" % (nirveda.pk, group.pk, user.pk)
    )
    assert response.status_code == 200
    assert response.json()["id"] == user.pk


def test_get_group_users(api_client, create_group, create_user, nirveda):
    group = create_group(organization=nirveda)
    users = [
        create_user(groups=[group], organization=nirveda),
        create_user(groups=[group], organization=nirveda),
    ]
    api_client.force_login(users[0])
    response = api_client.get(
        "/v2/organizations/%s/groups/%s/users/" % (nirveda.pk, group.pk)
    )
    assert response.status_code == 200
    assert response.json()["count"] == 2
    assert response.json()["data"][0]["id"] == users[1].pk
    assert response.json()["data"][1]["id"] == users[0].pk


def test_get_groups(api_client, create_group, create_user, nirveda):
    groups = [create_group(organization=nirveda), create_group(organization=nirveda)]
    user = create_user(groups=groups, organization=nirveda)
    api_client.force_login(user)
    response = api_client.get("/v2/organizations/%s/groups/" % nirveda.pk)
    assert response.status_code == 200
    assert response.json()["data"] == [
        {
            "id": groups[1].pk,
            "name": groups[1].name,
            "description": groups[1].description,
            "roles": [],
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "description": None,
                "slug": nirveda.slug,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": None,
                "is_active": nirveda.is_active,
                "created_at": api_datetime_string(nirveda.created_at),
                "updated_at": api_datetime_string(nirveda.updated_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
            "users": [
                {
                    "id": user.pk,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "full_name": user.full_name,
                    "email": user.email,
                    "category": user.category,
                    "username": user.email,
                    "is_active": user.is_active,
                    "is_admin": user.is_admin,
                    "is_superuser": user.is_superuser,
                    "is_staff": user.is_staff,
                }
            ],
        },
        {
            "id": groups[0].pk,
            "name": groups[0].name,
            "description": groups[0].description,
            "roles": [],
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "description": None,
                "slug": nirveda.slug,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": None,
                "is_active": nirveda.is_active,
                "created_at": api_datetime_string(nirveda.created_at),
                "updated_at": api_datetime_string(nirveda.updated_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
            "users": [
                {
                    "id": user.pk,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "full_name": user.full_name,
                    "email": user.email,
                    "category": user.category,
                    "username": user.email,
                    "is_active": user.is_active,
                    "is_admin": user.is_admin,
                    "is_superuser": user.is_superuser,
                    "is_staff": user.is_staff,
                }
            ],
        },
    ]


def test_get_root_groups(
    api_client, create_group, create_user, nirveda, apex, superuser
):
    groups = [create_group(organization=nirveda), create_group(organization=apex)]
    api_client.force_login(superuser)
    response = api_client.get("/v2/auth/groups/")
    assert response.status_code == 200
    assert response.json()["data"] == [
        {
            "id": groups[0].pk,
            "name": groups[0].name,
            "description": groups[0].description,
            "roles": [],
            "users": [],
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "description": None,
                "slug": nirveda.slug,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": None,
                "is_active": nirveda.is_active,
                "created_at": api_datetime_string(nirveda.created_at),
                "updated_at": api_datetime_string(nirveda.updated_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
        },
        {
            "id": groups[1].pk,
            "name": groups[1].name,
            "description": groups[1].description,
            "roles": [],
            "users": [],
            "organization": {
                "id": apex.pk,
                "name": apex.name,
                "description": None,
                "slug": apex.slug,
                "num_users": apex.num_users,
                "num_groups": apex.num_groups,
                "json_info": {"document-extractor": ["apex"]},
                "logo": None,
                "is_active": apex.is_active,
                "created_at": api_datetime_string(apex.created_at),
                "updated_at": api_datetime_string(apex.updated_at),
                "type": {
                    "id": apex.type.pk,
                    "title": apex.type.title,
                    "slug": apex.type.slug,
                    "description": apex.type.description,
                },
            },
        },
    ]


def test_create_group(api_client, user, nirveda):
    api_client.force_login(user)
    response = api_client.post(
        "/v2/organizations/%s/groups/" % nirveda.pk,
        data={
            "name": "Test Group Name",
            "description": "Test group description.",
        },
    )
    assert response.status_code == 201
    created_group = NCGroup.objects.get(name="Test Group Name")
    assert response.json() == {
        "id": created_group.pk,
        "name": "Test Group Name",
        "description": "Test group description.",
        "roles": [],
        "users": [],
        "organization": {
            "id": nirveda.pk,
            "name": nirveda.name,
            "description": None,
            "slug": nirveda.slug,
            "num_users": nirveda.num_users,
            "num_groups": nirveda.num_groups,
            "json_info": {"document-extractor": ["invoice"]},
            "logo": None,
            "is_active": nirveda.is_active,
            "created_at": api_datetime_string(nirveda.created_at),
            "updated_at": api_datetime_string(nirveda.updated_at),
            "type": {
                "id": nirveda.type.pk,
                "title": nirveda.type.title,
                "slug": nirveda.type.slug,
                "description": nirveda.type.description,
            },
        },
    }
    assert created_group.name == "Test Group Name"
    assert created_group.description == "Test group description."
    assert created_group.organization == nirveda


def test_create_group_invalid_users(api_client, user, create_user, apex, nirveda):
    apex_user = create_user(organization=apex)
    api_client.force_login(user)
    response = api_client.post(
        "/v2/organizations/%s/groups/" % nirveda.pk,
        data={
            "name": "Test Group Name",
            "description": "Test group description.",
            "users": [apex_user.pk],
        },
    )
    assert response.status_code == 400


def test_create_group_duplicate_name(api_client, create_group, user, nirveda):
    create_group(organization=nirveda, name="Do Not Duplicate")
    api_client.force_login(user)
    response = api_client.post(
        "/v2/organizations/%s/groups/" % nirveda.pk,
        data={
            "name": "Do Not Duplicate",
            "description": "Test group description.",
        },
    )
    assert response.status_code == 400


def test_update_group(api_client, create_group, superuser, nirveda):
    api_client.force_login(superuser)
    group = create_group(
        organization=nirveda, name="Original Name", description="Original description."
    )
    response = api_client.patch(
        "/v2/organizations/%s/groups/%s/" % (nirveda.pk, group.pk),
        data={
            "name": "New Name",
            "description": "New description.",
        },
    )

    assert response.status_code == 200
    assert response.json() == {
        "id": group.pk,
        "name": "New Name",
        "description": "New description.",
        "roles": [],
        "users": [],
        "organization": {
            "id": nirveda.pk,
            "name": nirveda.name,
            "description": None,
            "slug": nirveda.slug,
            "num_users": nirveda.num_users,
            "num_groups": nirveda.num_groups,
            "json_info": {"document-extractor": ["invoice"]},
            "logo": None,
            "is_active": nirveda.is_active,
            "created_at": api_datetime_string(nirveda.created_at),
            "updated_at": api_datetime_string(nirveda.updated_at),
            "type": {
                "id": nirveda.type.pk,
                "title": nirveda.type.title,
                "slug": nirveda.type.slug,
                "description": nirveda.type.description,
            },
        },
    }
    # Make sure changes persist to database.
    group.refresh_from_db()
    assert group.name == "New Name"
    assert group.description == "New description."
    assert group.organization == nirveda


def test_update_group_invalid_users(
    api_client, create_group, superuser, apex, nirveda, create_user
):
    apex_user = create_user(organization=apex)
    api_client.force_login(superuser)
    group = create_group(
        organization=nirveda, name="Original Name", description="Original description."
    )
    response = api_client.patch(
        "/v2/organizations/%s/groups/%s/" % (nirveda.pk, group.pk),
        data={
            "name": "New Name",
            "description": "New description.",
            "users": [apex_user.pk],
        },
    )

    assert response.status_code == 400


def test_update_group_duplicate_name(api_client, create_group, superuser, nirveda):
    api_client.force_login(superuser)
    create_group(organization=nirveda, name="Do Not Duplicate")
    group = create_group(organization=nirveda, name="Original Name")
    response = api_client.patch(
        "/v2/organizations/%s/groups/%s/" % (nirveda.pk, group.pk),
        data={
            "name": "Do Not Duplicate",
            "description": "New description.",
        },
    )

    assert response.status_code == 400


def test_delete_group(api_client, create_group, user, nirveda):
    api_client.force_login(user)
    group = create_group(organization=nirveda, name="Test Name")
    response = api_client.delete(
        "/v2/organizations/%s/groups/%s/" % (nirveda.pk, group.pk)
    )

    assert response.status_code == 204
    group = NCGroup.objects.filter(name="Test Name").first()
    assert group is None


def test_get_roles(
    api_client, user, user_role, admin_role, account_manager_role, account_user_role, db
):
    api_client.force_login(user)
    response = api_client.get("/v2/auth/roles/")
    assert response.status_code == 200
    assert response.json()["data"] == [
        {
            "id": account_manager_role.id,
            "name": account_manager_role.name,
            "description": account_manager_role.description,
            "code": account_manager_role.code,
        },
        {
            "id": account_user_role.id,
            "name": account_user_role.name,
            "description": account_user_role.description,
            "code": account_user_role.code,
        },
        {
            "id": admin_role.id,
            "name": admin_role.name,
            "description": admin_role.description,
            "code": admin_role.code,
        },
        {
            "id": user_role.id,
            "name": user_role.name,
            "description": user_role.description,
            "code": user_role.code,
        },
    ]


def test_get_role(api_client, user, user_role):
    api_client.force_login(user)
    response = api_client.get("/v2/auth/roles/%s/" % user_role.pk)
    assert response.status_code == 200
    assert response.json() == {
        "id": user_role.id,
        "name": user_role.name,
        "description": user_role.description,
        "code": user_role.code,
    }


@pytest.mark.skip(
    "There is an issue with the ID  field on the Role model and " "auto-incrementing."
)
def test_create_role(api_client, superuser, user_role, db):
    api_client.force_login(superuser)
    response = api_client.post(
        "/v2/auth/roles/",
        data={
            "id": 100,
            "name": "Test Role",
            "description": "Test role description.",
            "code": 100,
        },
    )
    assert response.status_code == 201
    created_role = Role.objects.get(name="Test Role")
    assert response.json() == {
        "id": created_role.pk,
        "name": "Test Role",
        "description": "Test role description.",
        "code": 100,
    }


def test_create_role_requires_superuser(api_client, user, user_role, db):
    api_client.force_login(user)
    response = api_client.post(
        "/v2/auth/roles/",
        data={
            "name": "Test Role",
            "description": "Test role description.",
            "code": 100,
        },
    )
    assert response.status_code == 403

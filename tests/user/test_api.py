import datetime

import mock
import pytest
import pytz
from django.test import override_settings

from backend.app.user.models import CustomUser, UserCategory
from backend.lib.utils.dateutils import api_datetime_string


class TestOrganizationUsers:
    def test_get(self, api_client, user, nirveda, db):
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/users/%s/" % (nirveda.pk, user.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": user.pk,
            "email": user.email,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "full_name": user.full_name,
            "username": user.email,
            "is_admin": False,
            "is_superuser": False,
            "is_staff": False,
            "category": UserCategory.USER,
            "is_active": True,
            "timezone": user.timezone,
            "language": user.language,
            "updated_by": None,
            "updated_at": api_datetime_string(user.updated_at),
            "created_by": None,
            "created_at": api_datetime_string(user.updated_at),
            "last_login": api_datetime_string(user.last_login),
            "date_joined": api_datetime_string(user.date_joined),
            "groups": [],
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "description": nirveda.description,
                "slug": nirveda.slug,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": None,
                "is_active": nirveda.is_active,
                "created_at": api_datetime_string(nirveda.created_at),
                "updated_at": api_datetime_string(nirveda.updated_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
            "role": {
                "id": user.role.pk,
                "name": user.role.name,
                "description": user.role.description,
                "code": user.role.code,
            },
        }

    def test_get_all(self, api_client, user, create_user, nirveda, alfa, db):
        users = [
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 4, tzinfo=pytz.UTC),
            ),
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 1, tzinfo=pytz.UTC),
            ),
            create_user(
                organization=alfa,
                created_at=datetime.datetime(2020, 1, 1, tzinfo=pytz.UTC),
            ),
        ]
        api_client.force_login(user)
        response = api_client.get("/v2/organizations/%s/users/" % nirveda.pk)
        assert response.status_code == 200
        assert response.json()["data"] == [
            {
                "id": user.pk,
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "full_name": user.full_name,
                "username": user.email,
                "is_admin": False,
                "is_superuser": False,
                "is_staff": False,
                "category": UserCategory.USER,
                "is_active": True,
                "groups": [],
                "timezone": user.timezone,
                "language": user.language,
                "updated_by": None,
                "updated_at": api_datetime_string(user.updated_at),
                "created_by": None,
                "created_at": api_datetime_string(user.created_at),
                "last_login": api_datetime_string(user.last_login),
                "date_joined": api_datetime_string(user.date_joined),
                "organization": {
                    "id": nirveda.pk,
                    "name": nirveda.name,
                    "description": None,
                    "slug": nirveda.slug,
                    "num_users": nirveda.num_users,
                    "num_groups": nirveda.num_groups,
                    "json_info": {"document-extractor": ["invoice"]},
                    "logo": None,
                    "is_active": nirveda.is_active,
                    "created_at": api_datetime_string(nirveda.created_at),
                    "updated_at": api_datetime_string(nirveda.updated_at),
                    "type": {
                        "id": nirveda.type.pk,
                        "title": nirveda.type.title,
                        "slug": nirveda.type.slug,
                        "description": nirveda.type.description,
                    },
                },
                "role": {
                    "id": user.role.pk,
                    "name": user.role.name,
                    "description": user.role.description,
                    "code": user.role.code,
                },
            },
            {
                "id": users[0].pk,
                "email": users[0].email,
                "first_name": users[0].first_name,
                "last_name": users[0].last_name,
                "full_name": users[0].full_name,
                "username": users[0].email,
                "is_admin": False,
                "is_superuser": False,
                "is_staff": False,
                "category": UserCategory.USER,
                "is_active": True,
                "groups": [],
                "timezone": users[0].timezone,
                "language": users[0].language,
                "updated_by": None,
                "updated_at": api_datetime_string(users[0].updated_at),
                "created_by": None,
                "created_at": api_datetime_string(users[0].created_at),
                "last_login": None,
                "date_joined": api_datetime_string(users[0].date_joined),
                "organization": {
                    "id": nirveda.pk,
                    "name": nirveda.name,
                    "description": None,
                    "slug": nirveda.slug,
                    "num_users": nirveda.num_users,
                    "num_groups": nirveda.num_groups,
                    "json_info": {"document-extractor": ["invoice"]},
                    "logo": None,
                    "is_active": nirveda.is_active,
                    "created_at": api_datetime_string(nirveda.created_at),
                    "updated_at": api_datetime_string(nirveda.updated_at),
                    "type": {
                        "id": nirveda.type.pk,
                        "title": nirveda.type.title,
                        "slug": nirveda.type.slug,
                        "description": nirveda.type.description,
                    },
                },
                "role": {
                    "id": users[0].role.pk,
                    "name": users[0].role.name,
                    "description": users[0].role.description,
                    "code": users[0].role.code,
                },
            },
            {
                "id": users[1].pk,
                "email": users[1].email,
                "first_name": users[1].first_name,
                "last_name": users[1].last_name,
                "full_name": users[1].full_name,
                "username": users[1].email,
                "is_admin": False,
                "is_superuser": False,
                "is_staff": False,
                "category": UserCategory.USER,
                "is_active": True,
                "groups": [],
                "timezone": users[1].timezone,
                "language": users[1].language,
                "updated_by": None,
                "updated_at": api_datetime_string(users[1].updated_at),
                "created_by": None,
                "created_at": api_datetime_string(users[1].created_at),
                "last_login": None,
                "date_joined": api_datetime_string(users[1].date_joined),
                "organization": {
                    "id": nirveda.pk,
                    "name": nirveda.name,
                    "description": None,
                    "slug": nirveda.slug,
                    "num_users": nirveda.num_users,
                    "num_groups": nirveda.num_groups,
                    "json_info": {"document-extractor": ["invoice"]},
                    "logo": None,
                    "is_active": nirveda.is_active,
                    "created_at": api_datetime_string(nirveda.created_at),
                    "updated_at": api_datetime_string(nirveda.updated_at),
                    "type": {
                        "id": nirveda.type.pk,
                        "title": nirveda.type.title,
                        "slug": nirveda.type.slug,
                        "description": nirveda.type.description,
                    },
                },
                "role": {
                    "id": users[1].role.pk,
                    "name": users[1].role.name,
                    "description": users[1].role.description,
                    "code": users[1].role.code,
                },
            },
        ]

    def test_filter_by_role(
        self, api_client, user, nirveda, create_role, create_user, db
    ):
        roles = [create_role(name="Role1"), create_role(name="Role2")]
        users = [
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 3),
                role=roles[0],
            ),
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 2),
                role=roles[1],
            ),
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 1),
                role=roles[0],
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/users/?role__name=Role1" % nirveda.pk
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 2
        assert response.json()["data"][0]["id"] == users[0].pk
        assert response.json()["data"][1]["id"] == users[2].pk

    def test_filter_by_group(
        self, api_client, user, nirveda, create_group, create_user, db
    ):
        groups = [create_group(name="Group1"), create_group(name="Group2")]
        users = [
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 3),
                groups=[groups[0]],
            ),
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 2),
                groups=groups,
            ),
            create_user(
                organization=nirveda,
                created_at=datetime.datetime(2020, 1, 1),
                groups=[groups[1]],
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/users/?group__name=Group1" % nirveda.pk
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 2
        assert response.json()["data"][0]["id"] == users[0].pk
        assert response.json()["data"][1]["id"] == users[1].pk

    @pytest.mark.freeze_time("2020-01-01")
    def test_create(self, api_client, user, nirveda, user_role, create_group, db):
        group = create_group(organization=nirveda)
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/users/" % nirveda.pk,
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "groups": [group.pk],
            },
        )
        assert response.status_code == 201

        created_user = CustomUser.objects.get(email="jjsmooth@gmail.com")
        assert response.json() == {
            "id": created_user.pk,
            "email": "jjsmooth@gmail.com",
            "first_name": "Jack",
            "last_name": "Johnson",
            "full_name": "Jack Johnson",
            "username": "jjsmooth@gmail.com",
            "is_admin": False,
            "is_superuser": False,
            "is_staff": False,
            "category": UserCategory.USER,
            "is_active": True,
            "timezone": created_user.timezone,
            "language": created_user.language,
            "updated_by": user.pk,
            "updated_at": "2020-01-01 00:00:00",
            "created_by": user.pk,
            "created_at": "2020-01-01 00:00:00",
            "last_login": None,
            "date_joined": api_datetime_string(created_user.date_joined),
            "groups": [
                {
                    "id": group.pk,
                    "name": group.name,
                    "description": group.description,
                }
            ],
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "description": None,
                "slug": nirveda.slug,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": None,
                "is_active": nirveda.is_active,
                "created_at": api_datetime_string(nirveda.created_at),
                "updated_at": api_datetime_string(nirveda.updated_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
            "role": {
                "id": user_role.pk,
                "name": user_role.name,
                "description": user_role.description,
                "code": user_role.code,
            },
        }

        assert created_user.is_active

        assert len(created_user.groups.all()) == 1
        assert created_user.groups.all()[0] == group

        assert created_user.first_name == "Jack"
        assert created_user.last_name == "Johnson"
        assert created_user.email == "jjsmooth@gmail.com"
        assert created_user.created_by == user
        assert created_user.organization == nirveda

    def test_create_with_another_organization(
        self,
        api_client,
        user,
        create_group,
        create_organization,
        nirveda,
        generic_type,
        db,
    ):
        organization = create_organization(type=generic_type)
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/users/" % nirveda.pk,
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "organization": organization.pk,
            },
        )
        assert response.status_code == 400

    def test_create_with_groups_not_in_organization(
        self,
        api_client,
        user,
        create_group,
        create_organization,
        nirveda,
        generic_type,
        db,
    ):
        organization = create_organization(type=generic_type)
        groups = [
            create_group(organization=nirveda),
            create_group(organization=organization),
        ]
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/users/" % nirveda.pk,
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "is_admin": True,
                "groups": [group.pk for group in groups],
            },
        )
        assert response.status_code == 400

    def test_create_admin_user(self, api_client, user, nirveda, admin_role, db):
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/users/" % nirveda.pk,
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "is_admin": True,
            },
        )
        assert response.status_code == 201

        created_user = CustomUser.objects.get(email="jjsmooth@gmail.com")
        assert response.json()["is_admin"] is True
        assert response.json()["is_superuser"] is False
        assert response.json()["is_staff"] is False
        assert response.json()["role"] == {
            "id": admin_role.pk,
            "name": admin_role.name,
            "description": admin_role.description,
            "code": admin_role.code,
        }

        assert created_user.is_active
        assert created_user.role == admin_role
        assert created_user.is_admin is True
        assert created_user.is_superuser is False
        assert created_user.is_staff is False

    def test_create_superuser_as_user(self, api_client, user, nirveda, db):
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/users/" % nirveda.pk,
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "is_superuser": True,
            },
        )
        assert response.status_code == 403

    def test_create_superuser_as_superuser(self, api_client, superuser, nirveda, db):
        api_client.force_login(superuser)
        response = api_client.post(
            "/v2/organizations/%s/users/" % nirveda.pk,
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "is_superuser": True,
            },
        )
        assert response.status_code == 201
        assert response.json()["is_superuser"] is True
        assert response.json()["is_staff"] is True
        assert response.json()["is_admin"] is False

        created_user = CustomUser.objects.get(email="jjsmooth@gmail.com")
        assert created_user.is_superuser is True
        assert created_user.is_staff is True
        assert created_user.is_admin is False

    @pytest.mark.freeze_time("2020-01-01")
    def test_update(
        self,
        api_client,
        create_user,
        create_organization,
        create_group,
        generic_type,
        db,
    ):
        organization = create_organization(type=generic_type)
        user = create_user(superuser=True, organization=organization)
        another_organization = create_organization(slug="test-2", type=generic_type)
        group = create_group(organization=another_organization)
        user_being_updated = create_user(organization=another_organization)

        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/users/%s/"
            % (another_organization.pk, user_being_updated.pk),
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "is_admin": True,
                "groups": [group.pk],
            },
        )

        user_being_updated.refresh_from_db()
        assert response.json() == {
            "id": user_being_updated.pk,
            "email": user_being_updated.email,
            "first_name": "Jack",
            "last_name": "Johnson",
            "full_name": "Jack Johnson",
            "username": user_being_updated.username,
            "is_admin": True,
            "is_superuser": False,
            "is_staff": False,
            "category": UserCategory.ADMIN,
            "is_active": True,
            "timezone": user_being_updated.timezone,
            "language": user_being_updated.language,
            "updated_by": user.pk,
            "updated_at": "2020-01-01 00:00:00",
            "created_by": None,
            "created_at": "2020-01-01 00:00:00",
            "last_login": None,
            "date_joined": api_datetime_string(user_being_updated.date_joined),
            "groups": [
                {
                    "id": group.pk,
                    "name": group.name,
                    "description": group.description,
                }
            ],
            "organization": {
                "id": another_organization.pk,
                "name": another_organization.name,
                "description": another_organization.description,
                "slug": another_organization.slug,
                "num_users": another_organization.num_users,
                "num_groups": another_organization.num_groups,
                "json_info": another_organization.json_info,
                "logo": another_organization.logo,
                "is_active": another_organization.is_active,
                "created_at": api_datetime_string(another_organization.created_at),
                "updated_at": api_datetime_string(another_organization.updated_at),
                "type": {
                    "id": another_organization.type.pk,
                    "title": another_organization.type.title,
                    "slug": another_organization.type.slug,
                    "description": another_organization.type.description,
                },
            },
            "role": {
                "id": user_being_updated.role.pk,
                "name": user_being_updated.role.name,
                "description": user_being_updated.role.description,
                "code": user_being_updated.role.code,
            },
        }

        assert user_being_updated.updated_by == user
        assert user_being_updated.first_name == "Jack"
        assert user_being_updated.last_name == "Johnson"
        assert user_being_updated.is_admin is True
        assert list(user_being_updated.groups.all()) == [group]

    def test_update_with_another_organization_requires_superuser(
        self,
        api_client,
        user,
        create_user,
        create_group,
        create_organization,
        nirveda,
        generic_type,
        db,
    ):
        user_being_updated = create_user(organization=nirveda)
        organization = create_organization(type=generic_type)
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/users/%s/" % (nirveda.pk, user_being_updated.pk),
            {"organization": organization.pk},
        )
        assert response.status_code == 403

    def test_update_with_another_organization(
        self,
        api_client,
        superuser,
        create_user,
        create_group,
        create_organization,
        nirveda,
        generic_type,
        db,
    ):
        user_being_updated = create_user(organization=nirveda)
        organization = create_organization(type=generic_type)
        groups = [
            create_group(organization=organization),
            create_group(organization=organization),
        ]
        api_client.force_login(superuser)
        response = api_client.patch(
            "/v2/organizations/%s/users/%s/" % (nirveda.pk, user_being_updated.pk),
            {"organization": organization.pk, "groups": [g.pk for g in groups]},
        )
        assert response.status_code == 200
        user_being_updated.refresh_from_db()
        assert user_being_updated.organization == organization
        assert response.json()["organization"]["id"] == organization.pk

    def test_update_with_groups_not_in_organization(
        self,
        api_client,
        user,
        create_group,
        create_organization,
        create_user,
        nirveda,
        generic_type,
        db,
    ):
        user = create_user(organization=nirveda)
        organization = create_organization(type=generic_type)
        groups = [
            create_group(organization=nirveda),
            create_group(organization=organization),
        ]
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/users/%s/" % (nirveda.pk, user.pk),
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "is_admin": True,
                "groups": [group.pk for group in groups],
            },
        )
        assert response.status_code == 400

    def test_update_superuser_as_user(self, api_client, user, nirveda, db, create_user):
        superuser = create_user(superuser=True)
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/users/%s/"
            % (superuser.organization.pk, superuser.pk),
            {"first_name": "Jack", "last_name": "Johnson", "is_admin": True},
        )
        assert response.status_code == 403

    def test_update_superuser_as_superuser(
        self, api_client, superuser, nirveda, create_user, db
    ):
        other_superuser = create_user(
            superuser=True, organization=nirveda, email="jjsmooth@gmail.com"
        )
        api_client.force_login(superuser)
        response = api_client.patch(
            "/v2/organizations/%s/users/%s/"
            % (other_superuser.organization.pk, other_superuser.pk),
            {"first_name": "Jack", "last_name": "Johnson", "is_admin": True},
        )
        assert response.status_code == 200
        assert response.json()["is_superuser"] is True
        assert response.json()["is_staff"] is True
        assert response.json()["is_admin"] is True
        assert response.json()["first_name"] == "Jack"
        assert response.json()["last_name"] == "Johnson"

        created_user = CustomUser.objects.get(email="jjsmooth@gmail.com")
        assert created_user.is_superuser is True
        assert created_user.is_staff is True
        assert created_user.is_admin is True
        assert created_user.first_name == "Jack"
        assert created_user.last_name == "Johnson"

    @override_settings(EMAIL_ENABLED=True)
    def test_user_signed_up_email_sent(
        self, api_client, create_user, create_organization, generic_type, db
    ):
        organization = create_organization(type=generic_type)
        user = create_user(superuser=True, organization=organization)
        new_user_organization = create_organization(type=generic_type, slug="test-2")
        # TODO: Because the parameters of the email are passed in on the
        # __init__ of EmailMultiAlternatives, instead of send, it is difficult
        # to introspect and ensure that they are correct - because __init__ is
        # a pain to mock.  We should figure out a way to test that the contents
        # of the email are correct.
        api_client.force_login(user)
        with mock.patch(
            "backend.app.user.signals.EmailMultiAlternatives.send"
        ) as mocked:
            response = api_client.post(
                "/v2/organizations/%s/users/" % new_user_organization.pk,
                {
                    "first_name": "Jack",
                    "last_name": "Johnson",
                    "email": "jjsmooth@gmail.com",
                },
            )
        assert response.status_code == 201
        assert mocked.called

    def test_create_invalid_group(
        self, api_client, create_user, nirveda, create_group, db
    ):
        group = create_group()
        user = create_user(superuser=True)
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/users/" % nirveda.pk,
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "email": "jjsmooth@gmail.com",
                "groups": [group.pk, 5400],
            },
        )
        assert response.status_code == 400
        assert response.json() == {
            "errors": {
                "groups": [
                    {
                        "message": 'Invalid pk "5400" - object does not exist.',
                        "code": "does_not_exist",
                    }
                ]
            }
        }

    def test_delete(self, api_client, create_user, nirveda, user, db):
        user_to_delete = create_user(organization=nirveda)
        api_client.force_login(user)
        response = api_client.delete(
            "/v2/organizations/%s/users/%s/" % (nirveda.pk, user_to_delete.pk)
        )
        assert response.status_code == 204
        assert CustomUser.objects.filter(pk=user_to_delete.pk).first() is None


class TestActiveUsers:
    def test_get(self, api_client, user, db):
        api_client.force_login(user)
        response = api_client.get("/v2/users/user/")
        assert response.status_code == 200
        assert response.json()["id"] == user.pk

    @pytest.mark.freeze_time("2020-01-01")
    def test_update(self, api_client, user, nirveda, db):
        api_client.force_login(user)

        response = api_client.patch(
            "/v2/users/user/",
            {
                "first_name": "Jack",
                "last_name": "Johnson",
                "is_admin": True,
                "language": "ES",
                "timezone": "America/New_York",
            },
        )

        user.refresh_from_db()
        assert response.status_code == 200
        assert response.json() == {
            "id": user.pk,
            "email": user.email,
            "first_name": "Jack",
            "last_name": "Johnson",
            "full_name": "Jack Johnson",
            "username": user.username,
            "is_admin": True,
            "is_superuser": False,
            "is_staff": False,
            "category": UserCategory.ADMIN,
            "is_active": True,
            "timezone": user.timezone,
            "language": "ES",
            "updated_by": user.pk,
            "updated_at": "2020-01-01 00:00:00",
            "created_by": None,
            "created_at": "2020-01-01 00:00:00",
            "last_login": api_datetime_string(user.last_login),
            "date_joined": api_datetime_string(user.date_joined),
            "groups": [],
            "organization": {
                "id": nirveda.pk,
                "name": nirveda.name,
                "description": None,
                "slug": nirveda.slug,
                "num_users": nirveda.num_users,
                "num_groups": nirveda.num_groups,
                "json_info": {"document-extractor": ["invoice"]},
                "logo": None,
                "is_active": nirveda.is_active,
                "created_at": api_datetime_string(nirveda.created_at),
                "updated_at": api_datetime_string(nirveda.updated_at),
                "type": {
                    "id": nirveda.type.pk,
                    "title": nirveda.type.title,
                    "slug": nirveda.type.slug,
                    "description": nirveda.type.description,
                },
            },
            "role": {
                "id": user.role.pk,
                "name": user.role.name,
                "description": user.role.description,
                "code": user.role.code,
            },
        }

        assert user.first_name == "Jack"
        assert user.last_name == "Johnson"
        assert user.language == "ES"
        assert user.is_admin is True

    def test_change_password(self, api_client, user, db):
        api_client.force_login(user)
        user.set_password("OldPassword")
        user.save()

        response = api_client.patch(
            "/v2/users/user/change-password/",
            {
                "current": "OldPassword",
                "password": "Newpassword123",
            },
        )
        assert response.status_code == 201
        assert response.json()["id"] == user.pk
        user.refresh_from_db()
        assert user.check_password("Newpassword123")

    def test_change_password_invalid_current(self, api_client, user, db):
        api_client.force_login(user)
        user.set_password("OldPassword")
        user.save()

        response = api_client.patch(
            "/v2/users/user/change-password/",
            {
                "current": "OldPassword123",
                "password": "Newpassword123",
            },
        )
        assert response.status_code == 401

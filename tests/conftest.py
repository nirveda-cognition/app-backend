import logging
from datetime import timedelta

import pytest
import requests
from django.utils import timezone
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

from backend.app.authentication.models import Role
from backend.app.document.models import DocumentStatus
from backend.app.organization.models import Organization, OrganizationType
from backend.app.user.models import CustomUser

from .factories.fixtures import *  # noqa


@pytest.fixture(autouse=True)
def no_requests(monkeypatch):
    """
    Prevent any requests from actually executing.
    """
    original_session_request = requests.sessions.Session.request

    def failing_request(*args, **kwargs):
        # The responses package is used for mocking responses down the the most
        # granular level.  If this package is enabled, we do not want to raise
        # an Exception because the mocking itself prevents the HTTP request.
        if requests.adapters.HTTPAdapter.send.__qualname__.startswith("RequestsMock"):
            return original_session_request(*args, **kwargs)
        else:
            raise Exception("No network access allowed in tests")

    monkeypatch.setattr("requests.sessions.Session.request", failing_request)
    monkeypatch.setattr("requests.sessions.HTTPAdapter.send", failing_request)


@pytest.fixture(autouse=True)
def disable_logging(caplog):
    """
    Disable logging behavior in certain packages.
    """
    caplog.set_level(logging.CRITICAL, logger="factory")
    caplog.set_level(logging.CRITICAL, logger="faker.factory")
    caplog.set_level(logging.CRITICAL, logger="factory-boy")


@pytest.fixture
def api_client():
    """
    Fixture to provide a Custom API Client that wraps around Django REST
    Framework's test Client and configures for our authentication protocols.
    """

    class BackendAPIClient(APIClient):
        def force_login(self, user, **kwargs):
            token = RefreshToken.for_user(user)
            self.credentials(HTTP_AUTHORIZATION="Bearer %s" % str(token.access_token))
            super().force_login(user, **kwargs)

    return BackendAPIClient()


@pytest.fixture(autouse=True)
def user_role(db):
    return Role.objects.create(
        pk=1,
        name=Role.NAME_USER,
        code=1,
        description="This role is for client user",
        created_at=timezone.now() - timedelta(days=30),
        updated_at=timezone.now() - timedelta(days=2),
    )


@pytest.fixture(autouse=True)
def admin_role(db):
    return Role.objects.create(
        pk=2,
        name=Role.NAME_ADMIN,
        code=2,
        description="This role is for client admin",
        created_at=timezone.now() - timedelta(days=30),
        updated_at=timezone.now() - timedelta(days=2),
    )


@pytest.fixture(autouse=True)
def account_user_role(db):
    return Role.objects.create(
        pk=3,
        name=Role.NAME_ACCOUNT_USER,
        code=3,
        description="This role is for client user",
        created_at=timezone.now() - timedelta(days=30),
        updated_at=timezone.now() - timedelta(days=2),
    )


@pytest.fixture(autouse=True)
def account_manager_role(db):
    return Role.objects.create(
        pk=4,
        name=Role.NAME_ACCOUNT_MANAGER,
        code=4,
        description="Account Manager",
        created_at=timezone.now() - timedelta(days=30),
        updated_at=timezone.now() - timedelta(days=2),
    )


@pytest.fixture(autouse=True)
def generic_type(db):
    return OrganizationType.objects.create(title="Generic", slug="generic")


@pytest.fixture(autouse=True)
def alfa_type(db):
    return OrganizationType.objects.create(title="ALFA", slug="alfa")


@pytest.fixture(autouse=True)
def apex_type(db):
    return OrganizationType.objects.create(title="APEX", slug="apex")


@pytest.fixture(autouse=True)
@pytest.mark.freeze_time("2020-01-01")
def nirveda(generic_type, db):
    return Organization.objects.create(
        name="Nirveda",
        slug="nirveda",
        api_access=True,
        type=generic_type,
        is_active=True,
        created_at=timezone.now() - timedelta(days=180),
        updated_at=timezone.now(),
        json_info={"document-extractor": ["invoice"]},
    )


@pytest.fixture(autouse=True)
@pytest.mark.freeze_time("2020-02-01")
def alfa(alfa_type, db):
    return Organization.objects.create(
        name="ALFA",
        slug="alfa",
        api_access=True,
        type=alfa_type,
        is_active=True,
        created_at=timezone.now() - timedelta(days=180),
        updated_at=timezone.now(),
        json_info={"document-extractor": ["alfa"]},
    )


@pytest.fixture(autouse=True)
@pytest.mark.freeze_time("2020-03-01")
def apex(apex_type, db):
    return Organization.objects.create(
        name="APEX",
        slug="apex",
        api_access=True,
        type=apex_type,
        is_active=True,
        created_at=timezone.now() - timedelta(days=180),
        updated_at=timezone.now(),
        json_info={"document-extractor": ["apex"]},
    )


@pytest.fixture(autouse=True)
def document_statuses(db):
    """
    Fixture to create instances of `obj:DocumentStatus` that mimic contents
    of production database.
    """
    return [
        DocumentStatus.objects.create(
            pk=1,
            status=DocumentStatus.PENDING,
            description="Processing",
            created_at=timezone.now() - timedelta(days=30),
            updated_at=timezone.now() - timedelta(days=2),
        ),
        DocumentStatus.objects.create(
            pk=2,
            status=DocumentStatus.COMPLETED,
            description="Complete",
            created_at=timezone.now() - timedelta(days=30),
            updated_at=timezone.now() - timedelta(days=2),
        ),
        DocumentStatus.objects.create(
            pk=3,
            status=DocumentStatus.FAILURE,
            description="Failed",
            created_at=timezone.now() - timedelta(days=30),
            updated_at=timezone.now() - timedelta(days=2),
        ),
        DocumentStatus.objects.create(
            pk=4,
            status=DocumentStatus.NEW,
            description="New",
            created_at=timezone.now() - timedelta(days=30),
            updated_at=timezone.now() - timedelta(days=2),
        ),
    ]


@pytest.fixture
def user(nirveda, user_role, db):
    user = CustomUser.objects.create(
        email="test+user@gmail.com",
        username="test+user@gmail.com",
        first_name="Test",
        last_name="User",
        is_active=True,
        is_admin=False,
        is_staff=False,
        is_superuser=False,
        role=user_role,
        organization=nirveda,
        timezone="UTC",
        language="EN",
    )
    user.set_password("test-password")
    user.save()
    return user


@pytest.fixture
def alfa_user(alfa, user_role, db):
    return CustomUser.objects.create(
        email="test+alfa@gmail.com",
        password="test-password",
        username="test+alfa@gmail.com",
        first_name="Alfa",
        last_name="User",
        is_active=True,
        is_admin=False,
        is_staff=False,
        is_superuser=False,
        role=user_role,
        organization=alfa,
        timezone="UTC",
        language="EN",
    )


@pytest.fixture
def apex_user(apex, user_role, db):
    return CustomUser.objects.create(
        email="test+apex@gmail.com",
        password="test-password",
        username="test+apex@gmail.com",
        first_name="Apex",
        last_name="User",
        is_active=True,
        is_admin=False,
        is_staff=False,
        is_superuser=False,
        role=user_role,
        organization=apex,
        timezone="UTC",
        language="EN",
    )


@pytest.fixture
def admin_user(nirveda, admin_role, db):
    return CustomUser.objects.create(
        email="test+admin@gmail.com",
        password="test-password",
        username="test+admin@gmail.com",
        first_name="Admin",
        last_name="User",
        is_active=True,
        role=admin_role,
        is_admin=True,
        is_staff=False,
        is_superuser=False,
        organization=nirveda,
        timezone="UTC",
        language="EN",
    )


@pytest.fixture
def superuser(nirveda, user_role, db):
    return CustomUser.objects.create(
        email="test+superuser@gmail.com",
        username="test+superuser@gmail.com",
        first_name="Super",
        last_name="User",
        is_active=True,
        role=user_role,
        is_admin=False,
        is_staff=True,
        is_superuser=True,
        organization=nirveda,
        timezone="UTC",
        language="EN",
    )

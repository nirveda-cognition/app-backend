from backend.app.collection.models import Collection
from backend.lib.django_utils.models import increment_model_attribute_until_unique


def test_increment_model_attribute_until_unique(create_collection, nirveda, apex):
    create_collection(name="Test Collection", organization=nirveda)
    create_collection(name="Test Collection(2)", organization=apex)
    create_collection(name="Test Collection(1)", organization=nirveda)

    name = increment_model_attribute_until_unique(
        value="Test Collection",
        model_cls=Collection,
        attribute="name",
        organization=nirveda,
    )
    assert name == "Test Collection(2)"

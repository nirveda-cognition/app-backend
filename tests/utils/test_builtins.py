from backend.lib.utils import (
    find_in_dict,
    find_string_formatted_arguments,
    is_float,
    is_integer,
    place_in_dict,
)


def test_is_integer():
    assert is_integer("5") is True
    assert is_integer("5.15") is False
    assert is_integer(5) is True
    assert is_integer(5.15) is False
    assert is_integer("foo") is False


def test_is_float():
    assert is_float("5") is False
    assert is_float("5.15") is True
    assert is_float(5) is False
    assert is_float(5.15) is True
    assert is_float("foo") is False


def test_find_in_dict():
    data = {"foo": {"bar": {"banana": "pear"}}}
    assert find_in_dict(data, "foo") == {"bar": {"banana": "pear"}}
    assert find_in_dict(data, ["foo", "bar"]) == {"banana": "pear"}
    assert find_in_dict(data, ["foo", "bar", "banana"]) == "pear"


def test_place_in_dict():
    data = {"foo": {"bar": {"banana": "pear"}}}

    place_in_dict(data, "foo", {"apple": {"bar": "banana"}})
    assert data == {"foo": {"apple": {"bar": "banana"}}}

    place_in_dict(data, ["foo", "apple"], {"bat": "pear"})
    assert data == {"foo": {"apple": {"bat": "pear"}}}

    place_in_dict(data, ["foo", "apple", "bat"], "banana")
    assert data == {"foo": {"apple": {"bat": "banana"}}}


def test_find_string_formatted_arguments():
    string_no_arguments = "Lorem ipsum lorem ipsum."
    arguments = find_string_formatted_arguments(string_no_arguments)
    assert arguments == []

    string_arguments = "Lorem ipsum {arg1} lorem ipsum {arg2}."
    arguments = find_string_formatted_arguments(string_arguments)
    assert arguments == ["arg1", "arg2"]

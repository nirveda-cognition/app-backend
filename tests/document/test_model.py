import datetime

import pytest
import pytz

from backend.app.document.models import Document, DocumentStatus


@pytest.mark.freeze_time("2020-01-01")
def test_status_changed_at_defaults_on_create(user, db, nirveda):
    document = Document.objects.create(
        name="test-doc",
        owner=user,
        creator=user,
        organization=nirveda,
    )
    assert document.status_changed_at == datetime.datetime(2020, 1, 1, tzinfo=pytz.utc)
    assert document.document_status.status == Document.STATUS_DEFAULT


@pytest.mark.freeze_time("2020-01-01")
def test_status_changed_at_defaults_on_init(user, db, nirveda):
    document = Document(
        name="test-doc",
        owner=user,
        creator=user,
        organization=nirveda,
    )
    assert document.status_changed_at == datetime.datetime(2020, 1, 1, tzinfo=pytz.utc)
    assert document.document_status.status == Document.STATUS_DEFAULT


def test_status_changed_at_persistent(user, db, nirveda):
    document = Document(
        name="test-doc",
        owner=user,
        creator=user,
        organization=nirveda,
    )
    original_status_changed_at = document.status_changed_at

    document.save()
    assert document.status_changed_at == original_status_changed_at

    document = Document.objects.create(
        name="test-doc-2",
        owner=user,
        creator=user,
        organization=nirveda,
    )
    original_status_changed_at = document.status_changed_at

    document.save()
    assert document.status_changed_at == original_status_changed_at


def test_status_changed_at_triggered(user, db, nirveda):
    document = Document.objects.create(
        name="test-doc",
        owner=user,
        creator=user,
        document_status=DocumentStatus.objects.processing(),
        organization=nirveda,
    )
    original_status_changed_at = document.status_changed_at

    # Change the document status.
    document.document_status = DocumentStatus.objects.new()
    document.save()
    assert document.status_changed_at != original_status_changed_at

    # Reset the status back to the original.
    original_status_changed_at = document.status_changed_at
    document.document_status = DocumentStatus.objects.processing()
    document.save()
    assert document.status_changed_at != original_status_changed_at

    # Should not trigger here because the status is not changing, even if
    # refreshing from database.
    original_status_changed_at = document.status_changed_at
    document.refresh_from_db()
    document.document_status = DocumentStatus.objects.processing()
    document.save()
    assert document.status_changed_at == original_status_changed_at

    # Refresh from the database and change the document status.
    original_status_changed_at = document.status_changed_at
    document.refresh_from_db()
    document.document_status = DocumentStatus.objects.new()
    document.save()
    assert document.status_changed_at != original_status_changed_at

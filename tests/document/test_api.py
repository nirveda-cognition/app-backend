import datetime

import mock
import pytest

from backend.app.document.models import Document
from backend.lib.utils.dateutils import api_datetime_string


class TestGetDocument:
    @pytest.mark.freeze_time("2020-01-01")
    def test_get(self, nirveda, create_document, user, api_client, db):
        document = create_document(organization=nirveda)
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/documents/%s/" % (nirveda.pk, document.pk)
        )

        assert response.status_code == 200
        assert response.json() == {
            "id": document.pk,
            "name": document.name,
            "type": document.type,
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
            "status_changed_at": api_datetime_string(document.status_changed_at),
            "size": document.size,
            "processing_time": None,
            "task": None,
            "schema": document.schema,
            "collection_list": [],
            "state": Document.STATE_UPLOADED,
            "organization": nirveda.pk,
            "owner": document.owner.pk,
            "creator": document.creator.pk,
            "document_status": {"id": 4, "status": "New", "description": "New"},
        }


class TestGetDocuments:
    def test_get(self, create_document, user, api_client, nirveda, db):
        # The results will be ordered by the value of status_changed_at.
        documents = [
            create_document(
                status_changed_at=datetime.datetime(2020, 1, 1),
                organization=nirveda,
            ),
            create_document(
                organization=nirveda, status_changed_at=datetime.datetime(2020, 2, 1)
            ),
        ]
        api_client.force_login(user)
        response = api_client.get("/v2/organizations/%s/documents/" % nirveda.pk)
        assert response.status_code == 200
        assert response.json()["data"] == [
            {
                "id": documents[1].pk,
                "name": documents[1].name,
                "type": documents[1].type,
                "created_at": api_datetime_string(documents[1].created_at),
                "updated_at": api_datetime_string(documents[1].updated_at),
                "status_changed_at": api_datetime_string(
                    documents[1].status_changed_at
                ),
                "size": documents[1].size,
                "processing_time": None,
                "task": None,
                "schema": documents[1].schema,
                "collection_list": [],
                "state": Document.STATE_UPLOADED,
                "organization": nirveda.pk,
                "owner": documents[1].owner.pk,
                "creator": documents[1].creator.pk,
                "document_status": {"id": 4, "status": "New", "description": "New"},
            },
            {
                "id": documents[0].pk,
                "name": documents[0].name,
                "type": documents[0].type,
                "created_at": api_datetime_string(documents[0].created_at),
                "updated_at": api_datetime_string(documents[0].updated_at),
                "status_changed_at": api_datetime_string(
                    documents[0].status_changed_at
                ),
                "size": documents[0].size,
                "processing_time": None,
                "task": None,
                "state": Document.STATE_UPLOADED,
                "schema": documents[0].schema,
                "collection_list": [],
                "organization": nirveda.pk,
                "owner": documents[0].owner.pk,
                "creator": documents[0].creator.pk,
                "document_status": {"id": 4, "status": "New", "description": "New"},
            },
        ]

    def test_date_range_filter(self, create_document, api_client, user, nirveda, db):
        documents = [
            create_document(
                organization=nirveda, created_at=datetime.datetime(2020, 1, 1)
            ),
            create_document(
                organization=nirveda, created_at=datetime.datetime(2019, 1, 1)
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/documents/?created_at__gte=2019-01-06" % nirveda.pk
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 1
        assert response.json()["data"][0]["id"] == documents[0].pk

        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/documents/?created_at__lte=2019-01-06" % nirveda.pk
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 1
        assert response.json()["data"][0]["id"] == documents[1].pk

    def test_get_root(
        self, create_document, create_collection, api_client, user, nirveda, db
    ):
        collection = create_collection(organization=nirveda)
        documents = [
            create_document(
                collections=[collection],
                organization=nirveda,
            ),
            create_document(
                organization=nirveda, status_changed_at=datetime.datetime(2020, 2, 1)
            ),
            create_document(
                collections=[collection],
                organization=nirveda,
            ),
            create_document(
                organization=nirveda, status_changed_at=datetime.datetime(2020, 1, 1)
            ),
        ]
        api_client.force_login(user)
        response = api_client.get("/v2/organizations/%s/documents/root/" % nirveda.pk)
        print("DEBUG : In test folder for root")

        assert response.status_code == 200
        assert response.json()["count"] == 2
        assert len(response.json()["data"]) == 2
        assert response.json()["data"][0]["id"] == documents[1].pk
        assert response.json()["data"][1]["id"] == documents[3].pk


class TestGetCollectionDocuments:
    def test_get(
        self, create_document, create_collection, user, api_client, nirveda, db
    ):
        # The results will be ordered by the value of status_changed_at.
        collection = create_collection(organization=nirveda)
        documents = [
            create_document(
                status_changed_at=datetime.datetime(2020, 1, 1),
                organization=nirveda,
                collections=[collection],
            ),
            create_document(
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 1),
                collections=[collection],
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/%s/documents/"
            % (nirveda.pk, collection.pk)
        )
        assert response.status_code == 200
        assert response.json()["count"] == 2
        assert response.json()["data"][0]["id"] == documents[1].pk
        assert response.json()["data"][1]["id"] == documents[0].pk


class TestUpdateDocument:
    def test_update(self, create_document, user, api_client, nirveda, db):
        document = create_document(organization=nirveda, name="Original Name.jpeg")
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/documents/%s/" % (nirveda.pk, document.pk),
            data={"name": "New Document Name.pdf"},
        )
        assert response.status_code == 200
        assert response.json()["name"] == "New Document Name.pdf"
        assert response.json()["type"] == "pdf"
        document.refresh_from_db()
        assert document.name == "New Document Name.pdf"
        assert document.type == "pdf"


class TestDocumentsTrash:
    def test_get_documents(self, create_document, user, api_client, nirveda, db):
        documents = [
            create_document(organization=nirveda),
            create_document(state=Document.STATE_TRASH, organization=nirveda),
        ]
        api_client.force_login(user)
        response = api_client.get("/v2/organizations/%s/documents/trash/" % nirveda.pk)
        assert response.status_code == 200
        assert len(response.json()["data"]) == 1
        assert response.json()["data"][0]["id"] == documents[1].pk

    def test_delete(self, create_document, user, api_client, nirveda, db):
        document = create_document(organization=nirveda)
        api_client.force_login(user)

        response = api_client.delete(
            "/v2/organizations/%s/documents/%s/" % (nirveda.pk, document.pk)
        )
        assert response.status_code == 204

        document = Document.objects.first()
        assert document is not None
        assert document.state == Document.STATE_TRASH

    def test_permanently_delete(self, create_document, user, api_client, db, nirveda):
        document = create_document(
            state=Document.STATE_TRASH,
            organization=nirveda,
        )
        api_client.force_login(user)
        response = api_client.delete(
            "/v2/organizations/%s/documents/trash/%s/" % (nirveda.pk, document.pk)
        )
        assert response.status_code == 204

        document = Document.objects.first()
        assert document is None

    def test_restore(self, create_document, user, api_client, nirveda, db):
        document = create_document(
            state=Document.STATE_TRASH,
            organization=nirveda,
        )
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/documents/trash/%s/restore/"
            % (nirveda.pk, document.pk)
        )
        assert response.status_code == 201
        assert response.json()["id"] == document.pk
        assert response.json()["state"] == Document.STATE_UPLOADED

        document = Document.objects.first()
        assert document is not None
        assert document.trash is False
        assert document.state == Document.STATE_UPLOADED


class TestUploadDocument:
    def test_upload(
        self, create_document, user, api_client, nirveda, db, create_collection
    ):
        collection = create_collection(organization=nirveda)
        document = create_document(collections=[collection], organization=nirveda)

        api_client.force_login(user)
        with mock.patch(
            "backend.app.document.views.create_and_process_document"
        ) as mocked:  # noqa
            mocked.return_value = {
                "put_url": "https://nirveda.com/put-url",
                "get_url": "https://nirveda.com/get-url",
                "headers": {"Content-Type": "application/json"},
                "document": document,
            }
            response = api_client.post(
                "/v2/organizations/%s/documents/upload/" % nirveda.pk,
                data={
                    "filename": "testfile.pdf",
                    "collection": collection.pk,
                },
            )
        assert response.status_code == 201

        assert "headers" in response.json()
        assert response.json()["headers"] == {"Content-Type": "application/json"}

        assert "put_signed_url" in response.json()
        assert (
            response.json()["put_signed_url"] == "https://nirveda.com/put-url"
        )  # noqa

        assert "get_signed_url" in response.json()
        assert (
            response.json()["get_signed_url"] == "https://nirveda.com/get-url"
        )  # noqa

        assert "document" in response.json()
        assert response.json()["document"]["id"] == document.pk

        assert mocked.called
        assert mocked.call_args[1] == {
            "filename": "testfile.pdf",
            "file_type": "pdf",
            "user": user,
            "collection": collection,
        }

    def test_upload_without_collection(
        self, create_document, user, api_client, nirveda, db
    ):
        document = create_document(organization=nirveda)

        api_client.force_login(user)
        with mock.patch(
            "backend.app.document.views.create_and_process_document"
        ) as mocked:  # noqa
            mocked.return_value = {
                "put_url": "https://nirveda.com/put-url",
                "get_url": "https://nirveda.com/get-url",
                "headers": {"Content-Type": "application/json"},
                "document": document,
            }
            response = api_client.post(
                "/v2/organizations/%s/documents/upload/" % nirveda.pk,
                data={"filename": "testfile.pdf"},
            )

        assert response.status_code == 201

        assert "headers" in response.json()
        assert response.json()["headers"] == {"Content-Type": "application/json"}

        assert "put_signed_url" in response.json()
        assert (
            response.json()["put_signed_url"] == "https://nirveda.com/put-url"
        )  # noqa

        assert "get_signed_url" in response.json()
        assert (
            response.json()["get_signed_url"] == "https://nirveda.com/get-url"
        )  # noqa

        assert "document" in response.json()
        assert response.json()["document"]["id"] == document.pk

        assert mocked.called
        assert mocked.call_args[1] == {
            "filename": "testfile.pdf",
            "file_type": "pdf",
            "user": user,
            "collection": None,
        }

    def test_upload_duplicate_name(
        self, create_collection, user, nirveda, api_client, create_document, db
    ):
        document = create_document(name="testfile.pdf", organization=nirveda)
        collection = create_collection(documents=[document], organization=nirveda)

        api_client.force_login(user)
        with mock.patch(
            "backend.app.document.views.create_and_process_document"
        ) as mocked:  # noqa
            mocked.return_value = {
                "put_url": "https://nirveda.com/put-url",
                "get_url": "https://nirveda.com/get-url",
                "headers": {"Content-Type": "application/json"},
                "document": document,
            }
            response = api_client.post(
                "/v2/organizations/%s/documents/upload/" % nirveda.pk,
                data={
                    "filename": "testfile.pdf",
                    "collection": collection.pk,
                },
            )

        assert response.status_code == 201

        assert "headers" in response.json()
        assert response.json()["headers"] == {"Content-Type": "application/json"}

        assert "put_signed_url" in response.json()
        assert (
            response.json()["put_signed_url"] == "https://nirveda.com/put-url"
        )  # noqa

        assert "get_signed_url" in response.json()
        assert (
            response.json()["get_signed_url"] == "https://nirveda.com/get-url"
        )  # noqa

        assert "document" in response.json()
        assert response.json()["document"]["id"] == document.pk

        assert mocked.called
        assert mocked.call_args[1] == {
            "filename": "testfile(1).pdf",
            "file_type": "pdf",
            "user": user,
            "collection": collection,
        }

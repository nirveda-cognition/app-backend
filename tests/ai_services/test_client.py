import datetime
import json

import pytest
import pytz
import responses
from django.test import override_settings

from backend.app.ai_services.api import AIServicesClient, client


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="http://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_services():
    responses.add(
        method=responses.GET,
        url="http://test-ai-services.ai/v1",
        json={"services": ["foo", "bar"]},
    )

    response = client.get_services()
    assert response == ["foo", "bar"]

    assert len(responses.calls) == 1
    assert responses.calls[0].request.method == "GET"
    assert responses.calls[0].request.url == "http://test-ai-services.ai/v1"
    assert responses.calls[0].request.headers == {
        "Content-Type": "application/json",
        "X-API-Key": "test-ai-services-api-key",
        "request-type": "ai-services",
        "Content-Length": "4",
        "User-Agent": "python-requests/2.20.1",
        "Connection": "keep-alive",
    }


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_capabilities():
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1/fake-service/capability",
        json={"capabilities": ["foo", "bar"]},
    )
    client = AIServicesClient()
    response = client.get_capabilities("fake-service")
    assert response == ["foo", "bar"]

    assert len(responses.calls) == 1
    assert responses.calls[0].request.headers == {
        "Content-Type": "application/json",
        "X-API-Key": "test-ai-services-api-key",
        "request-type": "ai-services",
        "Content-Length": "4",
        "User-Agent": "python-requests/2.20.1",
        "Connection": "keep-alive",
    }


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_task(create_task, nirveda):
    task = create_task(organization=nirveda)
    responses.add(
        method=responses.GET,
        url=("https://test-ai-services.ai/v1/test-service/task/%s" % task.ai_uuid),
        json={"status": "SUCCESS"},
    )
    response = client.get_task(service="test-service", task=task)
    assert response == {"status": "SUCCESS"}

    assert len(responses.calls) == 1
    assert responses.calls[0].request.headers == {
        "Content-Type": "application/json",
        "X-API-Key": "test-ai-services-api-key",
        "request-type": "ai-services",
        "Content-Length": "4",
        "User-Agent": "python-requests/2.20.1",
        "Connection": "keep-alive",
    }


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_task_status(create_task, nirveda):
    task = create_task(organization=nirveda)
    responses.add(
        method=responses.GET,
        url=("https://test-ai-services.ai/v1/test-service/task/%s" % task.ai_uuid),
        json={"status": "SUCCESS"},
    )
    response = client.get_task_status(service="test-service", task=task)
    assert response == "SUCCESS"
    assert len(responses.calls) == 1
    assert responses.calls[0].request.headers == {
        "Content-Type": "application/json",
        "X-API-Key": "test-ai-services-api-key",
        "request-type": "ai-services",
        "Content-Length": "4",
        "User-Agent": "python-requests/2.20.1",
        "Connection": "keep-alive",
    }


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_create_task():
    responses.add(
        method=responses.POST,
        url="https://test-ai-services.ai/v1/fake-service/task",
        json={"payload": {"apple": "banana"}},
    )
    response = client.create_task(
        service="fake-service",
        body={"data": {"foo": "bar"}},
    )
    assert len(responses.calls) == 1
    assert responses.calls[0].request.body == json.dumps({"data": {"foo": "bar"}})
    assert responses.calls[0].request.headers == {
        "Content-Type": "application/json",
        "Content-Length": "24",
        "X-API-Key": "test-ai-services-api-key",
        "request-type": "ai-services",
        "User-Agent": "python-requests/2.20.1",
        "Connection": "keep-alive",
    }

    assert response == {"payload": {"apple": "banana"}}


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
@pytest.mark.freeze_time("2020-01-01")
def test_callback_request(create_callback_request, create_task, nirveda):
    responses.add(
        method=responses.POST,
        url="https://google.com",
        json={"payload": {"apple": "banana"}},
    )
    task = create_task(organization=nirveda)
    callback = create_callback_request(callback="https://google.com", task=task)
    client.make_callback_request(callback, body={"foo": "bar"})

    assert len(responses.calls) == 1
    assert responses.calls[0].request.body == json.dumps({"foo": "bar"})
    assert responses.calls[0].request.headers == {
        "Content-Type": "application/json",
        "Content-Length": "14",
        "X-API-Key": "test-ai-services-api-key",
        "request-type": "ai-services",
        "User-Agent": "python-requests/2.20.1",
        "Connection": "keep-alive",
    }
    callback.refresh_from_db()
    assert callback.status_code == 200
    assert callback.request_data == {"foo": "bar"}
    assert callback.response_data == {"payload": {"apple": "banana"}}
    assert callback.time_sent == datetime.datetime(2020, 1, 1, tzinfo=pytz.UTC)


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_revoke_api_key(create_organization, generic_type):
    organization = create_organization(
        api_key="api-key",
        json_info={"document-extractor": ["capability 1", "capability 2"]},
        type=generic_type,
    )
    # Mock the request to AI Services to revoke the existing key.
    responses.add(
        method=responses.DELETE,
        url="https://test-ai-services.ai/v1/admin/api-key/api-key",
    )
    client.revoke_api_key(organization)
    organization.refresh_from_db()
    assert organization.api_key is None

    # Make sure a request to AI Services was issued to delete the old API Key.
    assert len(responses.calls) == 1

    assert responses.calls[0].request.method == "DELETE"
    assert "X-API-Key" in responses.calls[0].request.headers
    assert (
        responses.calls[0].request.headers["X-API-Key"] == "test-ai-services-api-key"
    )  # noqa
    assert "request-type" in responses.calls[0].request.headers
    assert responses.calls[0].request.headers["request-type"] == "ai-services"


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_generate_api_key(create_organization, generic_type):
    organization = create_organization(
        api_key="old-api-key",
        json_info={"document-extractor": ["capability 1", "capability 2"]},
        type=generic_type,
    )
    # Mock the request to AI Services to revoke the existing key.
    responses.add(
        method=responses.DELETE,
        url="https://test-ai-services.ai/v1/admin/api-key/old-api-key",
    )
    # Mock the request to AI Services to generate the new key.
    responses.add(
        method=responses.POST,
        url="https://test-ai-services.ai/v1/admin/api-key",
        json={"api_key": "abcd-efgh-ijkl"},
    )
    client.generate_api_key(organization)
    organization.refresh_from_db()
    assert organization.api_key == "abcd-efgh-ijkl"

    # Make sure a request to AI Services was issued to generate the new API Key.
    assert len(responses.calls) == 2
    assert json.loads(responses.calls[0].request.body) == {
        "tenant_id": organization.tenant_id,
        "service": "document-extractor",
        "capabilities": ["capability_1", "capability_2"],
    }
    assert "X-API-Key" in responses.calls[0].request.headers
    assert (
        responses.calls[0].request.headers["X-API-Key"] == "test-ai-services-api-key"
    )  # noqa
    assert "request-type" in responses.calls[0].request.headers
    assert responses.calls[0].request.headers["request-type"] == "ai-services"

    # Make sure a request to AI Services was issued to delete the old API Key.
    assert responses.calls[1].request.method == "DELETE"
    assert "X-API-Key" in responses.calls[1].request.headers
    assert (
        responses.calls[1].request.headers["X-API-Key"] == "test-ai-services-api-key"
    )  # noqa
    assert "request-type" in responses.calls[1].request.headers
    assert responses.calls[1].request.headers["request-type"] == "ai-services"

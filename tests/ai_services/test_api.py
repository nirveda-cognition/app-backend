import responses
from django.test import override_settings


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_services(api_client, user):
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1",
        json={"services": [{"service": "fake-service", "label": "Fake Service"}]},
    )
    api_client.force_login(user)
    response = api_client.get("/v2/ai_services/services/")
    assert response.status_code == 200
    assert response.json() == [{"service": "fake-service", "label": "Fake Service"}]


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_capabilities(api_client, user):
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1/fake-service/capability",
        json={
            "capabilities": [
                {
                    "capability": "capability_1",
                    "label": "Capability 1",
                },
                {
                    "capability": "capability_2",
                    "label": "Capability 2",
                },
            ]
        },
    )
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1",
        json={"services": [{"service": "fake-service", "label": "Fake Service"}]},
    )
    api_client.force_login(user)
    response = api_client.get("/v2/ai_services/services/fake-service/capabilities/")
    assert response.status_code == 200
    assert response.json() == [
        {
            "capability": "capability_1",
            "label": "Capability 1",
        },
        {
            "capability": "capability_2",
            "label": "Capability 2",
        },
    ]


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_capabilities_invalid_service(api_client, user):
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1/fake-service/capability",
        json={
            "capabilities": [
                {
                    "capability": "capability_1",
                    "label": "Capability 1",
                },
                {
                    "capability": "capability_2",
                    "label": "Capability 2",
                },
            ]
        },
    )
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1",
        json={"services": [{"service": "fake-service", "label": "Fake Service"}]},
    )
    api_client.force_login(user)
    response = api_client.get("/v2/ai_services/services/missing-service/capabilities/")
    assert response.status_code == 404


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_get_capability(api_client, user):
    responses.add(
        method=responses.GET,
        url=("https://test-ai-services.ai/v1/fake-service/capability/" "capability_1"),
        json={"result_types": ["foo", "bar"]},
    )
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1/fake-service/capability",
        json={
            "capabilities": [
                {
                    "capability": "capability_1",
                    "label": "Capability 1",
                },
                {
                    "capability": "capability_2",
                    "label": "Capability 2",
                },
            ]
        },
    )
    responses.add(
        method=responses.GET,
        url="https://test-ai-services.ai/v1",
        json={"services": [{"service": "fake-service", "label": "Fake Service"}]},
    )
    api_client.force_login(user)
    response = api_client.get(
        "/v2/ai_services/services/fake-service/capabilities/capability_1/"
    )
    assert response.status_code == 200
    assert response.json() == {"result_types": ["foo", "bar"]}


def test_normalize_document_data(
    api_client, user, create_document, create_task, create_task_result, nirveda
):

    task_results_newdata_0 = {
        "data": {
            "capability": "invoice_v2",
            "result_type": "invoice",
            "extracted_data": [
                {
                    "id": "14",
                    "type": "field",
                    "index": 1,
                    "label": "Invoice Number",
                    "value": "46576355",
                    "regions": [
                        {
                            "x": 0.76346037507562,
                            "y": 0.24925181701581872,
                            "page": 1,
                            "type": "value",
                            "width": 0.036297640653357534,
                            "height": 0.006840530141085934,
                        }
                    ],
                    "property_id": "invoice_number",
                }
            ],
        },
        "meta": {
            "duration": 144.271777,
            "end_time": "2020-12-29T02:42:43Z",
            "start_time": "2020-12-29T02:40:19Z",
        },
        "status": "SUCCESS",
        "service": "document-extractor",
        "task_id": "2be40d72-b8de-452c-ac38-07b5b37e0047",
    }

    task_results_newdata_1 = {
        "data": {
            "capability": "invoice_v2",
            "result_type": "invoice",
            "extracted_data": [
                {
                    "id": "14",
                    "type": "field",
                    "index": 1,
                    "label": "Address",
                    "value": "Showroom: 10 Kaki Bukit Road 1",
                    "regions": [
                        {
                            "x": 0.76346037507562,
                            "y": 0.24925181701581872,
                            "page": 1,
                            "type": "value",
                            "width": 0.036297640653357534,
                            "height": 0.006840530141085934,
                        }
                    ],
                    "property_id": "invoice_number",
                }
            ],
        },
        "meta": {
            "duration": 144.271777,
            "end_time": "2020-12-29T02:42:43Z",
            "start_time": "2020-12-29T02:40:19Z",
        },
        "status": "SUCCESS",
        "service": "document-extractor",
        "task_id": "2be40d72-b8de-452c-ac38-07b5b37e0047",
    }

    tasks = [create_task(organization=nirveda), create_task(organization=nirveda)]
    documents = [
        create_document(organization=nirveda, task=tasks[0]),
        create_document(organization=nirveda, task=tasks[1]),
    ]
    create_task_result(
        new_data=task_results_newdata_0, document=documents[0], task=tasks[0]
    )
    create_task_result(
        new_data=task_results_newdata_1,
        document=documents[1],
        task=tasks[1],
    )

    api_client.force_login(user)
    response = api_client.post(
        "/v2/ai_services/normalize-document-data/",
        data={"documents": [doc.pk for doc in documents]},
    )

    assert response.status_code == 201
    assert response.json()["data"] == [
        {
            "DOCUMENT NAME": documents[1].name,
            "PAGE": 1,
            "ADDRESS": "Showroom: 10 Kaki Bukit Road 1",
            "INVOICE NUMBER": None,
        },
        {
            "DOCUMENT NAME": documents[0].name,
            "PAGE": 1,
            "ADDRESS": None,
            "INVOICE NUMBER": "46576355",
        },
    ]

import json

import pytest
import requests
from rest_framework import decorators, response

from backend.app.ai_services.api.exceptions import AIServicesHttpError


@decorators.api_view(["GET"])
def view_using_drf_response(request):
    resp = response.Response("Fake response.", status=400)
    raise AIServicesHttpError(
        error=requests.exceptions.RequestException(
            "There was a request error."
        ),  # noqa
        message="There was a request error.",
        code=AIServicesHttpError.CONNECTION_ERROR,
        response=resp,
        request=request,
        # A DRF Response will not have URL or reason property.
        url="/test",
        reason="BAD REQUEST",
    )


@decorators.api_view(["GET"])
def view_using_requests_response(request):
    resp = requests.Response()
    resp.status_code = 400
    resp.url = "/test"
    resp.reason = "BAD REQUEST"

    raise AIServicesHttpError(
        error=requests.exceptions.RequestException(
            "There was a request error."
        ),  # noqa
        message="There was a request error.",
        code=AIServicesHttpError.CONNECTION_ERROR,
        response=resp,
    )


@pytest.mark.parametrize(
    "test_view", [view_using_drf_response, view_using_requests_response]
)
def test_ai_services_http_error(test_view, rf, api_client, user, db):
    # Create a Fake Request Object for View
    request = rf.get("/test")
    request.user = user

    # Get the Response from the View - Note: We must call response.render()
    # before accessing the response content.
    resp = test_view(request)
    resp.render()
    assert json.loads(resp.content) == {
        "errors": {
            "__all__": [
                {
                    "message": (
                        "There was an error communicating with the " "AI Services API."
                    ),
                    "code": "ai_services_http_error",
                    "request": {
                        "status_code": 400,
                        "code": "connection_error",
                        "message": "There was a request error.",
                        "reason": "BAD REQUEST",
                        "url": "/test",
                    },
                }
            ]
        }
    }

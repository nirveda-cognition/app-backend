from backend.app.collection.models import Collection
from backend.app.document.models import Document


def test_get_unique_collection_name(create_collection, db):
    children = [
        create_collection(name="collection-name-1"),
        create_collection(name="collection-name-1(1)"),
    ]
    collection = create_collection(collections=children)
    name = collection.create_unique_collection_name("collection-name-1")
    assert name == "collection-name-1(2)"


def test_get_unique_document_name(create_collection, create_document, nirveda, db):
    collection = create_collection()
    [
        create_document(
            organization=nirveda, name="document-name-1.pdf", collections=[collection]
        ),
        create_document(
            organization=nirveda,
            name="document-name-1(1).txt",
            collections=[collection],
        ),
        create_document(
            organization=nirveda,
            name="document-name-1(2).jpeg",
            collections=[collection],
        ),
    ]
    name = collection.create_unique_document_name("document-name-1.png")
    assert name == "document-name-1(3).png"


def test_to_trash(user, create_collection, create_document, nirveda, db):
    collection = create_collection(
        collections=[
            create_collection(),
            create_collection(
                name="Nested Child",
                collections=[
                    create_collection(),
                    create_collection(),
                ],
                documents=[
                    create_document(organization=nirveda),
                    create_document(organization=nirveda),
                ],
            ),
        ],
        documents=[
            create_document(organization=nirveda),
            create_document(organization=nirveda),
        ],
    )
    collection.to_trash(user)
    assert collection.state == Collection.STATE_TRASH
    assert all(
        [c.state == Collection.STATE_TRASH for c in collection.collections.all()]
    )
    assert all([d.state == Document.STATE_TRASH for d in collection.documents.all()])
    nested_child_collection = Collection.objects.get(name="Nested Child")
    assert all(
        [
            c.state == Collection.STATE_TRASH
            for c in nested_child_collection.collections.all()
        ]
    )
    assert all(
        [
            d.state == Document.STATE_TRASH
            for d in nested_child_collection.documents.all()
        ]
    )


def test_restore(user, create_collection, create_document, nirveda, db):
    collection = create_collection(
        state=Collection.STATE_TRASH,
        collections=[
            create_collection(state=Collection.STATE_TRASH),
            create_collection(
                state=Collection.STATE_TRASH,
                name="Nested Child",
                collections=[
                    create_collection(state=Collection.STATE_TRASH),
                    create_collection(state=Collection.STATE_TRASH),
                ],
                documents=[
                    create_document(organization=nirveda, state=Document.STATE_TRASH),
                    create_document(organization=nirveda, state=Document.STATE_TRASH),
                ],
            ),
        ],
        documents=[
            create_document(organization=nirveda, state=Document.STATE_TRASH),
            create_document(organization=nirveda, state=Document.STATE_TRASH),
        ],
    )
    collection.restore()
    assert collection.state == Collection.STATE_ACTIVE
    assert all(
        [c.state == Collection.STATE_ACTIVE for c in collection.collections.all()]
    )
    assert all([d.state == Document.STATE_UPLOADED for d in collection.documents.all()])
    nested_child_collection = Collection.objects.get(name="Nested Child")
    assert all(
        [
            c.state == Collection.STATE_ACTIVE
            for c in nested_child_collection.collections.all()
        ]
    )
    assert all(
        [
            d.state == Document.STATE_UPLOADED
            for d in nested_child_collection.documents.all()
        ]
    )

import datetime

import mock
import pytest

from backend.app.collection.models import Collection
from backend.lib.utils.dateutils import api_datetime_string


class TestCreateCollection:
    @pytest.mark.freeze_time("2020-01-01")
    def test_create(
        self,
        superuser,
        api_client,
        create_organization,
        db,
        create_collection,
        generic_type,
    ):
        organization = create_organization(type=generic_type)
        children = [
            create_collection(organization=organization),
            create_collection(organization=organization),
        ]
        parents = [
            create_collection(organization=organization),
            create_collection(organization=organization),
        ]
        api_client.force_login(superuser)
        response = api_client.post(
            "/v2/organizations/%s/collections/" % organization.pk,
            data={
                "name": "Test Collection",
                "collections": [c.pk for c in children],
                "parents": [c.pk for c in parents],
            },
        )
        assert response.status_code == 201

        assert "id" in response.json()
        created_collection = Collection.objects.filter(pk=response.json()["id"]).first()

        # Make sure the Collection instance was properly created.
        assert created_collection is not None
        assert created_collection.name == "Test Collection"
        assert created_collection.organization == organization

        assert len(list(created_collection.collections.all())) == 2
        assert children[0] in list(created_collection.collections.all())
        assert children[1] in list(created_collection.collections.all())

        assert len(list(created_collection.parents.all())) == 2
        assert parents[0] in list(created_collection.parents.all())
        assert parents[1] in list(created_collection.parents.all())

        # Make sure the response is the serialized form of the created
        # Collection instance.
        assert response.json() == {
            "id": created_collection.id,
            "name": "Test Collection",
            "status_changed_at": "2020-01-01 00:00:00",
            "collection_state": Collection.COLLECTION_STATE_NEW,
            "state": Collection.STATE_ACTIVE,
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
            "schema": created_collection.schema,
            "size": float(created_collection.size),
            "result": None,
            "documents": [],
            "assigned_users": [],
            "organization": organization.pk,
            "created_by": superuser.pk,
            "owner": superuser.pk,
            "collections": [
                {
                    "id": children[0].pk,
                    "name": children[0].name,
                    "collection_state": Collection.COLLECTION_STATE_NEW,
                    "collections": [],
                    "documents": [],
                    "parents": [created_collection.id],
                },
                {
                    "id": children[1].pk,
                    "name": children[1].name,
                    "collection_state": Collection.COLLECTION_STATE_NEW,
                    "collections": [],
                    "documents": [],
                    "parents": [created_collection.id],
                },
            ],
            "parents": [
                {
                    "id": parents[0].pk,
                    "name": parents[0].name,
                    "collection_state": Collection.COLLECTION_STATE_NEW,
                    "collections": [created_collection.id],
                    "documents": [],
                    "parents": [],
                },
                {
                    "id": parents[1].pk,
                    "name": parents[1].name,
                    "collection_state": Collection.COLLECTION_STATE_NEW,
                    "collections": [created_collection.id],
                    "documents": [],
                    "parents": [],
                },
            ],
        }

    def test_just_name(
        self, superuser, api_client, create_organization, db, create_collection
    ):
        organization = create_organization()
        api_client.force_login(superuser)
        response = api_client.post(
            "/v2/organizations/%s/collections/" % organization.pk,
            data={
                "name": "Test Collection",
            },
        )
        assert response.status_code == 201

    def test_duplicate_name(self, user, api_client, nirveda, create_collection, db):
        create_collection(organization=nirveda, name="Test")
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/" % nirveda.pk,
            data={
                "name": "Test",
            },
        )
        assert response.status_code == 400

    def test_child_wrong_org(
        self, user, api_client, nirveda, apex, db, create_collection
    ):
        children = [
            create_collection(organization=apex),
            create_collection(organization=nirveda),
        ]
        parents = [
            create_collection(organization=nirveda),
            create_collection(organization=nirveda),
        ]
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/" % nirveda.pk,
            data={
                "name": "Test Collection",
                "collections": [c.pk for c in children],
                "parents": [c.pk for c in parents],
            },
        )
        assert response.status_code == 400
        assert "%s" % children[0].pk in response.json()["errors"]["collections"]

    def test_parent_wrong_org(
        self, user, api_client, nirveda, apex, db, create_collection
    ):
        children = [
            create_collection(organization=nirveda),
            create_collection(organization=nirveda),
        ]
        parents = [
            create_collection(organization=apex),
            create_collection(organization=nirveda),
        ]
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/" % nirveda.pk,
            data={
                "name": "Test Collection",
                "collections": [c.pk for c in children],
                "parents": [c.pk for c in parents],
            },
        )
        assert response.status_code == 400
        assert "%s" % parents[0].pk in response.json()["errors"]["parents"]

    def test_parents_children_conflict(
        self, user, api_client, nirveda, create_collection, db
    ):
        child_and_parent = create_collection(organization=nirveda)
        children = [create_collection(organization=nirveda), child_and_parent]
        parents = [create_collection(organization=nirveda), child_and_parent]
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/" % nirveda.pk,
            data={
                "name": "Test Collection",
                "collections": [c.pk for c in children],
                "parents": [c.pk for c in parents],
            },
        )
        assert response.status_code == 400
        assert response.json() == {
            "errors": {
                "__all__": [
                    {
                        "code": "invalid",
                        "message": (
                            "The children collections must be mutually exclusive "
                            "from the parent collections."
                        ),
                    }
                ]
            }
        }

    def test_create_as_child(self, user, api_client, nirveda, db, create_collection):
        parent = create_collection(organization=nirveda)
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/%s/child/" % (nirveda.pk, parent.pk),
            format="json",
            data={"name": "Test Collection"},
        )

        assert response.status_code == 201

        assert "id" in response.json()
        created_collection = Collection.objects.filter(pk=response.json()["id"]).first()

        # Make sure the Collection instance was properly created.
        assert created_collection is not None
        assert created_collection.name == "Test Collection"
        assert created_collection.organization == nirveda
        assert len(created_collection.parents.all()) == 1
        assert created_collection.parents.first() == parent

        assert len(response.json()["parents"]) == 1
        assert response.json()["parents"][0] == {
            "id": parent.pk,
            "name": parent.name,
            "collection_state": Collection.COLLECTION_STATE_NEW,
            "documents": [],
            "collections": [created_collection.pk],
            "parents": [],
        }


class TestUpdateCollection:
    def test_duplicate_name(self, user, api_client, nirveda, create_collection, db):
        create_collection(organization=nirveda, name="Test")
        collection = create_collection(organization=nirveda, name="Other")
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/collections/%s/" % (nirveda.pk, collection.pk),
            data={"name": "Test"},
        )
        assert response.status_code == 400

    def test_update_state(self, create_collection, user, api_client, nirveda, db):
        collection = create_collection(
            owner=user,
            created_by=user,
            organization=nirveda,
            collection_state=Collection.COLLECTION_STATE_READY,
        )
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/collections/%s/" % (nirveda.pk, collection.pk),
            data={"collection_state": Collection.COLLECTION_STATE_COMPLETED},
        )
        assert response.status_code == 200
        assert response.json()["collection_state"] == (
            Collection.COLLECTION_STATE_COMPLETED
        )

        # Make sure changes persist to database.
        collection.refresh_from_db()
        assert (
            collection.collection_state == Collection.COLLECTION_STATE_COMPLETED
        )  # noqa


class TestGetCollection:
    def test_get(self, create_collection, user, api_client, nirveda, db):
        collection = create_collection(
            owner=user, created_by=user, organization=nirveda
        )
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/%s/" % (nirveda.pk, collection.pk)
        )

        assert response.status_code == 200
        assert response.json() == {
            "id": collection.pk,
            "name": collection.name,
            "status_changed_at": api_datetime_string(collection.status_changed_at),
            "state": Collection.STATE_ACTIVE,
            "collection_state": Collection.COLLECTION_STATE_NEW,
            "schema": collection.schema,
            "size": float(collection.size),
            "collections": [],
            "parents": [],
            "documents": [],
            "result": None,
            "assigned_users": [],
            "created_at": api_datetime_string(collection.created_at),
            "updated_at": api_datetime_string(collection.updated_at),
            "organization": nirveda.pk,
            "created_by": user.pk,
            "owner": user.pk,
        }

    def test_get_no_user_access(
        self, create_collection, create_organization, create_user, api_client, db
    ):
        organization = create_organization()
        another_organization = create_organization()
        user = create_user(organization=organization)
        collection = create_collection(organization=another_organization)

        api_client.force_login(user)

        response = api_client.get(
            "/v2/organizations/%s/collections/%s/"
            % (another_organization.pk, collection.pk)
        )
        assert response.status_code == 403
        assert response.json() == {
            "errors": {
                "__all__": [
                    {
                        "message": (
                            "The user does not have access to this organization."
                        ),
                        "code": "no_user_access",
                    }
                ]
            }
        }

    def test_get_invalid_org(self, create_collection, user, api_client, db):
        collection = create_collection()
        api_client.force_login(user)

        response = api_client.get(
            "/v2/organizations/%s/collections/%s/" % (500, collection.pk)
        )
        assert response.status_code == 404
        assert response.json() == {
            "errors": {
                "__all__": [
                    {
                        "code": "not_found",
                        "message": "No Organization matches the given query.",
                    }
                ]
            }
        }

    def test_get_not_in_org(
        self, create_collection, create_organization, user, api_client, nirveda, db
    ):
        # Create a collection in another organization.
        organization = create_organization()
        collection = create_collection(organization=organization)

        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/%s/" % (nirveda.pk, collection.pk)
        )
        assert response.status_code == 404
        assert response.json() == {
            "errors": {
                "__all__": [
                    {
                        "code": "not_found",
                        "message": "No Collection matches the given query.",
                    }
                ]
            }
        }


class TestGetCollections:
    def test_get(
        self, create_collection, create_organization, user, api_client, nirveda, db
    ):
        collections = [
            create_collection(
                owner=user,
                created_by=user,
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 1),
            ),
            create_collection(
                owner=user,
                created_by=user,
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 2),
            ),
        ]

        api_client.force_login(user)
        response = api_client.get("/v2/organizations/%s/collections/" % nirveda.pk)
        assert response.status_code == 200
        assert response.json()["data"] == [
            {
                "id": collections[1].pk,
                "name": collections[1].name,
                "status_changed_at": api_datetime_string(
                    collections[1].status_changed_at
                ),
                "state": Collection.STATE_ACTIVE,
                "collection_state": Collection.COLLECTION_STATE_NEW,
                "schema": collections[1].schema,
                "size": collections[1].size,
                "collections": [],
                "parents": [],
                "documents": [],
                "result": None,
                "assigned_users": [],
                "created_at": api_datetime_string(collections[1].created_at),
                "updated_at": api_datetime_string(collections[1].updated_at),
                "organization": nirveda.pk,
                "created_by": user.pk,
                "owner": user.pk,
            },
            {
                "id": collections[0].pk,
                "name": collections[0].name,
                "status_changed_at": api_datetime_string(
                    collections[0].status_changed_at
                ),
                "state": Collection.STATE_ACTIVE,
                "collection_state": Collection.COLLECTION_STATE_NEW,
                "schema": collections[0].schema,
                "size": collections[0].size,
                "collections": [],
                "parents": [],
                "documents": [],
                "result": None,
                "assigned_users": [],
                "created_at": api_datetime_string(collections[0].created_at),
                "updated_at": api_datetime_string(collections[0].updated_at),
                "organization": nirveda.pk,
                "created_by": user.pk,
                "owner": user.pk,
            },
        ]

    def test_ordering(self, create_collection, user, api_client, nirveda, db):
        [
            create_collection(
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 1),
                name="Alpha",
            ),
            create_collection(
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 2),
                name="Beta",
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/?ordering=name" % nirveda.pk
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 2
        assert response.json()["data"][0]["name"] == "Alpha"
        assert response.json()["data"][1]["name"] == "Beta"

    def test_search(self, create_collection, user, api_client, nirveda, db):
        [
            create_collection(
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 1),
                name="Zzzzzz",
            ),
            create_collection(
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 2),
                name="Jack",
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/?search=jac" % nirveda.pk
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 1
        assert response.json()["data"][0]["name"] == "Jack"

    def test_get_root(self, create_collection, user, api_client, nirveda, db):
        collections = [
            create_collection(
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 2, 1),
                collections=[
                    create_collection(organization=nirveda),
                ],
            ),
            create_collection(
                organization=nirveda,
                status_changed_at=datetime.datetime(2020, 1, 1),
                collections=[
                    create_collection(organization=nirveda),
                    create_collection(organization=nirveda),
                ],
            ),
        ]
        api_client.force_login(user)
        response = api_client.get("/v2/organizations/%s/collections/root/" % nirveda.pk)
        assert response.status_code == 200
        assert response.json()["count"] == 2
        assert len(response.json()["data"]) == 2
        assert response.json()["data"][0]["id"] == collections[0].pk
        assert response.json()["data"][1]["id"] == collections[1].pk


class TestCollectionTrash:
    def test_get(self, create_collection, user, api_client, nirveda, db):
        collections = [
            create_collection(organization=nirveda),
            create_collection(state=Collection.STATE_TRASH, organization=nirveda),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/trash/" % nirveda.pk
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 1
        assert response.json()["data"][0]["id"] == collections[1].pk

    def test_delete(self, create_collection, user, api_client, nirveda, db):
        collection = create_collection(organization=nirveda)
        api_client.force_login(user)

        response = api_client.delete(
            "/v2/organizations/%s/collections/%s/" % (nirveda.pk, collection.pk)
        )
        assert response.status_code == 204

        collection = Collection.objects.first()
        assert collection is not None
        assert collection.state == Collection.STATE_TRASH

    def test_permanently_delete(self, create_collection, user, api_client, nirveda, db):
        collection = create_collection(
            state=Collection.STATE_TRASH,
            organization=nirveda,
        )
        api_client.force_login(user)
        response = api_client.delete(
            "/v2/organizations/%s/collections/trash/%s/" % (nirveda.pk, collection.pk)
        )
        assert response.status_code == 204
        collection = Collection.objects.first()
        assert collection is None

    def test_restore(self, create_collection, user, api_client, nirveda, db):
        collection = create_collection(
            state=Collection.STATE_TRASH,
            organization=nirveda,
        )
        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/collections/trash/%s/restore/"
            % (nirveda.pk, collection.pk)
        )
        assert response.status_code == 201
        assert response.json()["id"] == collection.pk
        assert response.json()["state"] == Collection.STATE_ACTIVE

        collection = Collection.objects.first()
        assert collection is not None
        assert collection.state == Collection.STATE_ACTIVE


class TestCollectionResults:
    def test_get(
        self, create_collection, create_collection_result, user, api_client, db, nirveda
    ):
        collection = create_collection(organization=nirveda)
        collection_result = create_collection_result(
            result={"data": {"foo": "bar"}}, collection=collection
        )
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/%s/results/%s/"
            % (nirveda.pk, collection.pk, collection_result.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": collection_result.pk,
            "result": {"data": {"foo": "bar"}},
            "time_created": api_datetime_string(collection_result.time_created),
        }

    def test_get_all(
        self, create_collection, create_collection_result, user, api_client, db, nirveda
    ):
        collection = create_collection(organization=nirveda)
        collection_results = [
            create_collection_result(
                result={"data": {"foo": "bar"}},
                collection=collection,
                time_created=datetime.datetime(2020, 1, 2),
            ),
            create_collection_result(
                result={"data": {"bar": "foo"}},
                collection=collection,
                time_created=datetime.datetime(2020, 1, 3),
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/collections/%s/results/"
            % (collection.organization.pk, collection.pk)
        )
        assert response.status_code == 200
        assert len(response.json()["data"]) == 2
        assert response.json()["data"] == [
            {
                "id": collection_results[1].pk,
                "result": {"data": {"bar": "foo"}},
                "time_created": api_datetime_string(collection_results[1].time_created),
            },
            {
                "id": collection_results[0].pk,
                "result": {"data": {"foo": "bar"}},
                "time_created": api_datetime_string(collection_results[0].time_created),
            },
        ]

    @pytest.mark.skip("This test needs to be finished.")
    def test_update(
        self, create_collection, create_collection_result, user, api_client, db
    ):
        result = create_collection_result(result={"transactions": {"bar": "apple"}})
        # The collection has to have a schema defined in the schema.json file.
        collection = create_collection(results=[result], loan=True)
        user.organization = collection.organization
        user.save()

        api_client.force_login(user)

        response = api_client.patch(  # noqa
            "/v2/organizations/%s/collections/%s/results/%s/"
            % (
                collection.organization.pk,
                collection.pk,
                collection.results.first().pk,
            ),
            data={
                "keys": ["transactions", "bar"],
                "value": "banana",
                "key_changed": "transactions",
            },
        )

    @pytest.mark.skip("This test needs to be finished.")
    def test_update_invalid_key(
        self, create_collection, user, api_client, create_collection_result, db
    ):
        result = create_collection_result(result={"foo": {"bar": "apple"}})
        collection = create_collection(results=[result])
        user.organization = collection.organization
        user.save()

        api_client.force_login(user)

        response = api_client.patch(
            "/v2/organizations/%s/collections/%s/results/%s/"
            % (
                collection.organization.pk,
                collection.pk,
                collection.results.first().pk,
            ),
            data={"keys": ["key1"], "value": 10, "key_changed": ["blah"]},
        )
        assert response.status_code == 400
        assert response.json() == {
            "errors": {
                "keys": [
                    {
                        "message": "The provided key key1 is not in the results.",
                        "code": "invalid",
                    }
                ]
            }
        }


class TestAssignUsers:
    def test_assign(
        self, create_collection, user, create_user, api_client, nirveda, db
    ):
        collection = create_collection(
            organization=nirveda,
            assigned_users=[
                create_user(),
                create_user(),
            ],
        )
        additional_users = [create_user(), create_user()]
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/%s/assign-users/"
            % (nirveda.pk, collection.pk),
            data={"users": [usr.pk for usr in additional_users]},
        )
        assert response.status_code == 201
        assert len(response.json()["assigned_users"]) == 4
        assert additional_users[0].pk in [
            usr["id"] for usr in response.json()["assigned_users"]
        ]
        assert additional_users[1].pk in [
            usr["id"] for usr in response.json()["assigned_users"]
        ]

    def test_unassign(
        self, create_collection, user, create_user, api_client, nirveda, db
    ):
        users = [create_user(), create_user(), create_user()]
        collection = create_collection(organization=nirveda, assigned_users=users)
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/%s/unassign-users/"
            % (nirveda.pk, collection.pk),
            data={"users": [users[0].pk, users[2].pk]},
        )
        assert response.status_code == 201
        assert len(response.json()["assigned_users"]) == 1
        assert [usr["id"] for usr in response.json()["assigned_users"]] == [
            users[1].pk
        ]  # noqa


class TestAssignCollections:
    def test_assign(self, create_collection, user, api_client, nirveda, db):
        original_children = [
            create_collection(organization=nirveda),
            create_collection(organization=nirveda),
        ]
        parent = create_collection(organization=nirveda, collections=original_children)
        new_children = [
            create_collection(organization=nirveda),
            create_collection(organization=nirveda),
        ]
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/%s/assign/" % (nirveda.pk, parent.pk),
            data={"collections": [child.pk for child in new_children]},
        )
        assert response.status_code == 201
        # Make sure the children collections are in the response.
        assert len(response.json()["collections"]) == 4
        assert original_children[0].pk in [
            c["id"] for c in response.json()["collections"]
        ]
        assert original_children[1].pk in [
            c["id"] for c in response.json()["collections"]
        ]
        assert new_children[0].pk in [c["id"] for c in response.json()["collections"]]
        assert new_children[1].pk in [c["id"] for c in response.json()["collections"]]

        # Make sure changes persist to database.
        parent.refresh_from_db()
        assert parent.collections.count() == 4
        assert original_children[0].pk in [c.pk for c in parent.collections.all()]
        assert original_children[1].pk in [c.pk for c in parent.collections.all()]
        assert new_children[0].pk in [c.pk for c in parent.collections.all()]
        assert new_children[1].pk in [c.pk for c in parent.collections.all()]

    def test_assign_invalid_organization(
        self, create_collection, user, api_client, nirveda, apex, db
    ):
        original_children = [
            create_collection(organization=nirveda),
            create_collection(organization=nirveda),
        ]
        parent = create_collection(organization=nirveda, collections=original_children)
        new_children = [
            original_children[0],
            create_collection(organization=apex),
            create_collection(organization=nirveda),
        ]
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/%s/assign/" % (nirveda.pk, parent.pk),
            data={"collections": [child.pk for child in new_children]},
        )
        assert response.status_code == 400

    def test_assign_to(self, create_collection, user, api_client, nirveda, db):
        original_children = [
            create_collection(organization=nirveda),
            create_collection(organization=nirveda),
        ]
        parent = create_collection(organization=nirveda, collections=original_children)
        child = create_collection(
            organization=nirveda,
        )
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/%s/assign-to/" % (nirveda.pk, child.pk),
            data={"parents": [parent.pk]},
        )
        assert response.status_code == 201

        # Make sure changes persist to database.
        parent.refresh_from_db()
        assert parent.collections.count() == 3
        assert original_children[0].pk in [c.pk for c in parent.collections.all()]
        assert original_children[1].pk in [c.pk for c in parent.collections.all()]
        assert child.pk in [c.pk for c in parent.collections.all()]

    def test_assign_to_invalid_organization(
        self, create_collection, user, api_client, nirveda, apex, db
    ):
        original_children = [
            create_collection(organization=apex),
            create_collection(organization=apex),
        ]
        parent = create_collection(organization=apex, collections=original_children)
        child = create_collection(
            organization=nirveda,
        )
        api_client.force_login(user)
        response = api_client.post(
            "/v2/organizations/%s/collections/%s/assign-to/" % (nirveda.pk, child.pk),
            data={"parents": [parent.pk]},
        )
        assert response.status_code == 400


class TestUploadDocuments:
    def test_upload(
        self, create_collection, user, api_client, create_document, nirveda, db
    ):
        api_client.force_login(user)
        collection = create_collection(organization=nirveda)
        document = create_document(collections=[collection], organization=nirveda)
        with mock.patch(
            "backend.app.collection.serializers.create_and_process_document"
        ) as mocked:  # noqa
            mocked.return_value = {
                "put_url": "https://nirveda.com/put-url",
                "get_url": "https://nirveda.com/get-url",
                "headers": {"Content-Type": "application/json"},
                "document": document,
            }
            response = api_client.post(
                "/v2/organizations/%s/collections/%s/upload/"
                % (nirveda.pk, collection.pk),
                data={"filename": "testfile.pdf"},
            )
        assert response.status_code == 201

        assert "headers" in response.json()
        assert response.json()["headers"] == {"Content-Type": "application/json"}

        assert "put_signed_url" in response.json()
        assert (
            response.json()["put_signed_url"] == "https://nirveda.com/put-url"
        )  # noqa

        assert "get_signed_url" in response.json()
        assert (
            response.json()["get_signed_url"] == "https://nirveda.com/get-url"
        )  # noqa

        assert "document" in response.json()
        assert response.json()["document"]["id"] == document.pk

        assert mocked.called
        assert mocked.call_args[1] == {
            "filename": "testfile.pdf",
            "file_type": "pdf",
            "user": user,
            "collection": collection,
        }

    def test_upload_duplicate_name(
        self, create_collection, user, api_client, create_document, nirveda, db
    ):
        document = create_document(name="testfile.pdf", organization=nirveda)
        collection = create_collection(documents=[document], organization=nirveda)
        api_client.force_login(user)
        with mock.patch(
            "backend.app.collection.serializers.create_and_process_document"
        ) as mocked:  # noqa
            mocked.return_value = {
                "put_url": "https://nirveda.com/put-url",
                "get_url": "https://nirveda.com/get-url",
                "document": document,
                "headers": {"Content-Type": "application/json"},
            }
            response = api_client.post(
                "/v2/organizations/%s/collections/%s/upload/"
                % (nirveda.pk, collection.pk),
                data={"filename": "testfile.pdf"},
            )
        assert response.status_code == 201

        assert "headers" in response.json()
        assert response.json()["headers"] == {"Content-Type": "application/json"}

        assert "put_signed_url" in response.json()
        assert (
            response.json()["put_signed_url"] == "https://nirveda.com/put-url"
        )  # noqa

        assert "get_signed_url" in response.json()
        assert (
            response.json()["get_signed_url"] == "https://nirveda.com/get-url"
        )  # noqa

        assert "document" in response.json()
        assert response.json()["document"]["id"] == document.pk

        assert mocked.called
        assert mocked.call_args[1] == {
            "filename": "testfile(1).pdf",
            "file_type": "pdf",
            "user": user,
            "collection": collection,
        }

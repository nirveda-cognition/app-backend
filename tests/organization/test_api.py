import json

import pytest
import responses
from django.test import override_settings

from backend.app.organization.models import Organization
from backend.app.user.models import UserCategory
from backend.lib.utils.dateutils import api_datetime_string


def test_get_organization(nirveda, superuser, api_client, db):
    api_client.force_login(superuser)
    response = api_client.get("/v2/organizations/%s/" % nirveda.pk)
    assert response.status_code == 200
    assert response.json() == {
        "id": nirveda.pk,
        "name": nirveda.name,
        "slug": nirveda.slug,
        "description": nirveda.description,
        "num_users": 1,
        "num_groups": nirveda.num_groups,
        "json_info": {"document-extractor": ["invoice"]},
        "logo": None,
        "is_active": nirveda.is_active,
        "created_at": api_datetime_string(nirveda.created_at),
        "updated_at": api_datetime_string(nirveda.updated_at),
        "api_access": True,
        "api_key": None,
        "api_url": None,
        "callback_whitelist": None,
        "groups": [],
        "type": {
            "id": nirveda.type.pk,
            "title": nirveda.type.title,
            "slug": nirveda.type.slug,
            "description": nirveda.type.description,
        },
        "users": [
            {
                "id": superuser.pk,
                "first_name": superuser.first_name,
                "last_name": superuser.last_name,
                "full_name": superuser.full_name,
                "email": superuser.email,
                "category": UserCategory.SUPERUSER,
                "username": superuser.email,
                "is_active": True,
                "is_admin": False,
                "is_superuser": True,
                "is_staff": True,
            }
        ],
    }


def test_get_organization_requires_superuser(nirveda, user, api_client, db):
    api_client.force_login(user)
    response = api_client.get("/v2/organizations/%s/" % nirveda.pk)
    assert response.status_code == 403


@pytest.mark.freeze_time("2020-01-01")
@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_create_organization(nirveda, superuser, api_client, db):
    api_client.force_login(superuser)

    # Mock the request to AI Services to generate the new key.
    responses.add(
        method=responses.POST,
        url="https://test-ai-services.ai/v1/admin/api-key",
        json={"api_key": "abcd-efgh-ijkl"},
    )

    response = api_client.post(
        "/v2/organizations/",
        format="json",  # Required because of nested json_info.
        data={
            "name": "Test Organization",
            "slug": "test-organization",
            "api_url": "https://google.com",
            "description": "Test organization description.",
            "json_info": {"document-extractor": ["invoice"]},
            "type": nirveda.type.pk,
        },
    )

    organization = Organization.objects.filter(name="Test Organization").first()
    assert organization is not None

    assert response.status_code == 201
    assert response.json() == {
        "id": organization.pk,
        "name": "Test Organization",
        "slug": "test-organization",
        "description": "Test organization description.",
        "num_users": 0,
        "num_groups": 0,
        "json_info": {"document-extractor": ["invoice"]},
        "logo": None,
        "is_active": True,
        "created_at": "2020-01-01 00:00:00",
        "updated_at": "2020-01-01 00:00:00",
        "api_access": True,
        "api_key": "abcd-efgh-ijkl",
        "api_url": "https://google.com",
        "callback_whitelist": None,
        "users": [],
        "groups": [],
        "type": {
            "id": nirveda.type.pk,
            "title": nirveda.type.title,
            "slug": nirveda.type.slug,
            "description": nirveda.type.description,
        },
    }

    assert organization.name == "Test Organization"
    assert organization.description == "Test organization description."
    assert organization.api_url == "https://google.com"
    assert organization.json_info == {"document-extractor": ["invoice"]}


def test_create_organization_requires_superuser(user, api_client, db):
    api_client.force_login(user)
    response = api_client.post(
        "/v2/organizations/",
        data={
            "id": 5,
            "name": "Test Organization",
            "api_url": "https://google.com",
            "description": "Test organization description.",
            "slug": "test",
        },
    )
    assert response.status_code == 403


def test_create_organization_invalid_json_info(superuser, api_client, nirveda, db):
    api_client.force_login(superuser)
    response = api_client.post(
        "/v2/organizations/",
        format="json",  # Required because of nested json_info.
        data={
            "name": "Test Organization",
            "slug": "test-organization",
            "api_url": "https://google.com",
            "description": "Test organization description.",
            "json_info": {"document-extractor": "hoopla"},
            "type": nirveda.type.pk,
        },
    )
    assert response.status_code == 400


@pytest.mark.freeze_time("2020-01-01")
def test_update_organization(
    nirveda, superuser, api_client, generic_type, create_organization, db
):
    api_client.force_login(superuser)
    organization = create_organization(
        name="Original Name",
        api_url="https://google.com",
        description="Original description.",
        json_info={"document-extractor": ["invoice"]},
        type=generic_type,
    )
    response = api_client.patch(
        "/v2/organizations/%s/" % organization.pk,
        format="json",
        data={
            "name": "New Name",
            "json_info": {"document-extractor": ["change_order"]},
            "description": "New description.",
            "api_url": "https://nirveda.com/v1/",
        },
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": organization.pk,
        "name": "New Name",
        "description": "New description.",
        "slug": "original-name",
        "num_users": 0,
        "num_groups": 0,
        "json_info": {"document-extractor": ["change_order"]},
        "logo": organization.logo,
        "is_active": organization.is_active,
        "created_at": "2020-01-01 00:00:00",
        "updated_at": "2020-01-01 00:00:00",
        "api_access": True,
        "api_key": organization.api_key,
        "api_url": "https://nirveda.com/v1/",
        "callback_whitelist": None,
        "users": [],
        "groups": [],
        "type": {
            "id": generic_type.pk,
            "title": generic_type.title,
            "slug": generic_type.slug,
            "description": generic_type.description,
        },
    }

    # Make sure changes persist to database.
    organization.refresh_from_db()
    assert organization.name == "New Name"
    assert organization.description == "New description."
    assert organization.api_url == "https://nirveda.com/v1/"
    assert organization.json_info == {"document-extractor": ["change_order"]}


def test_update_organization_invalid_json_info(
    nirveda, superuser, api_client, generic_type, create_organization, db
):
    api_client.force_login(superuser)
    organization = create_organization(
        name="Original Name",
        api_url="https://google.com",
        description="Original description.",
        json_info={"document-extractor": ["invoice"]},
        type=generic_type,
    )
    response = api_client.patch(
        "/v2/organizations/%s/" % organization.pk,
        format="json",
        data={
            "name": "New Name",
            "json_info": {"invalid-service": ["change_order"]},
            "description": "New description.",
            "api_url": "https://nirveda.com/v1/",
        },
    )
    assert response.status_code == 400


def test_update_organization_requires_superuser(
    user, api_client, create_organization, generic_type, db
):
    api_client.force_login(user)
    organization = create_organization(
        name="Original Name",
        api_url="https://google.com",
        description="Original description.",
        json_info={"document-extractor": ["invoice"]},
        type=generic_type,
    )
    response = api_client.patch(
        "/v2/organizations/%s/" % organization.pk,
        data={
            "name": "New Name",
            "json_info": json.dumps({"document-extractor": ["change_order"]}),
            "description": "New description.",
            "api_url": "https://nirveda.com/v1/",
        },
    )
    assert response.status_code == 403


def test_get_organizations(nirveda, alfa, apex, superuser, api_client, db):
    api_client.force_login(superuser)
    response = api_client.get("/v2/organizations/")
    assert response.status_code == 200
    assert response.json()["data"] == [
        {
            "id": alfa.pk,
            "name": alfa.name,
            "slug": alfa.slug,
            "description": alfa.description,
            "num_users": 0,
            "num_groups": 0,
            "json_info": {"document-extractor": ["alfa"]},
            "logo": None,
            "is_active": alfa.is_active,
            "created_at": api_datetime_string(alfa.created_at),
            "updated_at": api_datetime_string(alfa.updated_at),
            "api_access": True,
            "api_key": None,
            "api_url": None,
            "callback_whitelist": None,
            "users": [],
            "groups": [],
            "type": {
                "id": alfa.type.pk,
                "title": alfa.type.title,
                "slug": alfa.type.slug,
                "description": alfa.type.description,
            },
        },
        {
            "id": apex.pk,
            "name": apex.name,
            "slug": apex.slug,
            "description": apex.description,
            "num_users": 0,
            "num_groups": 0,
            "json_info": {"document-extractor": ["apex"]},
            "logo": None,
            "is_active": apex.is_active,
            "created_at": api_datetime_string(apex.created_at),
            "updated_at": api_datetime_string(apex.updated_at),
            "api_access": True,
            "api_key": None,
            "api_url": None,
            "callback_whitelist": None,
            "users": [],
            "groups": [],
            "type": {
                "id": apex.type.pk,
                "title": apex.type.title,
                "slug": apex.type.slug,
                "description": apex.type.description,
            },
        },
        {
            "id": nirveda.pk,
            "name": nirveda.name,
            "slug": nirveda.slug,
            "description": nirveda.description,
            "num_users": 1,
            "num_groups": 0,
            "json_info": {"document-extractor": ["invoice"]},
            "logo": None,
            "is_active": nirveda.is_active,
            "created_at": api_datetime_string(nirveda.created_at),
            "updated_at": api_datetime_string(nirveda.updated_at),
            "api_access": True,
            "api_key": None,
            "api_url": None,
            "callback_whitelist": None,
            "groups": [],
            "type": {
                "id": nirveda.type.pk,
                "title": nirveda.type.title,
                "slug": nirveda.type.slug,
                "description": nirveda.type.description,
            },
            "users": [
                {
                    "id": superuser.pk,
                    "first_name": superuser.first_name,
                    "last_name": superuser.last_name,
                    "full_name": superuser.full_name,
                    "email": superuser.email,
                    "category": UserCategory.SUPERUSER,
                    "username": superuser.email,
                    "is_active": True,
                    "is_admin": False,
                    "is_superuser": True,
                    "is_staff": True,
                }
            ],
        },
    ]


def test_get_organizations_requires_superuser(nirveda, user, api_client, db):
    api_client.force_login(user)
    response = api_client.get("/v2/organizations/")
    assert response.status_code == 403


def test_get_organization_types(
    superuser, api_client, alfa_type, apex_type, generic_type
):
    api_client.force_login(superuser)
    response = api_client.get("/v2/organizations/types/")
    assert response.status_code == 200
    assert response.json()["count"] == 3
    assert response.json()["data"] == [
        {
            "id": alfa_type.pk,
            "slug": alfa_type.slug,
            "title": alfa_type.title,
            "description": alfa_type.description,
        },
        {
            "id": apex_type.pk,
            "slug": apex_type.slug,
            "title": apex_type.title,
            "description": apex_type.description,
        },
        {
            "id": generic_type.pk,
            "slug": generic_type.slug,
            "title": generic_type.title,
            "description": generic_type.description,
        },
    ]


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_generate_api_key(api_client, create_organization, generic_type, superuser, db):
    # Mock the request to AI Services to generate the key and revoke the
    # existing key.
    responses.add(
        method=responses.POST,
        url="https://test-ai-services.ai/v1/admin/api-key",
        json={"api_key": "abcd-efgh-ijkl"},
    )
    responses.add(
        method=responses.DELETE,
        url="https://test-ai-services.ai/v1/admin/api-key/api-key-being-revoked",  # noqa
    )

    # If the organization's API Key is not None, a DELETE request will be issued
    # to AI Services to revoke the existing key.
    organization = create_organization(
        api_key="api-key-being-revoked",
        json_info={"document-extractor": ["capability 1", "capability 2"]},
        type=generic_type,
    )

    api_client.force_login(superuser)

    response = api_client.patch(
        "/v2/organizations/%s/generate-api-key/" % organization.pk
    )

    assert response.status_code == 201
    assert response.json()["api_key"] == "abcd-efgh-ijkl"

    # Make sure changes persist to database.
    organization.refresh_from_db()
    assert organization.api_key == "abcd-efgh-ijkl"

    # Make sure a request to AI Services was issued to generate the new API Key.
    assert len(responses.calls) == 2
    assert json.loads(responses.calls[0].request.body) == {
        "tenant_id": organization.tenant_id,
        "service": "document-extractor",
        "capabilities": ["capability_1", "capability_2"],
    }
    assert "X-API-Key" in responses.calls[0].request.headers
    assert (
        responses.calls[0].request.headers["X-API-Key"] == "test-ai-services-api-key"
    )  # noqa
    assert "request-type" in responses.calls[0].request.headers
    assert responses.calls[0].request.headers["request-type"] == "ai-services"

    # Make sure a request to AI Services was issued to delete the old API Key.
    assert responses.calls[1].request.method == "DELETE"
    assert "X-API-Key" in responses.calls[1].request.headers
    assert (
        responses.calls[1].request.headers["X-API-Key"] == "test-ai-services-api-key"
    )  # noqa
    assert "request-type" in responses.calls[1].request.headers
    assert responses.calls[1].request.headers["request-type"] == "ai-services"


@responses.activate
@override_settings(
    AI_SERVICES_API_URL="https://test-ai-services.ai/v1",
    AI_SERVICES_API_KEY="test-ai-services-api-key",
)
def test_revoke_api_key(api_client, create_organization, superuser, generic_type, db):
    # Mock the request to AI Services to revoke the existing key.
    responses.add(
        method=responses.DELETE,
        url="https://test-ai-services.ai/v1/admin/api-key/api-key-being-revoked",  # noqa
    )

    organization = create_organization(
        api_key="api-key-being-revoked",
        json_info={"document-extractor": ["capability 1", "capability 2"]},
        type=generic_type,
    )

    api_client.force_login(superuser)

    response = api_client.patch(
        "/v2/organizations/%s/revoke-api-key/" % organization.pk
    )
    assert response.status_code == 201
    assert response.json()["api_key"] is None

    # Make sure changes persist to database.
    organization.refresh_from_db()
    assert organization.api_key is None

    # Make sure a request to AI Services was issued to delete the old API Key.
    assert len(responses.calls) == 1
    assert responses.calls[0].request.method == "DELETE"
    assert "X-API-Key" in responses.calls[0].request.headers
    assert (
        responses.calls[0].request.headers["X-API-Key"] == "test-ai-services-api-key"
    )  # noqa
    assert "request-type" in responses.calls[0].request.headers
    assert responses.calls[0].request.headers["request-type"] == "ai-services"

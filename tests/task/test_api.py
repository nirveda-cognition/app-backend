import datetime

import pytest
import pytz

from backend.lib.utils.dateutils import api_datetime_string


class TestOrganizationTasks:
    @pytest.mark.freeze_time("2020-01-01")
    def test_get_task(self, user, create_task, nirveda, db, api_client):
        task = create_task(organization=nirveda)
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/tasks/%s/" % (nirveda.pk, task.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": task.pk,
            "uuid": task.uuid,
            "ai_uuid": task.ai_uuid,
            "organization": nirveda.pk,
            "request": {},
            "client_request": {},
            "response": {},
            "backend_id": task.backend_id,
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
            "results": [],
        }

    @pytest.mark.freeze_time("2020-01-01")
    def test_get_tasks(self, user, create_task, nirveda, apex, db, api_client):
        tasks = [
            create_task(organization=nirveda),
            create_task(organization=nirveda),
            create_task(organization=apex),
        ]
        api_client.force_login(user)
        response = api_client.get("/v2/organizations/%s/tasks/" % nirveda.pk)
        assert response.status_code == 200
        assert response.json()["count"] == 2
        assert response.json()["data"] == [
            {
                "id": tasks[1].pk,
                "uuid": tasks[1].uuid,
                "ai_uuid": tasks[1].ai_uuid,
                "organization": nirveda.pk,
                "request": {},
                "client_request": {},
                "response": {},
                "backend_id": tasks[1].backend_id,
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
                "results": [],
            },
            {
                "id": tasks[0].pk,
                "uuid": tasks[0].uuid,
                "ai_uuid": tasks[0].ai_uuid,
                "organization": nirveda.pk,
                "request": {},
                "client_request": {},
                "response": {},
                "backend_id": tasks[0].backend_id,
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
                "results": [],
            },
        ]

    @pytest.mark.freeze_time("2020-01-01")
    def test_get_all_task_result(
        self,
        user,
        create_task,
        create_task_result,
        nirveda,
        create_document,
        db,
        api_client,
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        result = create_task_result(task=task, document=document)

        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/tasks/results/%s/" % (nirveda.pk, result.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": result.pk,
            "uuid": result.uuid,
            "data": {},
            "new_data": {},
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
        }

    @pytest.mark.freeze_time("2020-01-01")
    def test_get_all_task_results(
        self,
        user,
        create_task,
        create_task_result,
        nirveda,
        db,
        create_document,
        api_client,
    ):
        tasks = [create_task(organization=nirveda), create_task(organization=nirveda)]
        documents = [
            create_document(task=tasks[0], organization=nirveda),
            create_document(task=tasks[1], organization=nirveda),
        ]
        results = [
            create_task_result(task=tasks[0], document=documents[0]),
            create_task_result(task=tasks[1], document=documents[1]),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/tasks/results/" % nirveda.pk,
        )
        assert response.status_code == 200
        assert response.json()["count"] == 2
        assert response.json()["data"] == [
            {
                "id": results[1].pk,
                "uuid": results[1].uuid,
                "data": {},
                "new_data": {},
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
            },
            {
                "id": results[0].pk,
                "uuid": results[0].uuid,
                "data": {},
                "new_data": {},
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
            },
        ]

    @pytest.mark.freeze_time("2020-01-01")
    def test_get_task_task_result(
        self,
        user,
        create_task,
        create_task_result,
        nirveda,
        create_document,
        db,
        api_client,
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        result = create_task_result(task=task, document=document)

        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/tasks/%s/results/%s/"
            % (nirveda.pk, task.pk, result.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": result.pk,
            "uuid": result.uuid,
            "data": {},
            "new_data": {},
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
        }

    def test_get_task_latest_task_result(
        self,
        user,
        create_task,
        create_task_result,
        create_document,
        nirveda,
        db,
        api_client,
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        results = [
            create_task_result(
                task=task,
                document=document,
                created_at=datetime.datetime(2020, 1, 1, tzinfo=pytz.UTC),
            ),
            create_task_result(
                task=task,
                document=document,
                created_at=datetime.datetime(2020, 3, 1, tzinfo=pytz.UTC),
            ),
            create_task_result(
                task=task,
                document=document,
                created_at=datetime.datetime(2020, 2, 1, tzinfo=pytz.UTC),
            ),
        ]

        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/tasks/%s/results/latest/" % (nirveda.pk, task.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": results[1].pk,
            "uuid": results[1].uuid,
            "data": {},
            "new_data": {},
            "created_at": api_datetime_string(results[1].created_at),
            "updated_at": api_datetime_string(results[1].updated_at),
        }

    @pytest.mark.freeze_time("2020-01-01")
    def test_get_task_task_results(
        self,
        user,
        create_task,
        create_task_result,
        nirveda,
        create_document,
        db,
        api_client,
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        results = [
            create_task_result(task=task, document=document),
            create_task_result(task=task, document=document),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/tasks/%s/results/" % (nirveda.pk, task.pk)
        )
        assert response.status_code == 200
        assert response.json()["count"] == 2
        assert response.json()["data"] == [
            {
                "id": results[1].pk,
                "uuid": results[1].uuid,
                "data": {},
                "new_data": {},
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
            },
            {
                "id": results[0].pk,
                "uuid": results[0].uuid,
                "data": {},
                "new_data": {},
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
            },
        ]

    @pytest.mark.freeze_time("2020-01-01")
    def test_update_apex_task_result(
        self,
        create_user,
        create_task,
        create_task_result,
        create_document,
        apex,
        db,
        api_client,
    ):
        user = create_user(organization=apex)
        task = create_task(organization=apex)
        document = create_document(task=task, organization=apex)
        result = create_task_result(
            task=task,
            document=document,
            new_data={
                "data": {
                    "extracted_data": [
                        {
                            "items": [
                                {
                                    "property_id": "document_type",
                                    "value": "invoice",
                                    "label": "Document Type",
                                },
                                {
                                    "property_id": "change_order_no",
                                    "value": 9,
                                    "label": "Change Order No.",
                                },
                            ]
                        }
                    ]
                }
            },
        )

        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/tasks/%s/results/%s/" % (apex.pk, task.pk, result.pk),
            data={
                "doc_type": "invoice",
                "field": "change_order_no",
                "value": 10,
            },
        )
        assert response.json() == {
            "id": result.pk,
            "uuid": result.uuid,
            "data": {},
            "new_data": {
                "data": {
                    "extracted_data": [
                        {
                            "items": [
                                {
                                    "property_id": "document_type",
                                    "value": "invoice",
                                    "label": "Document Type",
                                },
                                {
                                    "property_id": "change_order_no",
                                    # String because of JSONField
                                    "value": "10",
                                    "label": "Change Order No.",
                                },
                            ]
                        }
                    ]
                }
            },
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
        }


class TestDocumentTasks:
    @pytest.mark.freeze_time("2020-01-01")
    def test_get_task(
        self, user, create_task, create_document, nirveda, db, api_client
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/documents/%s/task/" % (nirveda.pk, document.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": task.pk,
            "uuid": task.uuid,
            "ai_uuid": task.ai_uuid,
            "organization": nirveda.pk,
            "request": {},
            "client_request": {},
            "response": {},
            "backend_id": task.backend_id,
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
            "results": [],
        }

    @pytest.mark.freeze_time("2020-01-01")
    def test_get_task_results(
        self,
        user,
        create_task,
        create_task_result,
        nirveda,
        db,
        create_document,
        api_client,
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        results = [
            create_task_result(task=task, document=document),
            create_task_result(task=task, document=document),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/documents/%s/task/results/"
            % (nirveda.pk, document.pk)
        )
        assert response.status_code == 200
        assert response.json()["count"] == 2
        assert response.json()["data"] == [
            {
                "id": results[1].pk,
                "uuid": results[1].uuid,
                "data": {},
                "new_data": {},
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
            },
            {
                "id": results[0].pk,
                "uuid": results[0].uuid,
                "data": {},
                "new_data": {},
                "created_at": "2020-01-01 00:00:00",
                "updated_at": "2020-01-01 00:00:00",
            },
        ]

    @pytest.mark.freeze_time("2020-01-01")
    def test_get_task_result(
        self,
        user,
        create_task,
        create_task_result,
        nirveda,
        create_document,
        db,
        api_client,
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        result = create_task_result(task=task, document=document)

        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/documents/%s/task/results/%s/"
            % (nirveda.pk, document.pk, result.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": result.pk,
            "uuid": result.uuid,
            "data": {},
            "new_data": {},
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
        }

    def test_get_latest_task_result(
        self,
        user,
        create_task,
        create_task_result,
        create_document,
        nirveda,
        db,
        api_client,
    ):
        task = create_task(organization=nirveda)
        document = create_document(task=task, organization=nirveda)
        results = [
            create_task_result(
                task=task,
                document=document,
                created_at=datetime.datetime(2020, 1, 1, tzinfo=pytz.UTC),
            ),
            create_task_result(
                task=task,
                document=document,
                created_at=datetime.datetime(2020, 3, 1, tzinfo=pytz.UTC),
            ),
            create_task_result(
                task=task,
                document=document,
                created_at=datetime.datetime(2020, 2, 1, tzinfo=pytz.UTC),
            ),
        ]
        api_client.force_login(user)
        response = api_client.get(
            "/v2/organizations/%s/documents/%s/task/results/latest/"
            % (nirveda.pk, document.pk)
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": results[1].pk,
            "uuid": results[1].uuid,
            "data": {},
            "new_data": {},
            "created_at": api_datetime_string(results[1].created_at),
            "updated_at": api_datetime_string(results[1].updated_at),
        }

    @pytest.mark.freeze_time("2020-01-01")
    def test_update_apex_task_result(
        self,
        create_user,
        create_task,
        create_task_result,
        create_document,
        apex,
        db,
        api_client,
    ):
        user = create_user(organization=apex)
        task = create_task(organization=apex)
        document = create_document(task=task, organization=apex)
        result = create_task_result(
            task=task,
            document=document,
            new_data={
                "data": {
                    "extracted_data": [
                        {
                            "items": [
                                {
                                    "property_id": "document_type",
                                    "value": "invoice",
                                    "label": "Document Type",
                                },
                                {
                                    "property_id": "change_order_no",
                                    "value": 9,
                                    "label": "Change Order No.",
                                },
                            ]
                        }
                    ]
                }
            },
        )

        api_client.force_login(user)
        response = api_client.patch(
            "/v2/organizations/%s/documents/%s/task/results/%s/"
            % (apex.pk, document.pk, result.pk),
            data={
                "doc_type": "invoice",
                "field": "change_order_no",
                "value": 10,
            },
        )
        assert response.json() == {
            "id": result.pk,
            "uuid": result.uuid,
            "data": {},
            "new_data": {
                "data": {
                    "extracted_data": [
                        {
                            "items": [
                                {
                                    "property_id": "document_type",
                                    "value": "invoice",
                                    "label": "Document Type",
                                },
                                {
                                    "property_id": "change_order_no",
                                    # String because of JSONField
                                    "value": "10",
                                    "label": "Change Order No.",
                                },
                            ]
                        }
                    ]
                }
            },
            "created_at": "2020-01-01 00:00:00",
            "updated_at": "2020-01-01 00:00:00",
        }

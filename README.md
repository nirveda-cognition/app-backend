# App Backend - Nirveda Cognition

## System Requirements

- Docker
- pyenv
- brew
- Python3
- MacOSX/Linux/Windows 10+
- poetry

## Getting Started

### Step 1: Repository

Clone this repository locally and `cd` into the directory.

```bash
git clone git clone https://<user>@bitbucket.org/nirveda-cognition/app-backend.git
```

### Step 2: Deploy Docker containers

Bring up `docker` and run migrations:

```bash
docker-compose -f {org.env.yml} up --build
docker-compose -f {org.env.yml} run --rm django python manage.py migrate
```

Load all of the fixtures manually

```bash
docker-compose -f {org.env.yml} run --rm django python manage.py loaddata types
docker-compose -f {org.env.yml} run --rm django python manage.py loaddata organizations
docker-compose -f {org.env.yml} run --rm django python manage.py loaddata roles
docker-compose -f {org.env.yml} run --rm django python manage.py loaddata documentstatuses
```

Create a superuser:

```bash
docker-compose -f {org.env.yml} run --rm python manage.py createsuperuser
```

### Testing

See the ReadMe in `backend/testing/ReadMe.md`.

#### Tox Testing

We use `tox` to automate our testing, linting and code coverage processes. `tox` was a project that began
as a way to run tests across Python versions, Django versions and other package versions - but has extended
itself to much more.

Our `tox` setup can be very useful for local development. One benefit of `tox` is that it completely isolates
it's own cached virtual environments for purposes of running tests and producing coverage reports. The following
commands can all be run outside of a virtual environment:

Run the complete `pytest` suite and generate coverage reports in `/reports`:

```bash
tox
```

or

```bash
tox -e test
```

Clean all build and test directories as well as extraneous artifacts:

```bash
tox -e clean
```

Run `flake8` linting checks and generate linting reports in `/reports`:

```bash
tox -e lint
```

Running these commands will usually be slow the first time, just because `tox` has to setup the cached
environment - but after that, they all run very quickly and can be very useful.

## Troubleshooting

- If you are noticing an issue uploading documents in the frontend UI, it might
  be related to the organization that your user belongs to not being setup
  properly. Go into the Django Admin and local the organization that your
  user belongs to (it should be "Nirveda"). Make sure that your organization
  has an API Key assigned to it and the field `json_info` is populated.

## Steps to Deploy

1. `docker-compose -f {org.env.yml} build`
2. `docker-compose -f {org.env.yml} run --rm django python manage.py migrate`
3. `docker-compose -f {org.env.yml} run --rm django python manage.py loaddata types organizations roles documentstatuses`
4. `docker-compose -f {org.env.yml} run --rm django python manage.py createsuperuser`
5. `docker-compose -f {org.env.yml} up`

To shutdown containers, run

`docker-compose -f {org.env.yml} down`

To run build & up together, run

`docker-compose -f {org.env.yml} up --build`

`docker save $(docker images --format '{{.Repository}}:{{.Tag}}') -o allinone.tar`

`docker stop $(docker ps -q)`

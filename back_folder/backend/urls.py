from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.http import JsonResponse
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from two_factor.urls import urlpatterns as tf_urls

from backend.app.authentication.views import TFLoginView
from backend.app.common.views import AdminSiteOTPRequiredMixinRedirSetup

admin.site.site_header = "Nirveda Cognitive Platform"
admin.site.site_title = "Nirveda Cognitive Platform"
admin.site.index_title = "Nirveda Cognitive Platform"
admin.site.__class__ = AdminSiteOTPRequiredMixinRedirSetup


def empty_view(request):
    return JsonResponse({"status": "200"})


urlpatterns = [
    path("nv_admin/", admin.site.urls),
    url(
        regex=r"^account/login/$",
        view=TFLoginView.as_view(),
        name="login",
    ),
    path(
        "v1/",
        include(
            [
                path("", include("backend.app.nc_core.urls")),
            ]
        ),
    ),
    path("v2/", include("backend.app.urls")),
    path("", include(tf_urls)),
    path("", empty_view),
    re_path(r"^hcheck/", include("health_check.urls")),
]


if settings.DEBUG:
    schema_view = get_schema_view(
        openapi.Info(
            title="Nirveda Cognitive Platform API",
            default_version="1.0.0",
            description="Description",
            host=settings.APP_URL,
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
    )

    urlpatterns += [
        url(
            r"^swagger(?P<format>\.json|\.yaml)$",
            schema_view.without_ui(cache_timeout=0),
            name="schema-json",
        ),
        url(
            r"^redoc/$",
            schema_view.with_ui("redoc", cache_timeout=0),
            name="schema-redoc",
        ),
    ]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

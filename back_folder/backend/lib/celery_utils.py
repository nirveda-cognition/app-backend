import functools
import logging

import celery
from django.db import IntegrityError, transaction


def log_errors_base(logger_name="celery.task"):
    """
    Naturally, Celery asynchronous background tasks will suppress exceptions
    and exit the task without providing any feedback about the exception.  In
    order to prevent that from happening, we provide Celery tasks with a base
    :obj:`celery.Task` class that can be used to hook into the exception handler
    and implement our own logging.
    """
    logger = logging.getLogger(logger_name)

    class LoggingTask(celery.Task):
        def on_failure(self, exc, task_id, args, kwargs, einfo):
            logger.exception("Uncaught exception in task %s." % task_id, exc_info=exc)
            super(LoggingTask, self).on_failure(exc, task_id, args, kwargs, einfo)

    return LoggingTask


class retriable_task:
    """
    A Celery task that will retry on errors, most notably database transactional
    errors.

    The task also provides defaults and will implement :obj:`LoggingTask` which
    is responsible for logging errors that occur in the background task threads.

    The :obj:`transactional_task` works by wrapping Celery's
    :obj:`celery.shared_task`.

    Parameters:
    ----------
    logger_name: :obj:`str` (optional)
        The name of the logger that will be used to log exceptions.

        Default: celery.task

    retry_on: :obj:`type` or :obj:`list` or :obj:`tuple` (optional)
        Either a single :obj:`Exception` class or an iterable of
        :obj:`Exception` classes that will trigger the task retry.

        Default: :obj:`django.db.IntegrityError`

    countdown: :obj:`int` (optional)
        The countdown to be used when the task is retried, in the case of an
        exception.

        Default: 5

    serializer: :obj:`str` (optional)
        The task serializer.

        Default: json

    base: :obj:`celery.Task` (optional)
        The base task to inherit from.

        Default: :obj:`LoggingTask`.
    """

    def __init__(self, *args, **kwargs):
        self._logger_name = kwargs.pop("logger_name", "celery.task")
        self._countdown = kwargs.pop("countdown", 5)

        # TODO: Include HTTP errors from AI Services.
        self._retry_on = kwargs.pop("retry_on", IntegrityError)

        kwargs.setdefault("serializer", "json")
        kwargs.setdefault("base", log_errors_base(logger_name=self._logger_name))

        self.task_args = args
        self.task_kwargs = kwargs

    def __call__(self, func):
        logger = logging.getLogger(self._logger_name)

        @functools.wraps(func)
        def wrapper_func(*args, **kwargs):
            try:
                with transaction.atomic():
                    return func(*args, **kwargs)
            except self._retry_on as exc:
                logger.exception("Error in task - retrying.", exc_info=exc)
                raise task_func.retry(exc=exc, countdown=self._countdown)

        task_func = celery.shared_task(*self.task_args, **self.task_kwargs)(
            wrapper_func
        )
        return task_func

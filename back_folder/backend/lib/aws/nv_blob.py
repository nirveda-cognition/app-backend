import copy
import logging
from datetime import datetime, timedelta

from azure.storage.blob import (
    AccountSasPermissions,
    BlobServiceClient,
    ResourceTypes,
    generate_account_sas,
)
from django.conf import settings

logger = logging.getLogger("backend")


class NVBlob(object):
    """
    Class for managing files within an azure storage container.
    """

    def __init__(self):
        self.container = settings.AZURE_CONTAINER_NAME
        self.endpoint_suffix = settings.AZURE_ENDPOINT_SUFFIX
        self.storage_account_name = settings.AZURE_STORAGE_ACCOUNT_NAME
        self.blob_service_client = BlobServiceClient.from_connection_string(
            "DefaultEndpointsProtocol=https;"
            + "AccountName="
            + self.storage_account_name
            + ";"
            + "AccountKey="
            + settings.AZURE_ACCOUNT_KEY
            + ";"
            + "EndpointSuffix="
            + self.endpoint_suffix
        )

        self.sas_token_args = {
            "account_name": settings.AZURE_STORAGE_ACCOUNT_NAME,
            "account_key": settings.AZURE_ACCOUNT_KEY,
            "resource_types": ResourceTypes(object=True),
        }

    def generate_presigned_url(self, container, key, expiration, account_permissions):
        # TODO: Why are we wrapping this ENTIRE block in a try/except?  This is
        # super dangerous.  If there is an exception, we should allow the
        # calling logic to handle it, not handle it inside the method.
        try:
            blobname = key
            sas_token_args = copy.deepcopy(self.sas_token_args)
            sas_token_args.update(
                expiry=datetime.utcnow() + timedelta(seconds=expiration),
                permission=AccountSasPermissions(**account_permissions),
            )
            sas_token = generate_account_sas(**sas_token_args)
            # TODO: We should create and use URL utilities for things like this.
            # At least we should use os.path.join().
            presigned_url = (
                "https://"
                + self.storage_account_name
                + ".blob."
                + self.endpoint_suffix
                + "/"
                + container
                + "/"
                + blobname
                + "?"
                + sas_token
            )

        except Exception as e:
            logger.error("generate_presigned_url failed")
            logger.exception(e)
            return None

        return presigned_url

    def generate_put_presigned_url(
        self, container, key, expiration=1800, content_type=None
    ):
        presigned_put_url = self.generate_presigned_url(
            container,
            key,
            expiration,
            {"write": True, "list": True, "update": True, "read": True},
        )
        logger.info("presigned url: " + presigned_put_url)
        return presigned_put_url

    def generate_get_presigned_url(self, container, key, filename="", expiration=1800):
        return self.generate_presigned_url(container, key, expiration, {"read": True})

    def generate_head_presigned_url(self, container, key, expiration=1800):
        return self.generate_get_presigned_url(container, key, expiration=expiration)

    def file_fetch(self, bucket, obj_name):
        logger.info("file_fetch called")
        # TODO: We should catch the expected exceptions instead of a blind
        # exception catch.
        # TODO: We should probably let the calling method catch the exception
        # and handle it appropriately - the exception being thrown if there
        # is an error should be the default.
        blob_client = self.blob_service_client.get_blob_client(
            container=bucket, blob=obj_name
        )
        try:
            fetch_obj = blob_client.download_blob()
        except Exception as e:
            logger.error(
                "Failed to retrieve the document %s from the Azure container."
                % obj_name
            )
            logger.exception(e)
            return None
        else:
            return fetch_obj.readall()

    def file_delete(self, bucket, obj_name):
        logger.info("file_delete called")
        blob_client = self.blob_service_client.get_blob_client(
            container=bucket, blob=obj_name
        )
        # TODO: We should catch the expected exceptions instead of a blind
        # exception catch.
        # TODO: We should probably let the calling method catch the exception
        # and handle it appropriately - the exception being thrown if there
        # is an error should be the default.
        try:
            blob_client.delete_blob()
        except Exception as e:
            logger.error("Failed to delete %s from Azure container." % obj_name)
            logger.exception(e)
            return None

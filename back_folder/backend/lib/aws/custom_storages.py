from django.conf import settings
from storages.backends.s3boto3 import S3Boto3Storage

from .utils import Boto3Session


class StaticStorage(S3Boto3Storage):
    location = settings.STATICFILES_LOCATION
    querystring_auth = False
    gzip = True

    @property
    def connection(self):
        connection = getattr(self._connections, "connection", None)
        if connection is None:
            if settings.AWS_ROLE_ARN is None:
                self._connections.connection = super().connection
            else:
                session = Boto3Session(
                    role_arn=settings.AWS_ROLE_ARN,
                    access_key_id=settings.AWS_ACCESS_KEY_ID,
                    secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                    region=settings.AWS_REGION,
                )
                self._connections.connection = session.resource(
                    "s3",
                    aws_access_key_id=session.access_key_id,
                    aws_secret_access_key=session.secret_access_key,
                    aws_session_token=session.session_token,
                    region_name=self.region_name,
                    use_ssl=self.use_ssl,
                    endpoint_url=self.endpoint_url,
                    config=self.config,
                    verify=self.verify,
                )

        return self._connections.connection

from botocore.exceptions import BotoCoreError, ClientError
from django.conf import settings
from django.core.mail.backends.base import BaseEmailBackend
from django.core.mail.message import sanitize_address
from django.dispatch import Signal

from .utils import get_boto_client

pre_send = Signal(providing_args=["message"])
post_send = Signal(providing_args=["message", "message_id"])


class CustomEmailBackend(BaseEmailBackend):
    """
    An email backend for use with Amazon SES.

    Args:
        fail_silently: Flag that determines whether Amazon SES
            client errors should throw an exception.
    """

    def __init__(
        self,
        fail_silently=False,
        aws_access_key_id=None,
        aws_secret_access_key=None,
        **kwargs
    ):
        super(CustomEmailBackend, self).__init__(fail_silently=fail_silently)
        self.conn = get_boto_client(
            "ses",
            role_arn=settings.AWS_ROLE_ARN,
            access_key_id=aws_access_key_id or settings.AWS_ACCESS_KEY_ID,
            secret_access_key=(aws_secret_access_key or settings.AWS_SECRET_ACCESS_KEY),
            region=settings.AWS_SES_REGION,
        )

    def send_messages(self, email_messages):
        """
        Sends one or more EmailMessage objects and returns the
        number of email messages sent.

        Args:
            email_messages: A list of Django EmailMessage objects.
        Returns:
            An integer count of the messages sent.
        Raises:
            ClientError: An interaction with the Amazon SES HTTP API failed.
        """
        if not email_messages:
            return

        sent_message_count = 0
        for email_message in email_messages:
            if self._send(email_message):
                sent_message_count += 1
        return sent_message_count

    def _send(self, email_message):
        """
        Sends an individual message via the Amazon SES HTTP API.

        Args:
            email_message: A single Django EmailMessage object.
        Returns:
            True if the EmailMessage was sent successfully, otherwise False.
        Raises:
            ClientError: An interaction with the Amazon SES HTTP API failed.
        """
        pre_send.send(self.__class__, message=email_message)

        if not email_message.recipients():
            return False

        from_email = sanitize_address(email_message.from_email, email_message.encoding)
        recipients = [
            sanitize_address(addr, email_message.encoding)
            for addr in email_message.recipients()
        ]
        message = email_message.message().as_bytes(linesep="\r\n")

        # TODO: Which part of the block is the catch related to?  We don't want
        # to wrap a single catch in a block with multiple failing lines, so we
        # should isolate the block that this catch is intended for and rewrite
        # it so that the isolated line is in the try block.
        try:
            result = self.conn.send_raw_email(
                Source=from_email, Destinations=recipients, RawMessage={"Data": message}
            )
            post_send.send(
                self.__class__, message=email_message, message_id=result["MessageId"]
            )
        except (ClientError, BotoCoreError):
            if not self.fail_silently:
                raise
            return False
        return True

from django.apps import apps


def find_model_by_name(name):
    """
    Finds a model by name in the case that we do not know what app it belongs
    to or only have the name to work with.

    Parameters:
    ----------
    name: :obj:`str`
        The string name of the model class.

    NOTE:
    ----
    Right now, this method is only needed because we are not using the
    models.ContentTypeField on applicable models.  Once that change is made,
    this method will no longer be needed and show
    """
    # This is a bug that occurs because we have a TaskResult model, but the
    # Django installed app django-celery-results also has a TaskResult model.
    if name == "TaskResult":
        return apps.get_model("task", "TaskResult")
    try:
        return [model for model in apps.get_models() if model.__name__ == name][0]
    except IndexError:
        raise LookupError("The provided app name %s does not exist." % name)

from django.contrib import admin

from .models import NCGroup, ResetUID, Role, Token


class NCGroupAdmin(admin.ModelAdmin):
    list_per_page = 500
    ordering = ("id",)
    list_display = ("name", "description", "organization")


class RoleAdmin(admin.ModelAdmin):
    list_per_page = 500
    ordering = ("id",)
    list_display = ("name", "description", "code")


class ResetUidAdmin(admin.ModelAdmin):
    list_per_page = 500
    ordering = ("id",)
    list_display = ("token", "used", "user", "created_at")


class TokenAdmin(admin.ModelAdmin):
    list_per_page = 500
    ordering = ("id",)


admin.site.register(NCGroup, NCGroupAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(ResetUID, ResetUidAdmin)
admin.site.register(Token, TokenAdmin)

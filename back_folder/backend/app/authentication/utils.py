import logging

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.html import strip_tags

from backend.app.custom_auth.models import CustomUser, SSOAccount
from backend.app.organization.models import Organization
from backend.lib.utils.urls import add_query_params_to_url

from .auth import AzureSSOAuthentication
from .ms_graph import get_user_profile

logger = logging.getLogger("backend.graphAPI")


def send_forgot_password_email(user, token):
    """
    Sends a reset password email to the provided user with the token embedded
    in the email.

    Parameters:
    ----------
    user: :obj:`backend.app.user.models.CustomUser`
        The user who submitted the password reset request.
    token: :obj:`str`
        The randomly generated token that will be used to verify the password
        recovery.
    """
    html_message = render_to_string(
        "email/forgot_password.html",
        {
            "PWD_RESET_LINK": add_query_params_to_url(
                settings.RESET_PWD_UI_LINK, token=token
            ),
            "from_email": settings.FROM_EMAIL,
            "EMAIL": user.email,
            "year": timezone.now().year,
            "brand": user.organization.brand,
            "NAME": "{0} {1}".format(user.first_name, user.last_name),
        },
    )
    mail = EmailMultiAlternatives(
        "Forgot Password", strip_tags(html_message), settings.FROM_EMAIL, [user.email]
    )
    mail.attach_alternative(html_message, "text/html")

    if settings.EMAIL_ENABLED:
        mail.send()


def create_ncp_sso_id(auth: AzureSSOAuthentication, sso_id: str):
    ncp_sso_id = f"{auth.backend.azure_org}-ncp-{sso_id}"
    return ncp_sso_id


def remove_ncp_sso_uid(auth: AzureSSOAuthentication, ncp_sso_id: str):
    sso_id = ncp_sso_id.removeprefix(f"{auth.backend.azure_org}-ncp-")
    return sso_id


def get_or_create_sso_account(auth: AzureSSOAuthentication, access_token: str):
    # Fetch profile data from MS Graph API
    # Cross validate with the users in backend
    # If user does not exist, create the user
    # Else fetch the user related to the claim from MS Graph API.

    try:
        graph_response = get_user_profile(access_token)
    except Exception as e:
        logger.debug(e)
        raise e

    email: str = graph_response.get("userPrincipalName").split("#")[0].replace("_", "@")
    sso_id: str = graph_response.get("id")
    org_name: str = auth.backend.azure_org
    first_name: str = graph_response.get("givenName")
    last_name: str = graph_response.get("surname")

    ncp_sso_id = create_ncp_sso_id(auth, sso_id)

    if SSOAccount.objects.filter(sso_id=ncp_sso_id).exists():
        sso_obj: SSOAccount = SSOAccount.objects.get(sso_id=ncp_sso_id)
        print("SSO ACCOUNT EXISTS", sso_obj)
        print("USER EXISTS", sso_obj.user)
        return sso_obj

    if CustomUser.objects.filter(email=email).exists():

        sso_obj: SSOAccount = SSOAccount.objects.create(
            sso_id=ncp_sso_id, user=CustomUser.objects.get(email=email)
        )
        print("SSO ACCOUNT DOESN'T EXTSIS", sso_obj)
        print("USER EXISTS", sso_obj.user)
        return sso_obj
    else:
        org_obj = Organization.objects.get(name=org_name)

        is_admin = False
        is_staff = False
        is_superuser = False

        if email.__contains__("@nirvedacognition.ai") and settings.DEBUG:
            is_admin = True
            is_staff = True
            is_superuser = True
        user = CustomUser.objects.create(
            email=email,
            organization=org_obj,
            first_name=first_name,
            last_name=last_name,
            is_active=True,
            is_admin=is_admin,
            is_staff=is_staff,
            is_superuser=is_superuser,
        )
        sso_obj = SSOAccount.objects.create(sso_id=ncp_sso_id, user=user)
        print("NO SSO ACCOUNT EXISTS", sso_obj)
        print("NO USER EXISTS", sso_obj)
        return sso_obj

import logging
from datetime import timedelta
from typing import Dict

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication
from rest_framework.request import Request

from .backends import AzureModelBackend
from .models import Token

logger = logging.getLogger("backend")


def get_authorization_header(request: Request):
    """
    Return request's 'Authorization:' header, as a bytestring.

    Hide some test client ickyness where the header can be unicode.
    """
    auth = request.headers.get("Authorization")
    if isinstance(auth, str):
        return auth


class AzureSSOAuthentication(BaseAuthentication):
    keyword = "Bearer"
    model = None
    backend = None

    def get_model(self):
        if self.model is not None:
            return self.model
        return Token

    def get_backend(self, request: Request):
        if not self.backend:
            return AzureModelBackend(request)
        return self.backend

    def authenticate(self, request: Request):

        auth = (
            get_authorization_header(request).split()
            if get_authorization_header(request)
            else None
        )

        if auth:
            print("--- AUTH PASSED INIT CHECKS ---", auth)
            return self.authenticate_credentials(auth[1])

    def authenticate_credentials(self, key):
        model = self.get_model()
        try:
            token: Token = self.verify_token(key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed(_("Invalid token."))

        if not token.user:
            raise exceptions.AuthenticationFailed(_("User Not Found."))

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed(_("User deactivated or deleted."))

        if not token.is_active:
            raise exceptions.AuthenticationFailed(_("Token inactive or deleted."))

        if not self.verify_token_expiry(token):
            raise exceptions.AuthenticationFailed(_("Token Expired."))

        return (token.user, token)

    def verify_token_expiry(self, token: Token):
        now = timezone.now()
        expiry = token.created_at + timedelta(0, float(token.expires_in))

        if now <= expiry:
            return True

        token.deactivate_token()

        return False

    def verify_token(self, key: str):
        model = self.get_model()
        token: Token = model.objects.get(access_token=key)
        print("TOKEN :  ", token)
        return token or None

    def authenticate_header(self, request):
        return self.keyword

    def authorize(self, request: Request):
        self.backend = self.get_backend(request)
        self.backend._create_and_store_state_in_session()
        return self.backend.get_auth_url()

    def generate_token(self, request: Request, params: Dict = None):
        self.backend.request = request
        if not params:
            params = self.backend.get_request_params_as_dict()
        # self.backend._verify_state(params)
        data = self.backend.generate_token_from_code(params)
        return data

    def refresh_access_token(self, request: Request, sso_id: str):
        return self.backend.get_new_token_with_refresh_token()

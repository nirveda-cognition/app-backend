import functools
import json
import logging

import requests
from django.conf import settings
from requests.auth import AuthBase

from backend.conf.constants import HttpMethods
from backend.lib.utils.urls import (
    add_query_params_to_url,
    make_url_absolute,
    safe_format_url,
)

from .exceptions import AIServicesHttpError

logger = logging.getLogger("backend")


proxies = {"http": settings.API_PROXY_URL, "https": settings.APIS_PROXY_URL}


class AIServicesAuth(AuthBase):
    """
    Attaches token authentication to a requests.Request() instance for the
    AI Services API.

    Parameters:
    ----------
    api_key: :obj:`str` (optional)
        The AI Services API key.  If not provided, the API Key from the Django
        settings module will be used.
    """

    def __init__(self, api_key=None):
        self._api_key = api_key

    def __call__(self, req):
        req.headers["X-API-Key"] = self.api_key
        req.headers["request-type"] = "ai-services"
        return req

    @property
    def api_key(self):
        return self._api_key or settings.AI_SERVICES_API_KEY


def handle_http_errors(func):
    @functools.wraps(func)
    def inner(instance, url, **kwargs):
        try:
            response = func(instance, url, **kwargs)
        except requests.exceptions.ConnectionError as e:
            logger.error(
                "There was a Connection Error making a request to "
                "AI Services at %s." % url
            )

            raise AIServicesHttpError(
                error=e,
                url=url,
                code=AIServicesHttpError.CONNECTION_ERROR,
                message=("There was a problem connecting to AI services." + str(e)),
            )
        except requests.exceptions.Timeout as e:
            logger.error(
                "There was a Timeout Error making a request to "
                "AI Services at %s." % url
            )
            raise AIServicesHttpError(
                error=e,
                url=url,
                code=AIServicesHttpError.TIMED_OUT_ERROR,
                message=("The request to AI services timed out."),
            )
        except requests.exceptions.RequestException as e:
            logger.error(
                "There was an unknown error making a request to "
                "AI Services at %s." % url
            )
            raise AIServicesHttpError(
                error=e,
                url=url,
                code=AIServicesHttpError.UNKNOWN_ERROR,
                message=(
                    "There was a network error making a request to " "AI Services."
                ),
            )
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.HTTPError as e:

                logger.error(
                    "There was a Client Error making a request to "
                    "AI Services at %s." % response.request.url,
                )
                raise AIServicesHttpError(
                    error=e,
                    code=AIServicesHttpError.CLIENT_ERROR,
                    response=response,
                    message=(
                        "There was a client error making a request to " "AI Services."
                    ),
                )
            else:
                # Catch any errors that might be embedded in the JSON and
                # try to convert them to a local form of AIServicesHttpError.
                try:
                    data = response.json()
                except json.decoder.JSONDecodeError:
                    return response
                else:
                    if "errors" in data:
                        raise AIServicesHttpError(
                            code=AIServicesHttpError.CLIENT_ERROR,
                            response=response,
                            reason=data["errors"][0]["detail"],
                            status_code=int(data["errors"][0]["status"]),
                            message=(
                                "There was a client error making a request to "
                                "AI Services."
                            ),
                        )
                    return response

    return inner


def transform_url(func):
    """
    Prepares the URL for the request by affixing query parameters, string
    formatting URL PATH parameters and putting the URL in the AI Services API
    domain.

    When a URL is provided to the methods of :obj:`AIServicesSession`, the
    query parameters and PATH parameters can be supplied as keyword arguments.
    Furthermore, the URL need not be prefixed with the AI Services API domain.

    >>> session = AIServicesSession()
    >>> # Sends a request to /services/my-service?foo=bar
    >>> session.get("/services/{name}", query={"foo": "bar},
    >>>     url_kwargs={"name": "my-service"})
    """

    @functools.wraps(func)
    def inner(instance, url, **kwargs):
        # Put the URL in the context of the right domain.
        url = make_url_absolute(url, base=settings.AI_SERVICES_API_URL)

        # Affix URL with query parameters if provided.
        query = kwargs.pop("query", {})
        url = add_query_params_to_url(url, **query)

        # Format the URL with provided keyword arguments if provided.
        url_kwargs = kwargs.pop("url_kwargs", None)
        if url_kwargs:
            url = safe_format_url(url, **url_kwargs)
        return func(instance, url, **kwargs)

    return inner


class AIServicesSession(requests.Session):
    """
    Responsible for making the authenticated requests to the AI Services
    API and properly handling HTTP errors.

    Parameters:
    ----------
    headers: :obj:`dict`
        A dictionary of headers to include in the request.  These headers will
        be applied on top of the default headers for requests to the AI Services
        API.

        Can be provided either on instantiation of the session or explicitly
        when calling methods on the session.

    proxies: :obj:`dict` (optional)
        Proxies to funnel the requests through.  Note that
        the :obj:`AIServicesSession` uses proxies defined in the Django settings
        module - but those can be overridden by including this parameter.

        Can be provided either on instantiation of the session or explicitly
        when calling methods on the session.

    api_key: :obj:`str` (optional)
        The AI Services API key.  If not provided, the API Key from the Django
        settings module will be used.

        Can be provided either on instantiation of the session or explicitly
        when calling methods on the session.
    """

    def __init__(
        self,
        access_token=None,
        request=None,
        headers=None,
        proxies=proxies,
        api_key=None,
    ):

        super(AIServicesSession, self).__init__()
        self._api_key = api_key
        self._headers = headers or {}
        self._proxies = proxies or {}

    def _auth(self, request=None, api_key=None):
        return AIServicesAuth(api_key=api_key or self._api_key)

    def _prepare_request(
        self, url, method, body=None, api_key=None, content_type="application/json"
    ):
        prepared_request = requests.Request(
            method,
            url,
            headers={
                **self._headers,
                **{
                    "Content-Type": content_type,
                    "User-Agent": "Chrome",
                    "Connection": "close",
                },
            },
            data=json.dumps(body),
        ).prepare()

        # Attach authentication to the prepared request.
        auth = self._auth(api_key=api_key)
        prepared_request.prepare_auth(auth, url)
        return prepared_request

    def _send(self, url, method, proxies=None, **kwargs):
        req = self._prepare_request(url, method=method, **kwargs)
        logger.info("[%s]: %s" % (method.upper(), url))
        return self.send(
            req,
            proxies=proxies or self._proxies,
            allow_redirects=True,
        )

    @handle_http_errors
    @transform_url
    def get(self, url, **kwargs):
        """
        Sends an AI Services authenticated HTTP GET request to the provided
        endpoint.

        Parameters:
        ----------
        url: :obj:`str`
            The endpoint to send the HTTP GET request.  The end point does
            not need to include the domain.

        query: :obj:`dict` (optional)
            A dictionary of query parameters to affix on the URL.
        """
        return self._send(url, HttpMethods.GET, **kwargs)

    @handle_http_errors
    @transform_url
    def post(self, url, **kwargs):
        """
        Sends an authenticated HTTP POST request to the provided endpoint
        of the AI Services API.

        Parameters:
        ----------
        url: :obj:`str`
            The endpoint to send the HTTP POST request.  The end point does
            not need to include the domain.

        body: :obj:`dict` (optional)
            The request body.
        """
        return self._send(url, HttpMethods.POST, **kwargs)

    @handle_http_errors
    @transform_url
    def delete(self, url, **kwargs):
        """
        Sends an authenticated HTTP DELETE request to the provided endpoint
        of the AI Services API.

        Parameters:
        ----------
        url: :obj:`str`
            The endpoint to send the HTTP DELETE request.  The end point does
            not need to include the domain.
        """
        return self._send(url, HttpMethods.DELETE, **kwargs)


session = AIServicesSession()

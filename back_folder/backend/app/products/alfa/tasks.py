import io
import uuid as UUID

import pandas as pd

from backend.app.collection.models import TrackedOperation
from backend.app.document.models import Document
from backend.app.task.models import TaskResult
from backend.lib.aws.nv_bucket_manager import NVBucketManager
from backend.lib.celery_utils import retriable_task
from backend.lib.utils import parse_bytes


@retriable_task(name="backend.app.products.alfa.start_loan_extraction")
def start_loan_extraction(doc_id):
    document = Document.objects.get(pk=doc_id)

    # Loan extraction started
    bucket = NVBucketManager()
    file_fetch = bucket.file_fetch(document.aws_document_key)

    bytes_io = io.BytesIO(file_fetch)
    df = pd.read_csv(parse_bytes(bytes_io), delimiter=",")

    for index, row in df.iterrows():
        loan_info = dict(row)
        if loan_info.get("forgiveness_state") not in (
            "New",
            "Declined",
            "Approved",
            "In Review",
            "More Info Needed",
        ):
            loan_info["forgiveness_state"] = "New"

        unique_id = str(UUID.uuid4())
        name = loan_info.get("company_name") or unique_id  # noqa
        # Needs to be fixed for ALFA Case!
        # loan, cr = Collection.objects.setup_with_schema(
        #     document.creator, name=name)

        # for key in loan_info:
        #     if str(loan_info[key]) != 'nan':
        #         cr.result['collection_data'][key] = loan_info[key]

        # cr.save()

    tr = TaskResult(
        document=document,
        task=document.task,
        data={"": ""},
        new_data={
            "type": "loan_extraction",
            "status": "SUCCESS",  # TODO: Reference as constant.
            "service": "document-extractor",
            "task_id": document.task.uuid,
        },
        created_by=document.task.created_by,
    )
    tr.save(track=True, user=document.owner, operation=TrackedOperation.NEW)
    document.completed_processing()
    document.save()
    document.task.save()
    return True

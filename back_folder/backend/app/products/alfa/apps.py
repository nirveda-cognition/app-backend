from django.apps import AppConfig


class AlfaConfig(AppConfig):
    name = "backend.app.products.alfa"

from django.urls import include, path

urlpatterns = [
    path("alfa/", include("backend.app.products.alfa.urls")),
    path("apex/", include("backend.app.products.apex.urls")),
    path("kpmg/", include("backend.app.products.kpmg.urls")),
]

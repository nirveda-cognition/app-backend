from django.apps import AppConfig


class ApexConfig(AppConfig):
    name = "backend.app.products.apex"

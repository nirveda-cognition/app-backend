import uuid

from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from backend.app.collection.models import TrackedOperation
from backend.app.common.models import TimestampedModel
from backend.app.task.models import TaskResult
from backend.lib.django_utils.models import track_field_changes

from .managers import DocumentManager, DocumentStatusManager


def dynamic_default_now():
    return timezone.now()


# TODO: Deprecate this model in favor of a simple integer field on the models.
class DocumentStatus(TimestampedModel):
    PENDING = "Processing"
    COMPLETED = "Complete"
    FAILURE = "Failed"
    NEW = "New"

    TASK_STATUS = [
        (PENDING, _("Processing")),
        (COMPLETED, _("Complete")),
        (FAILURE, _("Failed")),
        (NEW, _("New")),
    ]

    status = models.CharField(choices=TASK_STATUS, unique=True, max_length=64)
    description = models.CharField(max_length=256, blank=True, null=True)
    objects = DocumentStatusManager()

    class Meta:
        verbose_name = "Document Status"
        verbose_name_plural = "Document Statuses"

    def __str__(self):
        return self.status


@track_field_changes({"document_status": "status_changed_at"})
class Document(TimestampedModel):
    name = models.CharField(max_length=256)
    path = models.CharField(max_length=512, blank=True, null=True)
    type = models.CharField(max_length=64)
    uuid = models.CharField(max_length=64, default=uuid.uuid4)
    organization = models.ForeignKey(
        to="organization.Organization",
        on_delete=models.CASCADE,
        related_name="documents",
    )
    # TODO: Change to `collections` to stay consistent.
    collection_list = models.ManyToManyField(
        to="collection.Collection", blank=True, related_name="documents"
    )
    owner = models.ForeignKey(
        to="custom_auth.CustomUser",
        on_delete=models.CASCADE,
        related_name="owned_documents",
        null=True,
    )
    # TODO: Change to `created_by` to stay consistent.
    # TODO: Make required field.
    creator = models.ForeignKey(
        to="custom_auth.CustomUser",
        on_delete=models.CASCADE,
        related_name="created_documents",
        null=True,
    )
    status_changed_at = models.DateTimeField(default=dynamic_default_now)
    storage_path = models.CharField(max_length=8192, blank=True, null=True)
    task = models.OneToOneField(
        to="task.Task",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="document",
    )
    aws_document_key = models.CharField(max_length=8192, blank=True, null=True)
    size = models.BigIntegerField(blank=True, null=True)
    trash = models.BooleanField(default=False)
    content_type = models.CharField(max_length=64, blank=True, null=True)

    # TODO: Deprecate this model in favor of a simple integer field.
    STATUS_DEFAULT = STATUS_PENDING = "Processing"
    STATUS_COMPLETED = "Complete"
    STATUS_FAILURE = "Failed"
    STATUS_NEW = "New"
    document_status = models.ForeignKey(
        to="document.DocumentStatus",
        on_delete=models.DO_NOTHING,
        default=1,  # Processing
    )

    # TODO: Make required field with a default.
    SCHEMA_EXPENSE = "Expense"
    SCHEMA_INVOICE = "Invoice"
    SCHEMA_LOAN = "Loan"
    SCHEMA_WORKITEM = "WorkItem"
    SCHEMAS = [
        (SCHEMA_EXPENSE, _("Expense")),
        (SCHEMA_INVOICE, _("Invoice")),
        (SCHEMA_LOAN, _("Loan")),
        (SCHEMA_WORKITEM, _("WorkItem")),
    ]
    schema = models.CharField(max_length=256, choices=SCHEMAS, blank=True, null=True)

    # TODO: Change to boolean fields.
    STATE_DEFAULT = STATE_UPLOADING = "Uploading"
    STATE_UPLOADED = "Uploaded"
    STATE_TRASH = "Trash"
    STATES = [
        (STATE_UPLOADING, _("Uploading")),
        (STATE_UPLOADED, _("Uploaded")),
        (STATE_TRASH, _("Trash")),
    ]
    state = models.CharField(max_length=10, choices=STATES, default=STATE_DEFAULT)

    objects = DocumentManager()

    class Meta:
        # TODO: Make organization/name unique together.
        unique_together = ()
        get_latest_by = "time_created"
        ordering = ("-status_changed_at",)

    def __str__(self):
        return str(self.id) + ": " + self.name

    @cached_property
    def latest_task_result(self):
        if self.task is not None:
            try:
                return self.task.results.latest()
            except TaskResult.DoesNotExist:
                return None
        return None

    @property
    def processing_time(self):
        if (
            self.latest_task_result is not None
            and "meta" in self.latest_task_result.new_data
            and "duration" in self.latest_task_result.new_data["meta"]
        ):
            return self.latest_task_result.new_data["meta"].get("duration")
        return None

    def restore(self):
        """
        Removes the :obj:`Document` instance from the Trash and restores
        it's state to it's pre-Trash value.

        TODO:
        ----
        This logic needs to be improved - the uploaded state needs to be
        separated  from the Trash state, otherwise, there is no way to know what
        it's state was before it was sent to the Trash.
        """
        self.state = self.STATE_UPLOADED
        self.save()

    def to_trash(self, user):
        """
        Associates the :obj:`Document` instance with the Trash.  This is
        opposed to permanently deleting the :obj:`Document` instance.

        When a :obj:`Document` is moved to the Trash, it needs to be
        disassociated from any :obj:`Collection`(s) it might belong to.
        """
        # self.collection_list.set([])
        self.state = self.STATE_TRASH
        self.save(user=user, operation=TrackedOperation.DELETE)

    def failed_processing(self, error_type, error=None):
        """
        Marks the :obj:`Document` and associated :obj:`Task` as having failed
        to process.  The error information will be embedded in the :obj:`Task`
        instance.
        """
        self.document_status = DocumentStatus.objects.get(status=DocumentStatus.FAILURE)
        self.task.response["error"] = {
            "type": error_type,
            "description": error_type,
            "time": str(timezone.now()),
        }
        if error is not None:
            self.task.response["error"]["detail"] = str(error)

    def started_processing(self):
        """
        Marks the :obj:`Document` and associated :obj:`Task` as having started
        processing.
        """
        self.document_status = DocumentStatus.objects.get(status=DocumentStatus.PENDING)

    def completed_processing(self):
        """
        Marks the :obj:`Document` and associated :obj:`Task` as having
        successfully completed processing.
        """
        self.document_status = DocumentStatus.objects.get(
            status=DocumentStatus.COMPLETED
        )

    @property
    def document_types(self):
        # TODO: Find a way of doing this that doesn't involve hardcoding in the
        # document types.
        if self.schema is None:
            raise Exception("Cannot determine document types without a schema.")
        document_types = [self.schema]
        if self.schema == "apex":
            document_types = [
                "purchase_order",
                "invoice",
                "receiver",
                "change_order",
                "packing_slip",
            ]
        return document_types

    def save(self, *args, **kwargs):
        self._user = kwargs.pop("user", None)
        self._option = kwargs.pop("operation", None)
        super(Document, self).save(*args, **kwargs)

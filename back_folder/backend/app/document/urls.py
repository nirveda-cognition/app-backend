from django.urls import include, path
from rest_framework import routers

from backend.app.task.urls import document_urlpatterns

from .views import (
    CollectionDocumentViewSet,
    DocumentTrashViewSet,
    DocumentViewSet,
    UploadDocumentView,
)

app_name = "document"

router = routers.SimpleRouter()
router.register(r"trash", DocumentTrashViewSet, basename="trash")
router.register(r"", DocumentViewSet, basename="document")

collection_router = routers.SimpleRouter()
collection_router.register(r"", CollectionDocumentViewSet, basename="document")

urlpatterns = [
    path("upload/", UploadDocumentView.as_view()),
    path("<int:document_pk>/task/", include(document_urlpatterns)),
] + router.urls

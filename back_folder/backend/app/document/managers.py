from django.db import models


class DocumentStatusManager(models.Manager):
    def new(self):
        return self.get(status=self.model.NEW)

    def processing(self):
        return self.get(status=self.model.PENDING)

    def failed(self):
        return self.get(status=self.model.FAILURE)

    def completed(self):
        return self.get(status=self.model.COMPLETED)


class DocumentQuerier(object):
    def processing(self):
        # pylint: disable=no-member
        return self.filter(state=self.model.STATE_UPLOADING)

    def uploaded(self):
        # pylint: disable=no-member
        return self.filter(state=self.model.STATE_UPLOADED)

    def inactive(self):
        # pylint: disable=no-member
        return self.filter(state=self.model.STATE_TRASH)

    def active(self):
        # pylint: disable=no-member
        return self.exclude(state=self.model.STATE_TRASH)


class DocumentQuery(DocumentQuerier, models.query.QuerySet):
    pass


class DocumentManager(DocumentQuerier, models.Manager):
    queryset_class = DocumentQuery

    def get_queryset(self):
        return self.queryset_class(self.model)

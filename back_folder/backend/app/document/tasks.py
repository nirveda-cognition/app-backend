import datetime
import logging
import time

import requests
from django.utils import timezone

from backend.app.ai_services.tasks import callback_if_applicable
from backend.conf.constants import API_USER_UPLOAD_ERROR
from backend.lib.celery_utils import retriable_task

from .models import Document, DocumentStatus

logger = logging.getLogger("celery.task")


def monitor_upload(head_url, timeout_threshold=900, sleep=5, max_attempts=180):

    max_attempts = 180

    logger.info("Watching file at %s." % head_url)
    start_time = time.time()
    attempts = 0

    def log_attempt(attempt, message, level="info", exc=None, extra=None):
        message = "Awaiting Document Upload - Attempt %s: %s" % (attempt, message)
        if exc is not None:
            message += "Error: %s" % exc
        getattr(logger, level)(message, extra=extra)

    def retry(num_attempts):
        if (
            timeout_threshold is not None
            and time.time() - start_time >= timeout_threshold
        ):
            log_attempt(num_attempts, message="Time threshold exceeded - exiting.")
            return False
        elif max_attempts is not None and attempts >= max_attempts:
            log_attempt(num_attempts, message="Attempt threshold exceeded - exiting.")
            return False
        return True

    while attempts <= max_attempts:
        attempts += 1
        try:
            response = requests.head(head_url)
        except requests.RequestException as e:
            log_attempt(attempt=attempts, message="Request Error", exc=e)
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.HTTPError as e:
                log_attempt(
                    attempt=attempts,
                    message="Received HTTP Error [%s]" % response.status_code,
                    exc=e,
                )
                if not retry(attempts):
                    return False

                if int(response.status_code) == 403:
                    #  quit if it's forbidden cuz that aint gonna change
                    return False

                time.sleep(sleep)
                continue
            else:
                log_attempt(attempts, message="Document Upload Successful")
                return True


@retriable_task(name="backend.app.document.tasks.manage_failed_tasks")
def manage_failed_tasks():
    for doc in Document.objects.processing():
        minutes_processing = (timezone.now() - doc.status_changed_at).seconds / 60
        if minutes_processing > 20:
            # logger.warning(
            #     "Document %s has been in processing state for %s minutes with "
            #     "exceeds the 20 minute threshold - marking as failure." % (
            #         doc.id, minutes_processing)
            # )
            doc.document_status = DocumentStatus.objects.get(
                status=DocumentStatus.FAILURE
            )
            doc.save()
    return True


@retriable_task(name="backend.app.document.tasks.watch_for_old_trash")
def watch_for_old_trash():
    documents = (
        Document.objects.inactive()
        .filter(status_changed_at__lte=timezone.now() - datetime.timedelta(days=30))
        .all()
    )
    if len(documents) == 0:
        logger.info("No document older than 30 days found.")
        return False
    for document in documents:
        logger.info("Deleting document %s, document older than 30 days." % document.pk)
        document.delete()
    return True


@retriable_task(bind=True, name="backend.app.document.tasks.set_file_size")
def set_file_size(self, doc_id):
    document = Document.objects.get(pk=doc_id)
    try:
        response = requests.get(document.storage_path, stream=True)
    except requests.exceptions.Timeout as e:
        logger.error(
            "Timeout Error: Error trying to fetch uploaded document "
            "and set file size.  Retrying...",
            extra={
                "status_code": response.status_code,
                "error": str(e),
                "document": document.pk,
                "url": document.storage_path,
            },
        )
        raise self.retry(exc=e, countdown=30, max_retries=2)
    except requests.exceptions.RequestException as e:
        logger.error(
            "Requeset Error: Error trying to fetch uploaded document "
            "and set file size.  Retrying...",
            extra={
                "status_code": response.status_code,
                "error": str(e),
                "document": document.pk,
                "url": document.storage_path,
            },
        )
        raise self.retry(exc=e, countdown=30, max_retries=1)
    else:
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            logger.error(
                "HTTP Error [%s]: Error trying to fetch uploaded document and "
                "set file size." % response.status_code,
                extra={
                    "status_code": response.status_code,
                    "error": str(e),
                    "document": document.pk,
                    "url": document.storage_path,
                },
            )
            return False
        else:
            try:
                document.size = int(response.headers.get("Content-Length", None))
            except ValueError:
                logger.error(
                    "Document size is invalid.",
                    extra={
                        "size": response.headers.get("Content-Length", None),
                        "document": document.pk,
                        "url": document.storage_path,
                    },
                )
                return False
            else:
                logger.info(
                    "Document size successfully set.",
                    extra={"size": document.size, "document": document.pk},
                )
                document.save()
                return document.pk


@retriable_task(name="backend.app.document.tasks.watch_for_uploads")
def watch_for_uploads(doc_id, head_url):
    document = Document.objects.get(pk=doc_id)
    logger.info(
        "Watching document %s for uploads." % document.name,
        extra={"document": document.pk, "url": head_url},
    )

    doc_uploaded = monitor_upload(head_url)
    if not doc_uploaded:
        logger.info(
            "The document failed to upload.",
            extra={"document": document.pk, "url": head_url},
        )
        # TODO: At this point, we need to delete the document and task.
        callback_if_applicable.delay(document.task.pk, API_USER_UPLOAD_ERROR)
        # TODO: Reference this as an uploaded failed error, with a different
        # state, than a processing failed error.
        document.failed_processing(error_type="UPLOAD_FAILED")
        document.save()
        document.task.save()
        return False

    logger.info(
        "Document %s successfully uploaded." % document.name,
        extra={"document": document.pk, "url": head_url},
    )
    document.state = Document.STATE_UPLOADED
    document.save()
    return document.pk

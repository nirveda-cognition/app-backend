import base64

from django_filters import rest_framework as filters
from rest_framework import decorators, mixins, response, views, viewsets
from rest_framework.exceptions import ParseError

from backend.app.collection.mixins import CollectionNestedMixin
from backend.app.common.serializers import DocumentSimpleSerializer
from backend.app.organization.mixins import OrganizationNestedMixin
from backend.app.products.kpmg.utils import classify_as_tax
from backend.app.user.models import UserCategory
from backend.lib.aws.nv_bucket_manager import NVBucketManager
from backend.lib.rest_framework_utils.pagination import paginate_action

from .models import Document, DocumentStatus
from .serializers import DocumentSerializer, UploadDocumentSerializer
from .utils import create_and_process_document


class DocumentFilter(filters.FilterSet):
    created_at__gte = filters.DateTimeFilter(field_name="created_at", lookup_expr="gte")
    created_at__lte = filters.DateTimeFilter(field_name="created_at", lookup_expr="lte")
    status_changed_at__lte = filters.DateTimeFilter(
        field_name="status_changed_at", lookup_expr="lte"
    )
    status_changed_at__gte = filters.DateTimeFilter(
        field_name="status_changed_at", lookup_expr="gte"
    )
    document_status = filters.ModelChoiceFilter(
        queryset=DocumentStatus.objects.all(),
        field_name="document_status__status",
        to_field_name="status",
    )

    class Meta:
        model = Document
        fields = (
            "created_at__gte",
            "created_at__lte",
            "status_changed_at__lte",
            "status_changed_at__gte",
            "document_status",
        )


class GenericDocumentViewSet(viewsets.GenericViewSet):
    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    filterset_class = DocumentFilter
    ordering_fields = ["status_changed_at", "name", "created_at", "updated_at"]
    search_fields = ["name"]
    serializer_class = DocumentSerializer

    @property
    def is_simple(self):
        # TODO: We want to be able to use a simple serialized form of the
        # collection on GET requests - however, there are smarter ways of doing
        # this than the one seen here.  We should investigate how to do this
        # more elegantly.
        simple = self.request.GET.get("simple", False)
        if simple is not False:
            return True
        return False

    def get_serializer_class(self):
        if self.is_simple:
            return DocumentSimpleSerializer
        return self.serializer_class


class DocumentViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    GenericDocumentViewSet,
    OrganizationNestedMixin,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/documents/
    (2) POST /organizations/<pk>/documents/
    (3) GET /organizations/<pk>/documents/<pk>/
    (4) DELETE /organizations/<pk>/documents/<pk>/
    """

    @property
    def is_include_trash(self):
        include_trash = self.request.GET.get("include_trash", False)
        if (
            include_trash is not False
            and self.request.user.category == UserCategory.SUPERUSER
            and self.action == "list"
        ):
            return True
        return False

    def get_queryset(self):
        simple = self.request.GET.get("simple", False)

        if self.is_include_trash:
            return self.organization.documents.all()

        if simple is not False:
            # Idea use .defer('task_results') or whatever to omit the bulky data

            pass
        return self.organization.documents.active()

    @paginate_action
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        simple = self.request.GET.get("simple")
        if simple:
            try:
                # values when document is part of a collection
                queryset = queryset.values(
                    "id",
                    "size",
                    "name",
                    "document_status__status",
                    "status_changed_at",
                    "created_at",
                    "document_status",
                    "organization_id",
                    "updated_at",
                    "collection_list__name",
                    "collection_list__id",
                )
            except Exception as e:
                print(e)

                #  values for independent document
                queryset = queryset.values(
                    "id",
                    "size",
                    "name",
                    "document_status__status",
                    "status_changed_at",
                    "created_at",
                    "document_status",
                    "organization_id",
                    "updated_at",
                )

        return queryset

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user, organization=self.organization)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.document_status == DocumentStatus.objects.get(status="Processing"):
            raise ParseError(code=400, detail="Document still processing")
        instance.to_trash(request.user)
        return response.Response(status=204)

    @paginate_action
    @decorators.action(detail=False, methods=["GET"])
    def root(self, request, *args, **kwargs):
        """
        Returns the root :obj:`Document` instances for the organization.
        These :obj:`Document`(s) do not belong to a collection.
        """

        qs = self.get_queryset()
        qs = qs.filter(collection_list=None)
        return self.filter_queryset(qs)

    @decorators.action(detail=True, methods=["GET"], url_path="fetch-file")
    def fetch_file(self, request, *args, **kwargs):
        instance = self.get_object()
        bucket = NVBucketManager()
        # TODO: Make sure we are obfuscating objects in buckets / containers
        # in hashed folders
        file_fetch = bucket.file_fetch(instance.aws_document_key)
        file_data = base64.b64encode(file_fetch).decode()
        return response.Response({"data": file_data})

    @decorators.action(detail=True, methods=["POST"], url_path="tax-classify")
    def tax_classification(self, request, *args, **kwargs):
        instance = self.get_object()
        classify_as_tax(instance.pk)
        return response.Response({})


class CollectionDocumentViewSet(
    mixins.ListModelMixin, GenericDocumentViewSet, CollectionNestedMixin
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/collections/<pk>/documents
    """

    collection_lookup_field = ("pk", "collection_pk")

    def get_queryset(self):
        return self.collection.documents.active()


class DocumentTrashViewSet(
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericDocumentViewSet,
    OrganizationNestedMixin,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/documents/trash/
    (2) GET /organizations/<pk>/documents/trash/<pk>/
    (3) PATCH /organizations/<pk>/documents/trash/<pk>/restore/
    (4) DELETE /organizations/<pk>/documents/trash/<pk>/
    """

    def get_queryset(self):
        return self.organization.documents.inactive()

    @decorators.action(detail=True, methods=["PATCH"])
    def restore(self, request, *args, **kwargs):
        """
        Moves the `obj:Document` that is in the trash out of the trash
        to the main set of `obj:Document`(s).

        PATCH /organizations/<pk>/documents/trash/<pk>/restore/
        """
        instance = self.get_object()
        instance.restore()
        return response.Response(self.serializer_class(instance).data, status=201)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return response.Response(status=204)


class UploadDocumentView(views.APIView):
    """
    View to handle requests to the following endpoints:

    (1) POST /organizations/<pk>/documents/upload/
    """

    def post(self, request, *args, **kwargs):
        serializer = UploadDocumentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # TODO: Start passing in the organization from the endpoint, not
        # the organization on the User - because it is safer.
        data = create_and_process_document(
            filename=serializer.validated_data["name"],
            file_type=serializer.validated_data["ext"],
            user=request.user,
            collection=serializer.validated_data.get("collection"),
        )
        return response.Response(
            {
                "put_signed_url": data["put_url"],
                "get_signed_url": data["get_url"],
                "document_id": data["document"].pk,
                "headers": data["headers"],
                "document": DocumentSerializer(data["document"]).data,
            },
            status=201,
        )

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm

from backend.app.custom_auth.models import SSOAccount

from .models import CustomUser


class UserCreateForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ("username", "email")


class CustomUserAdmin(UserAdmin):
    list_per_page = 500
    ordering = ("id",)
    list_display = (
        "id",
        "timezone",
        "language",
        "email",
        "role",
        "first_name",
        "last_name",
        "organization",
        "is_staff",
        "is_admin",
        "is_superuser",
        "is_active",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "password",
                    "groups",
                    "email",
                    "role",
                    "first_name",
                    "last_name",
                    "organization",
                    "is_staff",
                    "is_admin",
                    "is_superuser",
                    "is_active",
                )
            },
        ),
    )
    add_form = UserCreateForm
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "username",
                    "email",
                    "password1",
                    "password2",
                ),
            },
        ),
    )


admin.site.register(CustomUser, CustomUserAdmin)


class SSOAccountAdmin(admin.ModelAdmin):
    list_per_page = 500


admin.site.register(SSOAccount, SSOAccountAdmin)

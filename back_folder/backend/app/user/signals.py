from django import dispatch
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.html import strip_tags

from backend.app.authentication.models import ResetUID
from backend.app.user.models import CustomUser
from backend.lib.utils.urls import add_query_params_to_url

user_signed_up = dispatch.Signal(providing_args=["instance", "invite_alfa"])


@dispatch.receiver(user_signed_up, sender=CustomUser)
def send_email(sender, instance, invite_alfa=False, **kwargs):
    reset_uid = ResetUID.objects.create(user=instance)

    brand = "nirveda"
    body_line1 = "You have been invited to the Nirveda Cognitive Platform."
    template = "email/onboard_user.html"
    email_subject = "Invitation to Nirveda Cognitive Platform"
    app_url = settings.RESET_PWD_UI_LINK

    if instance.organization.name.lower() == "kpmg":
        brand = "kpmg"
    elif kwargs.pop("invite_alfa", False):
        brand = "alfa"
        template = "email/onboard_alfa_user.html"
        body_line1 = "You have been invited to ALFA: PPP Loan Forgiveness Portal."
        email_subject = "Invitation to the ALFA: PPP Loan Forgiveness Portal"

    # temp code to make sure apex users go to the right place
    if instance.organization.type.slug == "apex" and not settings.DEBUG:
        app_url = app_url.replace("app", "apex")

    html_message = render_to_string(
        template,
        {
            "PWD_RESET_LINK": add_query_params_to_url(app_url, token=reset_uid.token),
            "from_email": settings.FROM_EMAIL,
            "EMAIL": instance.email,
            "year": timezone.now().year,
            "brand": brand,
            "NAME": "{0} {1}".format(instance.first_name, instance.last_name),
            "body_line1": body_line1,
            "body_line2": (
                "Please confirm your email address to activate your account."
            ),
        },
    )

    mail = EmailMultiAlternatives(
        email_subject, strip_tags(html_message), settings.FROM_EMAIL, [instance.email]
    )
    mail.attach_alternative(html_message, "text/html")
    if settings.EMAIL_ENABLED:
        mail.send()

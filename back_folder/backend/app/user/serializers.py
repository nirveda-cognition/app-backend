import logging

from django.contrib.auth import authenticate
from rest_framework import exceptions, serializers, validators

from backend.app.authentication.exceptions import InvalidCredentialsError
from backend.app.authentication.models import NCGroup, Role
from backend.app.authentication.serializers import GroupSerializer, RoleSerializer
from backend.app.custom_auth.models import SSOAccount
from backend.app.organization.models import Organization
from backend.conf.constants import HttpMethods
from backend.lib.rest_framework_utils.exceptions import InvalidFieldError
from backend.lib.rest_framework_utils.serializers import EnhancedModelSerializer

from .models import CustomUser, UserCategory

logger = logging.getLogger("backend")


class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=True, allow_null=False, allow_blank=False)
    current = serializers.CharField(required=True, allow_null=False, allow_blank=False)

    class Meta:
        model = CustomUser
        fields = ("password", "current")

    def validate(self, attrs):
        auth = authenticate(
            username=self.instance.get_username(), password=attrs["current"]
        )
        if not auth:
            raise InvalidCredentialsError()
        return attrs

    def update(self, instance, validated_data):
        instance.set_password(validated_data["password"])
        instance.save()
        return instance


class UserSerializer(EnhancedModelSerializer):
    first_name = serializers.CharField(
        required=True, allow_blank=False, allow_null=False
    )
    last_name = serializers.CharField(
        required=True, allow_blank=False, allow_null=False
    )
    full_name = serializers.CharField(read_only=True)
    email = serializers.EmailField(
        required=True,
        allow_blank=False,
        allow_null=False,
        validators=[validators.UniqueValidator(queryset=CustomUser.objects.all())],
    )
    username = serializers.EmailField(read_only=True)
    timezone = serializers.CharField(
        required=False,
        allow_blank=False,
        allow_null=False,
    )
    language = serializers.CharField(
        required=False,
        allow_blank=False,
        allow_null=False,
    )
    category = serializers.CharField(read_only=True)
    role = serializers.PrimaryKeyRelatedField(read_only=True)
    organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(),
        required=False,
        allow_null=False,
    )
    groups = serializers.PrimaryKeyRelatedField(
        many=True, queryset=NCGroup.objects, required=False
    )
    is_active = serializers.BooleanField(default=True)
    is_admin = serializers.BooleanField(default=False)
    is_staff = serializers.BooleanField(default=False)
    is_superuser = serializers.BooleanField(default=False)
    last_login = serializers.DateTimeField(read_only=True)
    date_joined = serializers.DateTimeField(read_only=True)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    updated_by = serializers.PrimaryKeyRelatedField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = CustomUser
        nested_fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "category",
            "username",
            "is_active",
            "is_admin",
            "is_superuser",
            "is_staff",
            "full_name",
        )
        fields = nested_fields + (
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            "timezone",
            "language",
            "last_login",
            "date_joined",
            "role",
            "organization",
            "groups",
        )
        expand = {
            "role": RoleSerializer,
            "organization": (
                "backend.app.organization.serializers.OrganizationSerializer",
                {"read_only": True, "nested": True},
            ),
        }
        http_toggle = {
            "role": {HttpMethods.GET: (RoleSerializer, {"read_only": True})},
            "groups": {
                HttpMethods.GET: (
                    GroupSerializer,
                    {"nested": True, "many": True, "read_only": True},
                )
            },
            "organization": {
                HttpMethods.GET: (
                    "backend.app.organization.serializers.OrganizationSerializer",
                    {"read_only": True, "nested": True},
                )
            },
        }
        response = {
            "role": (RoleSerializer, {"read_only": True}),
            "groups": (
                GroupSerializer,
                {"nested": True, "many": True, "read_only": True},
            ),
            "organization": (
                "backend.app.organization.serializers.OrganizationSerializer",
                {"read_only": True, "nested": True},
            ),
        }

    def validate(self, attrs):
        request = self.context["request"]

        # The organization is determined from the URL (passed in via context)
        # unless we are a superuser changing the organization by including it
        # in the payload.
        organization = self.context["organization"]
        if request.method.upper() != HttpMethods.POST:
            assert self.instance.organization == organization
            organization = attrs.get("organization", organization)
        else:
            # The organization is determined solely from the URL.
            if "organization" in attrs:
                raise exceptions.ValidationError(
                    "Cannot specify the organization when creating a user."
                )

        groups = attrs.get("groups", [])
        invalid_groups = []
        for group in groups:
            if group.organization != organization:
                invalid_groups.append(group.name)
        if invalid_groups:
            raise InvalidFieldError(
                "groups",
                message=(
                    "The group(s) %s do not belong to organization %s."
                    % (
                        ", ".join(["%s" % pk for pk in invalid_groups]),
                        organization.name,
                    )
                ),
            )

        # Only super admins can change the organization of a user.
        if (
            self.instance is not None
            and organization != self.instance.organization
            and request.user.category != UserCategory.SUPERUSER
        ):
            raise exceptions.PermissionDenied(
                "Cannot change the organization of a user."
            )

        # Only super admins can create/update super admin users.
        is_superuser = attrs.get(
            "is_superuser",
            self.instance.is_superuser if self.instance is not None else False,
        )
        if is_superuser and request.user.category != UserCategory.SUPERUSER:
            raise exceptions.PermissionDenied("Cannot update or create superusers.")

        role = (
            Role.objects.get(name=Role.NAME_ADMIN)
            if attrs.get("is_admin", False)
            else Role.objects.get(name=Role.NAME_USER)
        )
        attrs.update(
            organization=organization,
            is_superuser=is_superuser,
            is_staff=is_superuser,
            role=role,
            username=(self.instance.email if self.instance is not None else "email"),
        )
        return attrs


class SSOAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = SSOAccount
        fields = "__all__"

from django.contrib.auth.models import UserManager as DjangoUserManager
from django.db import models

from backend.app.organization.models import Organization


class UserQuerier(object):
    def active(self):
        # pylint: disable=no-member
        return self.filter(is_active=True)

    def inactive(self):
        # pylint: disable=no-member
        return self.filter(is_active=False)


class UserQuery(UserQuerier, models.query.QuerySet):
    pass


class CustomUserManager(UserQuerier, DjangoUserManager):
    queryset_class = UserQuery

    def get_queryset(self):
        return self.queryset_class(self.model)

    def create(self, email, password=None, **kwargs):
        kwargs["username"] = email
        user = super(CustomUserManager, self).create(email=email, **kwargs)
        if password is not None:
            user.set_password(password)
            user.save()
        return user

    def create_user(self, email, **kwargs):
        kwargs.update(
            is_staff=False,
            is_superuser=False,
            is_admin=False,
        )
        return self.create(email, **kwargs)

    def create_client_admin(self, email, **kwargs):
        kwargs.update(is_staff=False, is_superuser=False, is_admin=True)
        return self.create(email, **kwargs)

    def create_superuser(self, email, **kwargs):
        try:
            organization = Organization.objects.get(name="Nirveda")
        except Organization.DoesNotExist:
            raise Exception(
                "Superusers require the Nirveda organization which is not "
                "present.  Either load the organization fixtures or ensure "
                "that this organization exists."
            )
        else:
            kwargs.update(
                organization=organization, email=email, is_staff=True, username=email
            )
            kwargs.setdefault("is_active", True)
            return super(CustomUserManager, self).create_superuser(**kwargs)

import copy
import logging
import os

from rest_framework import exceptions, serializers

from backend.app.common.serializers import (
    CollectionSimpleSerializer,
    DocumentSimpleSerializer,
)
from backend.app.document.models import Document
from backend.app.document.utils import create_and_process_document
from backend.app.user.models import CustomUser
from backend.app.user.serializers import UserSerializer
from backend.lib.rest_framework_utils.exceptions import (
    InvalidFieldError,
    RequiredFieldError,
)

from .models import Collection, CollectionResult

logger = logging.getLogger("backend")


class CollectionsChildMixin(object):
    def validate_collections(self, collections):
        sanitized = []
        errors = {}
        for collection in collections:
            # The organization in the context will always equal the organization
            # of the instance, but using the organization from the context will
            # work for POST requests when the instance is None.
            organization = self.context["organization"]
            if collection.organization != organization:
                errors[
                    collection.pk
                ] = "The collection %s does not belong to organization %s." % (
                    collection.pk,
                    organization.pk,
                )
            # In order to prevent infinite recursions, we need to make sure that
            # the children collections and the parent collections are mutually
            # exclusive.
            elif (
                self.instance is not None and collection in self.instance.parents.all()
            ):
                errors[collection.pk] = (
                    "The collection %s already exists as a parent of %s and "
                    "cannot be assigned as a child." % (collection.pk, self.instance.pk)
                )
            else:
                sanitized.append(collection)
        if errors:
            raise exceptions.ValidationError(errors)
        return sanitized


class CollectionsParentMixin(object):
    def validate_parents(self, parents):
        sanitized = []
        errors = {}
        for parent in parents:
            # The organization in the context will always equal the organization
            # of the instance, but using the organization from the context will
            # work for POST requests when the instance is None.
            organization = self.context["organization"]
            if parent.organization != organization:
                errors[
                    parent.pk
                ] = "The parent %s does not belong to organization %s." % (
                    parent.pk,
                    organization.pk,
                )
            # In order to prevent infinite recursions, we need to make sure that
            # the children collections and the parent collections are mutually
            # exclusive.
            elif (
                self.instance is not None and parent in self.instance.collections.all()
            ):
                errors[parent.pk] = (
                    "The collection %s already exists as a child of %s and "
                    "cannot be assigned as a parent." % (parent.pk, self.instance.pk)
                )
            else:
                sanitized.append(parent)
        if errors:
            raise exceptions.ValidationError(errors)
        return sanitized


class CollectionResultSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    result = serializers.DictField(required=True, allow_null=False)
    time_created = serializers.DateTimeField(read_only=True)

    class Meta:
        model = CollectionResult
        fields = ("id", "result", "time_created")


class CollectionSerializer(
    CollectionsChildMixin,
    CollectionSimpleSerializer,
    CollectionsParentMixin,
):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(
        read_only=False, required=True, allow_blank=False, allow_null=False
    )
    status_changed_at = serializers.DateTimeField(read_only=True)
    organization = serializers.PrimaryKeyRelatedField(read_only=True)
    created_by = serializers.PrimaryKeyRelatedField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(read_only=True)
    state = serializers.ChoiceField(choices=Collection.STATES, read_only=True)
    schema = serializers.ChoiceField(choices=Collection.SCHEMAS, read_only=True)
    size = serializers.FloatField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    collection_state = serializers.ChoiceField(
        choices=Collection.COLLECTION_STATE_CHOICES,
        default=Collection.COLLECTION_STATE_NEW,
        allow_blank=False,
        allow_null=False,
    )
    parents = CollectionSimpleSerializer(many=True, read_only=True)
    collections = CollectionSimpleSerializer(many=True, read_only=True)
    documents = DocumentSimpleSerializer(many=True, read_only=True)
    result = CollectionResultSerializer(read_only=True)
    assigned_users = UserSerializer(
        many=True,
        read_only=True,
        nested=True,
    )

    class Meta:
        model = Collection
        fields = CollectionSimpleSerializer.Meta.fields + (
            "status_changed_at",
            "created_by",
            "owner",
            "state",
            "schema",
            "created_at",
            "updated_at",
            "collection_state",
            "size",
            "organization",
            "collections",
            "result",
            "documents",
            "assigned_users",
            "parents",
        )
        http_toggle = {
            "collections": {
                ("POST", "PATCH"): (
                    serializers.PrimaryKeyRelatedField,
                    {
                        "queryset": Collection.objects.active(),
                        "many": True,
                        "required": False,
                    },
                )
            },
            "documents": {
                ("POST", "PATCH"): (
                    serializers.PrimaryKeyRelatedField,
                    {
                        "queryset": Document.objects.active(),
                        "many": True,
                        "required": False,
                    },
                )
            },
            "assigned_users": {
                ("POST", "PATCH"): (
                    serializers.PrimaryKeyRelatedField,
                    {
                        "queryset": CustomUser.objects.active(),
                        "many": True,
                        "required": False,
                    },
                )
            },
            "parents": {
                ("POST", "PATCH"): (
                    serializers.PrimaryKeyRelatedField,
                    {
                        "queryset": Collection.objects.active(),
                        "many": True,
                        "required": False,
                    },
                )
            },
        }
        response = {
            "collections": (CollectionSimpleSerializer, {"many": True}),
            "documents": (DocumentSimpleSerializer, {"many": True}),
            "parents": (CollectionSimpleSerializer, {"many": True}),
            "assigned_users": (UserSerializer, {"nested": True, "many": True}),
        }

    def validate_name(self, value):
        organization = self.context["organization"]
        validator = serializers.UniqueTogetherValidator(
            queryset=Collection.objects.filter(organization=organization),
            fields=("name",),
        )
        validator({"name": value, "organization": organization}, self)
        return value

    def validate(self, attrs):
        parents = attrs.get("parents", [])
        children = attrs.get("collections", [])
        for parent in parents:
            if parent in children:
                raise exceptions.ValidationError(
                    "The children collections must be mutually exclusive "
                    "from the parent collections."
                )
        for child in children:
            if child in parents:
                raise exceptions.ValidationError(
                    "The children collections must be mutually exclusive "
                    "from the parent collections."
                )
        return attrs


class UpdateCollectionResultSerializer(serializers.ModelSerializer):
    keys = serializers.ListField(
        child=serializers.CharField(allow_blank=False, allow_null=False),
        required=True,
        allow_null=False,
    )
    value = serializers.CharField(required=True, allow_blank=False, allow_null=False)
    key_changed = serializers.CharField(
        required=True, allow_blank=False, allow_null=False
    )

    class Meta:
        model = CollectionResult
        fields = ("keys", "value", "key_changed")

    def validate(self, attrs):
        # Since we update the collection result via a PATH request, DRF will
        # not raise exceptions if the fields are not provided - so we must do
        # it manually.
        if any([field not in attrs for field in self.Meta.fields]):
            raise RequiredFieldError(
                [field for field in self.Meta.fields if field not in attrs]
            )

        final_key = attrs["keys"].pop()

        # TODO: Validate provided result.
        target = copy.deepcopy(self.instance.result)
        for key in attrs["keys"]:
            if key not in target:
                raise InvalidFieldError(
                    "keys", message="The provided key %s is not in the results." % key
                )
            target = target[key]

        if final_key not in target:
            raise InvalidFieldError(
                "keys", message="The provided key %s is not in the results." % final_key
            )

        target[final_key] = attrs["value"]
        return {"result": target, "key_changed": attrs["key_changed"]}

    def update(self, instance, validated_data):
        instance._keys_changed = [validated_data["key_changed"]]
        instance._result = validated_data["result"]
        instance.save()
        return instance


class AssignCollectionSerializer(CollectionsChildMixin, serializers.ModelSerializer):
    """
    A :obj:`serializers.ModelSerializer` class that assigns the
    :obj:`Collection` a series of :obj:`Collection` children.  The old
    children will not be removed, just appended to.
    """

    collections = serializers.PrimaryKeyRelatedField(
        required=True, many=True, queryset=Collection.objects.active()
    )

    class Meta:
        model = Collection
        fields = ("collections",)

    def validate(self, attrs):
        return {
            "collections": list(self.instance.collections.all()) + attrs["collections"]
        }


class AssignCollectionToSerializer(CollectionsParentMixin, serializers.ModelSerializer):
    """
    A :obj:`serializers.ModelSerializer` class that assigns the
    :obj:`Collection` instance to a series of parent :obj:`Collection`
    instances.
    """

    parents = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Collection.objects.active(),
    )

    class Meta:
        model = Collection
        fields = ("parents",)

    def create(self, validated_data):
        parents = validated_data.pop("parents", [])
        instance = super().create(validated_data)
        for parent in parents:
            parent.collections.add(instance)
            parent.save()
        return instance


class AssignCollectionUsersSerializer(serializers.ModelSerializer):
    users = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=CustomUser.objects.all(),
    )

    class Meta:
        model = Collection
        fields = ("users",)

    def validate(self, attrs):
        net_users = list(self.instance.assigned_users.all())
        for user in attrs["users"]:
            if user not in net_users:
                net_users.append(user)
        return {"assigned_users": net_users}


class UnassignCollectionUsersSerializer(serializers.ModelSerializer):
    users = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=CustomUser.objects.all(),
    )

    class Meta:
        model = Collection
        fields = ("users",)

    def validate(self, attrs):
        net_users = list(self.instance.assigned_users.all())
        for user in attrs["users"]:
            if user in net_users:
                net_users = [usr for usr in net_users if usr != user]
        return {"assigned_users": net_users}


class UploadCollectionDocumentSerializer(serializers.ModelSerializer):
    filename = serializers.CharField(required=True, allow_blank=False, allow_null=False)

    class Meta:
        model = Collection
        fields = ("filename",)

    def validate_filename(self, filename):
        name = os.path.splitext(filename)[0].strip()
        ext = os.path.splitext(filename)[1]
        if ext == "":
            raise exceptions.ValidationError("The filename must include an extension.")
        elif name == "":
            raise exceptions.ValidationError(
                "The extension-less filename must not be blank."
            )
        else:
            document = self.instance.documents.filter(name=filename).first()
            if document is not None:
                name = self.instance.create_unique_document_name(name)  # noqa
            return {
                "name": "%s%s" % (name, ext),
                "ext": ext[1:],  # Remove preceeding period.
            }

    def update(self, instance, validated_data):
        return create_and_process_document(
            filename=validated_data["filename"]["name"],
            file_type=validated_data["filename"]["ext"],
            user=validated_data["user"],
            collection=self.instance,
        )

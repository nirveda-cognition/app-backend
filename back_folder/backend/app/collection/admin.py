from django.contrib import admin
from django.template.defaultfilters import truncatechars

from .models import Collection, CollectionResult


class CollectionAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "uuid",
        "name",
        "parent_collections",
        "state",
        "trash",
        "organization",
        "created_by",
        "status_changed_at",
    )

    def parent_collections(self, obj):
        return ", ".join(
            [str(c.id) + ": " + c.name for c in obj.collections.all()]
        ).strip(", ")


class CollectionResultAdmin(admin.ModelAdmin):
    list_display = ("id", "uuid", "shortened_result", "collection", "time_created")

    def shortened_result(self, obj):
        return truncatechars(str(obj.result), 100)


admin.site.register(Collection, CollectionAdmin)
admin.site.register(CollectionResult, CollectionResultAdmin)

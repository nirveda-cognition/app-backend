import copy

from rest_framework import serializers

from backend.app.task.models import TaskResult


class DataUpdateSerializer(serializers.ModelSerializer):
    data = serializers.JSONField(required=False)
    table_data = serializers.JSONField(required=False)
    update_data = serializers.JSONField(required=False)
    delete_data = serializers.JSONField(required=False)
    add_data = serializers.JSONField(required=False)

    class Meta:
        model = TaskResult
        fields = ("data", "table_data", "update_data", "delete_data", "add_data")

    def update(self, instance, validated_data):
        new_data = copy.deepcopy(instance.new_data)

        data = validated_data.pop("data", None)
        new_data["data"] = data if data else new_data["data"]

        table_data = validated_data.pop("table_data", None)
        try:
            new_data["table_data"] = (
                table_data if table_data else new_data["table_data"]
            )
        except KeyError:
            pass

        # Alternate way of updating the table data by accepting only
        # table index, table row index, column index and new value.
        # Useful for updating table data of xls files where accepting
        # entire table data was failing the request due to its huge size.

        # TODO: Isolate each line that we are expecting to fail in it's own
        # try/except.
        try:
            update_data = validated_data.pop("update_data", None)
            if update_data:
                user = self.context["request"].user
                if user.organization.type.slug == "apex":  # noqa
                    doc_type = update_data["doc_type"]
                    field = update_data["field"]
                    value = update_data["value"]

                    new_data = copy.deepcopy(instance.new_data)
                    extracted_data = new_data["data"]["extracted_data"]
                    items = []
                    for item in extracted_data:
                        obj_list = [
                            obj
                            for obj in item["items"]
                            if obj["property_id"] == "document_type"
                            and obj["value"] == doc_type
                        ]
                        if obj_list and obj_list[0].get("value") == doc_type:
                            items = item["items"]
                            break

                    for item in items:
                        if item["property_id"] == field:
                            item["value"] = value
                            break

                else:
                    table_index = update_data["table_index"]
                    cell_list = update_data["update_list"]
                    table = [
                        item
                        for item in new_data["data"]["extracted_data"]
                        if item.get("index") == table_index
                    ][0]
                    for table_cell in cell_list:
                        row = [
                            item
                            for item in table["items"]
                            if item["row_index"] == table_cell["row"]
                        ][0]
                        cell = [
                            elem
                            for elem in row["elements"]
                            if elem["column_index"] == table_cell["column"]
                        ][0]
                        cell["value"] = table_cell["value"]

        # TODO: We have got to stop doing this, we shouldn't have these blanket
        # try/except's with large blocks in the try that will blindly suppress
        # serious errors.
        except Exception:
            pass

        # TODO: Isolate each line that we are expecting to fail in it's own
        # try/except.
        try:
            update_data = validated_data.pop("update_header", None)
            if update_data:
                table_index = update_data["table_index"]
                cell_list = update_data["update_list"]
                # value = update_data["value"]
                # print(table_index)
                # print(cell_list)

                table = list(
                    filter(
                        lambda item: item.get("index") == table_index,
                        new_data["data"]["extracted_data"],
                    )
                )[0]

                for table_cell in cell_list:
                    row = [
                        item
                        for item in table["items"]
                        if item["row_index"] == table_cell["row"]
                    ][0]
                    cell = [
                        elem
                        for elem in row["elements"]
                        if elem["column_index"] == table_cell["column"]
                    ][0]
                    cell["value"] = table_cell["value"]

        # TODO: We have got to stop doing this, we shouldn't have these blanket
        # try/except's with large blocks in the try that will blindly suppress
        # serious errors.
        except Exception:
            pass

        # TODO: Isolate each line that we are expecting to fail in it's own
        # try/except.
        try:
            delete_data = validated_data.pop("delete_data", None)
            if delete_data:
                data_type = delete_data.get("data_type")
                table_index = delete_data.get("table_index")

                table = list(
                    filter(
                        lambda item: item.get("index") == table_index,
                        new_data["data"]["extracted_data"],
                    )
                )[0]

                if data_type == "row":
                    header_row = delete_data.get("header_row")
                    if header_row:
                        index = [
                            idx
                            for idx, item in enumerate(table["items"])
                            if item["subtype"] == "header_row"
                        ][0]
                        table["items"][index] = header_row

                    row_indices = delete_data.get("row_indices")
                    table["items"] = list(
                        filter(
                            lambda item: item.get("row_index") not in row_indices,
                            table["items"],
                        )
                    )

                    # Reassign row indices
                    for index, row in enumerate(table["items"]):
                        row["row_index"] = index
                elif data_type == "column":
                    column_index = delete_data.get("column_index")
                    # desc_column_index = delete_data.get("desc_column_index")

                    header_row = list(
                        filter(
                            lambda item: item.get("subtype") == "header_row",
                            table["items"],
                        )
                    )[0]
                    data_rows = list(
                        filter(
                            lambda item: item.get("subtype") == "data_row",
                            table["items"],
                        )
                    )

                    # Delete the values from given column in data rows
                    for val in data_rows:
                        del val["elements"][column_index]

                    # Reassign indices for data rows
                    for val in data_rows:
                        for index, cell in enumerate(val["elements"]):
                            cell["index"] = index
                            cell[column_index] = index

                    # Delete the values from given column in header row
                    del header_row["elements"][column_index]

                    # Reassign indices for header row
                    for idx, cell in enumerate(header_row["elements"]):
                        cell["index"] = idx
                        cell[column_index] = idx

        # TODO: We have got to stop doing this, we shouldn't have these blanket
        # try/except's with large blocks in the try that will blindly suppress
        # serious errors.
        except Exception:
            pass

        # TODO: Isolate each line that we are expecting to fail in it's own
        # try/except.
        try:
            add_data = validated_data.pop("add_data", None)
            if add_data:
                data_type = add_data.get("data_type")
                table_index = add_data.get("table_index")

                table = list(
                    filter(
                        lambda item: item.get("index") == table_index,
                        new_data["data"]["extracted_data"],
                    )
                )[0]

                if data_type == "row":
                    row_index = add_data.get("row_index")
                    table_row = add_data.get("table_row")
                    table["items"].insert(row_index, table_row)

                    # Reassign row indices
                    for index, row in enumerate(table["items"]):
                        row["row_index"] = index
                elif data_type == "column":
                    column_index = add_data.get("column_index")
                    column_cell = add_data.get("column_cell")
                    header_cell = add_data.get("header_cell")

                    for item in table["items"]:
                        if item["subtype"] == "header_row":
                            item["elements"].insert(column_index, header_cell)
                        elif item["subtype"] == "data_row":
                            item["elements"].insert(column_index, column_cell)

                    # Reassign indices
                    for item in table["items"]:
                        for idx, element in enumerate(item["elements"]):
                            element["index"] = idx
                            element["column_cell"] = idx
        # TODO: We have got to stop doing this, we shouldn't have these blanket
        # try/except's with large blocks in the try that will blindly suppress
        # serious errors.
        except Exception:
            pass

        validated_data["new_data"] = new_data
        validated_data["_user"] = self.context["request"].user
        validated_data["_operation"] = "UPDATE"
        return super().update(instance, validated_data)

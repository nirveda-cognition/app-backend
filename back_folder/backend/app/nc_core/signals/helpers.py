import copy

from deepdiff import DeepDiff

from backend.app.collection.models import (
    Collection,
    CollectionPointer,
    CollectionResult,
    TrackedOperation,
)
from backend.app.nc_core.business_logic.business_logic_functions import (
    all_logic,
    formatters,
)
from backend.app.nc_core.business_logic.common import all_extractions
from backend.lib.django_utils.apps import find_model_by_name

OPERATIONS_DICT = dict(TrackedOperation.OPERATION_CHOICES)


def process_collections(collections):
    """
    This function takes a set of collection and reduces them to the unique set
    of collections with schema. Rather than utilizing a recursive search, we
    utilize collection pointers (simply check the collection pointers for each
    collection and store them in a set, returned as to_process).
    """
    to_process = set()
    for collection in collections:
        pointers = list(collection.collectionpointer_set.all())
        if collection.schema:
            to_process.add(collection)
        if len(pointers):
            for pointer in pointers:
                to_process.add(Collection.objects.get(id=pointer.collection_head))
    return to_process


def write_to_tracked_operations(uid, model, operation, collections, user, schm=None):
    """
    Writes an instance to the TrackedOperation table. We process
    post_save signals on this table in process_tracked_operation
    in nc_core/signals/signals.py
    """
    if operation in OPERATIONS_DICT:
        operation = OPERATIONS_DICT[operation]
        tracked_op = TrackedOperation(
            uid=uid,
            model=model,
            operation=operation,
            user=user,
        )
        tracked_op._collections = collections
        tracked_op._collections_to_process = collections
        # TODO: THIS IS A HUGE BUG - If the schema is None (which we are
        # allowing it to be) we get some serious bugs!
        tracked_op._schema = schm
        tracked_op.save()
        # TODO: Is there a reason we are not saving the model again after this
        # operation?
        tracked_op.collections.set([col.id for col in collections])


def get_schema(target):
    """
    Retrieve schema from a Task Result
    """
    CHILD_TO_PARENT_ATTR = {
        "TaskResult": "document",
    }
    CHILD_DATA_KEY = {"TaskResult": "new_data", "new_data": "data"}
    PARENT_TO_CHILD = {"Document": "TaskResult"}

    if target.__class__.__name__ in PARENT_TO_CHILD:
        child_name = PARENT_TO_CHILD[target.__class__.__name__]

        child_model = find_model_by_name(child_name)
        childs_parent_attr = CHILD_TO_PARENT_ATTR[child_name]
        args = {childs_parent_attr: target}
        child = child_model.objects.filter(**args).order_by("-created_at")

        if child.count():
            child = child[0]
            data_keys = []
            key = CHILD_DATA_KEY.get(child_name)
            while key:
                data_keys.append(key)
                key = CHILD_DATA_KEY.get(key)

            child_result = getattr(child, data_keys.pop(0))
            for key in data_keys:
                child_result = child_result.get(key)

            if child_result:
                result_type = child_result["result_type"]
                return result_type


def get_changed_keys(old_key_values, new_key_values):
    """
    Here we use the deepdiff library:
    Warning - There maybe some diff permutations not captured here
    If you notice this function not returning a clearly changed key,
    Most likely you need to add an if statement below based on the
    change type (we arent catching all, just the most common).
    """
    if not old_key_values:
        old_key_values = {}
    diff = DeepDiff(old_key_values, new_key_values, ignore_order=True)
    key_list = set()
    if not len(diff.keys()):
        return key_list
    if diff.get("dictionary_item_added"):
        key_list.update(diff["dictionary_item_added"])
    if diff.get("values_changed"):
        key_list.update(list(diff["values_changed"].keys()))
    if diff.get("dictionary_item_removed"):
        key_list.update(list(diff["dictionary_item_removed"]))
    if diff.get("iterable_item_added"):
        key_list.update(list(diff["iterable_item_added"].keys()))
    if diff.get("type_changes"):
        key_list.update(list(diff["type_changes"].keys()))
    keys = [
        key[key.find("[") + 1 : key.find("]")].replace("'", "")  # noqa
        for key in key_list  # noqa
    ]  # noqa
    return keys


def diff_and_update_collection_result(
    collection_result, parent, new_key_values, operation, schm
):
    """
    Looks up previous collection result (child_data[schema]) and diff that
    with the new result aka feature_dict
    """
    # schema = parent.schema
    schema = schm

    all_schema = collection_result.collection.organization.get_schema()

    # TODO: This is a HUGE bug - we are allowing schema = None to pass through
    # which is causing this to fail!
    schema_data = all_schema.get(schm)
    results = collection_result.result
    if not results:
        collection_result.result = {}
        results = collection_result.result
    child_data = results.get("child_data")
    if not child_data:
        results["child_data"] = {}
        child_data = results["child_data"]
    if not child_data.get(schema):
        child_data[schema] = {}

    has_uuid = hasattr(parent, "uuid")
    if has_uuid and not schema_data.get("singular"):
        uuid = parent.uuid
        current_result = child_data[schema].get(uuid)
        key_diff = get_changed_keys(current_result, new_key_values)

        if operation == "Delete" and current_result:
            child_data[schema].pop(uuid)

        if operation in ("New", "Update"):
            child_data[schema][uuid] = new_key_values

    else:
        current_result = child_data.get(schema)
        key_diff = get_changed_keys(current_result, new_key_values)

        if operation == "Delete" and current_result:
            child_data.pop(schema)

        if operation in ("New", "Update"):
            child_data[schema] = new_key_values

    collection_result.save()
    keys_changed = {schema: key_diff}
    return keys_changed


def run_key_functions(collection_result_uuid, signals, key, schema, parent_uuid=None):
    """
    This triggers signals defined in key_field_listeners. They are passed as a
    list of strings. In this function they are executed in sequence for a
    given key change.
    """
    collection_result = CollectionResult.objects.get(uuid=collection_result_uuid)
    collection = collection_result.collection
    user = collection.owner
    org = user.organization.type.slug
    for signal in signals:
        if signal in all_logic[org]:
            all_logic[org][signal](collection_result_uuid)
        elif signal in all_logic["All"]:
            all_logic["All"][signal](collection_result_uuid, key, schema, parent_uuid)


def process_keys_changed(
    keys_changed, collection_result_uuid, result=False, parent_uuid=None
):
    """
    Result=False
    _____________
    keys_changed - {schema: [list of keys changed in child_data]}
    We loop over the keys_changed and execute their signals as defined in
    key_field_listeners

    Result=True
    ___________
    keys_changed - {schema: [list of keys changed in collection_data]
    We loop over the keys_changed and execute their signals as defined in
    key_fields
    """
    collection_result = CollectionResult.objects.get(uuid=collection_result_uuid)
    all_schema = collection_result.collection.organization.get_schema()

    result_schema = list(keys_changed.keys())[0]
    keys = keys_changed[result_schema]
    cached_result = copy.deepcopy(collection_result.result)

    collection = collection_result.collection
    collection_schema = all_schema[collection.schema]

    # This is how the data model hinges between updates to child_data vs
    # collection_data key_field_listeners means child_data has changed - process
    # key_field_listeners signals
    # key_fields means collection_data has changed - process key_field_signals
    target_key = "key_field_listeners" if not result else "key_fields"
    kf_listeners = collection_schema[target_key].get(result_schema)

    if result:
        kf_listeners = collection_schema[target_key]

    if not kf_listeners:
        return

    result_update = False
    for key in keys:
        if key in list(kf_listeners.keys()):
            if kf_listeners[key].get("signals"):
                result_update = True
                run_key_functions(
                    collection_result_uuid,
                    kf_listeners[key]["signals"],
                    key,
                    result_schema,
                    parent_uuid,
                )
    if result_update:
        collection_result = CollectionResult.objects.get(uuid=collection_result_uuid)
        collection_result._result = cached_result
        collection_result.save(process=False)


def get_feature_dict(result, operation, organization, schema):
    """
    This function uses an extraction function (defined or default)
    to produce a set of keys which have changed in the child_data object of the
    collection result corresponding to the data source schema.
    The return object resembles: {schema: [list of keys changed in child_data]}
    """
    if operation == OPERATIONS_DICT[TrackedOperation.DELETE]:
        return {}

    # result_model_name = result.__class__.__name__
    # schema_model_attr = nc_core_child_to_parent_attr.get(result_model_name)
    # if schema_model_attr:
    #     schema = schm
    #     # schema = getattr(result, schema_model_attr).schema
    # else:
    #     # In the case of moving a document, we dont have a direct pointer to a
    #     # TaskResult, so we retrieve the latest.
    #     schema = result.schema
    #     result = TaskResult.objects.filter(document_id=result.id).latest()

    result_schema = organization.schema.get(schema)
    if not result_schema:
        return None

    if "extraction" in result_schema:
        # Get organization specific extraction_func_name
        extraction_function = all_extractions[organization.type.slug][
            result_schema["extraction"]
        ]
        feature_dict = extraction_function(result)
    else:
        # This is the case of a default extraction function, note
        # that we use property_id at the top level of the AI result
        # As you can see, this will only work for TaskResult currently
        # based on the keys used i.e. data, extracted_data, property_id
        feature_dict = {}

        # TODO: CHANGE THIS BACK
        # key_fields = list(result_schema['schema']['key_fields'].keys())

        key_fields = list(result_schema["key_fields"].keys())
        for item in result.new_data["data"]["extracted_data"]:
            if item["property_id"] in key_fields:
                feature_dict[item["property_id"]] = item["value"]

    return feature_dict


def get_sub_collections(collection):
    subcollections = set()
    subcollections.update(collection.collections.all())
    for child in collection.collections.all():
        if child.collections.count() != 0:
            nested_sub_collections = get_sub_collections(child)
            subcollections.update(nested_sub_collections)
    return subcollections


def diff_collections(c1, c2):
    c1 = process_collections(c1)
    c2 = process_collections(c2)
    return c1 - c2


def filter_pointers(from_collection, pointers, sub_col=None):
    from_collections = from_collection.collections.all()
    from_pointers = set()
    if sub_col:
        sub_col_ids = [s.id for s in sub_col]
        from_collections = from_collections.exclude(id__in=sub_col_ids)
    for collection in from_collections:
        p = list(collection.collectionpointer_set.all())
        p.extend(list(CollectionPointer.objects.filter(collection_head=collection.id)))
        from_pointers.update(p)

    return pointers - from_pointers


def format_result(collection_result):
    """
    Called in process_cr_update after collection_data
    updates we format the result. This logic uses
    the formatters defined for a given result schema (key_fields)
    to format collection_data keys

    TODO: This is how we will address product specific functionality hinging
    in the short term.
    """
    schema = collection_result.collection.organization.get_schema()[
        collection_result.collection.schema
    ]

    preformatted_result = collection_result.result[schema["key_field_property"]]

    collection_result.result["presentation_data"] = {}
    for k, v in preformatted_result.items():
        key_schema = schema["key_fields"].get(k, {})
        if key_schema.get("type") == "dict" and "key_fields" in key_schema:
            collection_result.result["presentation_data"][k] = {}
            for sub_k, sub_v in v.items():
                if (
                    sub_k in key_schema["key_fields"]
                    and "formatter" in key_schema["key_fields"][sub_k]
                ):
                    sub_v = formatters[key_schema["key_fields"][sub_k]["formatter"]](
                        sub_v
                    )

                collection_result.result["presentation_data"][k][sub_k] = sub_v
        else:
            value = v
            if "formatter" in key_schema:
                value = formatters[key_schema["formatter"]](v)

            collection_result.result["presentation_data"][k] = value

import logging

import requests
from django.conf import settings
from django.http import Http404, JsonResponse
from jsonpath_rw import parse
from rest_framework import decorators, exceptions, generics, mixins

from backend.app.common.views import UpdateAPIView
from backend.app.document.models import Document
from backend.app.task.models import TaskResult
from backend.app.task.utils import (
    convert_df_to_table,
    convert_table_to_df,
    generate_agg_table,
)
from backend.lib.aws.nv_bucket_manager import NVBucketManager
from backend.lib.aws.nv_s3_bucket import NVs3bucket

from .serializers import DataUpdateSerializer

__all__ = (
    "ResultUpdateView",
    "get_document_tables",
    "normalize_document_tables",
    "modify_table",
    "ModifyColumnsView",
    "update_multiple_tables",
)


logger = logging.getLogger("backend")


@decorators.api_view(["PUT"])
def upload(request, key):
    """
    This is a KPMG specific function that allows us to upload files from the FE
    to the backend so that the backend can make the authenticated calls for
    uploading the documents to the storage bucket and relay the file to s3

    Because KPMG is behind 900000 firewalls we are also uploading the file to
    s3 so it will be accessible to AI.

    This is kind of mental

    TODO: We will need to delete the file from s3 after the extraction!
    """
    upload = NVBucketManager()
    put_url = upload.generate_put_presigned_url(key)

    try:
        resp = requests.put(put_url, data=request.body, headers=upload.headers)
    except requests.exceptions.RequestException as e:
        logger.error("The upload failed: " % str(e), extra={"key": key})
        raise exceptions.ParseError("The upload failed.")
    else:
        try:
            resp.raise_for_status()
        except requests.exceptions.HTTPError as e:
            logger.error("The upload failed: %s" % str(e), extra={"key": key})
            raise exceptions.ParseError("The upload failed.")
        else:
            s3upload = NVs3bucket()
            s3_put_url = s3upload.generate_put_presigned_url(
                settings.NC_AWS_BUCKET_NAME, key
            )

            try:
                resp = requests.put(
                    s3_put_url,
                    data=request.body,
                    headers=upload.headers,
                    proxies={
                        "http": settings.API_PROXY_URL,
                        "https": settings.APIS_PROXY_URL,
                    },
                    allow_redirects=True,
                )
            except requests.exceptions.RequestException as e:
                logger.error("The upload failed: %s" % str(e), extra={"key": key})
                raise exceptions.ParseError("The upload failed.")
            else:
                try:
                    resp.raise_for_status()
                except requests.exceptions.HTTPError as e:
                    logger.error("The upload failed: %s" % str(e), extra={"key": key})
                    raise exceptions.ParseError("The upload failed.")
                else:
                    return JsonResponse({"key": key})


def get_unique_column_name(column_name, elements):
    """
    Given a column name, return a unique name of the form name[_][count].
    """
    name_list = [column["value"] for column in elements]
    try:
        name_list.index(column_name)
        name_count_lst = column_name.rsplit("_", 1)

        # Case when the input column name has no "_" in it.
        if len(name_count_lst) == 1:
            new_column_name = column_name + "_1"
        else:
            name_part, count = column_name.rsplit("_", 1)
            new_column_name = name_part + "_" + str(int(count) + 1)
        return get_unique_column_name(new_column_name, elements)
    except ValueError:
        return column_name


@decorators.api_view(["GET"])
def get_document_tables(request):
    doc_id = request.GET.get("id")
    if doc_id is None:
        # TODO: Raise a custom exception or a more appropriate rest framework
        # exception.  This should more likely be an error indicating that the
        # parameter is missing and required...
        return JsonResponse({"response": "Documents not found."})

    pk_list = doc_id.split(",")
    return JsonResponse(
        {
            "status": 200,
            "payload": generate_agg_table(
                pk_list,
                column_list=[
                    "Description",
                    ["Amount", "Cost"],
                    "Date",
                    "Summary",
                    "Tax_Category",
                    "RoC",
                    "Confidence",
                ],
            ),
        }
    )


@decorators.api_view(["GET"])
def normalize_document_tables(request):
    doc_id = request.GET.get("id")
    if doc_id is None:
        # TODO: Raise a custom exception or a more appropriate rest framework
        # exception.  This should more likely be an error indicating that the
        # parameter is missing and required...
        return JsonResponse({"response": "Documents not found."})

    pk_list = doc_id.split(",")
    return JsonResponse(
        {
            "status": 200,
            "payload": generate_agg_table(
                pk_list,
                column_list=[
                    "Description",
                    ["Amount", "Cost"],
                    "Date",
                    "Tax_Category",
                    "RoC",
                    "Confidence",
                ],
            ),
        }
    )


@decorators.api_view(
    [
        "POST",
    ]
)
def modify_table(request, pk=None):
    meta = request.data.get("add_data")
    if meta is None:
        return JsonResponse({"result": 200})

    try:
        doc = Document.objects.get(pk=pk)
    except Document.DoesNotExist:
        raise Http404("The document does not exist.")
    else:
        if doc.task is None:
            raise Http404("The document does not have an associated task.")
        try:
            result = doc.task.results.latest()
        except Document.DoesNotExist:
            raise Http404("The document task does not have any associated results.")

    tables = []
    for table in result.new_data["data"]["extracted_data"]:
        if table["type"] == "collection" and table["subtype"] == "table":
            tables.append(table)

    table_index = meta["table_index"] - 1
    column_index = meta["column_index"]

    # this is currently simply returning the correct table
    new_table = tables[table_index]
    temp_table = convert_table_to_df(new_table["items"])

    # add column via pandas. EG something like:
    blanks = [""] * len(temp_table)

    # Generate unique new column name.
    new_column_name = get_unique_column_name(
        "New Column", new_table["items"][0]["elements"]
    )
    temp_table.insert(column_index, new_column_name, blanks, True)
    temp_table = convert_df_to_table(temp_table)
    new_table["items"] = temp_table["items"]

    # Save updated data in database.
    result.new_data["data"]["extracted_data"][table_index]["items"] = temp_table[
        "items"
    ]
    result.save()

    return JsonResponse({"result": 200, "table": new_table})


@decorators.api_view(
    [
        "POST",
    ]
)
def update_multiple_tables(request):
    update_data_dict = request.data.get("update_data")
    if update_data_dict:
        for doc_pk, value_list in update_data_dict.items():
            task = Document.objects.get(pk=doc_pk).task
            task_result = TaskResult.objects.filter(task=task).first()
            new_data = task_result.new_data
            for update_data in value_list:
                table_index = update_data["table_index"]
                cell_list = update_data["update_list"]

                table = list(
                    filter(
                        lambda item: item.get("index") == table_index,
                        new_data["data"]["extracted_data"],
                    )
                )[0]

                for table_cell in cell_list:
                    row = [
                        item
                        for item in table["items"]
                        if item["row_index"] == table_cell["row"]
                    ][0]
                    cell = [
                        elem
                        for elem in row["elements"]
                        if elem["column_index"] == table_cell["column"]
                    ][0]
                    cell["value"] = table_cell["value"]

            # Save updated data in database.
            task_result.new_data = new_data
            task_result.save()

    return JsonResponse({"result": 200})


class ModifyColumnsView(generics.GenericAPIView, mixins.UpdateModelMixin):
    def patch(self, request):
        update_data = request.data.get("add_data")
        pk = update_data.get("document_id", None)
        table_index = update_data.get("table_index", None)
        column_index = update_data.get("column_index", None)

        if pk is not None and table_index is not None:
            doc = Document.objects.get(pk=pk)
        if doc is not None:
            result = (
                TaskResult.objects.filter(document_id=doc.id)
                .order_by("-created_at")
                .first()
            )
        if result is not None:
            doc_data = result.new_data

        # This uses jsonpath_rw for parsing large objects.
        # This function is a test... we could use it other places.
        json_search_path = parse("$..extracted_data[*]")
        found_index = None
        for match in json_search_path.find(doc_data):
            if (
                match.value.get("subtype") == "table"
                and match.value.get("index") == table_index
            ):
                found_index = int(str(match.path).replace("[", "").replace("]", ""))

        if found_index is not None:

            try:
                table = doc_data["data"]["extracted_data"][found_index]
            except KeyError:
                # TODO see if this fails in other ways
                # TODO raise standard drf error
                return JsonResponse({"error": "TODO: Make this a real DRF Error"})

            temp_table = convert_table_to_df(table["items"])

            # add blank column
            blank_column = [""] * len(temp_table)

            # Generate unique new column name.
            new_column_name = get_unique_column_name(
                "New Column", table["items"][0]["elements"]
            )
            temp_table.insert(column_index, new_column_name, blank_column, True)
            temp_table = convert_df_to_table(temp_table)

            # Save updated data in database.
            result.new_data["data"]["extracted_data"][found_index][
                "items"
            ] = temp_table["items"]
            result.save()

            data = {}
            data["table"] = result.new_data["data"]["extracted_data"][found_index]
            return JsonResponse(data)

        return JsonResponse({})


class ResultUpdateView(UpdateAPIView):
    queryset = TaskResult.objects.all()
    serializer_class = DataUpdateSerializer
    model = TaskResult

    def get_object(self):
        document = Document.objects.get(pk=self.kwargs["pk"])
        if document.task is None:
            raise Http404()
        try:
            return document.task.results.latest()
        except Document.DoesNotExist:
            raise Http404()

    def update_or_raise(self, request, *args, **kwargs):
        return super().update_or_raise(request, *args, **kwargs)

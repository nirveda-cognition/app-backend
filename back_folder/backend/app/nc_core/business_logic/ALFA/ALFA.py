import copy
from datetime import datetime

from dateutil.relativedelta import relativedelta

from backend.app.collection.models import CollectionResult
from backend.app.nc_core.business_logic.helpers import (
    collect_by_key,
    parseNumericalField,
    total,
)


def collect_invoice_accounts(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}

    target_dict = result["child_data"]["invoice"]
    all_accounts = collect_by_key(target_dict, "account_num")

    result["collection_data"]["invoice_accounts"] = all_accounts
    collection_result.save()


def calculate_invoice_totals(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}

    target_results = result["child_data"]["invoice"]
    invoice_totals = collect_by_key(target_results, "amount_total")
    invoice_totals = total(invoice_totals)
    result["collection_data"]["invoice_totals"] = invoice_totals
    collection_result.save()


def collect_expense_accounts(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}

    target_dict = result["child_data"]["expense"]
    all_accounts = collect_by_key(target_dict, "account_num")

    result["collection_data"]["expense_accounts"] = all_accounts
    collection_result.save()


def calculate_expense_totals(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}

    target_results = result["child_data"]["expense"]
    expense_totals = collect_by_key(target_results, "amount_total")
    expense_totals = total(expense_totals)
    result["collection_data"]["expense_totals"] = expense_totals
    collection_result.save()


def calculate_net_income(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}

    collection_data = result["collection_data"]
    expense_totals = collection_data.get("expense_totals", 0)
    invoice_totals = collection_data.get("invoice_totals", 0)

    net = invoice_totals - expense_totals
    collection_data["net_income"] = net
    collection_result.save()


def update_schedule_a_data(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}
    gusto_data = result["child_data"]["gusto_ppp_forgiveness_report"]
    schedule_a = gusto_data["schedule_a"]
    if not result["collection_data"].get("schedule_a"):
        result["collection_data"]["schedule_a"] = {}
    for item in schedule_a:
        line = item["Schedule A line"].lower().replace(" ", "_")
        result["collection_data"]["schedule_" + line] = parseNumericalField(
            item["Amount or number to enter"]
        )
    collection_result.save()


def update_alerts(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}
    gusto_data = result["child_data"]["gusto_ppp_forgiveness_report"]
    alerts = gusto_data["alerts"]
    result["collection_data"]["alerts"] = alerts

    collection_result.save()


def process_filtered_transactions(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data", {})
    transactions = cd["filtered_transactions"]
    if not transactions:
        return

    filt_transactions_mortgage = transactions.get("mortgage_interest", [])
    filt_transactions_util = transactions.get("utilities", [])
    filt_transactions_rent = transactions.get("rent", [])
    all_filt_transactions = (
        filt_transactions_mortgage + filt_transactions_util + filt_transactions_rent
    )
    result["collection_data"]["mortgage_expense_total"] = sum(
        t["amount"] for t in filt_transactions_mortgage
    )
    result["collection_data"]["utilities_expense_total"] = sum(
        t["amount"] for t in filt_transactions_util
    )
    result["collection_data"]["rent_expense_total"] = sum(
        t["amount"] for t in filt_transactions_rent
    )
    result["collection_data"]["non_payroll_expense_total"] = sum(
        t["amount"] for t in all_filt_transactions
    )
    collection_result.save()


def calc_non_payroll_expense_total(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result

    mortgage_expense_total = parseNumericalField(
        result["collection_data"]["mortgage_expense_total"]
    )
    utilities_expense_total = parseNumericalField(
        result["collection_data"]["utilities_expense_total"]
    )
    rent_expense_total = parseNumericalField(
        result["collection_data"]["rent_expense_total"]
    )
    non_payroll_expense_total = (
        mortgage_expense_total + utilities_expense_total + rent_expense_total
    )
    result["collection_data"]["non_payroll_expense_total"] = non_payroll_expense_total
    collection_result.result = result
    collection_result.save()


def process_covered_period(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    if not result.get("collection_data"):
        result["collection_data"] = {}
    gusto_data = result["child_data"]["gusto_ppp_forgiveness_report"]
    cp = gusto_data.get("covered_period")
    if not cp:
        return
    cp = cp.split(" to ")
    if len(cp) != 2:
        cp = cp.split(" - ")
        if len(cp) != 2:
            return
    result["collection_data"]["covered_period_start"] = cp[0]
    result["collection_data"]["covered_period_end"] = cp[1]
    collection_result.save()


def calc_ppp_line_6(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result

    if not result.get("collection_data"):
        result["collection_data"] = {}

    mortgage = parseNumericalField(
        result["collection_data"].get("mortgage_expense_total", 0)
    )
    rent = parseNumericalField(result["collection_data"].get("rent_expense_total", 0))
    utilities = parseNumericalField(
        result["collection_data"].get("utilities_expense_total", 0)
    )
    total_payroll_cost = parseNumericalField(
        result["collection_data"].get("schedule_line_10")
    )
    wage_reduction = parseNumericalField(
        result["collection_data"].get("schedule_line_3")
    )
    ppp_line_6 = (total_payroll_cost + mortgage + rent + utilities) - wage_reduction
    result["collection_data"]["ppp_line_6"] = ppp_line_6
    collection_result.result = result
    collection_result.save()
    return collection_result


def calc_ppp_line_8(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result

    if not result.get("collection_data"):
        result["collection_data"] = {}

    # Need to figure out how to process keys based on dependencies
    # Becomes an issue when attempting to process individual keys out of order
    # Nesting those keys solves this since the object is then keyed uniquely
    # And the order of the nested keys is irrelevant (since the biz logic
    # function
    # Operates on the dict as a whole). But this is inefficient for update logic
    # since an update can be triggered on an individual key

    result = calc_ppp_line_6(collection_result.uuid).result
    ppp_line_6 = result["collection_data"].get("ppp_line_6")

    ppp_line_7 = result["collection_data"].get("schedule_line_13")
    ppp_line_8 = parseNumericalField(ppp_line_6) * parseNumericalField(ppp_line_7)
    result["collection_data"]["ppp_line_8"] = ppp_line_8
    collection_result.result = result
    collection_result.save()
    return collection_result


def calc_ppp_line_10(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result

    if not result.get("collection_data"):
        result["collection_data"] = {}

    ppp_line_10 = (
        parseNumericalField(result["collection_data"].get("schedule_line_10", 0)) / 0.6
    )
    result["collection_data"]["ppp_line_10"] = ppp_line_10
    collection_result.result = result
    collection_result.save()
    return collection_result


def calc_ppp_line_11(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")
    if not cd:
        result["collection_data"] = {}

    calc_ppp_line_8(collection_result.uuid)
    result = calc_ppp_line_10(collection_result.uuid).result

    ppp_line_8 = parseNumericalField(result["collection_data"].get("ppp_line_8", 0))
    ppp_line_9 = parseNumericalField(
        result["collection_data"].get("total_loan_amount", 0)
    )
    ppp_line_10 = parseNumericalField(result["collection_data"].get("ppp_line_10", 0))
    ppp_line_11 = min(ppp_line_8, ppp_line_9, ppp_line_10)
    result["collection_data"]["ppp_line_11"] = ppp_line_11
    collection_result.result = result
    collection_result.save()


def calc_schedule_line_10(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")
    line_1 = parseNumericalField(cd.get("schedule_line_1", 0))
    line_4 = parseNumericalField(cd.get("schedule_line_4", 0))
    line_6 = parseNumericalField(cd.get("schedule_line_6", 0))
    line_7 = parseNumericalField(cd.get("schedule_line_7", 0))
    line_8 = parseNumericalField(cd.get("schedule_line_8", 0))
    line_9 = parseNumericalField(cd.get("schedule_line_9", 0))
    line_10 = line_1 + line_4 + line_6 + line_7 + line_8 + line_9
    cd["schedule_line_10"] = line_10
    collection_result.result = result
    collection_result.save()


def build_ppp_application(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result

    if not result.get("collection_data"):
        result["collection_data"] = {}
    schedule_a = result["collection_data"].get("schedule_a")
    if not schedule_a:
        return
    mortgage = result["collection_data"].get("mortgage_expense_total")
    rent = result["collection_data"].get("rent_expense_total")
    utilities = result["collection_data"].get("utilities_expense_total")

    app = {}
    app["line_1"] = parseNumericalField(schedule_a["line_10"])  # payroll cost
    app["line_2"] = mortgage or 0
    app["line_3"] = rent or 0
    app["line_4"] = utilities or 0
    app["line_5"] = parseNumericalField(schedule_a["line_3"])  # wage reduction
    app["line_6"] = (
        app["line_1"] + app["line_2"] + app["line_3"] + app["line_4"]
    ) - app["line_5"]
    app["line_7"] = parseNumericalField(schedule_a["line_13"])
    app["line_8"] = app["line_6"] * app["line_7"]
    app["line_9"] = parseNumericalField(
        result["collection_data"].get("total_loan_amount") or 0
    )
    app["line_10"] = parseNumericalField(schedule_a["line_10"]) / 0.6
    app["line_11"] = min(app["line_8"], app["line_9"], app["line_10"])
    result["collection_data"]["ppp_application"] = app
    collection_result.save()


def process_accrued_interest(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")
    if not cd:
        result["collection_data"] = {}
        cd = result["collection_data"]
    int_rate = cd.get("interest_rate")
    if not int_rate:
        cd["interest_rate"] = 1
        int_rate = cd["interest_rate"]
    disb_date = cd.get("disbursement_date")
    if not disb_date:
        cd["disbursement_date"] = "4/30/20"
        disb_date = datetime.strptime("4/30/20", "%m/%d/%y")

    # We have to stop doing this - we need to catch the specific error here.
    else:
        try:
            disb_date = datetime.strptime(disb_date, "%d-%B-%y")
        except:  # noqa
            try:
                disb_date = datetime.strptime(disb_date, "%d-%b-%y")
            except:  # noqa
                return

    loan_total = cd.get("total_loan_amount")
    if not loan_total:
        cd["total_loan_amount"] = "$323,783.00"
        loan_total = cd["total_loan_amount"]

    loan_total = float(loan_total.replace("$", "").replace(",", ""))

    total_days = (datetime.now() - disb_date).days
    accrued_interest = (loan_total * (int_rate / 100.0)) * (total_days / 365.0)
    cd["accrued_interest"] = accrued_interest
    collection_result.save()


def calculate_forgiveness_period(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")

    if not cd.get("disbursement_date"):
        return

    cp_start = cd.get("covered_period_start")
    db = cd.get("disbursement_date")

    # We have to stop doing this - we need to catch the specific error here.
    try:
        db = datetime.strptime(db, "%m/%d/%y")
    except:  # noqa
        try:
            db = datetime.strptime(db, "%d-%b-%y")
        except:  # noqa
            return

    if cp_start:
        cp_start = datetime.strptime(cp_start, "%m/%d/%Y")

    # Sounds like this date doesn't really matter currently since either way,
    # we are adding 24 weeks to the start of db_date/cp_start
    case_1_deadline = datetime.strptime("31-Dec-20", "%d-%b-%y")

    fp_start = cp_start if cp_start else db
    fp_end = db + relativedelta(weeks=+24)

    if fp_end > case_1_deadline:
        fp_end = case_1_deadline

    cd["forgiveness_period_start"] = fp_start.strftime("%m-%d-%Y")
    cd["forgiveness_period_end"] = fp_end.strftime("%m-%d-%Y")
    collection_result.save()


def calc_box_1_2(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")
    box_1 = 0
    box_2 = 0
    ws = cd["schedule_a_worksheet"]
    for row in ws[: len(ws) - 1]:
        box_1 += parseNumericalField(row["Adjusted covered wages"])
        box_2 += parseNumericalField(row["FTE"])

    ws[len(ws) - 1]["Adjusted covered wages"] = "Box 1: $" + str(round(box_1, 2))
    ws[len(ws) - 1]["FTE"] = "Box 2: " + str(round(box_2, 2))
    cd["box_1"] = box_1
    cd["box_2"] = box_2
    cd["schedule_line_1"] = box_1
    cd["schedule_line_2"] = box_2
    collection_result.save()


def calc_box_3(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")
    r = cd["reduction"]
    box_3 = 0
    for row in r[: len(r) - 1]:
        box_3 += parseNumericalField(row["Reduction"])

    r[len(r) - 1]["Reduction"] = "Box 3: $" + str(round(box_3, 2))
    cd["box_3"] = box_3
    cd["schedule_line_3"] = box_3
    collection_result.save()


def calc_box_4_5(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")
    box_4 = 0
    box_5 = 0
    ws = cd["schedule_a_worksheet_more_than_100k"]
    for row in ws[: len(ws) - 1]:
        box_4 += parseNumericalField(row["Adjusted covered wages"])
        box_5 += parseNumericalField(row["FTE"])

    ws[len(ws) - 1]["Adjusted covered wages"] = "Box 4: $" + str(round(box_4, 2))
    ws[len(ws) - 1]["FTE"] = "Box 5: " + str(round(box_5, 2))
    cd["box_4"] = box_4
    cd["box_5"] = box_5
    cd["schedule_line_4"] = box_4
    cd["schedule_line_5"] = box_5
    collection_result.save()


def filter_transactions(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = collection_result.result
    cd = result.get("collection_data")
    transactions = cd.get("transactions", [])
    filtered_transactions = {"utilities": [], "mortgage_interest": []}
    for t in transactions:
        if "Mortgage Interest" == t["top_level_category"]:
            filtered_transactions["mortgage_interest"].append(t)
        if "Utilities" in t["top_level_category"]:
            filtered_transactions["utilities"].append(t)
    cd["filtered_transactions"] = filtered_transactions
    collection_result.save()


def reprocess_reductions(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = copy.deepcopy(collection_result.result)
    cd = result.get("collection_data")
    worksheet = cd.get("schedule_a_worksheet")
    reduction_table = cd.get("reduction")
    worksheet_reductions = [
        item for item in worksheet if item["Reduction? (Y/N)"] in ("Y", "Yes", "yes")
    ]
    reduction_names = [item["Employee name"] for item in reduction_table]
    new_reductions = [
        item
        for item in worksheet_reductions
        if item["Employee name"] not in reduction_names
    ]
    totals = reduction_table.pop()
    if len(new_reductions):
        for item in new_reductions:
            q1_pr = item["Avg pay rate Q1"]
            emp_type = "yearly" if "year" in q1_pr else "hourly"
            reduction_table.append(
                {
                    "75 % of Q1 Rate": "",
                    "Average Q1 Weekly Hours": "",
                    "Employee name": item["Employee name"],
                    "Employee type": emp_type,
                    "Reduction": "",
                    "Reduction in rate": "",
                }
            )
    reduction_table.append(totals)
    result["collection_data"]["reduction"] = reduction_table
    collection_result.result = result
    collection_result.save()


def calculate_invoice_expense_totals(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = copy.deepcopy(collection_result.result)

    child_data = result["child_data"]
    invoices = child_data.get("invoice", {})

    all_invoice_total = 0
    for key in invoices.keys():
        invoice = invoices[key]
        invoice_total = parseNumericalField(invoice["amount_total"])
        all_invoice_total += invoice_total

    result["collection_data"]["non_payroll_expense_total"] = all_invoice_total
    collection_result.result = result
    collection_result.save()


def calculate_schedule_line_12(result_uuid):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    result = copy.deepcopy(collection_result.result)

    cd = result["collection_data"]
    line2 = parseNumericalField(cd["schedule_line_2"])
    line5 = parseNumericalField(cd["schedule_line_5"])

    total_fte = line2 + line5
    result["collection_data"]["schedule_line_12"] = total_fte
    collection_result.result = result
    collection_result.save()

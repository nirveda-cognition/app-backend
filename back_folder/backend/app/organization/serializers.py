from rest_framework import serializers, validators
from schema import Schema

from backend.app.authentication.serializers import GroupSerializer
from backend.app.user.serializers import UserSerializer
from backend.conf.constants import HttpMethods
from backend.lib.rest_framework_utils.fields import SchemaField
from backend.lib.rest_framework_utils.serializers import EnhancedModelSerializer

from .models import Organization, OrganizationType

ORGANIZATION_JSON_INFO_SCHEMA = Schema({"document-extractor": [str]})


class OrganizationTypeSerializer(serializers.ModelSerializer):
    title = serializers.CharField(read_only=True)
    slug = serializers.SlugField(read_only=True)
    description = serializers.CharField(read_only=True)

    class Meta:
        model = OrganizationType
        fields = ("id", "slug", "title", "description")


class OrganizationSerializer(EnhancedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(
        validators=[validators.UniqueValidator(queryset=Organization.objects.all())]
    )
    slug = serializers.SlugField(
        required=True,
        validators=[validators.UniqueValidator(queryset=Organization.objects.all())],
    )
    description = serializers.CharField(
        allow_null=True, allow_blank=True, required=False
    )
    type = OrganizationTypeSerializer()
    num_users = serializers.IntegerField(read_only=True)
    num_groups = serializers.IntegerField(read_only=True)
    json_info = SchemaField(schema=ORGANIZATION_JSON_INFO_SCHEMA)
    logo = serializers.CharField(required=False)
    is_active = serializers.BooleanField(default=True, required=False)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    api_access = serializers.BooleanField(read_only=True)
    api_key = serializers.CharField(read_only=True)
    api_url = serializers.URLField(
        required=False,
        allow_blank=True,
        allow_null=True,
    )
    callback_whitelist = serializers.CharField(read_only=True)
    users = UserSerializer(
        many=True,
        read_only=True,
        nested=True,
    )
    groups = GroupSerializer(many=True, read_only=True, nested=True)

    class Meta:
        model = Organization
        nested_fields = (
            "id",
            "name",
            "description",
            "type",
            "num_users",
            "json_info",
            "logo",
            "is_active",
            "created_at",
            "updated_at",
            "slug",
            "num_groups",
        )
        fields = nested_fields + (
            "api_access",
            "api_key",
            "api_url",
            "logo",
            "callback_whitelist",
            "users",
            "groups",
        )
        response = {
            "type": OrganizationTypeSerializer,
        }
        http_toggle = {
            "type": {
                (HttpMethods.POST, HttpMethods.PATCH): (
                    serializers.PrimaryKeyRelatedField,
                    {
                        "queryset": OrganizationType.objects.all(),
                        "allow_null": False,
                        "required": True,
                    },
                )
            }
        }

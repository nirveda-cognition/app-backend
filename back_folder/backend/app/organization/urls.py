from django.urls import include, path
from rest_framework import routers

from backend.app.authentication.urls import (
    organization_urlpatterns as authentication_organization_urlpatterns,
)
from backend.app.task.urls import (
    organization_urlpatterns as task_organization_urlpatterns,
)
from backend.app.user.urls import (
    organization_urlpatterns as user_organization_urlpatterns,
)

from .views import OrganizationTypeViewSet, OrganizationViewSet

app_name = "organization"

router = routers.SimpleRouter()
router.register(r"types", OrganizationTypeViewSet, basename="organization-type")
router.register(r"", OrganizationViewSet, basename="organization")

urlpatterns = router.urls + [
    path(
        "<int:organization_pk>/",
        include(
            [
                path("collections/", include("backend.app.collection.urls")),
                path("documents/", include("backend.app.document.urls")),
                path("tasks/", include(task_organization_urlpatterns)),
                path("users/", include(user_organization_urlpatterns)),
                path("groups/", include(authentication_organization_urlpatterns)),
            ]
        ),
    ),
]

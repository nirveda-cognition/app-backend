from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.html import strip_tags

from backend.app.authentication.models import ResetUID
from backend.lib.utils.urls import add_query_params_to_url, make_url_absolute


def email_new_client_keys(client_keys, user, organization):
    uid = ResetUID.objects.create(user=user)
    relative_url = reverse("nc_core:retrieve-client-secret")
    url = add_query_params_to_url(
        make_url_absolute(relative_url), api_key=client_keys.client_id, uid=uid.token
    )
    html_message = render_to_string(
        "email/regenerate_client_secret.html",
        {
            "url": url,
            "api_url": settings.APP_URL,
            "from_email": settings.FROM_EMAIL,
            "email": user.email,
            "brand": organization.brand,
            "name": user.email,
            "api_key": client_keys.client_id,
        },
    )
    mail = EmailMultiAlternatives(
        "Nirveda API Client Secret",
        strip_tags(html_message),
        settings.FROM_EMAIL,
        [user.email],
    )
    mail.attach_alternative(html_message, "text/html")
    if settings.EMAIL_ENABLED:
        mail.send()

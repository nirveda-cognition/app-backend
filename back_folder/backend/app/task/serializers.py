import copy
import logging

from rest_framework import serializers

from backend.app.document.models import DocumentStatus
from backend.lib.rest_framework_utils.exceptions import InvalidFieldError

from .models import Task, TaskResult

logger = logging.getLogger("backend")


class TaskStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    status = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)

    class Meta:
        model = DocumentStatus
        fields = ("id", "status", "description")


class TaskResultSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    uuid = serializers.UUIDField(read_only=True)
    data = serializers.DictField(read_only=True)
    new_data = serializers.DictField(read_only=True)
    created_at = serializers.DateTimeField()
    updated_at = serializers.DateTimeField()

    class Meta:
        model = TaskResult
        fields = ("id", "uuid", "data", "new_data", "created_at", "updated_at")


class TaskSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    uuid = serializers.CharField(read_only=True)
    ai_uuid = serializers.CharField(read_only=True)
    status = TaskStatusSerializer(read_only=True)
    organization = serializers.PrimaryKeyRelatedField(read_only=True)
    request = serializers.DictField(read_only=True)
    client_request = serializers.DictField(read_only=True)
    response = serializers.DictField(read_only=True)
    backend_id = serializers.CharField(read_only=True)
    created_at = serializers.DateTimeField()
    updated_at = serializers.DateTimeField()
    results = TaskResultSerializer(many=True)

    class Meta:
        model = Task
        fields = (
            "id",
            "uuid",
            "ai_uuid",
            "status",
            "organization",
            "request",
            "client_request",
            "response",
            "backend_id",
            "created_at",
            "updated_at",
            "results",
        )


class UpdateApexTaskResultSerializer(serializers.ModelSerializer):
    """
    A :obj:`serializers.ModelSerializer` class to handle updates for a
    :obj:`TaskResult` instance in the context of the APEX organization.

    NOTE:
    ----
    This is just APEX specific for now, eventually we need to come up with
    a more systematic approach for updating document task results across
    organizations.  Most of this logic should live outside the serializer and
    either in methods on the model or logic external to the serializer.
    """

    doc_type = serializers.ChoiceField(
        choices=[
            "invoice",
            "change_order",
            "receiver",
            "jde",
            "packing_slip",
            "purchase_order",
        ]
    )
    field = serializers.ChoiceField(
        choices=[
            "change_order_no",
            "freight",
            "invoice_date",
            "invoice_due_date",
            "invoice_description",
            "invoice_number",
            "payment_terms",
            "po_number",
            "remittance_address",
            "sub_total",
            "tax_total",
            "total_amount",
            "vendor_number",
        ]
    )
    value = serializers.CharField(allow_null=False, allow_blank=True)

    class Meta:
        model = TaskResult
        fields = ("doc_type", "value", "field")

    def validate(self, attrs):
        new_data = copy.deepcopy(self.instance.new_data)
        found_document_type = False

        def find_page_items_on_page(page, field):
            # Find the specific items in the page_item for which the property_id
            # is equal to the field we are updating.
            page_items = []
            # There might be multiple items in the page for the given field.
            for page_item in page["items"]:
                if page_item["property_id"] == field:
                    page_items.append(page_item)
            return page_items

        def find_document_type_for_page(page):
            # Find Document Type for Page
            try:
                document_type_item = [
                    item
                    for item in page["items"]
                    if item["property_id"] == "document_type"
                ][0]
            except IndexError:
                return None
            else:
                return document_type_item["value"]

        def update_page_item(item, field, value):
            if item["value"] != value:
                logger.info(
                    "Updating Field {field} on Page {page} from "
                    "{current_value} to {new_value}.".format(
                        page=page_item["label"],
                        field=field,
                        current_value=item["value"],
                        new_value=value,
                    ),
                    extra={
                        "field": field,
                        "current_value": item["value"],
                        "new_value": value,
                    },
                )
                item["value"] = value
            else:
                logger.warning(
                    "Not Updating Field {field} on Page {page} - "
                    "{current_value} = {new_value}.".format(
                        page=page_item["label"],
                        field=field,
                        current_value=item["value"],
                        new_value=value,
                    ),
                    extra={
                        "field": field,
                        "current_value": item["value"],
                        "new_value": value,
                    },
                )

        def add_page_item(page, field, value):
            logger.info(
                "Adding new item with field {field} and value "
                "{value} to page {page}.".format(
                    field=field, value=value, page=page["label"]
                ),
                extra={"field": field, "value": value, "page": page["label"]},
            )
            # Note there is no confidence, regions, label or index here.
            page["items"].append(
                {
                    "type": "field",
                    "value": value,
                    "property_id": field,
                    "label": field.replace("_", " ").title(),
                    "manually_set": True,
                }
            )

        # Loop over the pages in the extracted data.
        extracted_data = new_data["data"]["extracted_data"]
        for page in extracted_data:
            # Find Document Type for Page
            document_type = find_document_type_for_page(page)
            if document_type is None:
                logger.warn(
                    "Could not identify the document type for page %s - "
                    "there is not an item with `property_id` equal to "
                    "`document_type`." % page["label"],
                    extra={
                        "page": page,
                        "extracted_data": extracted_data,
                    },
                )
                continue

            if document_type == attrs["doc_type"]:
                found_document_type = True

                # Find the page items on the page that correspond to the
                # provided field.
                page_items = find_page_items_on_page(page, attrs["field"])
                if len(page_items) == 0:
                    logger.warning(
                        "Could not find field {field} in page {page}.".format(
                            field=attrs["field"], page=page["label"]
                        ),
                        extra={
                            "field": attrs["field"],
                            "new_value": attrs["value"],
                        },
                    )
                    # This isn't the best solution, but here we have to add
                    # the field with dummy data to the document.
                    add_page_item(page, attrs["field"], attrs["value"])
                else:
                    for page_item in page_items:
                        update_page_item(page_item, attrs["field"], attrs["value"])

        if not found_document_type:
            raise InvalidFieldError(
                "document_type",
                message=(
                    "Could not find the document type in any of the pages of the "
                    "task result."
                ),
            )

        return {"new_data": new_data}

import copy

import numpy as np
import pandas as pd
from django.conf import settings
from fuzzywuzzy import fuzz, process

# import logging


if settings.TAX_CLASSIFIER_VERSION == "2":

    from backend.app.nc_core.business_logic.ai_tax_classes import (
        AI_TAX_CLASSES_V2 as AI_TAX_CLASSES,
    )
else:

    from backend.app.nc_core.business_logic.ai_tax_classes import AI_TAX_CLASSES

from backend.app.document.models import Document

from .models import TaskResult

# logger = logging.getLogger('celery.task')


def merge_tax_data(doc_id, task_results):
    """
    Goal: take newly extracted categories from the tax classifier and merge them
    with tabular data in an existing task_results object for a document
    Steps:
    - get the previous task result from the doc id
    - convert the old results and the newly returned categories to dfs
    - merge them
    - convert back to our format
    - return to the watch_for_results func to be saved as a new object
    """
    # get category values returned from ai services
    table_categories = task_results["data"]["extracted_data"]
    document = Document.objects.get(pk=doc_id)
    result = (
        TaskResult.objects.filter(document=document).order_by("-created_at").first()
    )

    # logger.info(" +++++++++++++ results %s" % result)
    tables = []
    table_indices = []
    idx = 0  # there could be an index mismatch
    for table in result.new_data["data"]["extracted_data"]:
        if table["type"] == "collection" and table["subtype"] == "table":
            tables.append(table["items"])
            table_indices.append(idx)
        idx += 1

    # the index increments through both the returned set of classified
    # descriptions and the location of the tables in the extracted data

    """
    There are two versions of the tax_classifier in AI services
    v1 returns just a 'value'
    v2 returns 'value' and 'value2' the first is the "general category" the second is the "summary"
    Which is derived from a different model

    """
    idx = 0
    for table in tables:
        df = convert_table_to_df(table)
        # compile list of tax categories and roc values for line items in table
        summary_list = []
        cat_list = []
        roc_list = []
        conf_list = []
        for cat in table_categories[idx]:
            try:
                summary_list.append(cat["value2"])
            except Exception as e:
                print(e)
                summary_list.append("")
            try:
                lcat = str(cat["value"]).lower()
                cat_list.append(AI_TAX_CLASSES[lcat]["label"])
                roc_list.append(AI_TAX_CLASSES[lcat]["roc"])
                conf_list.append(cat["confidence"])
            except KeyError:
                cat_list.append(AI_TAX_CLASSES["other"]["label"])
                roc_list.append(AI_TAX_CLASSES["other"]["roc"])
                conf_list.append("0.50")

        # If the user has miscategorized or not categorized the table
        # we don't write the empty set back to the table
        if len(table_categories[idx]) != 0:

            if settings.TAX_CLASSIFIER_VERSION == "1":
                df = df.assign(
                    Summary=summary_list,
                    Tax_Category=cat_list,
                    RoC=roc_list,
                    Confidence=conf_list,
                )
            else:
                df = df.assign(
                    Summary=summary_list,
                    Tax_Category=cat_list,
                    RoC=roc_list,
                    Confidence=conf_list,
                )

            table = convert_df_to_table(df)
            result.new_data["data"]["extracted_data"][table_indices[idx]][
                "items"
            ] = table["items"]
        idx += 1

    result.new_data["meta"] = task_results["meta"]
    return result.new_data


def convert_df_to_table(df, base_index=0):
    # Get all the headers of the csv/xls file
    table = dict(
        {
            "type": "collection",
            "index": base_index,
            "subtype": "table",
            "property_id": "table",
            "label": "Table",
            "items": [],
        }
    )
    base_index += 1
    items = []
    header = {
        "index": base_index,
        "type": "item",
        "subtype": "header_row",
        "elements": [],
        "row_index": 0,
    }
    base_index += 1

    # Header object, the first object in items array/list
    for idx, hdr in enumerate(list(df.columns)):
        header["elements"].append(
            {
                "index": idx + base_index,
                "column_index": idx,
                "type": "field",
                # ag grid will bork if you have periods in the header text idk
                "value": str(hdr).replace(".", "-"),
                "subtype": "header",
            }
        )
    # Add header as the first element of items array
    table["items"].append(header)

    for idx, rows in enumerate(df.to_numpy()):
        row_dict = {
            "index": idx + base_index + 1,
            "row_index": idx + 1,
            "type": "item",
            "subtype": "data_row",
            "elements": [],
        }
        for idy, cell in enumerate(rows):
            val_dict = {
                "index": idy,
                "type": "field",
                "subtype": "cell",
                "column_index": idy,
            }
            try:
                if np.isnan(cell):
                    val_dict["value"] = ""
                else:
                    val_dict["value"] = str(cell)
            # TODO: Blind exception catch!  Isolate what we are catching here.
            except:  # noqa
                val_dict["value"] = str(cell)
            row_dict["elements"].append(val_dict)
        items.append(row_dict)

    table["items"].extend(items)
    return table


def convert_table_to_df(table):
    """
    Attempt to convert any found tabular data in extracted data into a
    dataframe.

    Args: a 'collection' json object that has a set of items
    Returns: a dataFrame or None

    A problem we have is that the ai system doesn't always return a coherent
    table.  Sometimes it returns multiple headers and sometimes it returns
    table rows that have different lengths.
    """
    headers = []
    rows = []
    for item in table:
        if item["subtype"] == "header_row":
            header = []
            # header = item['elements']
            # header = [ sub['value'] for sub in item['elements'] ]
            for sub in item["elements"]:
                try:
                    header.append(sub["value"])
                except KeyError:
                    header.append("")
            headers.append(header)
        elif item["subtype"] == "data_row":
            # rows.append( [ sub['value'] for sub in item['elements'] ] )
            row = []
            for sub in item["elements"]:
                try:
                    row.append(sub["value"])
                except KeyError:
                    row.append("")
            rows.append(row)

    # add header if none found in the extracted data
    if len(headers) == 0 and len(rows) > 0:
        header = [""] * len(rows[0])
        df = pd.DataFrame(rows, columns=header)
        return df

    elif len(headers) == 1 and len(rows) > 0:
        df = pd.DataFrame(rows, columns=headers[0])
        return df

    # If Ai services comes back with a table with multiple header rows
    elif len(headers) > 1 and len(rows) > 0:
        idx = 0
        for hrow in headers:
            if idx > 0:
                rows = [hrow] + rows
            idx += 1

        df = pd.DataFrame(rows, columns=headers[0])
        return df

    elif len(header) == 0 and len(rows) == 0:
        return None


def generate_agg_table(
    doc_list,
    column_list=[
        "Description",
        "Amount",
        "Date",
        "Summary",
        "Tax_Category",
        "RoC",
        "Confidence",
    ],
    scorer=fuzz.WRatio,
):
    """
    Goal: Accept list of ids of documents, get all task results, combine
    them based on column name
    and return a completed table that contains
    (for now): document uuid, desc, amount, date, Summary, Tax_Category, RoC, confidence
    score

    Further Goal: Generalize this function to return an aggregate table based
    on a set of declared columns
    """
    column_list = column_list or (
        "Description",
        "Amount",
        "Date",
        "Tax_Category",
        "RoC",
        "Confidence",
    )
    results = {}
    doc_names = {}
    # get all documents for comparison
    for doc_pk in doc_list:
        document = Document.objects.filter(pk=doc_pk).order_by("-created_at").first()

        # This can be optimized to only request the tabular data
        result = (
            TaskResult.objects.filter(document=document).order_by("-created_at").first()
        )
        # complete results for all files
        results[doc_pk] = result
        doc_names[doc_pk] = document.name

    # Create the word_lookup dictionary to be used for aliases
    word_lookup = dict()
    for elem in column_list:
        if isinstance(elem, str):
            word_lookup[elem] = [elem]
        else:
            word_lookup[elem[0]] = elem

    # Set up the column list to be a list of strings
    column_list = [i if isinstance(i, str) else i[0] for i in column_list]

    tables = []
    # get all tables in all docs

    for doc_pk, result in results.items():
        idx = 1
        if result is None:
            continue

        for item in result.new_data["data"]["extracted_data"]:
            if item["type"] == "collection" and item["subtype"] == "table":
                table_name = (
                    doc_names[doc_pk] + " - " + item["label"] + " (" + str(idx) + ")"
                )
                tables.append(
                    {
                        "uuid": doc_pk,
                        "table_index": item["index"],
                        "name": table_name,
                        "items": item["items"],
                    }
                )
                idx += 1

    # start with empty df and iteratively build up all relevant columns
    df = pd.DataFrame()

    for table in tables:
        filename = table["name"]
        tabledf = convert_table_to_df(table["items"])

        tmp = pd.DataFrame()
        if tabledf is None:
            continue

        empty = [""] * len(tabledf.index)

        filename = [filename] * len(tabledf.index)
        uuidcolumn = [table["uuid"]] * len(tabledf.index)
        table_indexcolumn = [table["table_index"]] * len(tabledf.index)
        # loop through all important columns in the table
        # attempt to find a fuzzy match
        # assign that to the tmp df
        # if there are no candidates assign an empty column
        # concat that to the completed set
        tmp = tmp.assign(uuid=uuidcolumn)
        tmp = tmp.assign(table_index=table_indexcolumn)
        tmp = tmp.assign(Document=filename)

        for key in column_list:
            candidates = [
                process.extract(alias, tabledf.columns, limit=1)[0]
                for alias in word_lookup[key]
            ]
            candidates = sorted(
                candidates, key=lambda pos_candidate: pos_candidate[1], reverse=True
            )
            candidate = candidates[0]
            if candidate[1] > 70:

                # using a two step process because we can't use a var name
                # as a column name

                # also note we are ALWAYS using the first match for each key
                # in the column list
                tmp = tmp.assign(blank=tabledf[[candidate[0]]].iloc[:, 0])
                tmp = tmp.rename(columns={"blank": key})

            else:
                tmp = tmp.assign(blank=empty)
                tmp = tmp.rename(columns={"blank": key})

        # append finalized table to set
        df = pd.concat([df, tmp], ignore_index=True)

    table = convert_df_to_table(df)
    return table


def normalize_table_headers(task_result):
    for table in task_result["data"]["extracted_data"]:
        if table.get("type") == "collection" and table.get("subtype") == "table":
            try:
                rows = [row["subtype"] for row in table["items"]]
                # if table items has both subtype header_row and subtype
                # data_row then skip
                if "data_row" in rows and "header_row" in rows:
                    continue

                # if table items has header_row but not data_row then add it
                if "data_row" not in rows and "header_row" in rows:
                    new_row = copy.deepcopy(table["items"][0])
                    new_row.update(
                        subtype="data_row", row_index=new_row["row_index"] + 1
                    )
                    for row in new_row["elements"]:
                        row.update(value="", subtype="cell")
                    table["items"].append(new_row)

                # if table items has data_row but not header_row then add it
                if "data_row" in rows and "header_row" not in rows:
                    new_row = copy.deepcopy(table["items"][0])
                    new_row.update(subtype="header_row", row_index=0)
                    for row in new_row["elements"]:
                        row.update(value="", subtype="header")
                    for current_row in table["items"]:
                        current_row["row_index"] = current_row["row_index"] + 1

                    table["items"].insert(0, new_row)

            # TODO: This needs to be fixed - we shouldn't be applying a KeyError
            # so broadly like this.
            # If key error it means there is no data in the table at all.
            except KeyError:
                table["items"] = [
                    {
                        "type": "item",
                        "index": 1,
                        "subtype": "header_row",
                        "elements": [
                            {
                                "type": "field",
                                "index": 2,
                                "value": "",
                                "subtype": "header",
                                "column_index": 0,
                            },
                            {
                                "type": "field",
                                "index": 3,
                                "value": "",
                                "subtype": "header",
                                "column_index": 1,
                            },
                        ],
                        "row_index": 0,
                    },
                    {
                        "type": "item",
                        "index": 3,
                        "subtype": "data_row",
                        "elements": [
                            {
                                "type": "field",
                                "index": 0,
                                "value": "",
                                "subtype": "cell",
                                "column_index": 0,
                            },
                            {
                                "type": "field",
                                "index": 1,
                                "value": "",
                                "subtype": "cell",
                                "column_index": 1,
                            },
                        ],
                        "row_index": 1,
                    },
                ]

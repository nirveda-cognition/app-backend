import uuid

from django.db import models

from backend.app.common.models import TimestampedModel


class Task(TimestampedModel):
    uuid = models.CharField(unique=True, max_length=1024, default=uuid.uuid4)
    ai_uuid = models.CharField(max_length=1024, null=True, blank=True)
    # TODO: Remove - we don't need a reference to this since we have it from
    # the document.
    organization = models.ForeignKey(
        to="organization.Organization", on_delete=models.CASCADE, related_name="tasks"
    )
    request = models.JSONField(default=dict)
    response = models.JSONField(default=dict)
    backend_id = models.CharField(max_length=1024, blank=True, null=True)
    client_request = models.JSONField(null=True, blank=True)

    ERROR_AI_RESPONSE = 1
    ERROR_AI_TASK_RESULTS = 2
    ERROR_GENERAL_RESULTS = 3
    ERROR_UPLOAD = 4

    TYPE_EXTRACTION = 1
    TYPE_TAX_CLASSIFICATION = 2


class Chunks(TimestampedModel):
    uuid = models.CharField(unique=True, max_length=1024, default=uuid.uuid4)
    ai_uuid = models.CharField(max_length=1024, null=True, blank=True)
    sequence_number = models.IntegerField(null=True)
    task = models.ForeignKey(
        to="task.Task",
        on_delete=models.CASCADE,
        related_name="chunked_task",
    )
    status = models.CharField(max_length=1024, blank=True, null=True)
    chunked_payload = models.JSONField(default=dict)
    ai_response = models.JSONField(default=dict)

    ERROR_AI_RESPONSE = 1
    ERROR_AI_TASK_RESULTS = 2
    ERROR_GENERAL_RESULTS = 3
    ERROR_UPLOAD = 4

    TYPE_EXTRACTION = 1
    TYPE_TAX_CLASSIFICATION = 2


class TaskResult(TimestampedModel):
    # TODO: Remove the document field - the TaskResult is already associated
    # with a document via the Task ForeignKey!
    document = models.ForeignKey(
        to="document.Document", on_delete=models.CASCADE, related_name="results"
    )
    # TODO: Is will make our lives a lot simpler if this is a OneToOneField!
    task = models.ForeignKey(
        to="task.Task", on_delete=models.CASCADE, related_name="results"
    )
    uuid = models.CharField(unique=True, max_length=256, default=uuid.uuid4)
    data = models.JSONField(default=dict)
    new_data = models.JSONField(default=dict)

    class Meta:
        get_latest_by = "created_at"
        ordering = ("-created_at",)
        verbose_name = "Task Result"
        verbose_name_plural = "Task Results"

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        track = kwargs.pop("track", False)
        if track is True:
            self._operation = kwargs.pop("operation")
            self._user = kwargs.pop("user")
        super(TaskResult, self).save(*args, **kwargs)


class CallbackRequest(models.Model):
    task = models.ForeignKey(
        to="task.Task",
        on_delete=models.CASCADE,
        related_name="callback_requests",
    )
    callback = models.CharField(max_length=1024)
    request_data = models.JSONField(default=dict, null=True)
    response_data = models.JSONField(default=dict, null=True)
    status_code = models.IntegerField(null=True)
    callback_processing_error = models.CharField(max_length=1024, null=True)
    time_sent = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = "Callback Request"
        verbose_name_plural = "Callback Requests"

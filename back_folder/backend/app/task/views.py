from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import decorators, exceptions, mixins, response, viewsets

from backend.app.document.mixins import DocumentNestedMixin
from backend.app.organization.mixins import OrganizationNestedMixin

from .models import TaskResult
from .serializers import (
    TaskResultSerializer,
    TaskSerializer,
    UpdateApexTaskResultSerializer,
)


class UpdateTaskResultMixin(mixins.UpdateModelMixin):
    def get_serializer_class(self):
        if self.request.method == "PATCH" and self.action == "partial_update":
            if self.organization.type.slug != "apex":
                raise exceptions.PermissionDenied(
                    "This endpoint does not yet support organization %s."
                    % self.organization.name
                )
            return UpdateApexTaskResultSerializer
        else:
            return self.serializer_class

    def partial_update(self, request, *args, **kwargs):
        # NOTE: This method is needed because we are using a different
        # serializer for the response.
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(
            instance=self.get_object(),
            data=request.data,
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return response.Response(self.serializer_class(instance).data, status=201)


class OrganizationTaskViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    OrganizationNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/tasks/
    (2) GET /organizations/<pk>/tasks/<task_pk>/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    serializer_class = TaskSerializer

    def get_queryset(self):
        # TODO: Eventually we want to get rid of the Organization FK on
        # the Task since it is redundant - the Task is already related to an
        # Organization via the Document model.  This query will have to be
        # adjusted when we do so.
        return self.organization.tasks.all()


class OrganizationTaskResultViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    OrganizationNestedMixin,
    UpdateTaskResultMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/tasks/results/
    (2) GET /organizations/<pk>/tasks/results/<pk>/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    serializer_class = TaskResultSerializer

    def get_queryset(self):
        return TaskResult.objects.filter(task__organization=self.organization)


class OrganizationTaskTaskResultViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    UpdateTaskResultMixin,
    OrganizationNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/tasks/<task_pk>/results/
    (2) GET /organizations/<pk>/tasks/<task_pk>/results/<pk>/
    (3) GET /organizations/<pk>/tasks/<task_pk>/results/latest/
    """

    lookup_field = "pk"
    task_lookup_field = "task_pk"
    organization_lookup_field = ("pk", "organization_pk")
    serializer_class = TaskResultSerializer

    def get_task(self):
        return get_object_or_404(
            self.organization.tasks.all(), pk=self.kwargs[self.task_lookup_field]
        )

    def get_queryset(self):
        task = self.get_task()
        return task.results.all()

    @decorators.action(detail=False, methods=["GET"])
    def latest(self, request, *args, **kwargs):
        qs = self.get_queryset()
        try:
            instance = qs.latest()
        except TaskResult.DoesNotExist:
            raise Http404("The task does not have any results.")
        else:
            serializer_class = self.get_serializer_class()
            return response.Response(serializer_class(instance).data)


class DocumentNestedTaskMixin(DocumentNestedMixin):
    def get_task(self):
        if self.document.task is None:
            raise Http404(
                "Document %s does not have an associated task." % self.document.pk
            )
        return self.document.task


class DocumentTaskViewSet(
    DocumentNestedTaskMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/documents/<document_pk>/task/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    document_lookup_field = ("pk", "document_pk")
    serializer_class = TaskSerializer

    def get_object(self):
        return self.get_task()


class DocumentTaskResultViewSet(
    UpdateTaskResultMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    DocumentNestedTaskMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/documents/<document_pk>/task/results/
    (2) GET /organizations/<pk>/documents/<document_pk>/task/results/<pk>/
    (3) GET /organizations/<pk>/documents/<document_pk>/task/results/latest/
    (4) PATCH /organizations/<pk>/documents/<document_pk>/task/results/<pk>/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    document_lookup_field = ("pk", "document_pk")
    serializer_class = TaskResultSerializer

    def get_queryset(self):
        task = self.get_task()
        return task.results.all()

    @decorators.action(detail=False, methods=["GET"])
    def latest(self, request, *args, **kwargs):
        qs = self.get_queryset()
        try:
            instance = qs.latest()
        except TaskResult.DoesNotExist:
            raise Http404("The task does not have any results.")
        else:
            serializer_class = self.get_serializer_class()
            return response.Response(serializer_class(instance).data)

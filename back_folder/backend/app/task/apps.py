from django.apps import AppConfig


class TaskConfig(AppConfig):
    name = "backend.app.task"
    verbose_name = "Task"

    def ready(self):
        import backend.app.task.signals  # noqa

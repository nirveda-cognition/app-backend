from django.contrib.auth.management.commands import createsuperuser

from backend.management.decorators import requires_organizations
from backend.management.mixins import OrganizationMixin


class Command(createsuperuser.Command, OrganizationMixin):
    @requires_organizations
    def handle(self, *args, **kwargs):
        super().handle(*args, **kwargs)

    def get_input_data(self, field, message, default=None):
        if field.name == "organization":
            return self.query_organization()
        return super().get_input_data(field, message, default=default)

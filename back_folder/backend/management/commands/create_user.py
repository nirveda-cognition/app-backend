from django.core import management

from backend.app.user.models import CustomUser
from backend.management.base import CustomCommand
from backend.management.decorators import debug_only, requires_organizations
from backend.management.mixins import OrganizationMixin, UserMixin


class Command(CustomCommand, OrganizationMixin, UserMixin):
    @requires_organizations
    @debug_only
    @management.base.no_translations
    def handle(self, *args, **options):
        info = self.query_user_information()
        organization = self.query_organization()
        user = CustomUser.objects.create(
            organization=organization, is_active=True, **info
        )
        self.success("Successfully created user %s." % user.pk)

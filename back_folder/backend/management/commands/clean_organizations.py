import json

import six
from django.core import management

from backend.app.organization.models import Organization
from backend.management.base import CustomCommand


class Command(CustomCommand):
    """
    Django management command to perform cleanup operations on the organizations
    saved in the database.
    """

    @management.base.no_translations
    def handle(self, *args, **options):
        organizations_to_update_field = []
        organizations_to_remove_field = []

        def mark_for_clear(organization):
            self.warn(
                "Organization %s's `json_info` is improperly stored as %s (%s)."
                "\n--> The field will be cleared to an empty dict."
                % (
                    organization.name,
                    organization.json_info,
                    type(organization.json_info),
                )
            )
            organization.json_info = {}
            organizations_to_remove_field.append(organization)

        def mark_for_update(organization, json_info):
            self.info(
                "Organization %s's `json_info` is improperly stored as %s (%s)."
                "\n--> The field will be updated to it's dictionary form."
                % (
                    organization.name,
                    organization.json_info,
                    type(organization.json_info),
                )
            )
            organization.json_info = json_info
            organizations_to_update_field.append(organization)

        self.newline()

        for organization in Organization.objects.all():
            if isinstance(organization.json_info, six.string_types):
                try:
                    json_info = json.loads(organization.json_info)
                except json.decoder.JSONDecodeError:
                    mark_for_clear(organization)
                else:
                    mark_for_update(organization, json_info)
            elif isinstance(organization.json_info, dict):
                self.prompt(
                    "Organization %s's `json_info` is properly stored as "
                    "%s." % (organization.name, dict)
                )
            else:
                mark_for_clear(organization)

        self.newline()
        if len(organizations_to_update_field) == 0 and len(
            organizations_to_remove_field
        ):
            self.success("No organizations to update.")
            return

        if len(organizations_to_update_field) != 0:
            self.info(
                "%s organizations will have their JSON field changed."
                % len(organizations_to_update_field)
            )
            ans = self.query_boolean("Continue?")
            if ans:
                [org.save() for org in organizations_to_update_field]

        if len(organizations_to_remove_field) != 0:
            self.info(
                "%s organizations will have their JSON field cleared."
                % len(organizations_to_remove_field)
            )
            ans = self.query_boolean("Continue?")
            if ans:
                [org.save() for org in organizations_to_remove_field]

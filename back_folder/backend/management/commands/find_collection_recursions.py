from django.core import management

from backend.app.collection.models import Collection
from backend.management.base import CustomCommand


class Command(CustomCommand):
    @management.base.no_translations
    def handle(self, *args, **options):
        recursions = 0
        for collection in Collection.objects.all():
            parents = [c.pk for c in collection.parents.all()]
            children = [c.pk for c in collection.collections.all()]
            relationships = set(parents + children)
            for id in relationships:
                if id in parents and id in children:
                    self.info(
                        "The collection %s has %s as both a parent and a "
                        "child." % (collection.pk, id)
                    )
                    recursions += 1
        if recursions == 0:
            self.success("No recursions detected.")

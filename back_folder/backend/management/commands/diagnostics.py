import pprint
import re
import traceback

from django.core import management
from django.core.management.base import BaseCommand
from django.views import debug as django_debug_view


class Reporter:
    def __init__(self):
        self.reports = {}

    def __call__(self, func):
        self.reports[func.__name__] = func
        return func


reporter = Reporter()


@reporter
def settings():
    django_debug_view.HIDDEN_SETTINGS = re.compile(
        "PWD|API|TOKEN|KEY|SECRET|PASS|SIGNATURE", flags=re.IGNORECASE
    )
    safe = django_debug_view.get_safe_settings()
    # Scrub these noisy settings that take up a lot of space and aren't really
    # all that useful.
    for key in ["LANGUAGES", "DATETIME_INPUT_FORMATS", "DATE_INPUT_FORMATS"]:
        safe[key] = ["..."]
    pprint.pprint(safe)


@reporter
def database():
    db_settings = {"DATABASES": django_debug_view.get_safe_settings()["DATABASES"]}
    pprint.pprint(db_settings)
    management.call_command("showmigrations")


class Command(BaseCommand):
    """
    Management command that can output various diagnostics about the overall
    health of the backend application.  The reports here can be expanded upon
    to include other important information as well.
    """

    help = "{}\nAvailable reports: {}".format(__doc__, ", ".join(reporter.reports))

    def add_arguments(self, parser):
        parser.add_argument(
            "--only",
            "-o",
            action="append",
            default=[],
            help=("Run only the specified report(s). Can be used multiple times."),
        )

    def handle(self, *args, **options):
        names = options["only"] or list(reporter.reports)
        for name in names:
            report = reporter.reports[name]
            print("{divider}\n{name}\n{divider}".format(divider="-" * 40, name=name))
            try:
                report()
            except Exception:
                traceback.print_exc()

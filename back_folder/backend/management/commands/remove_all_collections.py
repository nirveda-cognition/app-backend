from django.core import management

from backend.app.collection.models import Collection
from backend.management.base import CustomCommand

BASE_COLLECTION_NAME = "All Collections"


class Command(CustomCommand):
    @management.base.no_translations
    def handle(self, *args, **options):
        for collection in Collection.objects.all():
            for parent in collection.parents.filter(name=BASE_COLLECTION_NAME).all():
                self.info(
                    "Removing collection %s as a parent to %s."
                    % (parent.name, collection.pk)
                )
                collection.parents.remove(parent)
            for child in collection.collections.filter(name=BASE_COLLECTION_NAME).all():
                self.info(
                    "Removing collection %s as a child to %s."
                    % (child.name, collection.pk)
                )
                collection.collections.remove(child)
            collection.save()

        self.info("Deleting remnant collections with name %s." % BASE_COLLECTION_NAME)
        found = 0
        for collection in Collection.objects.filter(name=BASE_COLLECTION_NAME).all():
            collection.delete()
            found += 1
        if found == 0:
            self.success(
                "No collections with name %s to delete." % BASE_COLLECTION_NAME
            )
        else:
            self.success(
                "Deleted %s collections with name %s to delete."
                % (found, BASE_COLLECTION_NAME)
            )

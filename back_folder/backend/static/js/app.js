/**
 * Created by Akhtar on 24/06/2020.
 */

const getErrorElement = function (message) {
  return  ""
  + "<td class='errorMessage' colspan='2'>"
  + "<i class='fas fa-exclamation-circle'></i>&nbsp;"
  + "<span>" + message + "</span>"
  + "</td>"
}


const update_login_page = function () {
  $("label[for='id_auth-username']").html("Email");
  $("label[for='id_auth-password']").html("Password");

  const user_input = $("#id_auth-username")
  if (user_input.length > 0) {
    user_input[0].setAttribute("placeholder", "Email Address")
    const auth_user_element = '<div class="icon_cont"><i class="fas fa-envelope"></i></div>'
    $(auth_user_element).insertAfter(user_input)
  }

  const password_input = $("#id_auth-password")
  if (password_input.length > 0) {
    password_input[0].setAttribute("placeholder", "Password")
    const password_element = '<div class="icon_cont"><i class="fas fa-lock"></i></div>'
    $(password_element).insertAfter(password_input)
  }

  const error_ul = $(".form_container .errorlist")
  if (error_ul.length > 0) {
    if (error_ul.length === 2) {
      const error_ul_nonfield = $(".form_container .errorlist.nonfield")

      const error_tr = error_ul_nonfield.closest("tr")
      const text = error_ul_nonfield.children("li")[0].textContent.toLowerCase()
      const td = error_tr.find("td")

      if (text.includes("otp token")) {
        const authErrorElement = getErrorElement("Enter your OTP token")
        $(authErrorElement).insertAfter(td)
        td.remove()
      }
      error_ul[1].remove()
    } else {
      const error_tr = error_ul.closest("tr")
      const text = error_ul.children("li")[0].textContent.toLowerCase()
      const td = error_tr.find("td")

      if (text.includes("email address and password")) {
        const authErrorElement = getErrorElement("Enter a valid email/password combination")
        $(authErrorElement).insertAfter(td)
        td.remove()
      } else if (text.includes("invalid token")) {
        const tokenErrorElement = getErrorElement("Invalid Authentication Code")
        $(tokenErrorElement).insertAfter(td)
        td.remove()
      } else if (text.includes("token is not valid")) {
        const table = error_ul.closest("table")
        const tokenErrorElement = getErrorElement("Invalid Authentication Code")
        table.prepend("<tr>" + tokenErrorElement + "</tr>>")
        error_ul.remove()
      }

      // Error in login page, display at bottom
      // const text = error_ul.children("li")[0].textContent
      // const errorDiv = $(".form_container .errorDiv")
      // if (text.toLowerCase().includes("email address and password")) {
      //   const authErrorDiv = errorDiv.find($(".authError")).removeClass("display-none")
      //   authErrorDiv.addClass("errorMessage")
      //   authErrorDiv.find("span").html("Enter a valid email/password combination.")
      // }
    }
  }
}


const update_token_page = function() {
  $("label[for='id_token-otp_token']").html('Authentication Code');
  const token_input = $('#id_token-otp_token')
  if (token_input.length > 0) {
    token_input[0].setAttribute('placeholder', 'Authentication Code');
    const auth_token_element = '<div class="icon_cont"><i class="fas fa-lock"></i></div>';
    $(auth_token_element).insertAfter(token_input);
  }
}


const update_token_generator_page = function() {
  $("label[for='id_generator-token']").html('Authentication Code');
  const generator_token_input = $('#id_generator-token')
  if (generator_token_input.length > 0) {
    generator_token_input[0].setAttribute('placeholder', 'Authentication Code');
    const generator_token_element = '<div class="icon_cont"><i class="fas fa-lock"></i></div>';
    $(generator_token_element).insertAfter(generator_token_input);
  }
}

$(document).ready(function () {
  update_login_page()
})


$(window).load(function () {
  update_token_page()
  update_token_generator_page()
});

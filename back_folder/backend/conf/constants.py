# Platform API callback errors -
AI_SERVICES_RESPONSE_ERROR = {
    "error": "Error in AI Services Response",
    "status": "FAILURE",
}

DOCUMENT_UPLOAD_ERROR = {"error": "Document not uploaded", "status": "FAILURE"}

DOCUMENT_PROCESSING_ERROR = {"error": "Failed To Process Document", "status": "FAILURE"}

API_USER_UPLOAD_ERROR = {
    "status": "FAILURE",
    "errors": [
        {
            "detail": "Document not uploaded.",
            "code": "87",
            "title": "Unable to retrieve file",
        }
    ],
}

API_USER_PROCESSING_ERROR = {
    "status": "FAILURE",
    "errors": [
        {
            "detail": "We were unable to process your request.",
            "code": "85",
            "title": "Processing error",
        }
    ],
}


class HttpMethods:
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    PATCH = "PATCH"
    DELETE = "DELETE"

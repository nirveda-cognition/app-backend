import copy
import requests
from datetime import datetime, timedelta
from azure.storage.blob import generate_account_sas, ResourceTypes, AccountSasPermissions
account_name = 'sgmsqasaabncplt01'
container_name = 'nirvedadocs'
blob_name = 'random_file'
sas_token_args = {
            "account_name": 'sgmsqasaabncplt01',
            "account_key": 'ggc28AlFkJUF2TMWTvK0oa2JHxdoedPILXgTdzf0rYstZupVor70jwkvGz/iqu/3T4j44suqG8NBuROou6N0uw==',
            "resource_types": ResourceTypes(object=True),
        }
account_permissions = {"write": True, "list": True, "update": True, "read": True}
expiration = 1800
sas_token_args_copy = copy.deepcopy(sas_token_args)
sas_token_args_copy.update(
                expiry=datetime.utcnow() + timedelta(seconds=expiration),
                permission=AccountSasPermissions(**account_permissions),
            )
sas_token = generate_account_sas(**sas_token_args_copy)

presigned_url="https://"+account_name+".blob."+"core.windows.net"+"/"+container_name+"/"+blob_name+"?"+sas_token
headers = {"Content-Type": "application/octet-stream",
                "x-ms-blob-type": "BlockBlob",
                "x-ms-version": "2019-07-07" }
put_url = presigned_url
# sas_token_arg = {"account_name" = account_name,"account_key" = account_key, "resource_type" = ResourceTypes(object=True)}
# resp = requests.put(put_url, data=request.body, headers=headers)
print("sas_token_arg :", sas_token_args)
print("*********************************************************************")
print("sas_token_arg_copy :", sas_token_args_copy)
print("*********************************************************************")
print("sas_token :", sas_token)
print("*********************************************************************")
print("presigned_url :", presigned_url)
print("*********************************************************************")
# prinr("Response :", resp)
print("Response status :", requests.head(put_url))

# PRECAUTION: avoid production dependencies that aren't in development

-r base.txt

Collectfast==2.2.0  # https://github.com/antonagestam/collectfast

# Django
# ------------------------------------------------------------------------------
django-storages==1.12.3  # https://github.com/jschneier/django-storages

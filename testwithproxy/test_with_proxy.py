import copy
import requests
from datetime import datetime, timedelta
from azure.storage.blob import generate_account_sas, ResourceTypes, AccountSasPermissions
import os
# import requests
import mimetypes
from azure.storage.blob import ContentSettings
from urllib.parse import urlparse


account_name = 'sgmsqasaabncplt01'
container_name = 'nirvedadocs'
blob_name = 'file_with_header_change_1049'
sas_token_args = {
            "account_name": 'sgmsqasaabncplt01',
            "account_key" : 'ggc28AlFkJUF2TMWTvK0oa2JHxdoedPILXgTdzf0rYstZupVor70jwkvGz/iqu/3T4j44suqG8NBuROou6N0uw==',
            "resource_types": ResourceTypes(object=True),
        }
account_permissions = {"write": True, "list": True, "update": True, "read": True}
expiration = 1800
sas_token_args_copy = copy.deepcopy(sas_token_args)
sas_token_args_copy.update(
                expiry=datetime.utcnow() + timedelta(seconds=expiration),
                permission=AccountSasPermissions(**account_permissions),
            )
sas_token = generate_account_sas(**sas_token_args_copy)

presigned_url="https://"+account_name+".blob."+"core.windows.net"+"/"+container_name+"/"+blob_name+"?"+sas_token
headers = {"Content-Type": "application/octet-stream",
                "x-ms-blob-type": "BlockBlob",
                "x-ms-version": "2019-07-07" }


def upload_using_sas(sas_url , file_name_full_path):
    """
    Upload File using Azure SAS url.
    This function uploads file to Azure blob container
    :param sas_url:  Azure sas url with write access on a blob storage
    :param file_name_full_path:  File name with fill path
    :return:  HTTP response status of file upload
    """
    o = urlparse(sas_url)
    # Remove first / from path
    if o.path[0] == '/':
        blob_storage_path = o.path[1:]
    else:
        blob_storage_path = o.path

    storage_account = o.scheme + "://" + o.netloc + "/"
    file_name_only = os.path.basename(file_name_full_path)
    response_status = put_blob(storage_account,blob_storage_path,file_name_only,o.query,file_name_full_path)
    return response_status


def put_blob(storage_url,container_name, blob_name,qry_string,file_name_full_path):
    file_name_only = os.path.basename(file_name_full_path)
    try:
        file_ext = '.' + file_name_only.split('.')[1]
    except IndexError:
        file_ext = None
    # Set content Type
    if file_ext is not None:
        content_type_string = ContentSettings(content_type=mimetypes.types_map[file_ext])
    else:
        content_type_string = None

    with open(file_name_full_path , 'rb') as fh:
        response = requests.put(storage_url+container_name + '/' + blob_name+'?'+qry_string,
                                data=fh,
                                #headers={
                                 #            'content-type': content_type_string.content_type,
                                  #           'x-ms-blob-type': 'BlockBlob'
                                   #      },
                                headers=headers,                                             # added
                                proxies={                                                    # added
                                "http": 'http://10.180.0.8:8080',       # added
                                #"http": 'http://103.16.71.116:8080',
                                "https": 'http://10.180.0.8:8080',     # added 
                    },
                                allow_redirects=True,                                        # added   
                                #params={'file': file_name_full_path}
                                )
        return response.status_code
    
if __name__ == "__main__":
    file_to_upload = 'demo_text.txt'
    r= upload_using_sas(presigned_url, file_to_upload)
    print ("Upload Status :" + str(r))
    print("url :", presigned_url)

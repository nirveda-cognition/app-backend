import copy
import requests
from datetime import datetime, timedelta
from azure.storage.blob import generate_account_sas, ResourceTypes, AccountSasPermissions
import os
# import requests
import mimetypes
from azure.storage.blob import ContentSettings
from urllib.parse import urlparse

# url_srting = 'sp=r&st=2022-07-18T04:02:51Z&se=2022-07-18T12:02:51Z&sip=168.1.5.68&spr=https&sv=2021-06-08&sr=c&sig=O%2FZrtu%2Bs3GcfK%2FoWhJDAlBS5odDAk645SnxWfMKJvO4%3D'
# url_srting = 'sv=2018-03-28&sr=c&sig=T4Q%2FzcfbI0QikfIN0HYI3M%2B7Kl5yJL5o9iEAymW%2BQHc%3D&se=2023-08-15T17%3A54%3A53Z&sp=rwdl'
url_srting = 'sv=2018-03-28&sr=c&sig=speSykaY6ms31rMN%2BaGMNWEQ1kxhkBgEYi79Ia7VbP4%3D&se=2023-08-15T17%3A54%3A53Z&sp=rl'

account_name = 'sgmsqasaabncplt01'
container_name = 'nirvedadocs'
blob_name = 'random_file'
sas_token = url_srting
#https://sgmsqasaabncplt01.blob.core.windows.net/nirvedadocs
presigned_url="https://"+account_name+".blob."+"core.windows.net"+"/"+container_name+"/"+blob_name+"?"+sas_token

def upload_using_sas(sas_url , file_name_full_path):
    """
    Upload File using Azure SAS url.
    This function uploads file to Azure blob container
    :param sas_url:  Azure sas url with write access on a blob storage
    :param file_name_full_path:  File name with fill path
    :return:  HTTP response status of file upload
    """
    o = urlparse(sas_url)
    # Remove first / from path
    print(o)
    if o.path[0] == '/':
        blob_storage_path = o.path[1:]
    else:
        blob_storage_path = o.path

    storage_account = o.scheme + "://" + o.netloc + "/"
    file_name_only = os.path.basename(file_name_full_path)
    response_status = put_blob(storage_account,blob_storage_path,file_name_only,o.query,file_name_full_path)
    return response_status


def put_blob(storage_url,container_name, blob_name,qry_string,file_name_full_path):
    file_name_only = os.path.basename(file_name_full_path)
    try:
        file_ext = '.' + file_name_only.split('.')[1]
    except IndexError:
        file_ext = None
    # Set content Type
    if file_ext is not None:
        content_type_string = ContentSettings(content_type=mimetypes.types_map[file_ext])
    else:
        content_type_string = None

    with open(file_name_full_path , 'rb') as fh:
        response = requests.put(storage_url+container_name + '/' + blob_name+'?'+qry_string,
                                data=fh,
                                headers={
                                            'content-type': content_type_string.content_type,
                                            'x-ms-blob-type': 'BlockBlob'
                                        },
                                params={'file': file_name_full_path}
                                )
        return response.status_code
    
if __name__ == "__main__":
    file_to_upload = 'demo_text.txt'
    r= upload_using_sas(presigned_url, file_to_upload)
    print ("Upload Status :" + str(r))
    print("url :", presigned_url)

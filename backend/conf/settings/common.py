"""
Settings configuration for settings that are shared between test, development,
local and production environments.
"""
import os
from pathlib import Path

BASE_DIR = Path(os.path.abspath(__file__)).parents[2]

APPS_DIR = BASE_DIR / "app"
DOCUMENTATION_DIR = BASE_DIR / "docs"

from backend.conf import Environments, config

AZURE_STORAGE_ACCOUNT_NAME = config(
    name="AZURE_STORAGE_ACCOUNT_NAME", required=[Environments.DEV, Environments.PROD]
)
AZURE_ACCOUNT_KEY = config(
    name="AZURE_ACCOUNT_KEY", required=[Environments.DEV, Environments.PROD]
)
AZURE_CONTAINER_NAME = config(
    name="AZURE_CONTAINER_NAME", required=[Environments.DEV, Environments.PROD]
)
AZURE_ENDPOINT_SUFFIX = "core.windows.net"

AWS_STORAGE_TARGET = config("STORAGE_TARGET", default="aws")
AWS_STORAGE_BUCKET_NAME = config(name="AWS_STORAGE_BUCKET_NAME", default="")

NC_AWS_BUCKET_NAME = config(
    name="NC_AWS_BUCKET_NAME", required=[Environments.DEV, Environments.PROD]
)

AWS_ACCESS_KEY_ID = config(
    name="AWS_ACCESS_KEY_ID", required=[Environments.DEV, Environments.PROD]
)
AWS_SECRET_ACCESS_KEY = config(
    name="AWS_SECRET_ACCESS_KEY", required=[Environments.DEV, Environments.PROD]
)
# This is not being used currently but probably should be.
AWS_SES_SECRET_ACCESS_KEY = config("AWS_SES_SECRET_ACCESS_KEY", default="")

AWS_REGION = config("AWS_REGION", default="us-east-1")
# This is not being used currently but probably should be.
AWS_SES_REGION = config("AWS_SES_REGION", default=AWS_REGION)
AWS_SECRET_REGION = config("AWS_SECRET_REGION", default="us-east-2")

AWS_ROLE_ARN = config("ROLE_ARN", default=None)
AWS_SECRETS_MANAGER = "secretsmanager"

if AWS_STORAGE_BUCKET_NAME:
    STATICFILES_LOCATION = ""
    STATICFILES_STORAGE = "backend.lib.aws.custom_storages.StaticStorage"

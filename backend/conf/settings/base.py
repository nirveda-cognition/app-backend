"""
Default settings configurations for all environments.

Each separate environment will import these configurations and override on a
per-case basis.
"""
import datetime
import os
from pathlib import Path

import dj_database_url
from corsheaders.defaults import default_headers
from django.utils.translation import gettext_lazy as _

from backend.conf import Environments, config

from .ai_services import *  # noqas
from .logging import *  # noqa

DEBUG = False

ENVIRONMENT = config("CODE_ENVIRONMENT")


BASE_DIR = Path(os.path.abspath(__file__)).parents[2]
APPS_DIR = BASE_DIR / "app"
DOCUMENTATION_DIR = BASE_DIR / "docs"

# Localization Configuration
TIME_ZONE = "UTC"
LOCALE_PATHS = [str(BASE_DIR / "locale")]
LANGUAGE_CODE = "en-us"
LANGUAGES = [
    ("en", _("English")),
    ("de", _("German")),
    ("es", _("Espanol")),
]
USE_I18N = True
USE_L10N = True
USE_TZ = True

APP_DOMAIN = config(
    name="APP_DOMAIN",
    required=[Environments.PROD],
    default={Environments.DEV: "localhost", Environments.TEST: "localhost"},
)
APP_URL = config(name="APP_URL", default="http://%s" % APP_DOMAIN)
APP_V1_URL = os.path.join(APP_URL, "v1")
APP_V2_URL = os.path.join(APP_URL, "v2")

TAX_CLASSIFIER_VERSION = config(name="TAX_CLASSIFIER_VERSION", default=1)

# This is only used for the logo in 2FA.
BRAND = config("ORG_BRAND", default="nirveda")

SECRET_KEY = config(
    name="DJANGO_SECRET_KEY",
    required=[Environments.PROD],
    default={
        Environments.TEST: "thefoxjumpedoverthelog",
        Environments.DEV: "thefoxjumpedoverthelog",
    },
)
DJANGO_WEB_HOST = config(
    name="DJANGO_WEB_HOST",
    required=[Environments.PROD],
    default={Environments.DEV: "localhost", Environments.TEST: "localhost"},
)
NC_APP_FRONTEND_URL = config(
    name="NC_APP_FRONTEND_URL",
    required=[Environments.PROD],
    default={
        Environments.DEV: "http://localhost:3000",
        Environments.TEST: "http://localhost:3000",
    },
)
NC_APP_FRONTEND_URL_2 = config("NC_APP_FRONTEND_URL_2", default=None)
NC_APP_FRONTEND_URL_3 = config("NC_APP_FRONTEND_URL_3", default=None)
FRONTEND_STORAGE_PROXY = config("FRONTEND_STORAGE_PROXY", default=None)

# Password Reset & Forgot
RESET_PWD_UI_LINK = NC_APP_FRONTEND_URL + "/reset/"
FORGOT_PWD_UI_LINK = NC_APP_FRONTEND_URL + "/reset"
PWD_RESET_LINK_EXPIRY_TIME_IN_HRS = 24
FORGOT_PWD_RESET_EXPIRY_LINK_TIME_IN_HRS = 24

# WE have to set max upload size because of #kpmg
DATA_UPLOAD_MAX_MEMORY_SIZE = 20971520

# Email Configurations
EMAIL_ENABLED = config("EMAIL_ENABLED", default=True)
EMAIL_BACKEND = config(
    name="EMAIL_BACKEND",
    default="backend.lib.rest_framework_utils.email_backend.CustomEmailBackend",
)
FROM_EMAIL = "support@nirvedacognition.ai"
SMTP_EMAIL_HOST = config(
    name="SMTP_EMAIL_HOST", required=[Environments.DEV, Environments.PROD]
)
SMTP_EMAIL_PORT = config("SMTP_EMAIL_PORT", default=25, cast=int)
MASTER_EMAIL_FOR_INVITATIONS = "do-not-reply@nirvedacognition.ai"

# Security Configurations
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
X_FRAME_OPTIONS = "DENY"
CLEAR_SITE_DATA_URL_WHITELIST = []  # URLs to send back clear-site-data header.
SECURE_HSTS_SECONDS = 3600  # Time to block insecure requests.
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
TWO_FACTOR_REMEMBER_COOKIE_AGE = 1209600

ALLOWED_HOSTS = ["*"]

# Session Configuration
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
SESSION_TIME_IN_SECONDS = 1209600

# CSRF Configuration
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True

# CORS Configuration
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = [
    "http://app-ncp.s3-website-us-east-1.amazonaws.com",
    "http://nirveda-dev2.s3-website-us-east-1.amazonaws.com",
    "https://kpmg-azure1.nirvedacognition.ai",
    "https://appfrontend.z13.web.core.windows.net",
    NC_APP_FRONTEND_URL,
]
if NC_APP_FRONTEND_URL_2 is not None:
    CORS_ORIGIN_WHITELIST.append(NC_APP_FRONTEND_URL_2)
if NC_APP_FRONTEND_URL_3 is not None:
    CORS_ORIGIN_WHITELIST.append(NC_APP_FRONTEND_URL_3)

CORS_ALLOW_HEADERS = list(default_headers) + [
    "x-ms-blob-type",
    "x-ms-version",
    "refresh_token",
]

CORS_EXPOSE_HEADERS = ["refresh_token"]

REFERRER_POLICY = "same-origin"

AUTH_USER_MODEL = "custom_auth.CustomUser"

AXES_ENABLED = False
AXES_PROXY_ORDER = None
AXES_PROXY_COUNT = 1
AXES_PROXY_TRUSTED_IPS = None
AXES_META_PRECEDENCE_ORDER = None

LOGIN_URL = "two_factor:login"

INSTALLED_APPS = [
    "backend",  # Must be before django authentication.
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_celery_beat",
    "django_celery_results",
    "django_extensions",
    "django_nose",
    "django_filters",
    "health_check",
    "axes",
    "security",
    "django_otp",
    "django_otp.plugins.otp_static",
    "django_otp.plugins.otp_totp",
    "two_factor",
    "health_check.db",
    "health_check.cache",
    "health_check.storage",
    "health_check.contrib.psutil",
    "rest_framework",
    "rest_framework_filters",
    "drf_yasg",
    "corsheaders",
    "backend.app.ai_services",
    "backend.app.authentication",
    "backend.app.custom_auth",
    "backend.app.nc_core",
    "backend.app.collection",
    "backend.app.document",
    "backend.app.organization",
    "backend.app.products.alfa",
    "backend.app.products.apex",
    "backend.app.products.kpmg",
    "backend.app.task",
    "backend.app.user",
]

HEALTH_CHECK = {
    "DISK_USAGE_MAX": 90,  # percent
    "MEMORY_MIN": 100,  # in MB
}

AUTHENTICATION_BACKENDS = [
    "axes.backends.AxesBackend",  # Must be first!
    # "django.contrib.auth.backends.ModelBackend",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "restrictedsessions.middleware.RestrictedSessionsMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "security.middleware.DoNotTrackMiddleware",
    "csp.middleware.CSPMiddleware",
    "axes.middleware.AxesMiddleware",
    "django.middleware.common.CommonMiddleware",
    # 'request_logging.middleware.LoggingMiddleware',
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django_otp.middleware.OTPMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django_referrer_policy.middleware.ReferrerPolicyMiddleware",
    "security.middleware.ClearSiteDataMiddleware",
    "security.middleware.DoNotTrackMiddleware",
]

ROOT_URLCONF = "backend.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "backend.wsgi.application"

# Database Configurations
DATABASES = {}
DATABASES["default"] = dj_database_url.parse(config("DATABASE_URL"))
DATABASES["default"]["ATOMIC_REQUESTS"] = True

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },  # noqa
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},  # noqa
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},  # noqa
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"
    },  # noqa
    {
        "NAME": "backend.lib.django_utils.validators.NumberValidator",  # noqa
        "OPTIONS": {"min_digits": 1},
    },
    {
        "NAME": "backend.lib.django_utils.validators.UppercaseValidator",
    },
    {
        "NAME": "backend.lib.django_utils.validators.LowercaseValidator",
    },
    {
        "NAME": "backend.lib.django_utils.validators.SymbolValidator",
    },
]

STATICFILES_DIRS = [
    str(BASE_DIR / "static"),
]
STATIC_URL = "/static/"
STATIC_ROOT = str(BASE_DIR / "statcdn/")

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": datetime.timedelta(days=1),
    "REFRESH_TOKEN_LIFETIME": datetime.timedelta(days=7),
    "ROTATE_REFRESH_TOKENS": False,
    "BLACKLIST_AFTER_ROTATION": True,
    "ALGORITHM": "HS256",
    "SIGNING_KEY": SECRET_KEY,
    "VERIFYING_KEY": None,
    "AUDIENCE": None,
    "ISSUER": None,
    "AUTH_HEADER_TYPES": ("Bearer",),
    "USER_ID_FIELD": "id",
    "USER_ID_CLAIM": "user_id",
    "AUTH_TOKEN_CLASSES": ("rest_framework_simplejwt.tokens.AccessToken",),
    "TOKEN_TYPE_CLAIM": "token_type",
    "JTI_CLAIM": "jti",
    "SLIDING_TOKEN_REFRESH_EXP_CLAIM": "refresh_exp",
    "SLIDING_TOKEN_LIFETIME": datetime.timedelta(minutes=5),
    "SLIDING_TOKEN_REFRESH_LIFETIME": datetime.timedelta(days=1),
}

REST_FRAMEWORK = {
    "DATETIME_FORMAT": "%Y-%m-%d %H:%M:%S",
    "NON_FIELD_ERRORS_KEY": "__all__",
    "EXCEPTION_HANDLER": ("backend.lib.rest_framework_utils.views.exception_handler"),
    "DEFAULT_FILTER_BACKENDS": [
        "rest_framework_filters.backends.RestFrameworkFilterBackend",
        "rest_framework.filters.SearchFilter",
        "rest_framework.filters.OrderingFilter",
    ],
    "DEFAULT_AUTHENTICATION_CLASSES": [
     #   "rest_framework_simplejwt.authentication.JWTAuthentication",
      #  "rest_framework.authentication.SessionAuthentication",
        "backend.app.authentication.auth.AzureSSOAuthentication",
     #"rest_framework.authentication.TokenAuthentication",
    ],
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated",
    ],
    "DEFAULT_PAGINATION_CLASS": "backend.lib.rest_framework_utils.pagination.Pagination",  # noqa
    "DEFAULT_THROTTLE_CLASSES": [
        "rest_framework.throttling.AnonRateThrottle",
        "rest_framework.throttling.UserRateThrottle",
    ],
    "DEFAULT_THROTTLE_RATES": {"anon": "20/minute", "user": "120/minute"},
    "DEFAULT_RENDERER_CLASSES": [
        "rest_framework.renderers.JSONRenderer",
    ],
}

API_PROXY_URL = config("API_PROXY_URL", default=None)
APIS_PROXY_URL = config("APIS_PROXY_URL", default=None)

ALFA = {
    "SCHEMA_TYPE": "Loan",
    "SCHEMA_DIR": APPS_DIR
    / "nc_core"
    / "business_logic"
    / "ALFA"
    / "schema.json",  # noqa
}

APEX = {
    "SCHEMA_TYPE": "WorkItem",
    "SCHEMA_DIR": APPS_DIR
    / "nc_core"
    / "business_logic"
    / "APEX"
    / "schema.json",  # noqa
}


'''AZURE_AD_CONFIG = {
    "SESSION_KEY": "azure_sso",
    "ORG": config("AZURE_AD_ORG", default="Nirveda"),
    "CLIENT_ID": config(
        "AZURE_AD_CLIENT_ID", default="ccc974fe-cc54-4ccb-bb27-9af8747cc0c3"
    ),
    "CLIENT_SECRET": config(
        "AZURE_AD_CLIENT_SECRET", default="QSB7Q~uf85qTwcp16xsZOOBhv6g4tqmpNeSli"
    ),
    "TENANT_ID": config(
        "AZURE_AD_TENANT_ID", default="d46def8d-8168-45af-9854-512d5c6ae11a"
    ),
    "SCOPES": [
        "User.Read",
        "email",
    ],
    "REDIRECT_URI": config(
        "AZURE_AD_REDIRECT_URI", default="http://localhost:8000/v2/auth/azure/redirect/"
    ),
}'''

#localhost Azure AD 
AZURE_AD_CONFIG = {
    "SESSION_KEY": "azure_sso",
    "ORG": config("AZURE_AD_ORG", default="Nirveda AD"),
    "CLIENT_ID": config(
        "AZURE_AD_CLIENT_ID", default="1169cd32-45f3-4247-ba3c-f2554ddf48ed"
    ),
    "CLIENT_SECRET": config(
        "AZURE_AD_CLIENT_SECRET", default="lDB8Q~msYufJ23.n8sR.ng1SYdcnswJNI4HtPdbc"
    ),
    "TENANT_ID": config(
        "AZURE_AD_TENANT_ID", default="e68eaf25-a859-4307-b6f6-019f1cd59a56"
    ),
    "SCOPES": [
        "User.Read",
        "email",
    ],
    "REDIRECT_URI": config(
        "AZURE_AD_REDIRECT_URI", default="http://localhost:8000/v2/auth/azure/redirect/"
    ),
}

AUTHENTICATION_BACKENDS.append("backend.app.authentication.backends.AzureModelBackend")

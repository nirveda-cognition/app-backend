from django.contrib.auth import get_user_model

from backend.app.organization.models import Organization
from backend.app.user.models import CustomUser

from .base import CustomCommandMixin


class UserMixin(CustomCommandMixin):
    def query_user_by_email(self, **kwargs):
        UserModel = get_user_model()
        return self.query_until(
            prefix="Email Address",
            prompt="Please enter the user's email address.",
            is_valid=lambda value: (
                UserModel.objects.filter(email=value, **kwargs).first() is not None
            ),
            invalid_prompt="Superuser with email does not exist.",
            converter=lambda ans: UserModel.objects.get(email=ans),
        )

    def query_user_information(self):
        info = {}
        info["email"] = self.query_until(
            prefix="Email Address",
            prompt="Please enter the user's email address.",
            is_valid=lambda value: CustomUser.objects.filter(email=value).first()
            is None,  # noqa
            invalid_prompt="User with email already exists.",
        )
        info["first_name"] = self.query(
            prompt="Please enter the user's first name.", prefix="First Name"
        )
        info["last_name"] = self.query(
            prompt="Please enter the user's last name.", prefix="Last Name"
        )
        info["password"] = self.query(
            prompt="Please enter the desired password.", prefix="Password"
        )
        self.query_until(
            prefix="Confirm",
            is_valid=lambda value: value == info["password"],
            invalid_prompt="The passwords do not match, please try again.",
        )
        return info


class OrganizationMixin(CustomCommandMixin):
    def query_organization(self):
        return self.query_options(
            options=Organization.objects.all(),
            option_display="name",
            is_valid=lambda value: (
                Organization.objects.filter(name__iexact=value).first() is not None
            ),
            converter=lambda ans: Organization.objects.get(name__iexact=ans),
            prompt="Select an organization for the users.",
            prefix="Name",
            invalid_prompt="Invalid organization, try again.",
        )

from django.core import management

from backend.management.base import CustomCommand

FIXTURES = ["types", "organizations", "roles", "documentstatuses"]


class Command(CustomCommand):
    def handle(self, **options):
        for fixture in FIXTURES:
            management.call_command("loaddata", fixture)

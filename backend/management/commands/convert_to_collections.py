import copy

from django.core.management.base import BaseCommand

from backend.app.collection.models import Collection
from backend.app.document.models import Document

# WIP
"""
collect all the ai.nirveda stuff
reduce those paths
generate collections for every path
correctly associate child collections with parent collections
associate the documents with the correct collections



"""


class Command(BaseCommand):
    help = (
        "converts a folder hierarchy defined in the former"
        "document syntax to proper collection objects"
    )

    def add_arguments(self, parser):
        # add logic here to handle a specific conversion
        pass

    def reduce_paths(self, paths):
        """
        This function will take a list of paths
        and eliminates any subpaths:
        '/hi/bye' & '/hi get reduced to -> '/hi/bye
        """
        new_paths = {}
        paths_copy = copy.deepcopy(list(paths.keys()))
        for path in paths.keys():
            paths_copy.remove(path)
            path_found = False
            for super_path in paths_copy:
                if path in super_path:
                    path_found = True
                    break
            if not path_found:
                new_paths[path] = paths[path]
        return new_paths

    def create_collection(self, path, owner, path_col_dict):
        """
        This function accepts a/path/to/a/document and
        builds collections based on that path. It then stores
        the path in plain text as a dictionary key whose
        value is the newly created collection.
        """
        col_to_create = path.split("/")[1:]

        print(col_to_create)

        tmp = []
        for i in range(0, len(col_to_create)):

            # link_path = '/' + '/'.join(col_to_create[0:i])
            # link_object = path_col_dict[link_path]
            col_path = col_to_create[i]
            # col_name = col_path.split('/')[-1:][0]

            col_object = Collection.objects.filter(name=col_path, owner=owner).first()

            if col_object is None:
                col_object = Collection(
                    name=col_to_create[i],
                    organization=owner.organization,
                    owner=owner,
                    created_by=owner,
                )
                col_object.save()

            tmp.append(col_object)

            path_col_dict[col_path] = col_object

        print("/////////")
        print(tmp)

        for i in range(0, len(tmp)):
            try:
                child = tmp[i + 1]
                tmp[i].collections.add(child)
                tmp[i].save()
            except Exception as e:
                print(e)
                pass

    def handle(self, *args, **options):
        """
        Function entry point. We process old-school, path-based
        collections into new-school Collection objects (nc_core.models)
        Then we go through all documents and add documents with
        matching paths to the newly created collection objects.
        """
        d = Document.objects.all()
        paths = {}
        for doc in d:
            if doc.name == "ai.nirveda":
                paths[doc.path] = doc.owner
                doc.state = "Trash"
                doc.save()
        reduced_paths = self.reduce_paths(paths)

        path_col_dict = {}
        for full_path in reduced_paths.keys():
            self.create_collection(full_path, reduced_paths[full_path], path_col_dict)

        for doc in d:
            if doc.name != "ai.nirveda":

                col_name = doc.path.split("/")[-1]

                col = None
                if col_name != "":
                    col = Collection.objects.filter(name=col_name).first()

                if col is not None:
                    doc.collection_list.add(col)
                    doc.save()

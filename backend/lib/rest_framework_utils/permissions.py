from rest_framework.permissions import IsAuthenticated


class IsSuperuser(IsAuthenticated):
    """
    Permission class to set superuser permission.
    """

    def has_permission(self, request, view):
        # TODO: We still need to be returning the super() here even if it
        # evaluates to False, because otherwise we are returning None (not
        # False).
        if super().has_permission(request, view):
            return request.user.is_superuser


class IsAdmin(IsAuthenticated):
    """
    Permission class to set admin permission.
    """

    def has_permission(self, request, view):
        # TODO: We still need to be returning the super() here even if it
        # evaluates to False, because otherwise we are returning None (not
        # False).
        if super().has_permission(request, view):
            return not request.user.is_staff and request.user.is_admin


class IsAdminOrSuperuser(IsAuthenticated):
    """
    Permission class to set admin or superuser permission.
    """

    def has_permission(self, request, view):
        # TODO: We still need to be returning the super() here even if it
        # evaluates to False, because otherwise we are returning None (not
        # False).
        if super().has_permission(request, view):
            return (
                (not request.user.is_staff and request.user.is_admin)
                or request.user.is_superuser
                or (
                    request.user.role.name == "account_manager"
                    if request.user.role
                    else False
                )
            )


class IsAccountManager(IsAuthenticated):
    """
    Permission class to set manager permission.
    """

    def has_permission(self, request, view):
        # TODO: We still need to be returning the super() here even if it
        # evaluates to False, because otherwise we are returning None (not
        # False).
        if super().has_permission(request, view):
            return (
                request.user.role.name == "account_manager"
                if request.user.role
                else False
            )

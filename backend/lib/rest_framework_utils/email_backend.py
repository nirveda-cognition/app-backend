from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend


class CustomEmailBackend(EmailBackend):
    """
    An SMTP email backend for use with an SMTP email server.

    Args:
        fail_silently
            Flag that determines SMTP backend errors should throw an exception.
    """

    def __init__(self, fail_silently=False, **kwargs):
        email_args = {
            "fail_silently": fail_silently,
            "host": settings.SMTP_EMAIL_HOST,
            "port": settings.SMTP_EMAIL_PORT,
        }
        super(CustomEmailBackend, self).__init__(**email_args)

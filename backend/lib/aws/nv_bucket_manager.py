from django.conf import settings

from .nv_blob import NVBlob
from .nv_s3_bucket import NVs3bucket


# TODO: This class should be deprecated, it is a completely useless abstraction
# on top of NVs3bucket.
class NVBucketManager(object):
    def __init__(self):
        self.env = settings.AWS_STORAGE_TARGET
        if self.env == "azure":
            self.bucket = NVBlob()
            self.container = settings.AZURE_CONTAINER_NAME
            self.headers = {
                "Content-Type": "application/octet-stream",
                "x-ms-blob-type": "BlockBlob",
                "x-ms-version": "2019-07-07",
            }
        if self.env == "aws":
            self.bucket = NVs3bucket()
            self.container = settings.NC_AWS_BUCKET_NAME
            self.headers = {
                "Content-Type": "application/octet-stream",
            }

    def generate_put_presigned_url(self, *args, **kwargs):
        return self.bucket.generate_put_presigned_url(self.container, *args, **kwargs)

    def generate_get_presigned_url(self, *args, **kwargs):
        return self.bucket.generate_get_presigned_url(self.container, *args, **kwargs)

    def generate_head_presigned_url(self, *args, **kwargs):
        return self.bucket.generate_head_presigned_url(self.container, *args, **kwargs)

    def file_delete(self, *args, **kwargs):
        return self.bucket.file_delete(self.container, *args, **kwargs)

    def file_fetch(self, *args, **kwargs):
        return self.bucket.file_fetch(self.container, *args, **kwargs)

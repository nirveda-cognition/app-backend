import logging
from copy import deepcopy

import django
from django.db.models import fields
from django.db.models.signals import post_init
from django.utils import timezone

from backend.lib.utils.fs import increment_name_count

# For safety reasons, we will set this to False for now - but we eventually
# want it to be True.  The IntegrityError seems to be getting triggered every
# now and then, so we should figure out why that is.
STRICT = False


logger = logging.getLogger("backend")


def increment_model_attribute_until_unique(
    value, model_cls, attribute="name", **kwargs
):
    """
    Provided with a string attribute on a Django model class, the provided
    base value will be suffixed with (i) (where i is an integer) until there
    is not already an instance of the model associated with the suffixed base
    value.

    Parameters:
    ----------
    value: :obj:`str`
        The base value that if already present on a model meeting the criteria,
        will be incremented with integer suffixes until it is unique.
    model_cls: :obj:`type`
        The Django model class.
    attribute: :obj:`type`
        The attribute on the Django model that the provided value refers to.
        Default: name
    """
    query = deepcopy(kwargs)
    query[attribute] = value
    if model_cls.objects.filter(**query).count() == 0:
        return value
    return increment_model_attribute_until_unique(
        value=increment_name_count(value),
        model_cls=model_cls,
        attribute=attribute,
        **kwargs
    )


def track_field_changes(*args):
    """
    Tracks the property changes on a :obj:`django.db.models.Model`.

    Corresponding time-oriented fields can be optionally provided so that the
    time a field was last changed can be stored and managed.

    Parameters:
    ----------
    *:
        An iterable of fields to track (as :obj:`str`) or fields to track paired
        with a corresponding time field to manage (as :obj:`dict`).

        To avoid confusion, we define the following:

        - Tracked Field: The field whose changes we want to track.
        - Timed Field:   The optional field that can be updated to store the
                         last time that the "Tracked Field" was changed.

        Using these definitions, each element of the provided iterable can be
        one of:

        (1) tracked-field
        (2) {tracked-field: timed-field}

        If the timed-field is provided, the field associated with that field
        name will be updated everytime the tracked-field changes.  The
        timed-field must correspond to a field on the model that is a
        :obj:`django.db.models.DateTimeField`.

    Usage:
    -----

    >>> @track_field_changes('weight', { 'color': 'color_changed_at' })
    >>> class Apple(db.Model):
    >>>     weight = models.IntegerField()
    >>>     color = models.CharField()
    >>>     color_changed_at = models.DateTimeField()

    >>> apple = Apple(weight=1, color='blue')
    >>> apple.save()
    >>> apple.fields_changed
    >>> {}

    >>> apple.color = "red"
    >>> apple.fields_changed = {"color": "red"}
    >>> apple.save()
    >>> apple.fields_changed
    >>> {}
    >>> apple.color_changed_at = "YY-MM-DD 00:00:00"

    Note:
    ----
    IMPORTANT: Since the Django `save` method is not called when a model
    is updated (i.e. Model.objects.filter().update()) - this will not work.
    Unfortunately, since the .update() method is so close to the SQL layer,
    there really isn't a way in Django to avoid this.
    """

    if len(args) == 0:
        raise ValueError("Must provide fields to track.")

    # Flatten/prune the provided fields into a uniform array of tuples.
    flattened_fields = []
    for field in args:
        if isinstance(field, dict):
            for subfield, subtimefield in field.items():
                flattened_fields.append((subfield, subtimefield))
        else:
            flattened_fields.append((field, None))

    def _store(self):
        self.__data = dict()
        if self.id:
            self.__data = dict(
                (f, getattr(self, f)) for f in [field[0] for field in flattened_fields]
            )

    def inner(cls):
        # Contains a local copy of the previous values of the fields.
        cls.__data = {}

        # The fields that are being tracked - after they are validated.
        cls.__tracked_fields = []

        # Stores a reference between the field that is being tracked and the
        # field that is being updated whenever the tracked field changes.
        cls.__timed_field_mapping = {}

        # Stores the last time that the tracked field was changed, if the
        # tracked field is provided with an associated DateTimeField to track
        # it's chronological changes.
        # Need to wait for initialization to populate because we need the
        # values stored from the database.
        cls.__last_time_field_changed = {}

        for field, timed_field in flattened_fields:
            # Validate and store the field we are tracking.
            if not isinstance(field, str):
                raise ValueError(
                    "Invalid Field %s - The field must be a string "
                    "corresponding to a field on the model." % field
                )
            try:
                field_cls = cls._meta.get_field(field)
            except django.core.exceptions.FieldDoesNotExist:
                raise ValueError(
                    "The field %s does not exist on the model %s."
                    % (field, cls.__name__)
                )
            else:
                cls.__tracked_fields.append(field)

            # Validate and store the field used to track the times at which the
            # tracked field was changed.
            if timed_field is not None:
                if not isinstance(timed_field, str):
                    raise ValueError(
                        "Invalid Field %s - The field must be a string "
                        "corresponding to a field on the model." % timed_field
                    )
                try:
                    field_cls = cls._meta.get_field(timed_field)
                except django.core.exceptions.FieldDoesNotExist:
                    raise ValueError(
                        "The field %s does not exist on the model %s."
                        % (timed_field, cls.__name__)
                    )
                else:
                    if not isinstance(field_cls, fields.DateTimeField):
                        raise ValueError(
                            "The field %s is used to track the last time that "
                            "field %s changed - so the field corresponding "
                            "to %s must correspond to a DateTimeField."
                            % (timed_field, field, timed_field)
                        )
                    cls.__timed_field_mapping[field] = timed_field

        def field_changed(self, field):
            """
            Returns a boolean to indicate whether or not the provided field
            has changed since the last time the model was saved.

            Parameters:
            ----------
            field: :obj:`str`
                The name of the field on the model.
            """
            if field not in self.__tracked_fields:
                raise ValueError("The field %s is not being tracked." % field)
            if field in self.__data and self.__data[field] != getattr(self, field):
                return True
            return False

        def previous_value(self, field):
            """
            Returns the previous value for the provided field before the last
            save.

            Parameters:
            ----------
            field: :obj:`str`
                The name of the field on the model.
            """
            if field not in self.__tracked_fields:
                raise ValueError("The field %s is not being tracked." % field)
            return self.__data.get(field)

        @property
        def fields_changed(self):
            """
            Returns a dictionary of field names and values for the fields that
            have changed since the last save.
            """
            changed = {}
            for k in self.__tracked_fields:
                if k in self.__data and self.__data[k] != getattr(self, k):
                    changed[k] = self.__data[k]
            return changed

        def save(self, *args, **kwargs):
            """
            Overrides the :obj:`django.db.models.Model` save behavior to
            update the fields on the instance that are used to track the last
            time a given field has changed.
            """
            for k in self.__tracked_fields:
                if k in self.__data and k in self.__timed_field_mapping:
                    if self.__data[k] != getattr(self, k):
                        # The field was changed - update the value of the
                        # field tracking the time the field was last changed
                        # and update this time local to the instance.
                        time_changed = timezone.now()
                        self.__last_time_field_changed[k] = time_changed
                        setattr(self, self.__timed_field_mapping[k], time_changed)
                    else:
                        # The field was not changed - make sure that the time
                        # tracking field isn't being manually updated.
                        # The field will only not have a record of the last
                        # time it was changed if the instance has not been saved
                        # yet.
                        if self.id is not None:
                            last_time_field_changed = self.__last_time_field_changed[
                                k
                            ]  # noqa
                            current_time_field_changed = getattr(
                                self, self.__timed_field_mapping[k]
                            )
                            if (
                                last_time_field_changed != current_time_field_changed
                            ):  # noqa
                                if STRICT:
                                    raise django.db.IntegrityError(
                                        "Field %s can only be updated when field "  # noqa
                                        "%s changes."
                                        % (self.__timed_field_mapping[k], k)
                                    )
                                # logger.warning(
                                #     "Field %s is being updated when field %s "
                                #     "did not change - this might indicate "
                                #     "invalid usage of the field." % (
                                #         self.__timed_field_mapping[k], k)
                                # )

            save._original(self, *args, **kwargs)
            _store(self)

        def _post_init(sender, instance, **kwargs):
            _store(instance)

            # When the instance is loaded from the DB, initialize the last
            # time that the time tracked fields were updated as the current
            # value of the fields used to track the times they were updated.
            for k, v in instance.__timed_field_mapping.items():
                instance.__last_time_field_changed[k] = getattr(instance, v)

        post_init.connect(_post_init, sender=cls, weak=False)

        # Expose helper methods on the model class.
        cls.fields_changed = fields_changed
        cls.field_changed = field_changed
        cls.previous_value = previous_value

        # Replace the model save method with the overridden one, but keep track
        # of the original save method so it can be reapplied.
        save._original = cls.save
        cls.save = save
        return cls

    return inner

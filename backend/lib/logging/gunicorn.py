import logging

from gunicorn import glogging


# TODO: We can most likely configure this from settings.
class HealthCheckFilter(logging.Filter):
    def filter(self, record):
        return record.getMessage().find("/healthcheck") == -1


class PollFilter(logging.Filter):
    def filter(self, record):
        return "poll=true" in record.getMessage()


class CustomGunicornLogger(glogging.Logger):
    def setup(self, cfg):
        super().setup(cfg)

        # Add filters to Gunicorn logger
        logger = logging.getLogger()
        logger.addFilter(HealthCheckFilter())
        logger.addFilter(PollFilter())

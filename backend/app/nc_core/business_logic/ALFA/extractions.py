enriched_transactions = [
    {
        "date": "2020-05-10",
        "name": "Chase",
        "amount": 2400,
        "pending": False,
        "top_level_category": "Mortgage Interest",
        "category": "Mortgage Interest",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Mortgage",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
    {
        "date": "2020-05-11",
        "name": "DWP",
        "amount": 289.4,
        "pending": False,
        "top_level_category": "Utilities",
        "category": "Utilities",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Utilities",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
    {
        "date": "2020-06-09",
        "name": "Chase",
        "amount": 2400,
        "pending": False,
        "top_level_category": "Mortgage Interest",
        "category": "Mortgage Interest",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Mortgage",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
    {
        "date": "2020-06-12",
        "name": "DWP",
        "amount": 289.4,
        "pending": False,
        "top_level_category": "Utilities",
        "category": "Utilities",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Utilities",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
    {
        "date": "2020-07-10",
        "name": "Chase",
        "amount": 2400,
        "pending": False,
        "top_level_category": "Mortgage Interest",
        "category": "Mortgage Interest",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Mortgage",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
    {
        "date": "2020-07-11",
        "name": "DWP",
        "amount": 289.4,
        "pending": False,
        "top_level_category": "Utilities",
        "category": "Utilities",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Utilities",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
    {
        "date": "2020-08-09",
        "name": "Chase",
        "amount": 2400,
        "pending": False,
        "top_level_category": "Mortgage Interest",
        "category": "Mortgage Interest",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Mortgage",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
    {
        "date": "2020-08-13",
        "name": "DWP",
        "amount": 289.4,
        "pending": False,
        "top_level_category": "Utilities",
        "category": "Utilities",
        "location": {
            "lat": None,
            "lon": None,
            "city": None,
            "region": None,
            "address": None,
            "country": None,
            "postal_code": None,
            "store_number": None,
        },
        "account_id": "Jp14jnaqNjI5p4AxlKl4SW7d6yw3GaCdg8KRX",
        "category_id": "13005000",
        "payment_meta": {
            "payee": None,
            "payer": None,
            "ppd_id": None,
            "reason": None,
            "by_order_of": None,
            "payment_method": None,
            "reference_number": None,
            "payment_processor": None,
        },
        "account_owner": None,
        "transaction_id": "NEA7ZlwBjZH51jJazyzjSKnmMNkRaxuWpnQPy",
        "date_transacted": "2020-05-10",
        "transaction_type": "place",
        "iso_currency_code": "USD",
        "original_description": "Utilities",
        "pending_transaction_id": None,
        "unofficial_currency_code": None,
    },
]


def extract_payroll_data(data):
    return {}


def extract_account_data(data):
    assets = data.records["asset_report_data"]
    assets = enriched_transactions + assets
    assets_dict = {
        "transactions": assets,
    }
    return assets_dict


def process_table(table):
    table_map = {
        "Employees who made $100k or less": "schedule_a_worksheet",
        "Employees who made more than $100k": ("schedule_a_worksheet_more_than_100k"),
        "Reductions": "reduction",
        "Schedule A": "schedule_a",
    }

    headers = [item["value"] for item in table["items"][0]["elements"]]
    headers = [h.replace("\n", " ") for h in headers]
    items = table["items"][1:]

    label = table["label"]
    key = table_map.get(label)
    table_data = []

    for item in items:
        item_data = {}
        for i in range(0, len(item["elements"])):
            item_data[headers[i]] = item["elements"][i]["value"]
        table_data.append(item_data)

    return key, table_data


def extract_gusto_ppp_report(result):
    extracted_data = result.new_data["data"]["extracted_data"]
    feature_dict = {}
    for item in extracted_data:
        prop_name = item["type"]

        if prop_name == "field":
            if item["label"] in (
                "Borrower Name",
                "Borrower Address",
                "FEIN",
                "Covered Period",
                "Total loan amount",
            ):
                label = item["label"].lower().replace(" ", "_")
                feature_dict[label] = item["value"]

        elif prop_name in ("collection"):
            table_type, table_data = process_table(item)
            if table_type:
                type = feature_dict.get(table_type)
                if not type:
                    feature_dict[table_type] = []
                feature_dict[table_type].extend(table_data)

    alerts = ["Congrats, you have successfully uploaded your Gusto PPP report."]
    feature_dict["alerts"] = alerts
    return feature_dict

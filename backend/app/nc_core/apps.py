from django.apps import AppConfig


class NcCoreConfig(AppConfig):
    name = "backend.app.nc_core"

    def ready(self):
        import backend.app.nc_core.signals.signals  # noqa

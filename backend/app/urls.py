from django.urls import include, path

from .views import APIDocumentation
print("DEBUG : In app urls")
urlpatterns = [
    path("ai_services/", include("backend.app.ai_services.urls")),
    path("auth/", include("backend.app.authentication.urls")),
    path("organizations/", include("backend.app.organization.urls")),
    path("products/", include("backend.app.products.urls")),
    path("users/", include("backend.app.user.urls")),
    path("api-documentation/", APIDocumentation.as_view(), name="api-documentation"),
]

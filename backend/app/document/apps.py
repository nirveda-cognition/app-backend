from django.apps import AppConfig


class DocumentConfig(AppConfig):
    name = "backend.app.document"
    verbose_name = "Document"

from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from backend.app.organization.mixins import OrganizationNestedMixin


class DocumentNestedMixin(OrganizationNestedMixin):
    """
    A mixin for views that extend off of a document's detail endpoint:
    """

    @property
    def document_lookup_field(self):
        raise NotImplementedError()

    @cached_property
    def document(self):
        params = {
            self.document_lookup_field[0]: (self.kwargs[self.document_lookup_field[1]])
        }
        return get_object_or_404(self.organization.documents.all(), **params)

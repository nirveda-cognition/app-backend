import base64

from django_filters import rest_framework as filters
from rest_framework import decorators, mixins, response, views, viewsets
from rest_framework.exceptions import ParseError

from backend.app.collection.mixins import CollectionNestedMixin
from backend.app.common.serializers import DocumentSimpleSerializer
from backend.app.organization.mixins import OrganizationNestedMixin
from backend.app.products.kpmg.utils import classify_as_tax
from backend.app.user.models import UserCategory
from backend.lib.aws.nv_bucket_manager import NVBucketManager
from backend.lib.rest_framework_utils.pagination import paginate_action

from .models import Document, DocumentStatus
from .serializers import DocumentSerializer, UploadDocumentSerializer
from .utils import create_and_process_document


class DocumentFilter(filters.FilterSet):
    created_at__gte = filters.DateTimeFilter(field_name="created_at", lookup_expr="gte")
    created_at__lte = filters.DateTimeFilter(field_name="created_at", lookup_expr="lte")
    status_changed_at__lte = filters.DateTimeFilter(
        field_name="status_changed_at", lookup_expr="lte"
    )
    status_changed_at__gte = filters.DateTimeFilter(
        field_name="status_changed_at", lookup_expr="gte"
    )
    document_status = filters.ModelChoiceFilter(
        queryset=DocumentStatus.objects.all(),
        field_name="document_status__status",
        to_field_name="status",
    )

    class Meta:
        model = Document
        fields = (
            "created_at__gte",
            "created_at__lte",
            "status_changed_at__lte",
            "status_changed_at__gte",
            "document_status",
        )


class GenericDocumentViewSet(viewsets.GenericViewSet):
    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    filterset_class = DocumentFilter
    ordering_fields = ["status_changed_at", "name", "created_at", "updated_at"]
    search_fields = ["name"]
    serializer_class = DocumentSerializer

    @property
    def is_simple(self):
        # TODO: We want to be able to use a simple serialized form of the
        # collection on GET requests - however, there are smarter ways of doing
        # this than the one seen here.  We should investigate how to do this
        # more elegantly.
        simple = self.request.GET.get("simple", False)
        if simple is not False:
            return True
        return False

    def get_serializer_class(self):
        if self.is_simple:
            return DocumentSimpleSerializer
        return self.serializer_class


class DocumentViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    GenericDocumentViewSet,
    OrganizationNestedMixin,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/documents/
    (2) POST /organizations/<pk>/documents/
    (3) GET /organizations/<pk>/documents/<pk>/
    (4) DELETE /organizations/<pk>/documents/<pk>/
    """

    @property
    def is_include_trash(self):
        print("DEBUG : IS_INCLUDE_TRASH FUNCTION IN DOCUMENTVIEWSET")
        include_trash = self.request.GET.get("include_trash", False)
        if (
            include_trash is not False
            and self.request.user.category == UserCategory.SUPERUSER
            and self.action == "list"
        ):
            return True
        return False

    def get_queryset(self):
        print("DEBUG : GET_QUERYSET FUNCTION IN DOCUMENTVIEWSET")
        simple = self.request.GET.get("simple", False)

        if self.is_include_trash:
            return self.organization.documents.all()

        if simple is not False:
            # Idea use .defer('task_results') or whatever to omit the bulky data

            pass
        return self.organization.documents.active()

    @paginate_action
    def list(self, request, *args, **kwargs):
        print("DEBUG : LIST FUNCTION IN DOCUMENTVIEWSET")
        queryset = self.get_queryset()
        simple = self.request.GET.get("simple")
        if simple:
            try:
                # values when document is part of a collection
                queryset = queryset.values(
                    "id",
                    "size",
                    "name",
                    "document_status__status",
                    "status_changed_at",
                    "created_at",
                    "document_status",
                    "organization_id",
                    "updated_at",
                    "collection_list__name",
                    "collection_list__id",
                )
            except Exception as e:
                print(e)

                #  values for independent document
                queryset = queryset.values(
                    "id",
                    "size",
                    "name",
                    "document_status__status",
                    "status_changed_at",
                    "created_at",
                    "document_status",
                    "organization_id",
                    "updated_at",
                )

        return queryset

    def perform_create(self, serializer):
        print("DEBUG : PERFORM_CREATE FUNCTION IN DOCUMENTVIEWSET")
        serializer.save(created_by=self.request.user, organization=self.organization)

    def destroy(self, request, *args, **kwargs):
        print("DEBUG : DESTROY FUNCTION IN DOCUMENTVIEWSET")
        instance = self.get_object()
        if instance.document_status == DocumentStatus.objects.get(status="Processing"):
            raise ParseError(code=400, detail="Document still processing")
        instance.to_trash(request.user)
        return response.Response(status=204)

    @paginate_action
    @decorators.action(detail=False, methods=["GET"])
    def root(self, request, *args, **kwargs):
        print("DEBUG : ROOT FUNCTION IN DOCUMENTVIEWSET")
        """
        Returns the root :obj:`Document` instances for the organization.
        These :obj:`Document`(s) do not belong to a collection.
        """

        qs = self.get_queryset()
        qs = qs.filter(collection_list=None)
        return self.filter_queryset(qs)

    @decorators.action(detail=True, methods=["GET"], url_path="fetch-file")
    def fetch_file(self, request, *args, **kwargs):
        print("DEBUG : FETCH_FILE FUNCTION IN DOCUMENTVIEWSET")
        instance = self.get_object()
        bucket = NVBucketManager()
        # TODO: Make sure we are obfuscating objects in buckets / containers
        # in hashed folders
        file_fetch = bucket.file_fetch(instance.aws_document_key)
        file_data = base64.b64encode(file_fetch).decode()
        return response.Response({"data": file_data})

    @decorators.action(detail=True, methods=["POST"], url_path="tax-classify")
    def tax_classification(self, request, *args, **kwargs):
        print("DEBUG :TAX_CLASSIFICATION FUNCTION IN DOCUMENTVIEWSET")
        instance = self.get_object()
        classify_as_tax(instance.pk)
        return response.Response({})


class CollectionDocumentViewSet(
    mixins.ListModelMixin, GenericDocumentViewSet, CollectionNestedMixin
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/collections/<pk>/documents
    """

    collection_lookup_field = ("pk", "collection_pk")

    def get_queryset(self):
        print("DEBUG : GET_QUERYSET FUNCTION IN COLLECTIONDOCUMENTVIEWSET")
        return self.collection.documents.active()


class DocumentTrashViewSet(
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericDocumentViewSet,
    OrganizationNestedMixin,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/documents/trash/
    (2) GET /organizations/<pk>/documents/trash/<pk>/
    (3) PATCH /organizations/<pk>/documents/trash/<pk>/restore/
    (4) DELETE /organizations/<pk>/documents/trash/<pk>/
    """

    def get_queryset(self):
        print("DEBUG : GET_QUERYSET FUNCTION IN DOCUMENTTRASHVIEWSET")
        return self.organization.documents.inactive()

    @decorators.action(detail=True, methods=["PATCH"])
    def restore(self, request, *args, **kwargs):
        print("DEBUG : RESTORE FUNCTION IN DOCUMENTTRASHVIEWSET")
        """
        Moves the `obj:Document` that is in the trash out of the trash
        to the main set of `obj:Document`(s).

        PATCH /organizations/<pk>/documents/trash/<pk>/restore/
        """
        instance = self.get_object()
        instance.restore()
        return response.Response(self.serializer_class(instance).data, status=201)

    def destroy(self, request, *args, **kwargs):
        print("DEBUG : DESTROY FUNCTION IN DOCUMENTTRASHVIEWSET")
        instance = self.get_object()
        instance.delete()
        return response.Response(status=204)


class UploadDocumentView(views.APIView):
    """
    View to handle requests to the following endpoints:

    (1) POST /organizations/<pk>/documents/upload/
    """

    def post(self, request, *args, **kwargs):
       
        from datetime import datetime
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S:%f")
        print("DEBUg : time check in uploaaddocumentview at",current_time )
        #import requests
        #import copy
        #import requests
        #from datetime import datetime, timedelta
       # from azure.storage.blob import generate_account_sas, ResourceTypes, AccountSasPermissions
        #import os
# import requests
        #import mimetypes
        #from azure.storage.blob import ContentSettings
        #from urllib.parse import urlparse

        #blob_name = "demo_blob_from_aws"
        #account_name = 'kpmgqa'
        #container_name = 'kpmgqacontainer'
#blob_name = 'cert'
        #sas_token_args = {
        #    "account_name": 'kpmgqa',
        #    "account_key": 'x2tMUWn7fWMJWIdPbK8f4TBEZscn0q7KSm7J62BQoMc16pdkDGOC+PFQHr0H1UI3T3KL5mpZaAXx+AStb1vvlA==',
        #    "resource_types": ResourceTypes(object=True),
        #}
        #account_permissions = {"write": True, "list": True, "update": True, "read": True}
        #expiration = 1800
        #sas_token_args_copy = copy.deepcopy(sas_token_args)
        #sas_token_args_copy.update(
        #        expiry=datetime.utcnow() + timedelta(seconds=expiration),
        #        permission=AccountSasPermissions(**account_permissions),
        #    )
        #sas_token = generate_account_sas(**sas_token_args_copy)

        #put_url='https://kpmgqa.blob.core.windows.net/kpmgqacontainer/'+str(blob_name) + "?" + str(sas_token)
        #print("DEBUG : azure upload put_url to upload to azure ", put_url)
        #headers = {"Content-Type": "application/octet-stream",
        #        "x-ms-blob-type": "BlockBlob",
        #        "x-ms-version": "2019-07-07" }
        #resp = requests.put(put_url, data = request.body, headers = headers)
        #print("DEBUG : azure upload response status ", resp.status_code)
        serializer = UploadDocumentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # TODO: Start passing in the organization from the endpoint, not
        # the organization on the User - because it is safer.
        print("DEBUG : IN APP->DOCUMENT->VIEW UPLOADDOCUMENTVIEW FUNCTION TO CALL CREATE AND PROCESS DOCUMENT")
        print("DEBUG : In post request ", request)
        
       # print("DEBUG : In post request body", request.body)
        print("DEBUG : In post request data ", request.data)
        data = create_and_process_document(
            filename=serializer.validated_data["name"],
            file_type=serializer.validated_data["ext"],
            user=request.user,
            collection=serializer.validated_data.get("collection"),
        )
        return response.Response(
            {
                "put_signed_url": data["put_url"],
                "get_signed_url": data["get_url"],
                "document_id": data["document"].pk,
                "headers": data["headers"],
                "document": DocumentSerializer(data["document"]).data,
            },
            status=201,
        )

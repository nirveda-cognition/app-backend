from django.urls import include, path
from rest_framework import routers

from backend.app.task.urls import document_urlpatterns

from .views import (
    CollectionDocumentViewSet,
    DocumentTrashViewSet,
    DocumentViewSet,
    UploadDocumentView,
)

app_name = "document"
print("DEBUG : IN URLS FILE 1 LINE")
router = routers.SimpleRouter()
print("DEBUG : IN URLS FILE 2 LINE")
router.register(r"trash", DocumentTrashViewSet, basename="trash")
print("DEBUG : IN URLS FILE 3 LINE")
router.register(r"", DocumentViewSet, basename="document")
print("DEBUG : IN URLS FILE 4 LINE")
collection_router = routers.SimpleRouter()
collection_router.register(r"", CollectionDocumentViewSet, basename="document")
print("DEBUG : UPLOADDOCUMENTVIEW IS BEEN CALLED IN APP->DOCUMENT->URLS")
urlpatterns = [
    path("upload/", UploadDocumentView.as_view()),
    path("<int:document_pk>/task/", include(document_urlpatterns)),
] + router.urls

import json

from django.conf import settings
from rest_framework import generics, mixins, response


class APIDocumentation(mixins.RetrieveModelMixin, generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        fmt = "json"
        if not fmt:
            accept = []
            for mime in request.META.get("HTTP_ACCEPT", "").split(","):
                mime, *q = mime.strip().split(";q=")
                if mime:
                    accept.append((mime, float(q[0]) if q else 1.0))
            accept.sort(key=lambda x: x[0].endswith("/*") + (x[0] == "*/*"))
            accept.sort(key=lambda x: x[1], reverse=True)
            accept = dict(accept)

            for mime in accept:
                if mime in ("text/html", "text/*", "*/*"):
                    break
                if mime in ("application/x-yaml", "application/*"):
                    fmt = "yaml"
                    break
                if mime == "application/json":
                    fmt = "json"
                    break

        if fmt == "yaml":
            path = settings.DOCUMENTATION_DIR / "nirveda-client-api.yaml"
            with open(path) as fp:
                res_body = fp.read()
        elif fmt == "json":
            path = settings.DOCUMENTATION_DIR / "nirveda-client-api.json"
            with open(path) as fp:
                res_body = fp.read()

        res_body = json.loads(res_body)
        return response.Response(res_body)

from rest_framework import exceptions, serializers

from backend.app.collection.models import Collection
from backend.app.document.models import Document


class ServiceSerializer(serializers.Serializer):
    service = serializers.CharField(read_only=True)
    label = serializers.CharField(read_only=True)


class NormalizeDocumentDataSerializer(serializers.Serializer):
    collection = serializers.PrimaryKeyRelatedField(
        queryset=Collection.objects.active(),
        required=False,
    )
    documents = serializers.PrimaryKeyRelatedField(
        queryset=Document.objects.active(),
        many=True,
        required=False,
    )

    def validate(self, attrs):
        if "collection" not in attrs and "documents" not in attrs:
            raise exceptions.ValidationError(
                "Either the collection or the documents must be explicitly "
                "provided.",
                code="required",
            )
        if "collection" in attrs:
            return attrs["collection"].documents.all()
        return attrs["documents"]

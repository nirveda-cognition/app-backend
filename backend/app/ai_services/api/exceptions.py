from rest_framework.exceptions import APIException, ErrorDetail

from backend.app.ai_services.exceptions import AIServicesErrorCodes
from backend.lib.utils import filtered_dict


class AttemptThresholdExceededError(Exception):
    """
    Raised when waiting for AI Services to return results for a task and the
    time or count threholds for the number of attempts has been exceeded.
    """

    def __init__(self, threshold):
        self._threshold = threshold

    def __str__(self):
        return "The attempt threshold of %s was exceeded." % self._threshold


class AIServicesHTTPErrorCodes(object):
    TIMED_OUT_ERROR = "timed_out_error"
    CONNECTION_ERROR = "connection_error"
    UNKNOWN_ERROR = "unknown_error"
    CLIENT_ERROR = "client_error"


class AIServicesRequestDetail(ErrorDetail):
    default_message = "There was an unknown error making a request to AI Services API."
    default_code = AIServicesHTTPErrorCodes.UNKNOWN_ERROR

    def __new__(
        cls,
        message=None,
        code=None,
        status_code=None,
        reason=None,
        url=None,
        response=None,
        request=None,
    ):
        self = super().__new__(
            cls, message or cls.default_message, code=code or cls.default_code
        )
        self.status_code = status_code or (
            getattr(response, "status_code", getattr(response, "status", None))
            if response is not None
            else None
        )
        self.reason = reason or (
            getattr(response, "reason", None) if response is not None else None
        )

        request = (
            request or getattr(response, "request", None)
            if response is not None
            else None
        )
        self.url = url or (
            getattr(response, "url", None)
            if response is not None
            else getattr(request, "url")
            if request is not None
            else None
        )
        return self


class AIServicesHttpError(APIException):
    """
    Raised when an error occurs making an HTTP request to the AI services
    API.

    This error is a Django REST Framwork APIException, so that any errors
    interacting with the AI Services API are translated into errors embedded
    in the response.  The error is overridden to embed additional context
    about the request that failed when communicating with the AI Services API.
    """

    TIMED_OUT_ERROR = AIServicesHTTPErrorCodes.TIMED_OUT_ERROR
    CONNECTION_ERROR = AIServicesHTTPErrorCodes.CONNECTION_ERROR
    UNKNOWN_ERROR = AIServicesHTTPErrorCodes.UNKNOWN_ERROR
    CLIENT_ERROR = AIServicesHTTPErrorCodes.CLIENT_ERROR

    status_code = 400
    default_code = AIServicesErrorCodes.HTTP_ERROR
    default_detail = "There was an error communicating with the AI Services API."

    def __init__(self, error=None, **kwargs):
        super().__init__()
        self._error = error
        self._request_detail = AIServicesRequestDetail(**kwargs)

    def __str__(self):
        return "%s" % self.get_full_details()

    @property
    def error(self):
        return self._error

    def get_full_details(self):
        details = super().get_full_details()
        details.update(
            request=filtered_dict(
                {
                    "status_code": self._request_detail.status_code,
                    "code": self._request_detail.code,
                    "message": self._request_detail,
                    "reason": self._request_detail.reason,
                    "url": self._request_detail.url,
                }
            )
        )
        return details


AIServicesHttpError.RequestDetail = AIServicesRequestDetail

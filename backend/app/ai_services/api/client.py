import functools
import logging
import time
import traceback

from django.utils import timezone

from backend.app.organization.exceptions import OrganizationApiKeyError

from .exceptions import AIServicesHttpError, AttemptThresholdExceededError
from .session import AIServicesSession

logger = logging.getLogger("backend")


def controlled_response(lookup=None):
    """
    Decorator to allow the AIServicesClient method to optionally return the
    full raw response from the request or part of the response data.

    Assuming the request will return a response with JSON body...
    >>> {'key': {'foo': 'bar'}}

    >>> @controlled(lookup="key")
    >>> def method(**kwargs):
    >>>     return self._session.get(**kwargs)

    >>> client.method(raw_response=True)
    >>> requests.Response(...)

    >>> client.method()
    >>> {'foo': 'bar'}
    """

    def decorator(func):
        @functools.wraps(func)
        def inner(instance, *args, **kwargs):
            raw_response = kwargs.pop("raw_response", False)
            response = func(instance, *args, **kwargs)

            if raw_response is True:
                return response

            data = response.json()
            if lookup is not None:
                if isinstance(lookup, str):
                    return data[lookup]
                return lookup(data)
            return data

        return inner

    return decorator


class AIServicesClient(object):
    """
    A HTTP client or interacting with the AI Services API.

    Parameters:
    ----------
    access_token: :obj:`str` (semi-optional)
        The access token that will be used to make requests to the AI Services
        API.

        Can be provided either on instantiation of the client or explicitly
        when calling methods on the client.

    request: :obj:`requests.Request` or :obj:`rest_framework.Request` (optional)
        A request to "piggy-back" the access token off of.  If provided, the
        access token will be parsed from the headers of the request.

        Can be provided either on instantiation of the client or explicitly
        when calling methods on the client.

    headers: :obj:`dict`
        A dictionary of headers to include in the request.  These headers will
        be applied on top of the default headers for requests to the AI Services
        API.

        Can be provided either on instantiation of the client or explicitly
        when calling methods on the client.

    proxies: :obj:`dict` (optional)
        Proxies to funnel the requests through.  Note that
        the :obj:`AIServicesClient` uses proxies defined in the Django settings
        module - but those can be overridden by including this parameter.

        Can be provided either on instantiation of the client or explicitly
        when calling methods on the client.

    api_key: :obj:`str` (optional)
        The AI Services API key.  If not provided, the API Key from the Django
        settings module will be used.

        Can be provided either on instantiation of the client or explicitly
        when calling methods on the client.
    """

    def __init__(self, **kwargs):
        self._session = AIServicesSession(**kwargs)

    @controlled_response(lookup="services")
    def get_services(self, **kwargs):
        """
        Gets a list of services in AI Services.

        Parameters:
        ----------
        raw_response: :obj:`boolean` (optional)
            If set to True, the raw response from the request will be returned.
            Otherwise, the services from the parsed JSON will be returned.

            Default: False
        """
        logger.info("Getting Services.")
        return self._session.get("/", **kwargs)

    @controlled_response(lookup="capabilities")
    def get_capabilities(self, service="document-extractor", **kwargs):
        """
        Gets a list of capabilities in AI Services for a particular service.

        GET /<service>/capability

        Parameters:
        ----------
        service: :obj:`str` (optional)
            The service which the capabilities should be fetched for.

            Default: document-extractor

        raw_response: :obj:`boolean` (optional)
            If set to True, the raw response from the request will be returned.
            Otherwise, the capabilities from the parsed JSON will be returned.

            Default: False
        """
        logger.info("Getting Capabilities for %s." % service)
        return self._session.get(
            url="/{service}/capability", url_kwargs={"service": service}, **kwargs
        )

    @controlled_response()
    def get_capability(self, capability, service="document-extractor", **kwargs):
        """
        Gets the result type for a specific capability in AI Services.

        GET /<service>/capability/<capability>

        Parameters:
        ----------
        service: :obj:`str` (optional)
            The service for which the capability belongs.

            Default: document-extractor

        capability: :obj:`str`
            The capability in AI Services which should be fetched.

        raw_response: :obj:`boolean` (optional)
            If set to True, the raw response from the request will be returned.
            Otherwise, the parsed JSON will be returned.

            Default: False
        """
        logger.info("Getting Capability %s." % capability)
        return self._session.get(
            url="/{service}/capability/{capability}",
            url_kwargs={"capability": capability, "service": service},
            **kwargs
        )

    @controlled_response()
    def create_task(self, body, service="document-extractor", **kwargs):
        """
        Submits a task to AI Services.

        POST /<service>/task/

        Parameters:
        ----------
        body: :obj:`dict`
            The JSON body to include on the POST request.

        service: :obj:`str` (optional)
            The service for which the created task belongs.

            Default: document-extractor
        """

        # TODO don't fill the logs with the whole body of the post
        logger.info("Creating Task")

        # headers = kwargs.get('headers')

        # if headers is not None:
        #     del kwargs['headers']
        # else:
        #     headers = {}

        # headers['Content-Encoding'] = 'gzip'
        # headers['Content-Type'] = 'application/json'

        # body = gzip.compress(json.dumps(body).encode('utf-8'))
        # raise AIServicesHttpError('Its missssssing')

        return self._session.post(
            url="/{service}/task",
            body=body,
            url_kwargs={"service": service},
            # headers=headers,
            **kwargs
        )

    def await_task_results(
        self,
        task_id,
        max_attempts=360,
        interval=5,
        timeout_threshold=1800,
        retry_on_http_error=True,
        **kwargs
    ):

        # Refactor this so we don't give the engineer the option of overriding this
        max_attempts = 360

        """
        Repeatedly makes a request to GET /<service>/task/<task_id> to get
        the status of a task published to AI Services until either that task
        has failed, has completed or the attempts/time has exceeded the
        provided thresholds.

        Parameters:
        ----------
        task_id: :obj:`int`
            The identifier of the task that was previously published to AI
            Services.

        max_attempts: :obj:`int` (optional)
            The maximum number of attempts to retrieve the failed or completed
            task status from AI Services, after which the routine will exit
            early.

            Default: None

        interval: :obj:`int` or :obj:`float` (optional)
            The number of seconds to wait in between subsequent requests to
            AI Services.

            Default: 5

        timeout_threshold: :obj:`int` or :obj:`float` (optional)
            The number of seconds to wait until after which the couroutine
            will exit early, if a failed or completed task status has not
            been returned from AI Services.

            Default: 5

        timeout_threshold: :obj:`bool` (optional)
            If True, will exit the routine early on all HTTP request errors
            between the client and the AI Services API.  IF False, these errors
            will trigger subsequent requests until either threshold is reached
            or a successful task state is returned.

            Default: True

        TODO:
        ----
        Consider leveraging asyncio and Python's async functionality here and
        include a callback to be executed in different circumstances.
        """
        start_time = time.time()
        attempts = 0

        def log_attempt(attempt, message, level="info", exc=None, extra=None):
            message = "Awaiting Task Results (Task = %s) - Attempt %s: %s" % (
                task_id,
                attempt,
                message,
            )
            if exc is not None:
                message += "Error: %s" % exc
            getattr(logger, level)(message, extra=extra)

        if max_attempts is not None and attempts >= max_attempts:
            log_attempt(attempts, message="Attempt threshold exceeded - exiting.")
            raise AttemptThresholdExceededError(max_attempts)

        while attempts <= max_attempts:
            attempts += 1
            try:
                response = self.get_task(task_id, **kwargs)
            except AIServicesHttpError as e:
                if not retry_on_http_error:
                    raise e
                try:
                    status_code = e.get_full_details()
                    status_code = status_code["request"]["status_code"]
                except Exception as e:
                    print(traceback.format_exc())
                    print("something else went wrong %s" % e)
                    return False

                if (
                    attempts <= max_attempts
                    and int(status_code) != 500
                    and int(status_code) != 403
                    and int(status_code) != 400
                ):
                    log_attempt(
                        attempt=attempts,
                        message="HTTP error when making request - retrying.",
                        level="error",
                        exc=e,
                    )
                else:
                    raise e
            else:
                if response["status"] == "SUCCESS" or response["status"] == "FAILURE":
                    log_attempt(
                        attempt=attempts,
                        message=(
                            "Received task status %s when making request "
                            "- exiting." % response["status"]
                        ),
                    )
                    return response
                else:
                    log_attempt(
                        attempt=attempts,
                        message=(
                            "Received task status %s when making "
                            "request." % response["status"]
                        ),
                    )
                    if (
                        timeout_threshold is not None
                        and time.time() - start_time >= timeout_threshold
                    ):
                        log_attempt(
                            attempts, message="Time threshold exceeded - exiting."
                        )
                        raise AttemptThresholdExceededError(timeout_threshold)
                    elif max_attempts is not None and attempts >= max_attempts:
                        log_attempt(
                            attempts, message="Attempt threshold exceeded - exiting."
                        )
                        raise AttemptThresholdExceededError(max_attempts)
                    else:
                        time.sleep(interval)
                        continue

    @controlled_response()
    def get_task(self, task, service="document-extractor", **kwargs):
        """
        Gets information about a speciic task from AI Services.

        GET /<service>/task/<task_id>

        Parameters:
        ----------
        task: :obj:`backend.app.task.models.Task` or :obj:`str`
            Either the :obj:`backend.app.task.models.Task` instance or the
            `ai_uuid` associated with the relevant
            :obj:`backend.app.task.models.Task` instance.

        service: :obj:`str` (optional)
            The service for which the task belongs.

            Default: document-extractor

        raw_response: :obj:`boolean` (optional)
            If set to True, the raw response from the request will be returned.
            Otherwise, the parsed JSON will be returned.

            Default: False
        """
        from backend.app.task.models import Task

        logger.info(
            "Getting Task %s" % task.ai_uuid if isinstance(task, Task) else task
        )
        return self._session.get(
            url="/{service}/task/{task_id}",
            url_kwargs={
                "service": service,
                "task_id": task.ai_uuid if isinstance(task, Task) else task,
            },
            **kwargs
        )

    @controlled_response(lookup="status")
    def get_task_status(self, task, service="document-extractor", **kwargs):
        """
        Gets the status of an AI Services task.

        GET /document-extractor/task/<task_id>

        Parameters:
        ----------
        task: :obj:`backend.app.task.models.Task` or :obj:`str`
            Either the :obj:`backend.app.task.models.Task` instance or the
            `ai_uuid` associated with the relevant
            :obj:`backend.app.task.models.Task` instance.

        service: :obj:`str` (optional)
            The service for which the task belongs.

            Default: document-extractor

        raw_response: :obj:`boolean` (optional)
            If set to True, the raw response from the request will be returned.
            Otherwise, the status will be returned from the parsed JSON.

            Default: False
        """
        return self.get_task(service=service, task=task, raw_response=True, **kwargs)

    def make_callback_request(self, callback_request, body=None, **kwargs):
        """
        Performs a callback request from the provided
        :obj:`backend.app.task.models.CallbackRequest` instance and updates
        the instance with the response data from the request.

        Parameters:
        ----------
        callback_request: :obj:`backend.app.task.models.CallbackRequest`
            The :obj:`backend.app.task.models.CallbackRequest` instance which
            has the corresponding URL in it's `callback` property that the
            request is made to.
        """
        callback_request.request_data = body
        callback_request.time_sent = timezone.now()

        # TODO: Do we want to catch errors here and attribute the errors on
        # the CallbackRequest instance?
        response = self._session.post(
            url=callback_request.callback, body=body, **kwargs
        )
        callback_request.status_code = response.status_code
        try:
            callback_request.response_data = response.json()
        except ValueError:
            callback_request.response_data = response.text
        callback_request.save()

    def revoke_api_key(self, organization, **kwargs):
        """
        Revokes an API Key for the provided organization.

        DELETE /admin/api-key/<key>

        Parameters:
        ----------
        organization: :obj:`backend.app.organization.models.Organization`
            The organization associated with the API Key that is being
            revoked.
        """
        if organization.api_key is None:
            raise OrganizationApiKeyError(organization=organization)

        logger.info(
            "Revoking API Key for %s." % organization.name,
            extra={organization: organization.pk},
        )
        self._session.delete(
            url="/admin/api-key/{key}",
            url_kwargs={"key": organization.api_key},
        )
        organization.api_key = None
        organization.save()

    def generate_api_key(self, organization, service="document-extractor", **kwargs):
        """
        Generates an API Key for the provided organization to authenticate
        requests to the AI Services API for the provided service.

        POST /admin/api-key

        Parameters:
        ----------
        organization: :obj:`backend.app.organization.models.Organization`
            The organization for which the API Key should be generated.

        capabilities: :obj:`list` or :obj:`tuple`
            A list of capabilities, each of which is a :obj:`str`, for which
            an AI Services API Key should be generated.

        service: :obj:`str` (optional)
            The service for which the API Key pertains to.

            Default: document-extractor

        raw_response: :obj:`boolean` (optional)
            If set to True, the raw response from the request will be returned.
            Otherwise, the API Key will be returned from the parsed JSON.

            Default: False
        """
        logger.info(
            "Generating API Key for %s." % organization.name,
            extra={organization: organization.pk},
        )
        # TODO: If the organization does not have any capabilities, should we
        # still allow it to generate an API Key?
        capabilities = organization.get_capabilities(service)
        if len(capabilities) == 0:
            logger.info(
                "Organization %s does not have any capabilities for service %s."
                % (organization.pk, service),
                extra={"json_info": organization.json_info},
            )
        response = self._session.post(
            url="/admin/api-key",
            body={
                "tenant_id": organization.tenant_id,
                "service": service,
                "capabilities": [cap.lower().replace(" ", "_") for cap in capabilities],
            },
        )
        # Only revoke the existing API Key if the new one is successfully
        # generated.
        if organization.api_key is not None:
            self.revoke_api_key(organization, **kwargs)

        organization.api_key = response.json()["api_key"]
        organization.save()


client = AIServicesClient()

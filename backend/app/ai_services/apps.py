from django.apps import AppConfig


class AIServicesConfig(AppConfig):
    name = "backend.app.ai_services"
    verbose_name = "AI Services"

import logging

from rest_framework import exceptions, generics, response, status, views

from backend.app.task.models import TaskResult

from .api import client
from .serializers import NormalizeDocumentDataSerializer
from .utils import normalize_document_results_into_table

logger = logging.getLogger("backend")


class ServicesView(views.APIView):
    def get(self, request):
        data = client.get_services()
        return response.Response(data)


class CapabilitiesView(views.APIView):
    def get(self, request, service=None):
        services = client.get_services()
        if service not in [srv["service"] for srv in services]:
            raise exceptions.NotFound("The service does not exist in AI Services.")
        data = client.get_capabilities(service=service)
        return response.Response(data)


class CapabilityView(views.APIView):
    def get(self, request, service=None, capability=None):
        services = client.get_services()
        if service not in [srv["service"] for srv in services]:
            raise exceptions.NotFound("The service does not exist in AI Services.")
        capabilities = client.get_capabilities(service=service)
        if capability not in [cap["capability"] for cap in capabilities]:
            raise exceptions.NotFound(
                "The capability does not exist for service %s." % service
            )
        data = client.get_capability(service=service, capability=capability)
        return response.Response(data)


class NormalizeDocumentDataView(generics.GenericAPIView):
    serializer_class = NormalizeDocumentDataSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        documents = serializer.validated_data

        tables_array = []
        for document in documents:
            if document.task is not None:
                try:
                    document.task.results.latest()
                except TaskResult.DoesNotExist:
                    logger.warning(
                        f"TaskResult does not " f"exist for document: {document.name}"
                    )
                    continue
                    # return response.Response(
                    # {"data": "TaskResult does not exist"},
                    # status=status.HTTP_400_BAD_REQUEST)
                else:
                    tables_array.append(
                        # [document.name, result.new_data['data']]
                        document
                    )

        output_df = normalize_document_results_into_table(tables_array)
        data = output_df.to_json(orient="records")
        return response.Response({"data": data}, status=status.HTTP_201_CREATED)

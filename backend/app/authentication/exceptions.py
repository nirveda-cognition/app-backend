from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied, Throttled


class AuthErrorCodes(object):
    ACCOUNT_DISABLED = "account_disabled"
    INVALID_CREDENTIALS = "invalid_credentials"
    NO_ORGANIZATION_PERMISSION = "no_organization_permission"
    NO_GROUP_PERMISSION = "no_group_permission"
    RATE_LIMITED = "rate_limited"
    EMAIL_NOT_CONFIRMED = "email_not_confirmed"
    PASSWORD_RESET_LINK_EXPIRED = "password_reset_link_expired"
    PASSWORD_RESET_LINK_USED = "password_reset_link_used"
    PASSWORD_FORGOT_LINK_USED = "password_forgot_link_used"
    INVALID_SOURCE_HEADER = "invalid_http_source_header"
    INVALID_TOKEN = "invalid_token"
    EMAIL_DOES_NOT_EXIST = "email_does_not_exist"


class AccountDisabledError(PermissionDenied):
    default_detail = _("Your account is not active, please contact customer care.")
    default_code = AuthErrorCodes.ACCOUNT_DISABLED


class EmailNotConfirmedError(PermissionDenied):
    default_code = AuthErrorCodes.EMAIL_NOT_CONFIRMED
    default_detail = _("The email address is not confirmed.")


class PasswordResetLinkExpiredError(PermissionDenied):
    default_code = AuthErrorCodes.PASSWORD_RESET_LINK_EXPIRED
    default_detail = _("The password reset link has expired.")


class PasswordResetLinkUsedError(PermissionDenied):
    default_code = AuthErrorCodes.PASSWORD_RESET_LINK_USED
    default_detail = _("The password reset link has already been used.")


class OrganizationPermissionError(PermissionDenied):
    default_detail = _(
        "You do not have permission to access the provided organization."
    )
    default_code = AuthErrorCodes.NO_ORGANIZATION_PERMISSION


class GroupPermissionError(PermissionDenied):
    default_detail = _("You do not have permission to access the provided group.")
    default_code = AuthErrorCodes.NO_GROUP_PERMISSION


class EmailDoesNotExist(AuthenticationFailed):
    default_detail = _("The provided username does not exist in our system.")
    default_code = AuthErrorCodes.EMAIL_DOES_NOT_EXIST


class InvalidCredentialsError(AuthenticationFailed):
    default_detail = _("The provided password is invalid.")
    default_code = AuthErrorCodes.INVALID_CREDENTIALS


class RateLimitedError(Throttled):
    default_detail = _("Request limit exceeded.")
    default_code = AuthErrorCodes.RATE_LIMITED


class InvalidToken(PermissionDenied):
    """
    General exception raised when a token is invalid.

    Note:
        This is not to be raised when the token is not provided.  That should
        be a 400 response, this is a 403.
    """

    default_detail = _("The provided token is invalid.")
    default_code = AuthErrorCodes.INVALID_TOKEN

import time
import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _

from backend.app.common.models import TimestampedModel
from backend.app.custom_auth.models import NCGroup, Role, SSOAccount  # noqa
from backend.app.user.models import CustomUser


def time_random_uuid():
    return str(uuid.uuid1()) + str(int(time.time()))


class ResetUID(TimestampedModel):
    token = models.CharField(max_length=1024, unique=True, default=time_random_uuid)
    used = models.BooleanField(default=False)
    user = models.ForeignKey(
        to=CustomUser, on_delete=models.CASCADE, blank=True, null=True
    )

    class Meta:
        verbose_name = "ResetUID"
        verbose_name_plural = "ResetUIDs"


class Token(TimestampedModel):
    sso_account = models.ForeignKey(
        SSOAccount,
        verbose_name=_("SSO Account"),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    token_type = models.CharField(_("Token Type"), max_length=255, default="Bearer")
    scope = models.CharField(_("Scope"), max_length=255, blank=True, null=True)
    access_token = models.TextField(_("Access Token"))
    refresh_token = models.TextField(_("Refresh Token"))
    id_token = models.TextField(_("ID Token"))
    expires_in = models.CharField(
        _("Expires In"), max_length=255, blank=True, null=True
    )
    ext_expires_in = models.CharField(
        _("Ext Expires In"), max_length=255, blank=True, null=True
    )
    client_info = models.CharField(
        _("Client Info"), max_length=255, blank=True, null=True
    )
    azure_json = models.JSONField(
        _("Azure Raw JSON"), default=dict, blank=True, null=True
    )
    id_token_claims = models.JSONField(_("ID Token claims"), default=dict)
    is_active = models.BooleanField(_("Is Token Active?"), default=True)

    class Meta:
        verbose_name = _("Token")
        verbose_name_plural = _("Tokens")

    @property
    def user(self):
        return self.sso_account.user

    def deactivate_token(self):
        self.is_active = False
        self.access_token = ""
        self.refresh_token = ""
        self.save()

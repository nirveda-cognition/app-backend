from rest_framework import serializers

from .models import Token

from datetime import datetime

class AzureSSOAuthorizeSerializer(serializers.Serializer):
    auth_url = serializers.URLField()


class AzureSSOTokenSerializer(serializers.ModelSerializer):
    azure_json = serializers.JSONField(required=False)

    class Meta:
        model = Token
        fields = "__all__"

    def create(self, validated_data):
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("DEBUG : at create function in AzureSSOTokenSerializer class", current_time)
        # sso_account_data = validated_data.pop("sso_account")
        # sso_account_serializer = SSOAccountSerializer(data=sso_account_data)
        # sso_account_serializer.is_valid(raise_exceptions=True)
        # sso_account_serializer.save()
        return super().create(validated_data)

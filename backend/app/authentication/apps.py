from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = "backend.app.authentication"
    verbose_name = "Authentications"

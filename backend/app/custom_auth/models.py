from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from backend.app.common.models import TimestampedModel
from backend.app.organization.models import Organization
from backend.app.user.managers import CustomUserManager


class UserCategory(object):
    SUPERUSER = "NirvedaSuperAdmin"
    ADMIN = "ClientAdmin"
    USER = "ClientUser"


class Role(TimestampedModel):
    name = models.CharField(unique=True, max_length=256)
    description = models.CharField(max_length=256, blank=True, null=True)
    code = models.IntegerField(unique=True)

    NAME_USER = "User"
    NAME_ADMIN = "Admin"
    NAME_ACCOUNT_USER = "account_user"
    NAME_ACCOUNT_MANAGER = "account_manager"
    NAMES = (NAME_USER, NAME_ADMIN, NAME_ACCOUNT_USER, NAME_ACCOUNT_MANAGER)

    class Meta:
        verbose_name = "Role"
        verbose_name_plural = "Roles"
        db_table = "auth_role"

    def __str__(self):
        return self.name


class NCGroup(TimestampedModel):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=254, blank=True, null=True)
    # TODO: Make organization a required field!!!!
    organization = models.ForeignKey(
        to=Organization,
        on_delete=models.DO_NOTHING,
        related_name="groups",
    )
    roles = models.ManyToManyField(to="custom_auth.Role", blank=True)

    class Meta:
        unique_together = (("name", "organization"),)
        verbose_name = "NCGroup"
        verbose_name_plural = "NCGroups"
        db_table = "auth_ncgroup"

    def __str__(self):
        return self.name


class CustomUser(AbstractUser):
    email = models.EmailField(_("email address"), unique=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    # TODO: Make the Role a required field.
    role = models.ForeignKey(
        to="custom_auth.Role", on_delete=models.DO_NOTHING, blank=True, null=True
    )
    groups = models.ManyToManyField(
        to="custom_auth.NCGroup", blank=True, related_name="users"
    )
    created_by = models.ForeignKey(
        to="self",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="user_created_by",
    )
    updated_by = models.ForeignKey(
        to="self",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="user_updated_by",
    )
    organization = models.ForeignKey(
        to="organization.Organization",
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
        related_name="users",
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    timezone = models.CharField(max_length=255, null=True, blank=True)
    language = models.CharField(max_length=2, null=True, blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username", "organization"]

    objects = CustomUserManager()

    class Meta:
        db_table = "auth_user"
        verbose_name = "User"
        verbose_name_plural = "Users"
        ordering = ("-created_at",)

    def __str__(self):
        return str(self.get_username())

    @property
    def salutation(self):
        return self.get_short_name().capitalize() if self.get_short_name() else ""

    @property
    def full_name(self):
        if self.first_name is not None and self.first_name != "":
            if self.last_name is not None and self.last_name != "":
                return "%s %s" % (self.first_name, self.last_name)
            return self.first_name
        if self.last_name is not None and self.last_name != "":
            return self.last_name
        return ""

    @property
    def category(self):
        # TODO: This is probably an unnecessary abstraction a level removed from
        # the core Django permissions system.  Determining whether the user is
        # a superuser, admin or neither should be sufficient for all other
        # logic.
        if self.is_superuser:
            return UserCategory.SUPERUSER
        elif self.is_admin:
            return UserCategory.ADMIN
        return UserCategory.USER

    def has_group_permission(self, group):
        """
        Returns whether or not the `obj:CustomUser` has permission to view
        the provided group.
        """
        # TODO: What about the case where the user category is just USER?
        # ClientAdmin can only fetch groups of its organization only.
        if (
            self.category == UserCategory.ADMIN
            and group.organization != self.organization
        ):
            return False
        return True

    def has_organization_permission(self, organization):
        """
        Returns whether or not the :obj:`CustomUser` has permission to view
        the provided organization.
        """
        if (
            self.category in (UserCategory.ADMIN, UserCategory.USER)
            and organization != self.organization
        ):
            return False
        return True


class SSOAccount(TimestampedModel):
    sso_id = models.CharField(_("SSO ID"), max_length=1024)
    user = models.OneToOneField(
        CustomUser,
        verbose_name=_("User"),
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = _("SSO Account")
        verbose_name_plural = _("SSO Accounts")

    def __str__(self):
        return self.sso_id

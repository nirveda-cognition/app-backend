from django_filters import rest_framework as filters
from rest_framework import decorators, mixins, response, viewsets

from backend.app.authentication.mixins import GroupNestedMixin
from backend.app.authentication.models import NCGroup, Role
from backend.app.organization.mixins import OrganizationNestedMixin
from backend.lib.rest_framework_utils.permissions import IsSuperuser

from .models import CustomUser
from .serializers import ChangePasswordSerializer, UserSerializer
from .signals import user_signed_up


class UserFilter(filters.FilterSet):
    role = filters.ModelChoiceFilter(
        field_name="role",
        to_field_name="pk",
        queryset=Role.objects.all(),
    )
    group = filters.ModelChoiceFilter(
        field_name="groups",
        to_field_name="pk",
        queryset=NCGroup.objects.all(),
    )
    role__name = filters.ModelChoiceFilter(
        field_name="role__name", to_field_name="name", queryset=Role.objects.all()
    )
    group__name = filters.ModelChoiceFilter(
        field_name="groups__name",
        to_field_name="name",
        queryset=NCGroup.objects.all(),
    )

    class Meta:
        model = CustomUser
        fields = ("role", "role__name", "group", "groups__name")


class ActiveUserViewSet(
    mixins.UpdateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /users/user/
    (2) PATCH /users/user/
    """

    serializer_class = UserSerializer
    lookup_field = "pk"

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update(organization=self.request.user.organization)
        return context

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        return serializer.save(updated_by=self.request.user)

    @decorators.action(methods=["PATCH"], detail=False)
    def change_password(self, request, *args, **kwargs):
        user = self.get_object()
        serializer = ChangePasswordSerializer(instance=user, data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return response.Response(self.serializer_class(instance).data, status=201)


class UserViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /users/
    """

    lookup_field = "pk"
    search_fields = ("first_name", "last_name", "role__name", "groups__name", "email")
    ordering_fields = ["created_at", "updated_at", "email", "name"]
    serializer_class = UserSerializer
    filterset_class = UserFilter
    permission_classes = [IsSuperuser]

    def get_queryset(self):
        return CustomUser.objects.all()


class GroupUserViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    GroupNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/groups/<pk>/users/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    group_lookup_field = ("pk", "group_pk")
    search_fields = ("first_name", "last_name", "role__name", "groups__name", "email")
    ordering_fields = ["created_at", "updated_at", "email", "name"]
    serializer_class = UserSerializer
    filterset_class = UserFilter

    def get_queryset(self):
        return self.group.users.all()


class OrganizationUserViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    OrganizationNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/users/
    (2) POST /organizations/<pk>/users/
    (3) GET /organizations/<pk>/users/<pk>/
    (4) PATCH /organizations/<pk>/users/<pk>/
    (5) DELETE /organizations/<pk>/users/<pk>/

    TODO:
    ----
    Assign appropriate permissions for deleting a user.
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    search_fields = ("first_name", "last_name", "role__name", "groups__name", "email")
    ordering_fields = ["created_at", "updated_at", "email", "name"]
    serializer_class = UserSerializer
    filterset_class = UserFilter

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update(organization=self.organization)
        return context

    def get_queryset(self):
        return self.organization.users.all()

    def get_object(self):
        user = super().get_object()
        self.organization.raise_no_access(user)
        return user

    def perform_create(self, serializer):
        instance = serializer.save(
            created_by=self.request.user,
            updated_by=self.request.user,
        )
        user_signed_up.send(sender=type(instance), instance=instance)
        return instance

    def perform_update(self, serializer):
        return serializer.save(updated_by=self.request.user)

from django.urls import path
from rest_framework import routers

from .views import (
    ActiveUserViewSet,
    GroupUserViewSet,
    OrganizationUserViewSet,
    UserViewSet,
)

app_name = "user"

user_router = routers.SimpleRouter()
user_router.register(r"", OrganizationUserViewSet, basename="user")
organization_urlpatterns = user_router.urls

group_router = routers.SimpleRouter()
group_router.register(r"", GroupUserViewSet, basename="user")
group_urlpatterns = user_router.urls

root_users_router = routers.SimpleRouter()
root_users_router.register("", UserViewSet, basename="user")

urlpatterns = root_users_router.urls + [
    path(
        "user/",
        ActiveUserViewSet.as_view(
            {
                "get": "retrieve",
                "patch": "partial_update",
            }
        ),
    ),
    path(
        "user/change-password/", ActiveUserViewSet.as_view({"patch": "change_password"})
    ),
]

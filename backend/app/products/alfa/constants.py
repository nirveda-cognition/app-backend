from django.conf import settings

FIXTURES_DIR = settings.APPS_DIR / "products" / "alfa" / "fixtures"
PPP_FORM_PATH = FIXTURES_DIR / "PPP.pdf"
PPP_DESTINATION_PATH = FIXTURES_DIR / "PPPOut.pdf"
TABLE_DESTINATION_PATH = FIXTURES_DIR / "PPPTable.pdf"

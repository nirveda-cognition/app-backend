import base64

from rest_framework import response, views

from backend.app.collection.models import Collection
from backend.lib.utils.formatters import (
    format_date_string,
    format_phone_no,
    present_dollar_field,
)

from .constants import PPP_DESTINATION_PATH, PPP_FORM_PATH
from .utils import add_schedule_a_data, add_schedule_a_more_than_100k_data
from .utils.pdf import PdfFileFiller, PdfFileReader


class FillPdfView(views.APIView):
    def get(self, request, *args, **kwargs):
        collection = Collection.objects.get(pk=self.request.GET["pk"])
        collection_result = collection.results.latest()
        cd = collection_result.get("collection_data", {})

        newvals = {
            "Business Legal Name BorrowerRow1": cd.get("borrower_name", ""),
            "DBA or Tradename if applicableRow1": cd.get("dba", ""),
            "Business TIN EIN SSNRow1": cd.get("tin", ""),
            "Business AddressRow1": cd.get("address", ""),
            "Business AddressRow2": cd.get("___", ""),
            "fill_29": format_phone_no(cd.get("phone", "")),  # business phone
            "Primary ContactRow1": cd.get("borrower_name", ""),
            "Email AddressRow1": cd.get("email", ""),
            "PPP Loan Amount": cd.get("total_loan_amount", ""),
            "SBA PPP Loan Number": cd.get("ppp_loan_number", ""),
            "Lender PPP Loan Number": cd.get("lender_loan_number", ""),
            "EIDL Advance Amount": cd.get("eidl_advance_amount", ""),
            "EIDL Application Number": cd.get("eidl_app_number", ""),
            "Employees at Time of Loan Application": cd.get("no_employees", ""),
            # TODO: Okay, what is going on with this triple casting?
            "Employees at Time of Forgiveness Application": (
                str(int(float(cd["schedule_a"].get("line_12", 0))))
            ),
            "PPP Loan Disbursement Date": format_date_string(
                cd.get("disbursement_date", "")
            ),
            "Covered Period": format_date_string(cd.get("covered_period_start", "")),
            "to": format_date_string(cd.get("covered_period_end", "")),
            "Alternative Payroll Covered Period if applicable": "",
            "If Borrower together with affiliates if applicable received PPP loans in excess of 2 million check here": "",  # noqa
            "1": present_dollar_field(cd["ppp_application"].get("line_1", "")),
            "2": present_dollar_field(cd["ppp_application"].get("line_2", "")),
            "3": present_dollar_field(cd["ppp_application"].get("line_3", "")),
            "4": present_dollar_field(cd["ppp_application"].get("line_4", "")),
            "1_2": present_dollar_field(cd["ppp_application"].get("line_5", "")),
            "2_2": present_dollar_field(cd["ppp_application"].get("line_6", "")),
            "3_2": cd["ppp_application"].get("line_7", ""),
            "1_3": present_dollar_field(cd["ppp_application"].get("line_8", "")),
            "2_3": present_dollar_field(cd["ppp_application"].get("line_9", "")),
            "3_3": present_dollar_field(cd["ppp_application"].get("line_10", "")),
            # Line 11. Forgiveness Amount (enter the smallest of lines 8, 9,
            # and 10):
            "undefined_2": present_dollar_field(
                cd["ppp_application"].get("line_11", "")
            ),
            "1_4": present_dollar_field(cd["schedule_a"].get("line_1", "")),
            "2_4": cd["schedule_a"].get("line_2", ""),
            "undefined_8": present_dollar_field(cd["schedule_a"].get("line_3", "")),
            "undefined_9": present_dollar_field(cd["schedule_a"].get("line_4", "")),
            "undefined_10": cd["schedule_a"].get("line_5", ""),
            "Total amount paid or incurred by Borrower for employer contributions for employee healthinsurance": present_dollar_field(  # noqa
                cd["schedule_a"].get("line_6", "")
            ),  # noqa
            "Total amount paid or incurred by Borrower for employer contributions to employee retirementplans": present_dollar_field(  # noqa
                cd["schedule_a"].get("line_7", "")
            ),  # noqa
            "undefined_11": present_dollar_field(cd["schedule_a"].get("line_8", "")),
            "undefined_12": present_dollar_field(cd["schedule_a"].get("line_9", "")),
            "undefined_13": present_dollar_field(cd["schedule_a"].get("line_10", "")),
            "undefined_17": cd["schedule_a"].get("line_12", ""),
            "lien13111": cd["schedule_a"].get("line_13", ""),
            "FTE Reduction Safe Harbor 2 If you satisfy FTE Reduction Safe Harbor 2 see PPP Schedule A Worksheet check here": cd[  # noqa
                "schedule_a"
            ].get(
                "line_11", ""
            ),  # noqa
        }

        worksheet_extra_data = []
        worksheet_more_than_100k_extra_data = []

        add_schedule_a_data(cd, newvals, worksheet_extra_data)
        add_schedule_a_more_than_100k_data(cd, newvals, worksheet_extra_data)

        for k, v in newvals.items():
            newvals[k] = str(v)

        newchecks = {
            "undefined": "/1",  # or /On or /Yes
        }

        # Referenced here just to explore the fields present in the pdf.
        # The keys in form_fields dictionary is to be set as keys in the
        # "newvals" dictionary above and values from the CollectionResult object
        # (or any other source).
        f = PdfFileReader(open(PPP_FORM_PATH, "rb"))

        extra_data = None
        if worksheet_extra_data or worksheet_more_than_100k_extra_data:
            extra_data = {
                "worksheet_extra_data": worksheet_extra_data,
                "worksheet_more_than_100k_extra_data": (
                    worksheet_more_than_100k_extra_data
                ),
            }

        c = PdfFileFiller(PPP_FORM_PATH)
        c.update_form_values(
            outfile=PPP_DESTINATION_PATH,
            newvals=newvals,
            newchecks=newchecks,
            extra_data=extra_data,
        )

        with open(PPP_DESTINATION_PATH, "rb") as f:
            bytes_read = f.read()

        file_data = base64.b64encode(bytes_read).decode()
        return response.Response({"file_data": file_data})

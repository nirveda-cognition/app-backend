from PyPDF2 import PdfFileReader, PdfFileWriter
from PyPDF2.generic import BooleanObject, IndirectObject, NameObject
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_LEFT
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import Paragraph, SimpleDocTemplate, Table, TableStyle

from backend.app.products.alfa.constants import TABLE_DESTINATION_PATH


def set_need_appearances_writer(writer):
    catalog = writer._root_object
    # get the AcroForm tree and add "/NeedAppearances attribute
    if "/AcroForm" not in catalog:
        writer._root_object.update(
            {
                NameObject("/AcroForm"): IndirectObject(
                    len(writer._objects), 0, writer
                )  # noqa
            }
        )

    need_appearances = NameObject("/NeedAppearances")
    writer._root_object["/AcroForm"][need_appearances] = BooleanObject(True)
    return writer


class MyPdfFileWriter(PdfFileWriter):
    def __init__(self):
        super().__init__()

    def updatePageFormCheckboxValues(self, page, fields):
        for j in range(0, len(page["/Annots"])):
            writer_annot = page["/Annots"][j].getObject()
            for field in fields:
                if writer_annot.get("/T") == field:
                    writer_annot.update(
                        {
                            NameObject("/V"): NameObject(fields[field]),
                            NameObject("/AS"): NameObject(fields[field]),
                        }
                    )


class PdfFileFiller(object):
    def __init__(self, infile):
        self.pdf = PdfFileReader(open(infile, "rb"), strict=False)
        self.fields = self.pdf.getFields()
        if "/AcroForm" in self.pdf.trailer["/Root"]:
            self.pdf.trailer["/Root"]["/AcroForm"].update(
                {NameObject("/NeedAppearances"): BooleanObject(True)}
            )

        # Table pdf styles
        self.styles = getSampleStyleSheet()
        self.styleN = self.styles["Normal"]
        self.styleH = self.styles["Heading1"]

    def header_footer(self, canvas, doc):
        # Save the state of our canvas so we can draw on it
        canvas.saveState()

        # Header
        header_line1_part1 = Paragraph(
            "<b>Paycheck Protection Program</b>",
            style=ParagraphStyle(
                name="center", alignment=TA_CENTER, fontSize=11, fontName="Times-Roman"
            ),
        )
        w, h = header_line1_part1.wrap(doc.width, doc.topMargin)
        header_line1_part1.drawOn(
            canvas, doc.leftMargin, doc.height + doc.topMargin - h
        )

        header_line1_part2 = Paragraph(
            "<b>OMB Control Number 3245-0407</b>",
            style=ParagraphStyle(
                name="center", alignment=TA_CENTER, fontSize=9, fontName="Times-Roman"
            ),
        )
        w, h = header_line1_part2.wrap(doc.width, doc.topMargin)
        header_line1_part2.drawOn(
            canvas, doc.leftMargin + (3 * inch), doc.height + doc.topMargin - h
        )

        header_line2_part1 = Paragraph(
            "<b>Loan Forgiveness Application Revised June 16, 2020</b>",
            style=ParagraphStyle(
                name="center", alignment=TA_CENTER, fontSize=10, fontName="Times-Roman"
            ),
        )
        w, h = header_line2_part1.wrap(doc.width, doc.topMargin)
        header_line2_part1.drawOn(
            canvas, doc.leftMargin, doc.height + doc.topMargin - 25
        )

        header_line2_part2 = Paragraph(
            "<b>Expiration Date: 10/31/2020</b>",
            style=ParagraphStyle(
                name="center", alignment=TA_CENTER, fontSize=9, fontName="Times-Roman"
            ),
        )
        w, h = header_line2_part2.wrap(doc.width, doc.topMargin)
        header_line2_part2.drawOn(
            canvas, doc.leftMargin + (3.1 * inch), doc.height + doc.topMargin - 25
        )

        header_line3 = Paragraph(
            "<b><u>PPP Schedule A Worksheet (additional information)</u></b>",
            style=ParagraphStyle(
                name="center", alignment=TA_CENTER, fontSize=11, fontName="Times-Roman"
            ),
        )
        w, h = header_line3.wrap(doc.width, doc.topMargin)
        header_line3.drawOn(canvas, doc.leftMargin, doc.height + doc.topMargin - 50)

        # Footer
        footer_line1 = Paragraph(
            "SBA Form 3508 (6/20)",
            style=ParagraphStyle(
                name="center", alignment=TA_LEFT, fontSize=9, fontName="Times-Roman"
            ),
        )
        w, h = footer_line1.wrap(doc.width, doc.bottomMargin)
        footer_line1.drawOn(canvas, doc.leftMargin, h + 5)

        footer_line2 = Paragraph(
            "Page {0}".format(doc.page + 5),
            style=ParagraphStyle(
                name="center", alignment=TA_LEFT, fontSize=9, fontName="Times-Roman"
            ),
        )
        w, h = footer_line2.wrap(doc.width, doc.bottomMargin)
        footer_line2.drawOn(canvas, doc.leftMargin, h - 5)

        # Release the canvas
        canvas.restoreState()

    def create_table(self, data, no_of_cols=4):
        if no_of_cols not in (4, 5):
            raise ValueError(
                "Unsupported number of columns %s, must be 4 or 5." % no_of_cols
            )

        if no_of_cols == 4:
            table = Table(data, colWidths=130)
            style = TableStyle(
                [
                    ("BACKGROUND", (0, 0), (3, 0), colors.lightgrey),
                    ("TEXTCOLOR", (0, 0), (-1, 0), colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    ("FONTNAME", (0, 0), (-1, 0), "Times-Roman"),
                    ("FONTSIZE", (0, 0), (-1, 0), 10),
                    ("BOTTOMPADDING", (0, 0), (-1, 0), 10),
                    ("BACKGROUND", (0, 1), (-1, -1), colors.whitesmoke),
                ]
            )

        if no_of_cols == 5:
            table = Table(data, colWidths=104)
            style = TableStyle(
                [
                    ("BACKGROUND", (0, 0), (4, 0), colors.lightgrey),
                    ("TEXTCOLOR", (0, 0), (-1, 0), colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    ("FONTNAME", (0, 0), (-1, 0), "Times-Roman"),
                    ("FONTSIZE", (0, 0), (-1, 0), 10),
                    ("BOTTOMPADDING", (0, 0), (-1, 0), 10),
                    ("BACKGROUND", (0, 1), (-1, -1), colors.whitesmoke),
                ]
            )

        table.setStyle(style)

        # Add borders
        ts = TableStyle(
            [
                ("BOX", (0, 0), (-1, -1), 1, colors.black),
                # Vertical
                ("LINEBEFORE", (2, 1), (2, -1), 1, colors.black),
                # Horizontal
                ("LINEABOVE", (0, 2), (-1, 2), 1, colors.black),
                ("GRID", (0, 1), (-1, -1), 2, colors.black),
            ]
        )
        table.setStyle(ts)
        return table

    def create_table_pdf(self, extra_data):
        worksheet_data = [
            [
                "Employee's Name",
                "Employee's Identifier",
                "Cash Compensation",
                "Average FTE",
                "Salary / Hourly Wage\nReduction",
            ]
        ]
        # TODO: Make worksheet_extra_data a **kwarg.
        worksheet_data.extend(extra_data.get("worksheet_extra_data", []))

        worksheet_more_than_100k_data = [
            [
                "Employee's Name",
                "Employee's Identifier",
                "Cash Compensation",
                "Average FTE",
            ]
        ]
        # TODO: Make worksheet_more_than_100k_extra_data a **kwarg.
        worksheet_more_than_100k_data.extend(
            extra_data.get("worksheet_more_than_100k_extra_data", [])
        )

        worksheet_table = self.create_table(worksheet_data, 5)
        worksheet_more_than_100k_table = self.create_table(
            worksheet_more_than_100k_data
        )

        center_style = ParagraphStyle(
            name="center",
            alignment=TA_CENTER,
            fontSize=13,
            fontName="Times-Roman",
            leading=24,
        )
        table1_title = Paragraph("Table 1 (continued)", style=center_style)
        table2_space = Paragraph(" ", style=center_style)
        table2_title = Paragraph("Table 2 (continued)", style=center_style)

        pdf = SimpleDocTemplate(
            TABLE_DESTINATION_PATH,
            pagesize=letter,
            leftMargin=25,
            topMargin=100,
            rightMargin=25,
            bottomMargin=35,
        )
        pdf.build(
            [
                table1_title,
                worksheet_table,
                table2_space,
                table2_space,
                table2_space,
                table2_title,
                worksheet_more_than_100k_table,
            ],
            onFirstPage=self.header_footer,
            onLaterPages=self.header_footer,
        )

    def update_form_values(
        self, outfile, newvals=None, newchecks=None, extra_data=None
    ):
        self.pdf2 = MyPdfFileWriter()

        trailer = self.pdf.trailer["/Root"]["/AcroForm"]
        self.pdf2._root_object.update({NameObject("/AcroForm"): trailer})

        set_need_appearances_writer(self.pdf2)
        if "/AcroForm" in self.pdf2._root_object:
            self.pdf2._root_object["/AcroForm"].update(
                {NameObject("/NeedAppearances"): BooleanObject(True)}
            )

        for i in range(self.pdf.getNumPages()):
            self.pdf2.addPage(self.pdf.getPage(i))
            self.pdf2.updatePageFormFieldValues(self.pdf2.getPage(i), newvals)
            self.pdf2.updatePageFormCheckboxValues(self.pdf2.getPage(i), newchecks)

        if extra_data:
            self.create_table_pdf(extra_data)
            table_pdf = PdfFileReader(open(TABLE_DESTINATION_PATH, "rb"), strict=False)
            for i in range(table_pdf.getNumPages()):
                self.pdf2.addPage(table_pdf.getPage(i))

        with open(outfile, "wb") as out:
            self.pdf2.write(out)

import logging

from django import dispatch
from django.db.models.signals import post_save

from backend.app.nc_core.business_logic.business_logic_functions import all_logic
from backend.app.nc_core.business_logic.common import direct_update

from .models import TaskResult

logger = logging.getLogger("backend")


def find_document_type_for_page(page):
    try:
        document_type_item = [
            item for item in page["items"] if item["property_id"] == "document_type"
        ][0]
    except IndexError:
        return None
    else:
        return document_type_item["value"]


def find_pages_of_document_type(pages, document_type):
    document_type_pages = []
    for page in pages:
        page_document_type = find_document_type_for_page(page)
        if page_document_type is None:
            # This really shouldn't happen.
            logger.warn(
                "Could not find a document type corresponding to the page.",
                extra={"page": page},
            )
            continue
        if page_document_type != document_type:
            continue
        document_type_pages.append(page)
    return document_type_pages


def get_field_pairs_from_page(page):
    updates = {}
    for page_item in page["items"]:
        if page_item["property_id"] == "document_type":
            continue
        updates[page_item["property_id"]] = page_item["value"]
    return updates


def update_collection_result_from_task_result(
    collection_result, document, task_result=None
):

    # Do we want to update all the task results or just the latest one?
    task_result = task_result or document.results.latest()
    extracted_data = task_result.new_data["data"]["extracted_data"]

    collection_result.result.setdefault("child_data", {})

    # TODO: Find a way of doing this that doesn't involve hardcoding in the
    # document types.
    document_types = [document.schema]
    if document.schema == "apex":
        document_types = [
            "purchase_order",
            "invoice",
            "receiver",
            "change_order",
            "packing_slip",
        ]

    for document_type in document_types:
        collection_result.result["child_data"].setdefault(document_type, {})

        document_type_pages = find_pages_of_document_type(extracted_data, document_type)

        # TODO: Figure out a better way to do this that doesn't involve hard
        # coding it in.
        if len(document_type_pages) == 0:
            if document_type == "packing_slip":
                document_type_pages = find_pages_of_document_type(
                    extracted_data, "ship_ticket"
                )

        if len(document_type_pages) == 0:
            logger.warning(
                "Could not find a page in the extracted data with document "
                "type {document_type}.".format(document_type=document_type)
            )
            continue

        # Since there may be multiple pages associated with the same document
        # type, we need to look over all of the pages and consolidate the
        # key-value pairs from each page.  The values should not differ for
        # a given key across pages.
        changes_across_pages = {}
        for page in document_type_pages:
            # Consolidate the key-value pairs from the page with the previous
            # pages.
            updates = get_field_pairs_from_page(page)
            for k, v in updates.items():
                # If a field is on multiple pages - it should have the
                # same value.  However, there might be cases in which this
                # happens - so we should just log a warning and pick one of the
                # values.  Eventually, we want to build in a system to identify
                # these types of situations as warnings to the user.
                if k in changes_across_pages and changes_across_pages[k] != v:  # noqa
                    logger.warning(
                        "Corrupted data! Found differing values %s and %s for "
                        "field %s across multiple pages of the extracted data."
                        % (changes_across_pages[k], v, k)
                    )
                changes_across_pages[k] = v

        # Apply the updates for the given document type based on the field-value
        # pairs across the pages in extracted data.
        updates = []
        entries = []
        for k, v in changes_across_pages.items():
            if k not in collection_result.result["child_data"]:
                entries.append((k, v))
            else:
                current_value = collection_result.result["child_data"][k]
                if current_value != v:
                    updates.append((k, current_value, v))
                else:
                    logger.debug(
                        "Not updating {field} for document type "
                        "{document_type}, parameter values are the same "
                        "({value} = {value}).".format(
                            field=k, value=v, document_type=document_type
                        )
                    )

        if len(updates) == 0 and len(entries) == 0:
            logger.info(
                "No updates or new entries to apply to document type "
                "{document_type}.".format(document_type=document_type)
            )
            continue

        for entry in entries:
            logger.info(
                "Adding field {field} for document type {document_type} with "
                "value {value}.".format(
                    field=entry[0], value=entry[1], document_type=document_type
                )
            )
            collection_result.result["child_data"][document_type][entry[0]] = entry[
                1
            ]  # noqa

        for update in updates:
            logger.info(
                "Updating field {field} for document type {document_type} from "
                "{previous} to {new}.".format(
                    field=update[0],
                    previous=update[1],
                    new=update[2],
                    document_type=document_type,
                )
            )
            collection_result.result["child_data"][document_type][update[0]] = update[
                2
            ]  # noqa
    collection_result.save(process=False)


def reprocess_collection_result_signals(collection_result, document):
    # TODO: Find a way of doing this that doesn't involve hardcoding in the
    # document types.
    document_types = [document.schema]
    if document.schema == "apex":
        document_types = [
            "purchase_order",
            "invoice",
            "receiver",
            "change_order",
            "packing_slip",
        ]

    # Reprocess the collection result.
    for document_type in document_types:
        document_schema = document.organization.schema[document_type]
        for receiver in document_schema["receivers"]:
            key_field_listeners = document.organization.schema[receiver][
                "key_field_listeners"
            ][document_type]
            for subkey, v in key_field_listeners.items():
                signals = v["signals"]
                for signal in signals:
                    if signal == "direct_update":
                        direct_update(collection_result.uuid, subkey, document_type)
                    else:
                        method = all_logic[document.organization.type.slug][signal]
                        method(collection_result.uuid)


@dispatch.receiver(post_save, sender=TaskResult)
def reprocess_collection_result(instance, **kwargs):
    # TODO: The logic here needs to be refactored with the overall framework
    # surroinding the application of business logic.  It is a mess right now,
    # so just leave this as is until the rest of the business logic can be
    # refactored.
    task = instance.task

    if len(task.document.collection_list.all()) == 0:
        # This really shouldn't happen, and is the sign of something more
        # sinister going on.
        logger.warning(
            "Document %s does not have any collections to update the "
            "results for." % task.document.pk
        )
        return

    for collection in task.document.collection_list.all():
        # This really shouldn't happen, and is the sign of something more
        # sinister going on.
        if len(collection.results.all()) == 0:
            logger.error(
                "Collection %s does not have any results and cannot be "
                "updated." % collection.pk
            )
            continue
        for cr_result in collection.results.all():
            update_collection_result_from_task_result(
                collection_result=cr_result,
                document=task.document,
            )
            reprocess_collection_result_signals(
                collection_result=cr_result, document=task.document
            )

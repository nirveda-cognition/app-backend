from rest_framework import serializers

from backend.app.collection.models import Collection
from backend.app.document.models import Document, DocumentStatus
from backend.lib.rest_framework_utils.serializers import EnhancedModelSerializer


class CollectionSimpleSerializer(EnhancedModelSerializer):
    # class CollectionSimpleSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(
        read_only=False, required=True, allow_blank=False, allow_null=False
    )
    collection_state = serializers.ChoiceField(
        choices=Collection.COLLECTION_STATE_CHOICES,
        default=Collection.COLLECTION_STATE_NEW,
        allow_blank=False,
        allow_null=False,
    )
    parents = serializers.PrimaryKeyRelatedField(
        read_only=True,
        many=True,
    )

    size = serializers.FloatField(read_only=True)

    collections = serializers.PrimaryKeyRelatedField(
        read_only=True,
        many=True,
    )
    updated_at = serializers.DateTimeField(read_only=True)

    documents = serializers.PrimaryKeyRelatedField(
        read_only=True,
        many=True,
    )

    class Meta:
        model = Collection
        fields = (
            "id",
            "name",
            "collection_state",
            "parents",
            "size",
            "collections",
            "updated_at",
            "documents",
        )


class DocumentStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    status = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)

    class Meta:
        model = DocumentStatus
        fields = ("id", "status", "description")


class DocumentSimpleSerializer(EnhancedModelSerializer):
    # class DocumentSimpleSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(
        required=True,
        allow_blank=False,
        allow_null=False,
    )
    # Used method field instead of using nested serializer
    document_status = serializers.SerializerMethodField("get_status")
    collection_list = serializers.SerializerMethodField("get_collection")
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    status_changed_at = serializers.DateTimeField(read_only=True)
    processing_time = serializers.FloatField(read_only=True)
    size = serializers.IntegerField(read_only=True)

    def get_collection(self, data):
        if type(data) is dict and data is not None:
            col_obj = data.get("collection_list__name")
            # response as per current front-end structure, can be changed
            return [
                {
                    "name": data.get("collection_list__name"),
                    "id": data.get("collection_list__id"),
                }
            ]
        col_obj = data.collection_list.first()
        if col_obj:
            return [{"name": col_obj.name, "id": col_obj.id}]

    def get_status(self, data):
        # response as per current front-end structure, can be changed
        if type(data) is dict:
            return {"status": data.get("document_status__status")}
        return {"status": data.document_status.status}

    class Meta:
        model = Document
        fields = (
            "id",
            "name",
            "document_status",
            "status_changed_at",
            "collection_list",
            "created_at",
            "updated_at",
            "processing_time",
            "size",
        )

from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions

from backend.lib.rest_framework_utils.response import NCPResponse, NCResponse


class UpdateModelMixin:
    def update_or_raise(self, request, request_data=None, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request_data or request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        serializer_class = self.get_response_serializer_class()
        response_data = serializer_class(instance).data
        return self.success_response(response_data, {"data": response_data})

    def update(self, request, *args, **kwargs):
        try:
            return self.update_or_raise(request, *args, **kwargs)
        except exceptions.APIException as e:
            return NCPResponse(code=e.status_code, detail=e).response()

    def perform_update(self, serializer):
        return serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)

    def get_response_serializer_class(self):
        return self.serializer_class

    def success_response(self, payload, drf_data=None):
        auth_header = self.request.META["HTTP_AUTHORIZATION"]
        access_token = auth_header.split()[1]
        user_email = self.request.user.email
        user_category = self.request.user.category

        resp_data = {
            "message_new": "",
            "error": False,
            "code": 200,
            "data": {},
        }

        if drf_data is not None:
            resp_data.update(drf_data)

        return NCResponse(
            200,
            _("RETURNING PAYLOAD"),
            access_token=access_token,
            category=user_category,
            user_name=user_email,
            payload=payload,
        ).ncresponse(resp_data)

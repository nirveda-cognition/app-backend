import operator
from functools import reduce

from django.db import models
from rest_framework import filters
from rest_framework.compat import distinct

from .models import CollectionResult


class CollectionSearchFilter(filters.SearchFilter):
    """
    An extension of :obj:`rest_framework.filters.SearchFilter` specifically
    for :obj:`Collection`(s) that allows the JSON fields of the
    :obj:`Collection`(s) nested :obj:`CollectionResult`(s) to be searched.

    Since :obj:`CollectionResult` points to :obj:`Collection` via a Foreign Key,
    the default :obj:`rest_framework.filters.SearchFilter` does not work due to
    the reverse Foreign Key search.  In other words, this Foreign Key means that
    a given :obj:`Collection` can have many :obj:`CollectionResult`(s), so we
    cannot search by a JSON field in the :obj:`CollectionResult` because
    there are potentially many of them.

    For instance, the following will not work with the generic
    :obj:`rest_framework.filters.SearchFilter`:

    >>> class CollectionViewSet(...):
    >>>     search_fields = [
    >>>         "name",
    >>>         "collection__result__result__collection_data__invoice_total"
    >>>     ]

    However, the :obj:`CollectionSearchFilter` will instead apply the filters
    to a queryset of :obj:`CollectionResult`(s) and then reverse the
    relationship to filter by the :obj:`Collection`(s) - effectively allowing
    this search to be performed.

    The caveat is that the search field will apply to all
    :obj:`CollectionResult`(s) belonging to a :obj:`Collection`, not just the
    latest :obj:`CollectionResult`.
    """

    def _include_result_search(self, query, view, request):
        fields = self.get_result_search_fields(view, request)
        search_terms = self.get_search_terms(request)
        prefix = getattr(
            view, "collection_result_search_prefix", "result__collection_data"
        )
        fields = ["%s__%s" % (prefix, field) for field in fields]
        conditions = [
            self._generate_condition(fields, search_term)
            for search_term in search_terms
        ]
        results = CollectionResult.objects.filter(
            reduce(operator.and_, conditions)
        ).all()
        ids = set([result.collection.id for result in results])
        return reduce(operator.or_, [query, models.Q(id__in=ids)])

    def get_result_search_fields(self, view, request):
        return getattr(view, "collection_result_search_fields", None)

    def _generate_condition(self, fields, search_term):
        orm_lookups = [
            self.construct_search(str(search_field)) for search_field in fields
        ]
        queries = [models.Q(**{orm_lookup: search_term}) for orm_lookup in orm_lookups]
        return reduce(operator.or_, queries)

    def filter_queryset(self, request, queryset, view):
        cr_search_fields = self.get_result_search_fields(view, request)
        if not cr_search_fields:
            return super().filter_queryset(request, queryset, view)

        search_fields = self.get_search_fields(view, request)
        search_terms = self.get_search_terms(request)
        if (not search_fields and not cr_search_fields) or not search_terms:
            return queryset

        base = queryset
        queries = []
        for search_term in search_terms:
            query = self._generate_condition(search_fields, search_term)
            query = self._include_result_search(query, view, request)
            queries.append(query)

        queryset = view.get_queryset().filter(reduce(operator.and_, queries))

        if self.must_call_distinct(queryset, search_fields):
            queryset = distinct(queryset, base)
        return queryset

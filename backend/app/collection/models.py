import logging
import uuid

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import APIException

from backend.app.common.models import TimestampedModel
from backend.lib.django_utils.models import track_field_changes
from backend.lib.utils.fs import construct_unique_name

from .managers import CollectionManager

logger = logging.getLogger("backend")


def dynamic_default_now():
    return timezone.now()


@track_field_changes({"collection_state": "status_changed_at"})
class Collection(TimestampedModel):
    name = models.CharField(max_length=256)
    uuid = models.CharField(max_length=64, default=uuid.uuid4)
    # TODO: Build in validation so that the organization of the collections
    # is the same as the organization of the instance.
    collections = models.ManyToManyField(
        to="self", symmetrical=False, blank=True, related_name="parents"
    )
    status_changed_at = models.DateTimeField(default=dynamic_default_now)
    organization = models.ForeignKey(
        to="organization.Organization",
        on_delete=models.CASCADE,
        related_name="collections",
    )
    trash = models.BooleanField(default=False)

    created_by = models.ForeignKey(
        to="custom_auth.CustomUser",
        on_delete=models.CASCADE,
        related_name="created_collections",
    )
    owner = models.ForeignKey(
        to="custom_auth.CustomUser",
        # TODO: Change to SET_NULL
        on_delete=models.DO_NOTHING,
        null=True,
        related_name="owned_collections",
    )
    collection_pointer = models.ForeignKey(
        to="collection.CollectionPointer",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    assigned_users = models.ManyToManyField(
        to="custom_auth.CustomUser",
        blank=True,
        related_name="assigned_collections",
    )

    STATE_DEFAULT = STATE_ACTIVE = "Active"
    STATE_TRASH = "Trash"
    STATES = [
        (STATE_ACTIVE, _("Active")),
        (STATE_TRASH, _("Trash")),
    ]
    state = models.CharField(
        max_length=8,
        choices=STATES,
        default=STATE_DEFAULT,
    )

    COLLECTION_STATE_DEFAULT = COLLECTION_STATE_NEW = "New"
    COLLECTION_STATE_READY = "Ready For Match"
    COLLECTION_STATE_COMPLETED = "Completed"
    COLLECTION_STATE_VOUCHEREXT = "Voucher Extraction"
    COLLECTION_STATE_REVIEW = "Exception Review"
    COLLECTION_STATE_CHOICES = [
        (COLLECTION_STATE_NEW, _("New")),
        (COLLECTION_STATE_READY, _("Ready For Match")),
        (COLLECTION_STATE_COMPLETED, _("Completed")),
        (COLLECTION_STATE_VOUCHEREXT, _("Voucher Extraction")),
        (COLLECTION_STATE_REVIEW, _("Exception Review")),
    ]
    collection_state = models.CharField(
        max_length=20,
        choices=COLLECTION_STATE_CHOICES,
        default=COLLECTION_STATE_DEFAULT,
    )

    SCHEMA_EXPENSE = "Expense"
    SCHEMA_INVOICE = "Invoice"
    SCHEMA_LOAN = "Loan"
    SCHEMA_WORKITEM = "WorkItem"
    SCHEMAS = [
        (SCHEMA_EXPENSE, _("Expense")),
        (SCHEMA_INVOICE, _("Invoice")),
        (SCHEMA_LOAN, _("Loan")),
        (SCHEMA_WORKITEM, _("WorkItem")),
    ]
    schema = models.CharField(max_length=20, choices=SCHEMAS, blank=True, null=True)

    objects = CollectionManager()

    class Meta:
        get_latest_by = "time_created"
        ordering = ("-status_changed_at",)

    @property
    def result(self):
        try:
            return self.results.latest()
        except CollectionResult.DoesNotExist:
            return None

    def create_unique_collection_name(self, name):
        """
        Using a base name, constructs a unique name for a new `obj:Collection`
        belonging to this `obj:Collection` instance based on the names of the
        other `obj:Collection`(s) belong to this `obj:Collection` instance.
        """
        return construct_unique_name(
            name,
            [collection.name for collection in self.collections.all()],
            case_sensitive=False,
        )

    def create_unique_document_name(self, filename):
        """
        Using a base name, constructs a unique name for a new `obj:Document`
        belonging to this `obj:Collection` instance based on the names of the
        other `obj:Document`(s) belonging to this `obj:Collection` instance.
        """
        return construct_unique_name(
            filename,
            [document.name for document in self.documents.all()],
            case_sensitive=False,
            with_extensions=False,
        )

    @property
    def size(self):
        return sum([collection.size for collection in self.collections.all()]) + sum(
            [
                document.size
                for document in self.documents.all()
                if document.size is not None
            ]
        )

    def restore(self):
        """
        Removes the :obj:`Collection` instance from the Trash and restores
        it's state to it's pre-Trash value.

        Note that there should not be any nested children :obj:`Collection`(s)
        or :obj:`Document`(s) at this point, because they should have been
        disassociated with this :obj:`Collection` when they were originally
        sent to the Trash.
        """
        self.state = self.STATE_ACTIVE
        if self.collections.count() != 0:
            logger.error(
                "Collection %s is in the trash but still has remnant children "
                "collections that should have been disassociated." % self.pk
            )
            for collection in self.collections.all():
                collection.restore()
        if self.documents.count() != 0:
            logger.error(
                "Collection %s is in the trash but still has remnant children "
                "documents that should have been disassociated." % self.pk
            )
            for document in self.documents.all():
                document.restore()
        self.save()

    def check_doc_status(self):
        for document in self.documents.all():
            if document.document_status == "Processing":
                raise APIException("Document still under processing", code=400)
        return True

    def to_trash(self, user):
        """
        Associates the :obj:`Collection` instance with the Trash.  This is
        opposed to permanently deleting the :obj:`Collection` instance.

        When a :obj:`Collection` is moved to the Trash, it's child
        :obj:`Collection`(s) and :obj:`Document`(s) will also be moved to the
        Trash, but they all need to be disassociated with their parent
        :obj:`Collection`(s) to prevent states from clashing on restore.
        """

        # Col == 1 | documents == 2 | child == 2 | proc = 0
        # child_1 == 0 | documents == 2 | proc = 0 ==> TRASH
        # child_2 == 1 | documents == 1 | proc = 1
        # child_2_1 == 0 | documents == 1 | proc = 1

        for child in self.collections.all():
            if self.check_doc_status():
                child.state = self.STATE_TRASH
                child.to_trash(user)

        docs_to_delete = []

        for document in self.documents.all():
            if self.check_doc_status():
                docs_to_delete.append(document)

        for doc in docs_to_delete:
            doc.to_trash(user)

        self.state = self.STATE_TRASH

        self.save()


class CollectionResult(models.Model):
    time_created = models.DateTimeField(auto_now=True)
    uuid = models.CharField(max_length=256, default=uuid.uuid4)
    result = models.JSONField(default=dict, null=True)
    collection = models.ForeignKey(
        to="collection.Collection", on_delete=models.CASCADE, related_name="results"
    )

    class Meta:
        get_latest_by = "time_created"
        ordering = ("-time_created",)
        verbose_name = "Collection Result"
        verbose_name_plural = "Collection Results"

    def initialize_with_schema(self, organization):
        # TODO: This will be deprecated.
        schema = organization.schema[organization.schema_type]

        self.result["child_data"] = {}
        key_field_property = schema["key_field_property"]
        self.result[key_field_property] = {}

        kf = schema.get("key_fields", {})
        for field, field_schema in kf.items():
            if "key_fields" in field_schema:
                self.result[key_field_property][field] = {}
                for sub_field, sub_field_schema in field_schema[
                    "key_fields"
                ].items():  # noqa
                    self.result[key_field_property][field][
                        sub_field
                    ] = sub_field_schema.get(
                        "default_value"
                    )  # noqa
            else:
                self.result[key_field_property][field] = field_schema.get(
                    "default_value"
                )  # noqa

        # Why are we doing this?
        self._result = {"collection_data": {}}

    def save(self, *args, **kwargs):
        self._skip_processing = not kwargs.pop("process", True)
        super(CollectionResult, self).save(*args, **kwargs)


class TrackedOperation(models.Model):
    time_created = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(to="custom_auth.CustomUser", on_delete=models.CASCADE)
    uid = models.IntegerField(null=True)
    # TODO: Use ContentType Field - That's what they're for.
    model = models.CharField(max_length=256)
    collections = models.ManyToManyField(to="collection.Collection", blank=True)

    NEW = "NEW"
    UPDATE = "UPDATE"
    DELETE = "DELETE"
    MOVE = "MOVE"
    MOVE_TO = "MOVE_TO"
    MOVE_FROM = "MOVE_FROM"
    OPERATION_CHOICES = [
        (NEW, _("New")),
        (UPDATE, _("Update")),
        (DELETE, _("Delete")),
        (MOVE, _("Move")),
        (MOVE_TO, _("Move To")),
        (MOVE_FROM, _("Move From")),
    ]
    operation = models.CharField(max_length=128, choices=OPERATION_CHOICES)


class CollectionPointer(TimestampedModel):
    collections = models.ManyToManyField(to="collection.Collection", blank=True)
    documents = models.ManyToManyField(to="document.Document", blank=True)
    collection_head = models.IntegerField(null=True)

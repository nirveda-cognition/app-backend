from django.urls import include, path
from rest_framework import routers

from backend.app.document.urls import collection_router

from .views import CollectionResultViewSet, CollectionTrashViewSet, CollectionViewSet

app_name = "collection"

router = routers.SimpleRouter()
router.register(r"trash", CollectionTrashViewSet, basename="trash")
router.register(r"", CollectionViewSet, basename="collection")

urlpatterns = router.urls + [
    path(
        "<int:collection_pk>/",
        include(
            [
                path("documents/", include(collection_router.urls)),
                path(
                    "results/",
                    CollectionResultViewSet.as_view(
                        {
                            "get": "list",
                        }
                    ),
                ),
                path(
                    "results/latest/",
                    CollectionResultViewSet.as_view(
                        {
                            "get": "latest",
                        }
                    ),
                ),
                path(
                    "results/<int:pk>/",
                    CollectionResultViewSet.as_view(
                        {
                            "get": "retrieve",
                        }
                    ),
                ),
            ]
        ),
    ),
]

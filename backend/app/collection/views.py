import rest_framework_filters
from django.http import Http404
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as dj_filters
from rest_framework import decorators, filters, mixins, response, status, viewsets
from rest_framework.exceptions import ParseError

from backend.app.collection.models import CollectionResult
from backend.app.common.serializers import CollectionSimpleSerializer
from backend.app.document.serializers import DocumentSerializer
from backend.app.organization.mixins import OrganizationNestedMixin
from backend.app.user.models import CustomUser, UserCategory
from backend.lib.rest_framework_utils.pagination import paginate_action

from .filters import CollectionSearchFilter
from .models import Collection
from .serializers import (
    AssignCollectionSerializer,
    AssignCollectionToSerializer,
    AssignCollectionUsersSerializer,
    CollectionResultSerializer,
    CollectionSerializer,
    UnassignCollectionUsersSerializer,
    UpdateCollectionResultSerializer,
    UploadCollectionDocumentSerializer,
)


class CollectionFilter(dj_filters.FilterSet):
    created_at__gte = dj_filters.DateTimeFilter(
        field_name="created_at", lookup_expr="gte"
    )
    created_at__lte = dj_filters.DateTimeFilter(
        field_name="created_at", lookup_expr="lte"
    )
    status_changed_at__lte = dj_filters.DateTimeFilter(
        field_name="status_changed_at", lookup_expr="lte"
    )
    status_changed_at__gte = dj_filters.DateTimeFilter(
        field_name="status_changed_at", lookup_expr="gte"
    )
    collection_state = dj_filters.ChoiceFilter(
        choices=Collection.COLLECTION_STATE_CHOICES,
        field_name="collection_state",
    )
    assigned_users = dj_filters.ModelMultipleChoiceFilter(
        field_name="assigned_users__pk",
        to_field_name="pk",
        queryset=CustomUser.objects.active(),
    )

    class Meta:
        model = Collection
        fields = (
            "created_at__gte",
            "created_at__lte",
            "status_changed_at__lte",
            "status_changed_at__gte",
            "collection_state",
            "assigned_users",
        )


class CollectionViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    OrganizationNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/collections/
    (2) POST /organizations/<pk>/collections/
    (3) GET /organizations/<pk>/collections/<pk>/
    (4) DELETE /organizations/<pk>/collections/<pk>/
    (5) GET /organizations/<pk>/collections/<pk>/assigned-users/
    (6) PATCH /organizations/<pk>/collections/<pk>/assign/
    (7) PATCH /organizations/<pk>/collections/<pk>/assign-to/
    (8) POST /organizations/<pk>/collections/<pk>/assign-users/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    ordering_fields = ["status_changed_at", "created_at", "name"]
    search_fields = ["name"]
    collection_result_search_prefix = "result__collection_data"
    collection_result_search_fields = [
        "invoice_number",
        "invoice_due_date",
        "total_amount",
    ]
    filterset_class = CollectionFilter
    filter_backends = [
        CollectionSearchFilter,
        filters.OrderingFilter,
        rest_framework_filters.backends.RestFrameworkFilterBackend,
    ]
    throttle_classes = []

    @property
    def is_simple(self):
        simple = self.request.GET.get("simple", False)
        if simple is not False:
            return True
        return False

    @property
    def is_include_trash(self):
        include_trash = self.request.GET.get("include_trash", False)
        if (
            include_trash is not False
            and self.request.user.category == UserCategory.SUPERUSER
            and self.action == "list"
        ):
            return True
        return False

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update(
            request=self.request,
            organization=self.organization,
        )
        return context

    def get_serializer_class(self):
        serializers = {
            ("POST", "assign_to"): AssignCollectionToSerializer,
            ("POST", "assign"): AssignCollectionSerializer,
            ("POST", "assign_users"): AssignCollectionUsersSerializer,
            ("POST", "unassign_users"): UnassignCollectionUsersSerializer,
            ("POST", "upload"): UploadCollectionDocumentSerializer,
        }
        # TODO: We want to be able to use a simple serialized form of the
        # collection on GET requests - however, there are smarter ways of doing
        # this than the one seen here.  We should investigate how to do this
        # more elegantly.
        default_serializer_cls = CollectionSerializer
        if self.is_simple:
            default_serializer_cls = CollectionSimpleSerializer
        return serializers.get(
            (self.request.method, self.action), default_serializer_cls
        )

    def get_queryset(self):
        if self.is_include_trash:
            return self.organization.collections.all()

        queryset = self.organization.collections.active()
        queryset = queryset.prefetch_related("documents")
        return queryset

    def perform_create(self, serializer):
        serializer.save(
            created_by=self.request.user,
            organization=self.organization,
            owner=self.request.user,
        )

    def destroy(self, request, *args, **kwargs):

        instance = self.get_object()

        def recurse_children(instance):
            print(instance.collections.all())

            
            for child in instance.collections.all():
                if child.documents.filter(document_status__status="Processing"):
                    raise ParseError(code=400, detail="Document still processing")
                recurse_children(child)

        if instance.documents.filter(document_status__status="Processing"):
            raise ParseError(code=400, detail="Document still processing")

        recurse_children(instance)

        instance.to_trash(request.user)
        return response.Response(status=204)

    @decorators.action(detail=True, methods=["POST"], url_path="child")
    def create_child(self, request, *args, **kwargs):
        """
        Creates a :obj:`Collection`instance from the payload that is a child of
        the current :obj:`Collection` instance associated with the detail
        endpoint.

        POST /organizations/<pk>/collections/<pk>/child/
        """
        instance = self.get_object()
        data = {**request.data, **{"parents": [instance.pk]}}
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save(
            created_by=request.user, organization=self.organization, owner=request.user
        )
        return response.Response(
            CollectionSerializer(
                instance,
                context={"request": request, "organization": self.organization},
            ).data,
            status=status.HTTP_201_CREATED,
        )

    @paginate_action
    @decorators.action(detail=False, methods=["GET"])
    def root(self, request, *args, **kwargs):
        """
        Returns the root :obj:`Collection` instances for the organization.
        These :obj:`Collection`(s) do not have any parents.
        """
        qs = self.get_queryset()
        qs = qs.prefetch_related("documents")
        qs = qs.filter(parents=None)
        return self.filter_queryset(qs)

    @paginate_action
    @decorators.action(detail=True, methods=["GET"])
    def collections(self, request, *args, **kwargs):
        """
        Returns the full serialized :obj:`Collection` instances associated with
        the :obj:`Collection` instance.  If the `simple` query parameter is
        provided, the serialization is minified.
        """
        obj = self.get_object()
        return obj.collections.all()

    @decorators.action(detail=True, methods=["POST"], url_path="assign-users")
    def assign_users(self, request, *args, **kwargs):
        """
        Assigns the :obj:`Collection`instance a series of `obj:Collection`(s)
        pertaining to specific users.

        POST /organizations/<pk>/collections/<pk>/assign-users/
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return response.Response(
            CollectionSerializer(
                instance,
                context={"request": request, "organization": self.organization},
            ).data,
            status=status.HTTP_201_CREATED,
        )

    @decorators.action(detail=True, methods=["POST"], url_path="unassign-users")
    def unassign_users(self, request, *args, **kwargs):
        """
        Unassigns users from the :obj:`Collection`instance.

        POST /organizations/<pk>/collections/<pk>/unassign-users/
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return response.Response(
            CollectionSerializer(
                instance,
                context={"request": request, "organization": self.organization},
            ).data,
            status=status.HTTP_201_CREATED,
        )

    @decorators.action(detail=True, methods=["POST"], url_path="assign-to")
    def assign_to(self, request, *args, **kwargs):
        """
        Assigns the :obj:`Collection`to another parent `obj:Collection`.

        POST /organizations/<pk>/collections/<pk>/assign-to/
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return response.Response(
            CollectionSerializer(
                instance,
                context={"request": request, "organization": self.organization},
            ).data,
            status=status.HTTP_201_CREATED,
        )

    @decorators.action(detail=True, methods=["POST"])
    def assign(self, request, *args, **kwargs):
        """
        Assigns the :obj:`Collection`a series of :obj:`Collection`children.

        POST /organizations/<pk>/collections/<pk>/assign/
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return response.Response(
            CollectionSerializer(
                instance,
                context={"request": request, "organization": self.organization},
            ).data,
            status=status.HTTP_201_CREATED,
        )

    @decorators.action(detail=True, methods=["POST"])
    def upload(self, request, *args, **kwargs):
        """
        Uploads a document to the collection.

        POST /organizations/<pk>/collections/<pk>/upload/
        """
        logger.info("mitesh1")
        instance = self.get_object()
        logger.info("mitesh2")
        serializer = self.get_serializer(instance=instance, data=request.data)
        logger.info("mitesh3")
        serializer.is_valid(raise_exception=True)
        logger.info("mitesh4")
        data = serializer.save(user=request.user)
        logger.info("mitesh5")
        return response.Response(
            {
                "put_signed_url": data["put_url"],
                "get_signed_url": data["get_url"],
                "document_id": data["document"].pk,
                "headers": data["headers"],
                "document": DocumentSerializer(data["document"]).data,
            },
            status=201,
        )


class CollectionTrashViewSet(
    mixins.DestroyModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    OrganizationNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/collections/trash/
    (2) GET /organizations/<pk>/collections/trash/<pk>/
    (3) PATCH /organizations/<pk>/collections/trash/<pk>/restore/
    (4) DELETE /organizations/<pk>/collections/trash/<pk>/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    serializer_class = CollectionSerializer
    ordering_fields = ["status_changed_at", "name", "created_at"]
    search_fields = ["name"]

    def get_queryset(self):
        return self.organization.collections.inactive()

    @decorators.action(detail=True, methods=["PATCH"])
    def restore(self, request, *args, **kwargs):
        """
        Moves the :obj:`Collection`that is in the trash out of the trash
        to the main set of `obj:Collection`(s).

        PATCH /organizations/<pk>/collections/trash/<pk>/restore/
        """
        instance = self.get_object()
        instance.restore()
        return response.Response(
            self.serializer_class(instance).data, status=status.HTTP_201_CREATED
        )

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class CollectionResultViewSet(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    OrganizationNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/collections/<pk>/results/
    (2) GET /organizations/<pk>/collections/<pk>/results/<pk>/
    (3) GET /organizations/<pk>/collections/<pk>/results/latest/
    (4) PATCH /organizations/<pk>/collections/<pk>/results/<pk>/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    ordering_fields = ["status_changed_at", "name"]
    search_fields = ["name"]
    serializer_class = CollectionResultSerializer

    def get_serializer_class(self):
        serializers = {
            ("PATCH", "partial_update"): UpdateCollectionResultSerializer,
        }
        return serializers.get(
            (self.request.method, self.action), CollectionResultSerializer
        )

    @decorators.action(detail=False, methods=["GET"])
    def latest(self, request, *args, **kwargs):
        qs = self.get_queryset()
        try:
            instance = qs.latest()
        except CollectionResult.DoesNotExist:
            raise Http404("The collection does not have any results.")
        else:
            return response.Response(self.serializer_class(instance).data)

    def get_queryset(self):
        collection = get_object_or_404(
            self.organization.collections.all(), pk=self.kwargs["collection_pk"]
        )
        # We are guaranteed that the user has access to the collection because
        # the collection is in the organization that the user has access to.
        return collection.results.all()

from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions


class OrganizationConfigurationError(Exception):
    """
    Raised when there is a configuration error related to a :obj:`Organization`
    instance.
    """

    def __init__(self, organization, message=None):
        msg = "Configuration Error for %s" % organization.name
        if message is not None:
            msg += ": %s" % message
        super(OrganizationConfigurationError, self).__init__(msg)


class OrganizationNotConfiguredError(OrganizationConfigurationError):
    """
    Raised when a :obj:`Organization` does not have the appropriate
    configuration in Django's settings.
    """

    def __init__(self, organization, message=None):
        super(OrganizationNotConfiguredError, self).__init__(
            organization=organization,
            message=message or "The organization is not configured in settings.",
        )


class SchemaNotConfiguredError(OrganizationNotConfiguredError):
    """
    Raised when a :obj:`Organization` does not have the SCHEMA_DIR
    defined in settings.
    """

    def __init__(self, organization, message=None):
        super(SchemaNotConfiguredError, self).__init__(
            organization=organization,
            message=message
            or ("A schema path for the organization is not configured " "in settings."),
        )


class SchemaFileNotFoundError(OrganizationConfigurationError, FileNotFoundError):
    """
    Raised when the :obj:`Organization`'s schema file, defined by SCHEMA_DIR
    in settings, does not exist.
    """

    def __init__(self, organization, message=None):
        OrganizationConfigurationError.__init__(
            self,
            organization=organization,
            message=message
            or ("The schema could not be found at the configured path."),
        )


class OrganizationErrorCodes(object):
    NO_USER_ACCESS = "no_user_access"
    NO_ORGANIZATION_API_KEY = "no_organization_api_key"
    NOT_ACTIVE = "not_active"
    NO_CAPABILITY = "no_capability"
    NO_SERVICE = "no_service"


class OrganizationPermissionError(exceptions.PermissionDenied):
    default_detail = _("The user does not have access to this organization.")
    default_code = OrganizationErrorCodes.NO_USER_ACCESS


class OrganizationNotActiveError(exceptions.ParseError):
    default_default = "The organization is not active."
    default_code = OrganizationErrorCodes.NOT_ACTIVE

    def __init__(self, organization, service, capability):
        from backend.app.organization.models import Organization

        super().__init__(
            detail=(
                "The organization {org} is not active.".format(
                    org=(
                        organization.name
                        if isinstance(organization, Organization)
                        else organization
                    )
                )
            )
        )


class OrganizationCapabilityError(exceptions.ParseError):
    default_default = "The organization does not have the required capability."
    default_code = OrganizationErrorCodes.NO_CAPABILITY

    def __init__(self, organization, service, capability):
        from backend.app.organization.models import Organization

        super().__init__(
            detail=(
                "The organization {org} does not have the required capability "
                "{capability} for service {service}.".format(
                    capability=capability,
                    service=service,
                    org=(
                        organization.name
                        if isinstance(organization, Organization)
                        else organization
                    ),
                )
            )
        )


class OrganizationServiceError(exceptions.ParseError):
    default_default = "The organization does not have the required service."
    default_code = OrganizationErrorCodes.NO_SERVICE

    def __init__(self, organization, service):
        from backend.app.organization.models import Organization

        super().__init__(
            detail=(
                "The organization {org} does not have the required service "
                "{service}.".format(
                    service=service,
                    org=(
                        organization.name
                        if isinstance(organization, Organization)
                        else organization
                    ),
                )
            )
        )


class OrganizationApiKeyError(exceptions.ParseError):
    default_detail = _("The organization does not have an associated API Key.")
    default_code = OrganizationErrorCodes.NO_ORGANIZATION_API_KEY

    def __init__(self, organization):
        from backend.app.organization.models import Organization

        super().__init__(
            detail=(
                "The organization {org} does not have an associated API "
                "Key.".format(
                    org=(
                        organization.name
                        if isinstance(organization, Organization)
                        else organization
                    )
                )
            )
        )

from django.contrib import admin

from .models import Organization, OrganizationType


class OrganizationTypeAdmin(admin.ModelAdmin):
    ordering = ("id",)
    list_display = ("id", "title", "slug")


class OrganizationAdmin(admin.ModelAdmin):
    ordering = ("id",)
    list_display = ("id", "name", "is_active", "api_access", "api_key", "json_info")


admin.site.register(Organization, OrganizationAdmin)
admin.site.register(OrganizationType, OrganizationTypeAdmin)

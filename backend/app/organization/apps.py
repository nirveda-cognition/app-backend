from django.apps import AppConfig


class OrganizationConfig(AppConfig):
    name = "backend.app.organization"
    verbose_name = "Organization"

import json

from django.conf import settings
from django.db import models
from django.utils.functional import cached_property

from backend.app.ai_services.constants import AIServicesService
from backend.app.common.models import TimestampedModel

from .exceptions import (
    OrganizationApiKeyError,
    OrganizationConfigurationError,
    OrganizationNotActiveError,
    OrganizationNotConfiguredError,
    OrganizationPermissionError,
    OrganizationServiceError,
    SchemaFileNotFoundError,
    SchemaNotConfiguredError,
)
from .managers import OrganizationManager, OrganizationTypeManager


class OrganizationType(models.Model):
    slug = models.SlugField(max_length=128, unique=True)
    description = models.CharField(max_length=256, null=True, blank=True)
    title = models.CharField(max_length=128)

    objects = OrganizationTypeManager()

    class Meta:
        verbose_name = "Organization Type"
        verbose_name_plural = "Organization Types"

    def __str__(self):
        return "<{cls} id={id} title={title} slug={slug}>".format(
            cls=self.__class__.__name__, title=self.title, id=self.pk, slug=self.slug
        )


class Organization(TimestampedModel):
    name = models.CharField(unique=True, max_length=256)
    slug = models.SlugField(max_length=32, unique=True)
    description = models.CharField(max_length=256, null=True, blank=True)
    api_access = models.BooleanField(default=True)
    api_key = models.CharField(max_length=256, null=True, blank=True)
    api_url = models.CharField(max_length=256, null=True, blank=True)
    type = models.ForeignKey(to=OrganizationType, on_delete=models.CASCADE)
    json_info = models.JSONField(default=dict, null=True)
    logo = models.CharField(max_length=1024, blank=True, null=True)
    callback_whitelist = models.CharField(max_length=1024, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    objects = OrganizationManager()

    class Meta:
        verbose_name = "Organization"
        verbose_name_plural = "Organizations"
        ordering = ("created_at",)

    def __str__(self):
        return "<{cls} id={id} name={name}>".format(
            cls=self.__class__.__name__,
            name=self.name,
            id=self.pk,
        )

    @property
    def num_users(self):
        return len(self.users.all())

    @property
    def num_groups(self):
        return len(self.groups.all())

    @property
    def brand(self):
        if self.name.lower() == "kpmg":
            return "kpmg"
        return "nirveda"

    @property
    def tenant_id(self):
        """
        An identifier unique to the organization that is passed back to
        AI Services to associate the organization with a generated API key,
        """
        return "%s-%s" % (self.name, self.pk)

    def user_has_access(self, user):
        if user.is_superuser:
            return True
        if user.organization != self:
            return False
        return True

    def raise_no_access(self, user):
        """
        Raises a PermissionError if the provided user does not have access to
        this organization.
        """
        if not self.user_has_access(user):
            raise OrganizationPermissionError()

    def validate_for_processing(self):
        if not self.is_active:
            raise OrganizationNotActiveError(organization=self)
        elif self.api_key is None:
            raise OrganizationApiKeyError(organization=self)
        elif not self.has_service(AIServicesService.DOCUMENT_EXTRACTOR):  # noqa
            raise OrganizationServiceError(
                organization=self, service=AIServicesService.DOCUMENT_EXTRACTOR
            )

    @property
    def settings(self):
        try:
            return getattr(settings, self.type.slug.upper())
        except AttributeError:
            raise OrganizationNotConfiguredError(self)

    @property
    def schema_type(self):
        try:
            return self.settings["SCHEMA_TYPE"]
        except KeyError:
            raise OrganizationConfigurationError(
                self,
                message="The "
                "SCHEMA_TYPE is not defined in the organizatin's settings.",
            )

    @property
    def schema_file(self):
        try:
            return self.settings["SCHEMA_DIR"]
        except KeyError:
            raise SchemaNotConfiguredError(self)

    def get_schema(self, strict=True):
        try:
            with open(self.schema_file) as schema_file:
                return json.load(schema_file)
        except FileNotFoundError:
            raise SchemaFileNotFoundError(self)
        except SchemaNotConfiguredError as e:
            if not strict:
                return None
            raise e

    @cached_property
    def schema(self):
        return self.get_schema(strict=True)

    def get_capabilities(self, service):
        if not self.has_service(service):
            return []
        return self.json_info[service]

    def has_service(self, service):
        if self.json_info is None or service not in self.json_info:
            return False
        return True

    def has_capability(self, service, capability):
        capabilities = self.get_capabilities(service)
        if capability not in capabilities:
            return False
        return True

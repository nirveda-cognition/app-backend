import logging

from django.contrib import admin
from django.template.defaultfilters import truncatechars

from backend.app.ai_services.tasks import process_document
from backend.app.task.models import TaskResult
from backend.app.task.utils import convert_df_to_table, convert_table_to_df

from .models import Document, DocumentStatus

logger = logging.getLogger("backend")


class DocumentAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
        "path",
        "uuid",
        "collections",
        "type",
        "size",
        "organization",
        "state",
        "document_status",
        "trash",
        "created_at",
        "ai_task",
    )

    def ai_task(self, obj):
        if obj.task is not None:
            return str(obj.task.ai_uuid)
        else:
            return "No AI uuid"

    def shortened_storage_path(self, obj):
        return truncatechars(str(obj.storage_path), 50)

    def collections(self, obj):
        return ", ".join(
            [str(c.id) + ": " + c.name for c in obj.collection_list.all()]
        ).strip(", ")

    def format_tables(self, request, queryset):
        for doc in queryset:
            """
            Get table data from extracted_data and make sure it is well
            formatted.
            """
            if doc.type in ["nirveda", "ai.nirveda", "folder"]:
                continue

            document = (
                Document.objects.filter(uuid=doc.uuid).order_by("-created_at").first()
            )

            result = (
                TaskResult.objects.filter(document=document)
                .order_by("-created_at")
                .first()
            )

            results = result.new_data["data"]["extracted_data"]

            if results is not None:
                for i, node in enumerate(results):
                    if node.get("type") == "collection":
                        base_index = node.get("index", 0)
                        tmp = convert_table_to_df(node.get("items"))
                        tmp = convert_df_to_table(tmp, base_index)
                        results[i] = tmp

            result.new_data["data"]["extracted_data"] = results
            result.save()
            self.message_user(request, "Document tables successfully reformatted.")

    def reprocess_document(self, request, queryset):
        for doc in queryset:
            if doc.type in ["nirveda", "ai.nirveda", "folder"]:
                logger.debug("Skipping folder with type %s." % doc.type)
                continue
            process_document.delay(doc.pk)

        self.message_user(request, "Documents successfully submitted for reprocessing.")

    # actions = [reprocess_document]
    format_tables.short_description = "Update Table Format for Selected Documents"
    reprocess_document.short_description = "Reprocess Selected Documents"

    def fail_document(self, request, queryset):
        qs = queryset.filter(document_status__id=1)
        print(qs)
        for doc in qs:
            doc.document_status = DocumentStatus.objects.get(id=4)
            doc.save()

        self.message_user(request, "Documents Status Changed Successfully.")

    actions = [fail_document, reprocess_document]
    fail_document.short_description = "Document status changed to Fail"


class DocumentStatusAdmin(admin.ModelAdmin):
    list_display = ("id", "status", "description")


admin.site.register(Document, DocumentAdmin)
admin.site.register(DocumentStatus, DocumentStatusAdmin)

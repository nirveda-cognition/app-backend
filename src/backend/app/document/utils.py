import logging
import os
import time
import uuid as UUID
from datetime import datetime
from django.conf import settings
from django.db import transaction

from backend.app.ai_services.tasks import process_document
from backend.app.collection.models import CollectionResult
from backend.app.task.models import Task
from backend.lib.aws.nv_bucket_manager import NVBucketManager
<<<<<<< HEAD

from backend.lib.aws.nv_bucket_manager import NVs3bucket
=======
from backend.lib.aws.nv_s3_bucket import NVs3bucket      # added
>>>>>>> 6c8e6af7bf09b9a506a50dddbe45d697ebcaedb1
from .models import Document
from .tasks import set_file_size, watch_for_uploads

logger = logging.getLogger("backend")


def start_document_upload(document, put_expiration=1800):
    logger.info("INSIDE START DOC UPLOAD")
    upload = NVBucketManager()
    headers = upload.headers
    if document.content_type is not None:
        headers["Content-Type"] = document.content_type

    document.aws_document_key = str(UUID.uuid4()) + str(int(time.time()))
    document.storage_path = upload.generate_get_presigned_url(
        document.aws_document_key, document.name
    )
   # put_url = upload.generate_put_presigned_url(
   #         document.aws_document_key,
   #         expiration=put_expiration,
   #         content_type=document.content_type,
   #     )
    head_url = upload.generate_head_presigned_url(document.aws_document_key)    
    upload_2 = NVs3bucket()     # added
    container_name = settings.NC_AWS_BUCKET_NAME
    put_url = upload_2.generate_put_presigned_url(container_name, document.aws_document_key, expiration=put_expiration,
    content_type=document.content_type,)


    upload_2 = NVs3bucket()
    container_name = settings.NC_AWS_BUCKET_NAME
    put_url_2 = upload_2.generate_put_presigned_url(container_name, document.aws_document_key, expiration=put_expiration, content_type=document.content_type)

    # KPMG We need to route requests from the frontend through the backend
    # because why not?
    #logger.info("DEBUG : ",settings.FRONTEND_STORAGE_PROXY)
    if settings.FRONTEND_STORAGE_PROXY is not None:
<<<<<<< HEAD
	#put_url = (
        #    os.path.join(settings.APP_V1_URL, "upload", document.aws_document_key) + "/"
        #)
        put_url = upload.generate_put_presigned_url(
            document.aws_document_key,
            expiration=put_expiration,
            content_type=document.content_type,
	)
=======
        logger.info("DEBUG : INSIDE IF")
        put_url = (
            os.path.join(settings.APP_V1_URL, "upload", document.aws_document_key) + "/"
        )
        logger.info("DEBUG : PUT URL IN IF :"+put_url)
>>>>>>> 6c8e6af7bf09b9a506a50dddbe45d697ebcaedb1
    else:
        logger.info("DEBUG : INSIDE ELSE")
        put_url = upload.generate_put_presigned_url(
            document.aws_document_key,
            expiration=put_expiration,
            content_type=document.content_type,
        )
<<<<<<< HEAD
    
    print("DEBUG-PUT-URL",put_url)
=======
        #document.storage_path = upload.generate_get_presigned_url(
        #document.aws_document_key, document.name   # bucket , key
   # )
        print("DEBUG : PUT URL IN ELSE :",put_url)
    print("DEBUG : Document type", type(document))
    print("DEBUG : Document", document)
    print("DEBUG : List of methods in document class : ", [m for m in dir(document) if not m.startswith('__')])
    import sys
    #sys.exit(0)
    #put_url=None
>>>>>>> 6c8e6af7bf09b9a506a50dddbe45d697ebcaedb1
    document.save()
    return headers, head_url, put_url


def create_and_process_document(
    filename, file_type, user, collection=None, content_type=None, put_expiration=1800
):
    if user.organization.type.slug == "apex" and collection is None:
        # TODO: raise a 400 error so that the frontend can show the error message.
        raise ValueError("The collection must be provided for APEX cases.")

    user.organization.validate_for_processing()
    logger.info("Starting document creation.", extra={"file_name": filename})
    logger.info("Starting document creation.", extra={"file_type":file_type})
    logger.info("Starting document creation.", extra={"user":user})
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("DEBUG : Before document variable declaration at ", current_time)
    document = Document.objects.create(
        name=filename,
        type=file_type,
        organization=user.organization,
        owner=user,
        creator=user,
        content_type=content_type,
        # TODO: We need to start creating this at a later point in time, when
        # we actually submit the request to AI Services.
        task=Task.objects.create(
            organization=user.organization,
            request={"task_type": Task.TYPE_EXTRACTION, "task_detail": "EXTRACTION"},
        ),
    )
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("PRINT DOCUMENT at time ",now)
    logger.info(document)

    # TODO: Eventually, we want to do this at the point it is needed (i.e.
    # further down the chain of logic) instead of defaulting it up this high.
    logger.info("PRINT COLLECTION")
    logger.info(collection)
    if collection is not None:
        document.collection_list.add(collection)
        document.save()

        if user.organization.type.slug == "apex":
            # Not sure why we are doing this...
            collection.schema = user.organization.schema_type
            collection.save()
            # TODO: This can introduce race condition bugs where mutliple
            # collection results will be created for the same collection.  This
            # needs to be addressed.
            cr, _ = CollectionResult.objects.get_or_create(collection=collection)
            cr.initialize_with_schema(user.organization)
            cr.save(process=False)

    headers, head_url, put_url = start_document_upload(
        document, put_expiration=put_expiration
    )
    #import sys
    #sys.exit(0)

    print(head_url)

    # Since ATOMIC_REQUESTS is True, the above database transactions will not
    # be committed until the response is rendered.  To avoid race conditions
    # in the subsequent task, watch_for_uploads, we need to trigger it as a
    # callback that occurs when the transaction is committed.
    if settings.CELERY_ENABLED:
        print("DEBUG : IN UTILS", document.pk)
        print("DEBUG : IN PUT URL",put_url)
        chain = (
            watch_for_uploads.s(document.pk, head_url)
            | set_file_size.s()
            | process_document.s()
        )
        transaction.on_commit(lambda: chain())

    #exit(0)
    return {
        "document": document,
        "put_url": put_url,
        "get_url": document.storage_path,
        "headers": headers,
    }

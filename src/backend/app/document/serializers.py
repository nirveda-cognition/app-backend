import os

from rest_framework import serializers

from backend.app.collection.models import Collection
from backend.app.common.serializers import (
    CollectionSimpleSerializer,
    DocumentSimpleSerializer,
)
from backend.app.task.serializers import TaskSerializer
from backend.conf.constants import HttpMethods
from backend.lib.rest_framework_utils.exceptions import InvalidFieldError

from .models import Document


class DocumentSerializer(DocumentSimpleSerializer):

    type = serializers.CharField(read_only=True)
    status_changed_at = serializers.DateTimeField(read_only=True)
    state = serializers.ChoiceField(
        choices=Document.STATES,
        read_only=True,
    )
    task = TaskSerializer(read_only=True)
    organization = serializers.PrimaryKeyRelatedField(read_only=True)
    schema = serializers.CharField(read_only=True)

    # TODO: Allow the owner to be updated on PATCH requests.
    owner = serializers.PrimaryKeyRelatedField(read_only=True)
    creator = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Document
        fields = DocumentSimpleSerializer.Meta.fields + (
            "type",
            "updated_at",
            "status_changed_at",
            "document_status",
            "task",
            "state",
            "schema",
            "organization",
            "owner",
            "creator",
        )
        http_toggle = {
            "collection_list": {
                HttpMethods.PATCH: (
                    serializers.PrimaryKeyRelatedField,
                    {"many": True, "queryset": Collection.objects.active()},
                )
            }
        }
        response = {"collection_list": (CollectionSimpleSerializer, {"many": True})}

    def validate(self, attrs):
        name = os.path.splitext(attrs["name"])[0].strip()
        ext = os.path.splitext(attrs["name"])[1].strip()
        # TODO: Validate that it is a supported extension.
        if ext == "" or ext == ".":
            raise InvalidFieldError(
                "name", message=("The name must include an extension.")
            )
        elif name == "":
            raise InvalidFieldError("name", message=("The name must not be blank."))
        else:
            # This validation method will only be triggered for PATCH requests
            # currently, since we are not using this serializer for POST
            # requests.  In the case of a PATCH request, the instance will
            # always be non null.
            # TODO: Should we maintain a unique document name across the
            # entire organization?  Instead of just in the collection?
            if self.instance is not None and self.instance.collection_list.count():
                # TODO: This logic needs to be built in.
                pass
            attrs.update(
                name="%s%s" % (name, ext),
                type=ext[1:],
            )
            return attrs


class UploadDocumentSerializer(serializers.Serializer):
    filename = serializers.CharField(required=True, allow_blank=False, allow_null=False)
    collection = serializers.PrimaryKeyRelatedField(
        queryset=Collection.objects.active(),
        required=False,
        allow_null=False,
    )

    def validate(self, attrs):
        name = os.path.splitext(attrs["filename"])[0].strip()
        ext = os.path.splitext(attrs["filename"])[1]
        if ext == "":
            raise InvalidFieldError(
                "filename", message=("The filename must include an extension.")
            )
        elif name == "":
            raise InvalidFieldError(
                "filename", message=("The filename must not be blank.")
            )
        else:
            # TODO: Should we maintain a unique document name across the
            # entire organization?  Instead of just in the collection?  Or
            # perhaps across all collections the document belongs to?
            if "collection" in attrs:
                document = (
                    attrs["collection"].documents.filter(name=attrs["filename"]).first()
                )
                if document is not None:
                    name = attrs["collection"].create_unique_document_name(name)  # noqa
            return {
                "collection": attrs.get("collection"),
                "name": "%s%s" % (name, ext),
                "ext": ext[1:],  # Remove preceeding period.
            }

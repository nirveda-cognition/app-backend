import json
import logging

import rest_framework_filters
from django.conf import settings
from django.contrib.auth import logout
from django.utils.decorators import method_decorator
from ratelimit.decorators import ratelimit
from rest_framework import filters, generics, mixins, response, views, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.request import Request
from two_factor import views as two_factor_views

from backend.app.custom_auth.models import CustomUser, SSOAccount
from backend.app.organization.mixins import OrganizationNestedMixin
from backend.app.user.serializers import SSOAccountSerializer, UserSerializer
from backend.lib.rest_framework_utils.exceptions import (
    InvalidFieldError,
    RequiredFieldError,
)
from backend.lib.rest_framework_utils.permissions import IsSuperuser

from .auth import AzureSSOAuthentication
from .exceptions import EmailNotConfirmedError, GroupPermissionError, RateLimitedError
from .models import NCGroup, ResetUID, Role
from .serializers import (
    GroupSerializer,
    LoginSerializer,
    ResetPasswordSerializer,
    RoleSerializer,
)
from .token_seriailziers import AzureSSOAuthorizeSerializer, AzureSSOTokenSerializer
from .utils import send_forgot_password_email

logger = logging.getLogger(__name__)


auth = AzureSSOAuthentication()


class RoleFilter(rest_framework_filters.FilterSet):
    class Meta:
        model = Role
        fields = {"name": ["iexact"]}


class GroupViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /groups/
    """

    lookup_field = "pk"
    search_fields = ("name",)
    ordering_fields = ["created_at", "status_changed_at", "email"]
    serializer_class = GroupSerializer
    permission_classes = [IsSuperuser]

    def get_queryset(self):
        return NCGroup.objects.all()


class OrganizationGroupViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    OrganizationNestedMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/<pk>/groups/
    (2) POST /organizations/<pk>/groups/
    (3) GET /organizations/<pk>/groups/<pk>/
    (4) PATCH /organizations/<pk>/groups/<pk>/
    """

    lookup_field = "pk"
    organization_lookup_field = ("pk", "organization_pk")
    filter_backends = [filters.OrderingFilter, filters.SearchFilter]
    search_fields = ("name",)
    ordering_fields = ["created_at", "status_changed_at", "email"]
    serializer_class = GroupSerializer

    def get_serializer_context(self):
        return {"organization": self.organization}

    def get_queryset(self):
        return self.organization.groups.all()

    def get_object(self):
        group = super().get_object()
        if not self.check_object_permissions(self.request, group):
            raise GroupPermissionError()
        return group

    def perform_update(self, serializer):
        serializer.save(organization=self.organization)

    def perform_create(self, serializer):
        serializer.save(organization=self.organization)


class RoleViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /authentication/roles/
    (2) GET /authentication/roles/<pk>/
    (2) POST /authentication/roles/
    """

    queryset = Role.objects.all()
    serializer_class = RoleSerializer

    def get_permissions(self):
        return (
            [IsSuperuser()]
            if self.request.method == "POST"
            else super().get_permissions()
        )


class TFLoginView(two_factor_views.LoginView):
    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form, **kwargs)
        context["BRAND"] = settings.BRAND
        return context


class LogoutView(views.APIView):
    def post(self, request, *args, **kwargs):
        logout(request)
        request.session.flush()
        return response.Response(status=201)


class LoginView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    authentication_classes = []
    permission_classes = []

    @method_decorator(ratelimit(key="user_or_ip", rate="3/s"))
    def post(self, request, *args, **kwargs):
        was_limited = getattr(request, "limited", False)
        if was_limited:
            raise RateLimitedError()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return response.Response(
            {
                "access_token": serializer.validated_data["access_token"],
                "refresh_token": serializer.validated_data["refresh_token"],
                # TODO: Use a DRF Package for Expanding Fields Dynamically
                "user": UserSerializer(
                    serializer.validated_data["user"], expand=["organization", "role"]
                ).data,
            },
            status=201,
        )


class ResetPasswordView(generics.GenericAPIView):
    serializer_class = ResetPasswordSerializer
    authentication_classes = []
    permission_classes = []

    @method_decorator(ratelimit(key="user_or_ip", rate="3/s"))
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data["user"]
        user.set_password(serializer.validated_data["password"])
        user.save()

        return response.Response(
            {
                "user": UserSerializer(user).data,
            },
            status=201,
        )


class ForgotPasswordView(generics.GenericAPIView):
    authentication_classes = []
    permission_classes = []

    @method_decorator(ratelimit(key="user_or_ip", rate="3/s"))
    def post(self, request, *args, **kwargs):
        if "email" not in request.data:
            raise RequiredFieldError("email")
        try:
            user = CustomUser.objects.get(email=request.data["email"])
        except CustomUser.DoesNotExist:
            raise InvalidFieldError(
                "email",
                message=("There is not a user associated with the provided " "email."),
            )
        else:
            if not user.is_active:
                raise EmailNotConfirmedError()

            reset_uid = ResetUID.objects.create(user=user)
            send_forgot_password_email(user, reset_uid.token)
            return response.Response(status=201)


class AzureSSOAuthorizeView(generics.GenericAPIView):
    """Azure SSO Authorization View

    Generic API View to construct a AAD tenant based authorization URL. This URL is
    generated using the `MSAL` library provided by Microsoft.

    For more info, checkout: [MS-IDP Docs](https://docs.microsoft.com/en-us/azure/active-directory/develop/)

    Flow:
        1. The client application calls this View
        2. This View constructs a new AAD authorization URL based on OAuth2 standards
        3. The client then redirects itself to this `auth_url`
        4. User consent is prompted for authentication via Azure SSO/Microsoft Identity Platform
        5. Post consent the user is then redirected to the
            a. The backend `v2/auth/azure/redirect/` where the `code` & `state`
            are passed as URL params via the MS-IDP (*This flow is used for Development/Testing for backend only*)
            b. In `staging/production`, the user is redirected to the client where the `code` & `state` are passed
            in the URL params.
        6. Whether the user is redirected to the backend or the client, in both scenarios, the said redirected app
        send the `code` & `state` to the `Code Auth` URL: `/v2/auth/azure/code/`.
        7. `AzureSSOCodeView` in the backend then fetches required `tokens` from MS-IDP/AAD SSO.
        8. The fetched `tokens/response` from MS-IDP is stored in the database in Table `Token` under the
        `authentication` django app.
        9. `AzureSSOCoddeView`, after successful creation on token data in the DB, sends these `tokens`
        to the client for authenticate & authorize the user.

    Args:
        generics ([type]): [description]

    Returns:
        [type]: [description]
    """

    permission_classes = (AllowAny,)
    serializer_class = AzureSSOAuthorizeSerializer

    def get(self, request: Request, *args, **kwargs):
        # If auth header is present means that the user is already authenticated,
        # validate the header & token & log the uesr in.
        if request.headers.get("Authorization"):
            user, token = auth.authenticate(request)
            user_serializer = UserSerializer(data=user)
            user_serializer.is_valid(raise_exception=True)

            token_serializer = AzureSSOTokenSerializer(data=token)
            token_serializer.is_valid(raise_exception=True)

            return response.Response(
                {
                    "user": user_serializer.validated_data,
                    "token": token_serializer.validated_data,
                }
            )
        # If the auth header is not present, then send the `auth_url`
        # and start the auth flow.
        auth_url = auth.authorize(request)
        serializer = self.get_serializer(data={"auth_url": auth_url})
        serializer.is_valid(raise_exception=True)

        print("ACCOUNTS: ", auth.get_backend(request).get_accounts())

        return response.Response(serializer.validated_data)


class AzureSSORedirectView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = AzureSSOTokenSerializer

    def perform_create(self, serializer):
        serializer.save()

    def get(self, request: Request, *args, **kwargs):

        data = auth.generate_token(request)
        sso_data = self.get_or_create_sso_account(data)
        data.update(
            {
                "is_active": True,
                "azure_json": json.dumps(data),
                "sso_account": sso_data["id"],
            }
        )

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return response.Response(serializer.data)

    def get_or_create_sso_account(self, data):
        from .utils import get_or_create_sso_account as gcsso

        sso_account: SSOAccount = gcsso(
            auth=auth,
            access_token=data["access_token"],
        )

        serializer = SSOAccountSerializer(sso_account)
        return serializer.data

    # def get(self, request: Request, *args, **kwargs):
    #     return response.Response(request.GET.dict())


class AzureSSOCodeView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = AzureSSOTokenSerializer

    def perform_create(self, serializer):
        serializer.save()

    def get_or_create_sso_account(self, data):
        from .utils import get_or_create_sso_account as gcsso

        sso_account: SSOAccount = gcsso(
            auth=auth,
            access_token=data["access_token"],
        )

        serializer = SSOAccountSerializer(sso_account)
        return serializer.data

    def post(
        self,
        request: Request,
        *args,
        **kwargs,
    ):
        params = None

        if request.data:
            params = {
                "code": request.data.get("code"),
                "state": request.data.get("state"),
            }

        print("CODE VIEW PARAMS", params)
        data = auth.generate_token(request, params)
        print("GENERATE TOKEN DATA: ", data)

        if data.get("error") == "invalid_grant":
            return response.Response(
                {
                    "message": "Auth Code already redeemed, use a Refresh Token to generate a new access token"
                },
                status=400,
            )

        sso_data = self.get_or_create_sso_account(data)
        data.update(
            {
                "is_active": True,
                "azure_json": json.dumps(data),
                "sso_account": sso_data["id"],
            }
        )

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return response.Response(serializer.data)


class AzureTokenRefreshView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):

        return response.Response(auth.refresh_access_token())

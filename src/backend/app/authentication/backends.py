import logging
import uuid
from typing import Dict, Optional

from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from msal import ConfidentialClientApplication, SerializableTokenCache
from rest_framework.exceptions import APIException
from rest_framework.request import Request

logger = logging.getLogger(__name__)
AZURE_AD_CONFIG = settings.AZURE_AD_CONFIG


class AzureModelBackend(ModelBackend):
    session_key: str = AZURE_AD_CONFIG.get("SESSION_KEY")
    azure_org = AZURE_AD_CONFIG.get("ORG")
    azure_client_id = AZURE_AD_CONFIG.get("CLIENT_ID")
    azure_client_secret = AZURE_AD_CONFIG.get("CLIENT_SECRET")
    azure_scopes = AZURE_AD_CONFIG.get("SCOPES")
    azure_redirect_uri = AZURE_AD_CONFIG.get("REDIRECT_URI")
    azure_tenant_id = AZURE_AD_CONFIG.get("TENANT_ID")
    client: Optional[ConfidentialClientApplication] = None
    has_changed: bool = True

    _azure_cache: Optional[SerializableTokenCache] = None
    _state: Optional[str] = None

    def __init__(self, request: Request = None) -> None:
        self.request: Request = request
        self._session: Request.session = request.session if self.request else None
        super().__init__()

    @property
    def session(self) -> None:
        return self._session

    @session.setter
    def session(self, value):
        self._session = value
        self.has_changed = True

    def _verify_session(self):
        azure_session: str = self.session.get(self.session_key)
        if azure_session != str(dict()):
            print(
                "AZURE SESSION, present: ", self.session.get(self.session_key, dict())
            )
            return True
        print("AZURE SESSION empty: ", self.session.get(self.session_key, dict()))
        return False

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value: str):
        self._state = value
        self.has_changed = True

    def _create_and_store_state_in_session(self):
        state = str(uuid.uuid4())
        self.state = state
        self.session[f"{self.session_key}_state"] = self.state

    def _verify_state(self, req_params: dict) -> None:
        state = req_params.get("state", None)
        # reject states that don't match
        if state is None or self.session[f"{self.session_key}_state"] != state:
            raise APIException(
                "Failed to match request state with session state", code=400
            )
        self.state = None

    @property
    def azure_cache(self):
        cache = SerializableTokenCache()
        if self._azure_cache:
            cache.deserialize(self._azure_cache)

        return cache

    @azure_cache.setter
    def azure_cache(self, value: SerializableTokenCache):
        if value.has_state_changed:
            self._azure_cache = value.serialize()
            self.has_changed = True

    def _get_client(self, use_cache: bool = False):
        if not isinstance(self.client, ConfidentialClientApplication):
            if use_cache:
                return ConfidentialClientApplication(
                    client_id=self.azure_client_id,
                    client_credential=self.azure_client_secret,
                    token_cache=self.azure_cache,
                    authority=f"https://login.microsoftonline.com/{self.azure_tenant_id}/",
                )
            else:
                return ConfidentialClientApplication(
                    client_id=self.azure_client_id,
                    client_credential=self.azure_client_secret,
                    token_cache=None,
                    authority=f"https://login.microsoftonline.com/{self.azure_tenant_id}/",
                )

    def get_auth_url(self):
        if AZURE_AD_CONFIG:
            logger.debug(f"REQUEST DATA {self.request}")
            print(f"{AZURE_AD_CONFIG} Azure AD login started -- {self.azure_org}")
        else:
            raise APIException(
                "Authentication not configured. Please contact Admin of organization",
                code=404,
            )

        auth_url = self._get_client().get_authorization_request_url(
            scopes=self.azure_scopes,
            redirect_uri=self.azure_redirect_uri,
            state=self.state,
        )
        print("AUTH_URL", auth_url)
        return auth_url

    def get_request_params_as_dict(self) -> dict:
        try:
            print("DEBUG : In get_request_params_as_dict try block")
            if self.request.method == "GET":
                print("DEBUG : In get_request_params_as_dict if method is GET")
                return self.request.GET.dict()
            elif self.request.method == "POST":
                print("DEBUG : In get_request_params_as_dict if method is POST")
                return self.request.POST.dict()
            else:
                raise APIException("Django request must be POST or GET", code=400)
        except Exception as e:
            print("DEBUG : In get_request_params_as_dict except block")
            if logger is not None:
                logger.warning(
                    "Failed to get param dict, substituting empty dict instead"
                )
                logger.warning(e)
        return dict()

    def generate_token_from_code(self, req_params: Dict):
        client = self._get_client(use_cache=True)
        azure_credentials = client.acquire_token_by_authorization_code(
            code=req_params.get("code"),
            scopes=self.azure_scopes,
            redirect_uri=self.azure_redirect_uri,
        )

        print("ACCESS CACHE ", self.azure_cache)
        self.get_accounts()
        return azure_credentials

    def acquire_token_silent_with_errors(self):
        client = self._get_client(use_cache=True)

        print("ACCOUNTS:    ", self.get_accounts())

        return client.acquire_token_silent_with_error(
            scopes=self.azure_scopes,
            authority=f"https://login.microsoftonline.com/{self.azure_tenant_id}/",
            force_refresh=True,
        )

    def get_new_token_with_refresh_token(self, refresh_token: str = None):
        client = self._get_client(use_cache=True)
        return client.acquire_token_by_refresh_token()

    def get_accounts(self):
        return self._get_client(use_cache=True).get_accounts()

    def reset_session_and_cache(self, request: Request):
        request.session.flush()
        # print("PRE", self._azure_cache)
        self._azure_cache = None
        # print("POST", self._azure_cache)
        # print("PRE", self._state)
        self._state = None
        # print("POST", self._state)
        return True

from requests import Session
from requests.auth import AuthBase


class MSGraphAuth(AuthBase):
    """
    Attaches token authentication to a requests.Request() instance for the
    MS Graph API.

    Parameters:
    ----------
    access_token: :obj:`str` (optional)
        The MS Graph `access_token`.
        If not provided, raies Error.
    """

    def __init__(self, access_token: str = None):
        self._access_token = access_token

    def __call__(self, req):
        req.headers["Authorization"] = self.access_token
        return req

    @property
    def access_token(self):
        return self._access_token


def create_session(access_token: str):
    with Session() as session:
        session.auth = MSGraphAuth(access_token)
    return session


def get_user_profile(access_token: str):
    response = create_session(access_token).get(
        url="https://graph.microsoft.com/v1.0/me"
    )
    response.raise_for_status()
    if response.json().get("message") == "Invalid version":
        raise Exception("Graph APi failed.")
    return response.json()

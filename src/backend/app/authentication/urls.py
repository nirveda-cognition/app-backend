from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import token_refresh

from backend.app.user.urls import group_router as group_users_router

from .views import (
    AzureSSOAuthorizeView,
    AzureSSOCodeView,
    AzureSSORedirectView,
    AzureTokenRefreshView,
    ForgotPasswordView,
    GroupViewSet,
    LoginView,
    LogoutView,
    OrganizationGroupViewSet,
    ResetPasswordView,
    RoleViewSet,
)

app_name = "authentication"

organization_group_router = routers.SimpleRouter()
organization_group_router.register(r"", OrganizationGroupViewSet, basename="group")

organization_urlpatterns = organization_group_router.urls + [
    path(
        "<int:group_pk>/", include([path("users/", include(group_users_router.urls))])
    ),
]

role_router = routers.SimpleRouter()
role_router.register(r"roles", RoleViewSet, basename="role")

urlpatterns = role_router.urls + [
    path("login/", LoginView.as_view()),
    path("logout/", LogoutView.as_view()),
    path("reset-password/", ResetPasswordView.as_view()),
    path("forgot-password/", ForgotPasswordView.as_view()),
    # This is currently not being used by the frontend.
    path("refresh-token/", token_refresh, name="refresh_token"),
    path("groups/", GroupViewSet.as_view({"get": "list"})),
    # Azure AD
    path("azure/authorize/", AzureSSOAuthorizeView.as_view()),
    path("azure/redirect/", AzureSSORedirectView.as_view()),
    path("azure/token/", AzureSSOCodeView.as_view()),
    path("azure/token/refresh/", AzureTokenRefreshView.as_view()),
]

import logging
from datetime import timedelta

from axes.decorators import axes_dispatch
from django.conf import settings
from django.contrib.auth import authenticate
from django.utils import timezone
from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken

from backend.app.user.models import CustomUser
from backend.lib.rest_framework_utils.exceptions import InvalidFieldError
from backend.lib.rest_framework_utils.serializers import EnhancedModelSerializer

from .exceptions import (
    AccountDisabledError,
    EmailDoesNotExist,
    EmailNotConfirmedError,
    InvalidCredentialsError,
    InvalidToken,
    PasswordResetLinkExpiredError,
    PasswordResetLinkUsedError,
)
from .models import NCGroup, ResetUID, Role

logger = logging.getLogger("backend")


class RoleSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, allow_blank=False, allow_null=False)
    description = serializers.CharField(
        required=False, allow_blank=False, allow_null=False
    )
    code = serializers.IntegerField(required=True, allow_null=False)

    class Meta:
        model = Role
        fields = ("id", "name", "description", "code")


class GroupSerializer(EnhancedModelSerializer):
    name = serializers.CharField(allow_blank=False, allow_null=False)
    description = serializers.CharField(
        allow_blank=False, allow_null=False, required=False
    )
    organization = serializers.PrimaryKeyRelatedField(read_only=True)
    users = serializers.PrimaryKeyRelatedField(
        queryset=CustomUser.objects.all(), required=False, many=True
    )
    roles = RoleSerializer(many=True, read_only=True)

    class Meta:
        model = NCGroup
        nested_fields = ("id", "name", "description")
        fields = nested_fields + ("organization", "users", "roles")
        http_toggle = {
            "users": {
                "GET": (
                    "backend.app.user.serializers.UserSerializer",
                    {"nested": True, "many": True},
                )
            },
            "organization": {
                "GET": (
                    "backend.app.organization.serializers.OrganizationSerializer",  # noqa
                    {"nested": True},
                )
            },
        }
        response = {
            "users": (
                "backend.app.user.serializers.UserSerializer",
                {"nested": True, "many": True},
            ),
            "organization": (
                "backend.app.organization.serializers.OrganizationSerializer",
                {"nested": True},
            ),
        }

    def validate_name(self, value):
        organization = self.context["organization"]
        validator = serializers.UniqueTogetherValidator(
            queryset=NCGroup.objects.filter(organization=organization),
            fields=("name",),
        )
        validator({"name": value, "organization": organization}, self)
        return value

    def validate(self, attrs):
        organization = self.context["organization"]
        invalid_users = []
        for user in attrs.get("users", []):
            if user.organization != organization:
                invalid_users.append(user.pk)
        if invalid_users:
            raise InvalidFieldError(
                "users",
                message=(
                    "The users %s do not belong to the group's organization."
                    % ", ".join(["%s" % pk for pk in invalid_users])
                ),
            )
        return attrs


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, allow_blank=False, allow_null=False)
    password = serializers.CharField(required=True, allow_blank=False, allow_null=False)

    @axes_dispatch
    class Meta:
        validators = []

    def validate_email(self, email):
        try:
            CustomUser.objects.get(email=email)
        except CustomUser.DoesNotExist:
            raise EmailDoesNotExist()
        return email

    def validate(self, attrs):
        user = authenticate(password=attrs["password"], email=attrs["email"])
        if user is None:
            raise InvalidCredentialsError()
        elif not user.is_active:
            # TODO: We should make sure this matches the exception
            # thrown by rest_framework_simplejwt.JWTAuthentication.
            raise AccountDisabledError()
        # TODO: Eventually, the user's organization will be required.
        elif user.organization is not None and not user.organization.is_active:
            raise AccountDisabledError()

        refresh = RefreshToken.for_user(user)
        return {
            "user": user,
            "access_token": str(refresh.access_token),
            "refresh_token": str(refresh),
        }


class ResetPasswordSerializer(serializers.Serializer):
    token = serializers.CharField(required=True, allow_blank=False, allow_null=False)
    password = serializers.CharField(required=True, allow_blank=False, allow_null=False)
    confirm = serializers.CharField(required=True, allow_blank=False, allow_null=False)

    def validate(self, attrs):
        try:
            reset_uid = ResetUID.objects.get(token=attrs["token"])
        except ResetUID.DoesNotExist:
            raise InvalidToken()
        else:
            if attrs["confirm"] != attrs["password"]:
                raise InvalidFieldError(
                    "confirm", message="The passwords do not match."
                )
            elif reset_uid.used:
                raise PasswordResetLinkUsedError()
            elif not reset_uid.user.is_active:
                raise EmailNotConfirmedError()
            else:
                expiry_time = timezone.now() - timedelta(
                    minutes=60 * settings.PWD_RESET_LINK_EXPIRY_TIME_IN_HRS
                )
                if not timezone.is_aware(expiry_time):
                    expiry_time = timezone.make_aware(expiry_time)

                if reset_uid.created_at < expiry_time:
                    raise PasswordResetLinkExpiredError()

                reset_uid.used = True
                reset_uid.save()

                return {"user": reset_uid.user, "password": attrs["password"]}

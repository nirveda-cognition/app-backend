from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from backend.app.organization.mixins import OrganizationNestedMixin

from .exceptions import GroupPermissionError


class GroupNestedMixin(OrganizationNestedMixin):
    """
    A mixin for views that extend off of an organization's group detail
    endpoint.
    """

    @property
    def group_lookup_field(self):
        raise NotImplementedError()

    @cached_property
    def group(self):
        params = {self.group_lookup_field[0]: (self.kwargs[self.group_lookup_field[1]])}
        obj = get_object_or_404(self.organization.groups.all(), **params)
        if not self.request.user.has_group_permission(obj):
            raise GroupPermissionError()
        return obj

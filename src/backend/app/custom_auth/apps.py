from django.apps import AppConfig


class CustomAuthConfig(AppConfig):
    name = "backend.app.custom_auth"

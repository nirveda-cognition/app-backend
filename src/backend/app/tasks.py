import os

import requests
from celery import shared_task
from django.conf import settings

# test deployment


@shared_task(serializer="json")
def health_check():
    url = os.path.join(settings.APP_URL, "hcheck")
    try:
        requests.get(url, headers={"Accept": "application/json"})
        return True
    except requests.exceptions.RequestException:
        return False

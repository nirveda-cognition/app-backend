from django.urls import path

from backend.app.nc_core.views import (
    ModifyColumnsView,
    ResultUpdateView,
    get_document_tables,
    modify_table,
    normalize_document_tables,
    update_multiple_tables,
    upload,
)
app_name = "nc_core"

print("DEBUG : In url file of nc_core")
urlpatterns = [
    path(
        "documents/<int:pk>/update_data/",
        ResultUpdateView.as_view(),
        name="data-update",
    ),
    path("modify-table/<int:pk>/", modify_table),
    path("modify-columns/", ModifyColumnsView.as_view()),
    path("update-multiple-tables/", update_multiple_tables),
    path("document-tables/", get_document_tables),
    path("normalized-document-tables/", normalize_document_tables),
    path("upload/<str:key>/", upload),
]

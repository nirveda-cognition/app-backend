import copy
import logging
import uuid

from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver

from backend.app.collection.models import (
    Collection,
    CollectionPointer,
    CollectionResult,
    TrackedOperation,
)
from backend.app.organization.exceptions import OrganizationNotConfiguredError
from backend.app.task.models import TaskResult
from backend.lib.django_utils.apps import find_model_by_name

from .helpers import (
    diff_and_update_collection_result,
    diff_collections,
    filter_pointers,
    format_result,
    get_changed_keys,
    get_feature_dict,
    get_schema,
    get_sub_collections,
    process_collections,
    process_keys_changed,
    write_to_tracked_operations,
)

logger = logging.getLogger("backend")


def get_parent_from_child(instance):
    CHILD_TO_PARENT_MAP = {
        "TaskResult": "document",
    }
    if instance.__class__.__name__ in CHILD_TO_PARENT_MAP:
        return getattr(instance, CHILD_TO_PARENT_MAP[instance.__class__.__name__])
    return None


@receiver(post_save, sender=TaskResult)
def check_for_tracked_operation(instance, **kwargs):
    """
    This function screens for tracked operations. To connect a new data source,
    Add a receiver, as defined above.

    We screen the data source update for any collections with schema matching
    the receiver property of the data source schema.

    If collections are determined and we have a valid operation and user,
    we proceed to write to the tracked operations table.

    Note that document movement (into new collections) or removal from
    collections is also handled here (using collection pointers to facilitate
    tracking).
    """
    operation = getattr(instance, "_operation", None)
    user = getattr(instance, "_user", None)
    if not (operation and user):
        return

    # TODO: This is hideous - this needs to be rethought.
    target = get_parent_from_child(instance) or instance

    if (
        instance.__class__.__name__ == "TaskResult"
        and user.organization.type.slug not in ["alfa", "apex"]
    ):
        logger.info(
            "Not checking for tracked operation - user not in right " "organization."
        )
        return

    try:
        all_schema = user.organization.get_schema()
    except OrganizationNotConfiguredError:
        logger.info(
            "Not checking for tracked operation - schema is not configured "
            "for organization %s." % user.organization.name
        )
        return

    if not (hasattr(target, "schema") and target.schema):
        schema = get_schema(target)
        if not schema:
            return

        target.schema = schema
        target.save()

    if target.schema == "apex":
        schema_list = [
            "purchase_order",
            "invoice",
            "receiver",
            "change_order",
            "packing_slip",
        ]
    else:
        schema_list = [target.schema]

    for schm in schema_list:
        schema = all_schema.get(schm)
        if not schema:
            logger.error("Target schema %s does not exist." % target.schema)
            return

        receivers = schema["receivers"]
        if not receivers:
            logger.info("There are no receivers in the schema.")
            return

        if not hasattr(instance, "_collections") and not hasattr(
            instance, "collection_list"
        ):
            return

        if hasattr(instance, "_collections"):
            collections = []
            for collection in instance._collections:
                if isinstance(collection, Collection):
                    collections.append(collection)
                else:
                    collections.append(Collection.objects.get(pk=collection))
            instance._collections = collections
        else:
            collections = list(target.collection_list.all())

        if operation == TrackedOperation.MOVE:
            move_from, move_to = instance._collections
            collections.remove(instance._collections[0])
            from_collections = diff_collections([move_from], collections)
            to_collections = diff_collections([move_to], collections)
            from_collections_filtered = list(from_collections - to_collections)
            to_collections_filtered = list(to_collections - from_collections)

            if len(from_collections_filtered) != 0:
                # TODO: THIS IS A HUGE BUG - We have to pass in the schema
                # as the last argument otherwise this will fail!!!
                write_to_tracked_operations(
                    instance.id,
                    instance.__class__.__name__,
                    TrackedOperation.MOVE_FROM,
                    from_collections_filtered,
                    user,
                )

            if len(to_collections_filtered) != 0:
                # TODO: THIS IS A HUGE BUG - We have to pass in the schema
                # as the last argument otherwise this will fail!!!
                write_to_tracked_operations(
                    instance.id,
                    instance.__class__.__name__,
                    TrackedOperation.MOVE_TO,
                    to_collections_filtered,
                    user,
                )

        elif operation == TrackedOperation.MOVE_TO:
            # The target_collections do not include the new collection
            # (move to) at this point, so no need to remove.
            to_collection = instance._collections[0]
            sub_cols = get_sub_collections(to_collection)
            sub_cols.add(to_collection)

            filtered_cols = [c for c in collections if c not in sub_cols]
            to_collections = diff_collections(instance._collections, filtered_cols)

            if len(to_collections) != 0:
                # TODO: THIS IS A HUGE BUG - We have to pass in the schema
                # as the last argument otherwise this will fail!!!
                write_to_tracked_operations(
                    instance.id,
                    instance.__class__.__name__,
                    TrackedOperation.MOVE_TO,
                    to_collections,
                    user,
                )
        elif operation == TrackedOperation.MOVE_FROM:
            from_collections = diff_collections(instance._collections, collections)

            if len(from_collections) != 0:
                # TODO: THIS IS A HUGE BUG - We have to pass in the schema
                # as the last argument otherwise this will fail!!!
                write_to_tracked_operations(
                    instance.id,
                    instance.__class__.__name__,
                    TrackedOperation.MOVE_FROM,
                    from_collections,
                    user,
                )

        elif operation in (
            TrackedOperation.UPDATE,
            TrackedOperation.DELETE,
            TrackedOperation.NEW,
        ):
            collections_to_process = list(process_collections(collections))
            if len(collections_to_process) != 0:
                write_to_tracked_operations(
                    instance.id,
                    instance.__class__.__name__,
                    operation,
                    collections_to_process,
                    user,
                    schm,
                )


@receiver(post_save, sender=CollectionResult)
def process_cr_update(instance, **kwargs):
    if getattr(instance, "_skip_processing", False) is True:
        return

    if not hasattr(instance, "_result") and not hasattr(instance, "_keys_changed"):
        return

    if hasattr(instance, "_keys_changed"):
        keys_changed = {instance.collection.schema: instance._keys_changed}

    else:
        prev_result = instance._result["collection_data"]
        curr_result = instance.result["collection_data"]

        key_diff = get_changed_keys(prev_result, curr_result)
        keys_changed = {instance.collection.schema: set(key_diff)}
        logger.info("Saving CollectionResult - Keys changed: %s" % keys_changed)

    # TODO Pass 'parent_uuid' in the function argument.
    process_keys_changed(keys_changed, instance.uuid, result=True)

    if instance.collection.organization.type.slug != "apex":
        format_result(instance)
        instance.save(process=False)


@receiver(post_save, sender=TrackedOperation)
def process_tracked_operation(instance, **kwargs):
    """
    Qualified data model operations get processed here. At this point,
    we know we have a data source which is being tracked by an affected
    collection which has been updated (either new/update/move).

    The function will attempt to retrieve the new result (get_feature_dict)
    and compare that with the previous (diff_and_update_collection_result).
    """
    if (
        not hasattr(instance, "_collections")
        or getattr(instance, "_schema", None) is None
    ):
        return

    collections = instance._collections
    if len(collections) == 0:
        return

    if instance.operation == "Move To":
        instance.operation = TrackedOperation.NEW

    if instance.operation == "Move From":
        instance.operation = TrackedOperation.DELETE

    # TODO: Use a ContentTypeField for instance.model
    result_model = find_model_by_name(instance.model)
    try:
        all_schema = instance.user.organization.get_schema()
    except OrganizationNotConfiguredError:
        logger.info(
            "Schema not configured for organization %s, skipping tracked "
            "operation processing." % instance.user.organization.name
        )
        return
    else:
        result_instance = result_model.objects.get(id=instance.uid)
        parent_instance = (
            get_parent_from_child(result_instance) or result_instance
        )  # noqa

        collection_results = []
        for collection in collections:
            cr, _ = CollectionResult.objects.get_or_create(
                collection=collection, defaults={"uuid": uuid.uuid4()}
            )
            collection_results.append(cr)

        new_key_values = get_feature_dict(
            result=result_instance,
            operation=instance.operation,
            organization=instance.user.organization,
            schema=instance._schema,
        )

        keys_changed_list = []
        for collection_result in collection_results:
            # TODO: This is a HUGE bug - we are allowing schema = None to pass
            # through which is causing this to fail!
            keys_changed_list.append(
                (
                    diff_and_update_collection_result(
                        collection_result,
                        parent_instance,
                        new_key_values,
                        instance.operation,
                        instance._schema,
                    ),
                    collection_result.uuid,
                )
            )

        singular = bool(all_schema[instance._schema].get("singular"))
        for keys_changed in keys_changed_list:
            key_diff_dict, result_uuid = keys_changed
            parent_uuid = None
            if not singular:
                parent_uuid = parent_instance.uuid
            process_keys_changed(key_diff_dict, result_uuid, False, parent_uuid)


@receiver(m2m_changed, sender=Collection.collections.through)
def process_collection(sender, instance, action, reverse, model, pk_set, **kwargs):
    """
    This function handles the following:
    1) Addition or removal of collections to a collection pointer
    2) Movement of collections and corresponding data model updates
    2) Collection pointer head creation
    """
    if action not in ("post_add", "post_remove"):
        return

    # THIS IS A TEMPORARY PATCH - The entire TrackedOperation nonsense is
    # complete bull and is causing a lot of bugs to happen if we allow it to
    # process.  It needs to be fixed, or better yet - removed, and until then
    # we will have to manually prevent saving of M2M's to avoid bugs.
    elif getattr(instance, "_track", True) is False:
        return

    if action == "post_remove":
        if not len(pk_set):
            return
        to_collection = Collection.objects.get(id=pk_set.pop())
        from_collection = instance
        pointers = set(to_collection.collectionpointer_set.all())
        pointers = filter_pointers(from_collection, pointers)
        if not to_collection.schema and not len(pointers):
            return

        if to_collection.schema:
            cp = CollectionPointer.objects.filter(collection_head=to_collection.id)
            if cp.count():
                pointers.add(cp[0])
            else:
                CollectionPointer.objects.create(collection_head=to_collection.id)

        from_subs = get_sub_collections(from_collection)
        all_collections = copy.deepcopy(from_subs)
        all_collections.add(from_collection)

        for pointer in pointers:
            pointer.collections.remove(from_collection)

        for sub in from_subs:
            pointers = filter_pointers(sub, pointers, sub_col=all_collections)
            for pointer in pointers:
                pointer.collections.remove(sub)

        docs = set(list(from_collection.documents.all()))
        for sub in from_subs:
            docs.update(list(sub.documents.all()))
        for doc in docs:
            doc._collections = [to_collection.id]
            doc._operation = TrackedOperation.MOVE_FROM
            doc._user = doc.owner  # update
            check_for_tracked_operation(doc)

    if action == "post_add":
        from_collection = instance.collections.through.objects.last().from_collection
        to_collection = instance.collections.through.objects.last().to_collection

        pointers = set(to_collection.collectionpointer_set.all())

        if not to_collection.schema and not len(pointers):
            return

        if to_collection.schema:
            cp = CollectionPointer.objects.filter(collection_head=to_collection.id)
            if cp.count():
                cp = cp[0]
            else:
                cp = CollectionPointer.objects.create(collection_head=to_collection.id)
            pointers.add(cp)

        from_subs = get_sub_collections(from_collection)
        for pointer in pointers:
            pointer.collections.add(from_collection.id)

            for sub in from_subs:
                pointer.collections.add(sub)

        docs = set(list(from_collection.documents.all()))
        for sub in from_subs:
            docs.update(list(sub.documents.all()))

        for doc in docs:
            doc._collections = [to_collection.id]
            doc._operation = TrackedOperation.MOVE_TO
            doc._user = doc.owner
            check_for_tracked_operation(doc)

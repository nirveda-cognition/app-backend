from backend.app.collection.models import CollectionResult
from backend.app.nc_core.business_logic.ALFA.extractions import (
    extract_account_data,
    extract_gusto_ppp_report,
)
from backend.app.nc_core.business_logic.APEX.extractions import (
    extract_change_order,
    extract_invoice,
    extract_packing_slip,
    extract_purchase_order,
    extract_receiver,
)


def direct_update(result_uuid, key, schema, parent_uuid=None):
    collection_result = CollectionResult.objects.get(uuid=result_uuid)
    collection_result.result.setdefault("collection_data", {})

    if (
        parent_uuid
        and collection_result.result["collection_data"].get(key, None) is not None
    ):
        collection_result.result["collection_data"][key] = collection_result.result[
            "child_data"
        ][schema][parent_uuid][key]
    else:
        if collection_result.result["child_data"].get(
            schema, None
        ) is not None and key in collection_result.result["child_data"].get(
            schema, None
        ):
            collection_result.result["collection_data"][key] = collection_result.result[
                "child_data"
            ][schema][key]

    collection_result.save()


all_extractions = {
    "alfa": {
        "extract_account_data": extract_account_data,
        "extract_gusto_ppp_report": extract_gusto_ppp_report,
    },
    "apex": {
        "extract_purchase_order": extract_purchase_order,
        "extract_invoice": extract_invoice,
        "extract_receiver": extract_receiver,
        "extract_change_order": extract_change_order,
        "extract_packing_slip": extract_packing_slip,
    },
}

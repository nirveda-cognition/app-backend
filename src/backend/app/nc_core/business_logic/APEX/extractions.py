def extract_data(data, doc_type):
    feature_dict = {}
    for item in data:
        obj_list = list(
            filter(
                lambda x: (x["property_id"] == "document_type")
                and (x["value"] == doc_type),
                item["items"],
            )
        )
        if len(obj_list) and obj_list[0].get("value", "") == doc_type:
            field_list = list(
                filter(lambda x: x["property_id"] != "document_type", item["items"])
            )
            for field in field_list:
                feature_dict[field["property_id"]] = field["value"]

    return feature_dict


def extract_purchase_order(result):
    result_data = result.new_data["data"]["extracted_data"]
    return extract_data(result_data, "purchase_order")


def extract_invoice(result):
    result_data = result.new_data["data"]["extracted_data"]
    return extract_data(result_data, "invoice")


def extract_receiver(result):
    result_data = result.new_data["data"]["extracted_data"]
    return extract_data(result_data, "receiver")


def extract_change_order(result):
    result_data = result.new_data["data"]["extracted_data"]
    return extract_data(result_data, "change_order")


def extract_packing_slip(result):
    result_data = result.new_data["data"]["extracted_data"]
    return extract_data(result_data, "ship_ticket")

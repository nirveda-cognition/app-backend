def collect_by_key(target_results, key):
    all_items = []
    for item in target_results.keys():
        all_items.append(target_results[item][key])
    return all_items


def total(items):
    totals = 0
    for item in items:
        if type(item) == str:
            item = item.replace(",", "")
            item = float(item)
        if type(item) in (int, float):
            totals += item
    return totals


# TODO: Snake case.
def parseNumericalField(field):
    if not field:
        return 0
    field = str(field)
    try:
        return float(field.replace(",", "").replace("$", ""))
    except ValueError:
        return 0

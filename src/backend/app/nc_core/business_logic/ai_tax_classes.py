AI_TAX_CLASSES = {
    "3d optical profiler system": {
        "value": 1,
        "label": "3D Optical Profiler System",
        "ml_label": "3d optical profiler system",
        "roc": "1 yr",
    },  # noqa  # noqa
    "active network hardware": {
        "value": 2,
        "label": "Active Network Hardware",
        "ml_label": "active network hardware",
        "roc": "1 yr",
    },  # noqa  # noqa
    "advanced driver-assistance system": {
        "value": 3,
        "label": "Advanced Driver-Assistance System",
        "ml_label": "advanced driver-assistance system",
        "roc": "1 yr",
    },  # noqa  # noqa
    "aerotors": {
        "value": 4,
        "label": "Aerotors",
        "ml_label": "aerotors",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "air conditioning system": {
        "value": 5,
        "label": "Air Conditioning System",
        "ml_label": "air conditioning system",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "air respirator": {
        "value": 6,
        "label": "Air Respirator",
        "ml_label": "air respirator",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "air shower": {
        "value": 7,
        "label": "Air Shower",
        "ml_label": "air shower",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "air supply equipment": {
        "value": 8,
        "label": "Air Supply Equipment",
        "ml_label": "air supply equipment",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "aircraft computer system": {
        "value": 9,
        "label": "Aircraft Computer System",
        "ml_label": "aircraft computer system",
        "roc": "1 yr",
    },  # noqa  # noqa
    "aircraft equipment": {
        "value": 10,
        "label": "Aircraft Equipment",
        "ml_label": "aircraft equipment",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "aircraft maintenance system": {
        "value": 11,
        "label": "Aircraft Maintenance System",
        "ml_label": "aircraft maintenance system",
        "roc": "1 yr",
    },  # noqa  # noqa
    "aircraft navigation system": {
        "value": 12,
        "label": "Aircraft Navigation System",
        "ml_label": "aircraft navigation system",
        "roc": "1 yr",
    },  # noqa  # noqa
    "aircraft servicing equipment": {
        "value": 13,
        "label": "Aircraft Servicing Equipment",
        "ml_label": "aircraft servicing equipment",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "alarm system with software": {
        "value": 14,
        "label": "Alarm System With Software",
        "ml_label": "alarm system with software",
        "roc": "1 yr",
    },  # noqa  # noqa
    "ambulance": {
        "value": 15,
        "label": "Ambulance",
        "ml_label": "ambulance",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "analyser": {
        "value": 16,
        "label": "Analyser",
        "ml_label": "analyser",
        "roc": "1 yr",
    },  # noqa  # noqa
    "angiography system": {
        "value": 17,
        "label": "Angiography System",
        "ml_label": "angiography system",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "annunciator programmable display": {
        "value": 18,
        "label": "Annunciator Programmable Display",
        "ml_label": "annunciator programmable display",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "anti-counterfeit pen": {
        "value": 19,
        "label": "Anti-Counterfeit Pen",
        "ml_label": "anti-counterfeit pen",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "assembly platform": {
        "value": 20,
        "label": "Assembly Platform",
        "ml_label": "assembly platform",
        "roc": "1 yr",
    },  # noqa  # noqa
    "assembly support": {
        "value": 21,
        "label": "Assembly Support",
        "ml_label": "assembly support",
        "roc": "3 yrs",
    },  # noqa  # noqa
    "asset tracker": {
        "value": 22,
        "label": "Asset Tracker",
        "ml_label": "asset tracker",
        "roc": "1 yr",
    },  # noqa  # noqa
    "audio accessories": {
        "value": 23,
        "label": "Audio Accessories",
        "ml_label": "audio accessories",
        "roc": "3 yrs",
    },  # noqa
    "audio equipment": {
        "value": 24,
        "label": "Audio Equipment",
        "ml_label": "audio equipment",
        "roc": "3 yrs",
    },  # noqa
    "audio visual equipment": {
        "value": 25,
        "label": "Audio Visual Equipment",
        "ml_label": "audio visual equipment",
        "roc": "3 yrs",
    },  # noqa
    "auto refractometer": {
        "value": 26,
        "label": "Auto Refractometer",
        "ml_label": "auto refractometer",
        "roc": "1 yr",
    },  # noqa
    "autoclave machine": {
        "value": 27,
        "label": "Autoclave Machine",
        "ml_label": "autoclave machine",
        "roc": "3 yrs",
    },  # noqa
    "automated carpark system": {
        "value": 28,
        "label": "Automated Carpark System",
        "ml_label": "automated carpark system",
        "roc": "1 yr",
    },  # noqa
    "automated diagnosis code": {
        "value": 29,
        "label": "Automated Diagnosis Code",
        "ml_label": "automated diagnosis code",
        "roc": "1 yr",
    },  # noqa
    "automated entrance barrier": {
        "value": 30,
        "label": "Automated Entrance Barrier",
        "ml_label": "automated entrance barrier",
        "roc": "1 yr",
    },  # noqa
    "automated equipment": {
        "value": 31,
        "label": "Automated Equipment",
        "ml_label": "automated equipment",
        "roc": "1 yr",
    },  # noqa
    "automated guided vehicle": {
        "value": 32,
        "label": "Automated Guided Vehicle",
        "ml_label": "automated guided vehicle",
        "roc": "1 yr",
    },  # noqa
    "automated machine": {
        "value": 33,
        "label": "Automated Machine",
        "ml_label": "automated machine",
        "roc": "1 yr",
    },  # noqa
    "automated main cell processing unit": {
        "value": 34,
        "label": "Automated Main Cell Processing Unit",
        "ml_label": "automated main cell processing unit",
        "roc": "1 yr",
    },  # noqa
    "automated nucleic acid extraction system": {
        "value": 35,
        "label": "Automated Nucleic Acid Extraction System",
        "ml_label": "automated nucleic acid extraction system",
        "roc": "1 yr",
    },  # noqa
    "automatic perimeter": {
        "value": 36,
        "label": "Automatic Perimeter",
        "ml_label": "automatic perimeter",
        "roc": "1 yr",
    },  # noqa
    "automotive service equipment": {
        "value": 37,
        "label": "Automotive Service Equipment",
        "ml_label": "automotive service equipment",
        "roc": "3 yrs",
    },  # noqa
    "av accessories": {
        "value": 38,
        "label": "Av Accessories",
        "ml_label": "av accessories",
        "roc": "3 yrs",
    },  # noqa
    "awning": {
        "value": 39,
        "label": "Awning",
        "ml_label": "awning",
        "roc": "S14Q",
    },  # noqa
    "back lit image": {
        "value": 40,
        "label": "Back Lit Image",
        "ml_label": "back lit image",
        "roc": "3 yrs",
    },  # noqa
    "bag": {"value": 41, "label": "Bag", "ml_label": "bag", "roc": "3 yrs"},
    # noqa
    "balancing machine": {
        "value": 42,
        "label": "Balancing Machine",
        "ml_label": "balancing machine",
        "roc": "3 yrs",
    },  # noqa
    "bank note counter": {
        "value": 43,
        "label": "Bank Note Counter",
        "ml_label": "bank note counter",
        "roc": "1 yr",
    },  # noqa
    "battery charger": {
        "value": 44,
        "label": "Battery Charger",
        "ml_label": "battery charger",
        "roc": "3 yrs",
    },  # noqa
    "battery monitoring system": {
        "value": 45,
        "label": "Battery Monitoring System",
        "ml_label": "battery monitoring system",
        "roc": "3 yrs",
    },  # noqa
    "bedding": {
        "value": 46,
        "label": "Bedding",
        "ml_label": "bedding",
        "roc": "Revenue",
    },  # noqa
    "bicycle": {
        "value": 47,
        "label": "Bicycle",
        "ml_label": "bicycle",
        "roc": "3 yrs",
    },  # noqa
    "biometric door access": {
        "value": 48,
        "label": "Biometric Door Access",
        "ml_label": "biometric door access",
        "roc": "1 yr",
    },  # noqa
    "blending tank": {
        "value": 49,
        "label": "Blending Tank",
        "ml_label": "blending tank",
        "roc": "3 yrs",
    },  # noqa
    "blinds": {
        "value": 50,
        "label": "Blinds",
        "ml_label": "blinds",
        "roc": "3 yrs",
    },  # noqa
    "boat": {"value": 51, "label": "Boat", "ml_label": "boat", "roc": "3 yrs"},
    # noqa
    "booking system": {
        "value": 52,
        "label": "Booking System",
        "ml_label": "booking system",
        "roc": "1 yr",
    },  # noqa
    "buggy": {
        "value": 53,
        "label": "Buggy",
        "ml_label": "buggy",
        "roc": "3 yrs",
    },  # noqa
    "building": {
        "value": 54,
        "label": "Building",
        "ml_label": "building",
        "roc": "No Claim",
    },  # noqa
    "building automation system": {
        "value": 55,
        "label": "Building Automation System",
        "ml_label": "building automation system",
        "roc": "1 yr",
    },  # noqa
    "bus": {"value": 56, "label": "Bus", "ml_label": "bus", "roc": "3 yrs"},
    # noqa
    "cable": {"value": 57, "label": "Cable", "ml_label": "cable", "roc": "Tba"},
    # noqa
    "cable locator": {
        "value": 58,
        "label": "Cable Locator",
        "ml_label": "cable locator",
        "roc": "3 yrs",
    },  # noqa
    "camera": {
        "value": 59,
        "label": "Camera",
        "ml_label": "camera",
        "roc": "3 yrs",
    },  # noqa
    "camera accessories": {
        "value": 60,
        "label": "Camera Accessories",
        "ml_label": "camera accessories",
        "roc": "3 yrs",
    },  # noqa
    "canopy": {
        "value": 61,
        "label": "Canopy",
        "ml_label": "canopy",
        "roc": "S14Q",
    },  # noqa
    "capacitor bank": {
        "value": 62,
        "label": "Capacitor Bank",
        "ml_label": "capacitor bank",
        "roc": "3 yrs",
    },  # noqa
    "card access system": {
        "value": 63,
        "label": "Card Access System",
        "ml_label": "card access system",
        "roc": "1 yr",
    },  # noqa
    "card reader": {
        "value": 64,
        "label": "Card Reader",
        "ml_label": "card reader",
        "roc": "1 yr",
    },  # noqa
    "cardiac image managment system": {
        "value": 65,
        "label": "Cardiac Image Managment System",
        "ml_label": "cardiac image managment system",
        "roc": "1 yr",
    },  # noqa
    "carpet": {
        "value": 66,
        "label": "Carpet",
        "ml_label": "carpet",
        "roc": "3 yrs",
    },  # noqa
    "cash box system with software": {
        "value": 67,
        "label": "Cash Box System With Software",
        "ml_label": "cash box system with software",
        "roc": "1 yr",
    },  # noqa
    "cashcode bill validator": {
        "value": 68,
        "label": "Cashcode Bill Validator",
        "ml_label": "cashcode bill validator",
        "roc": "1 yr",
    },  # noqa
    "cctv software": {
        "value": 69,
        "label": "Cctv Software",
        "ml_label": "cctv software",
        "roc": "1 yr",
    },  # noqa
    "ceiling works": {
        "value": 70,
        "label": "Ceiling Works",
        "ml_label": "ceiling works",
        "roc": "S14Q",
    },  # noqa
    "charging booth": {
        "value": 71,
        "label": "Charging Booth",
        "ml_label": "charging booth",
        "roc": "3 yrs",
    },  # noqa
    "chemical dosing system": {
        "value": 72,
        "label": "Chemical Dosing System",
        "ml_label": "chemical dosing system",
        "roc": "3 yrs",
    },  # noqa
    "chemical storage shed": {
        "value": 73,
        "label": "Chemical Storage Shed",
        "ml_label": "chemical storage shed",
        "roc": "3 yrs",
    },  # noqa
    "cleaning equipment": {
        "value": 74,
        "label": "Cleaning Equipment",
        "ml_label": "cleaning equipment",
        "roc": "3 yrs",
    },  # noqa
    "cleaning service": {
        "value": 75,
        "label": "Cleaning Service",
        "ml_label": "cleaning service",
        "roc": "Revenue",
    },  # noqa
    "collapsible tank": {
        "value": 76,
        "label": "Collapsible Tank",
        "ml_label": "collapsible tank",
        "roc": "3 yrs",
    },  # noqa
    "compactus": {
        "value": 77,
        "label": "Compactus",
        "ml_label": "compactus",
        "roc": "3 yrs",
    },  # noqa
    "compliant pin vision system": {
        "value": 78,
        "label": "Compliant Pin Vision System",
        "ml_label": "compliant pin vision system",
        "roc": "3 yrs",
    },  # noqa
    "computer": {
        "value": 79,
        "label": "Computer",
        "ml_label": "computer",
        "roc": "1 yr",
    },  # noqa
    "computer aided machine": {
        "value": 80,
        "label": "Computer Aided Machine",
        "ml_label": "computer aided machine",
        "roc": "1 yr",
    },  # noqa
    "computer network equipment": {
        "value": 81,
        "label": "Computer Network Equipment",
        "ml_label": "computer network equipment",
        "roc": "1 yr",
    },  # noqa
    "computer network peripherals": {
        "value": 82,
        "label": "Computer Network Peripherals",
        "ml_label": "computer network peripherals",
        "roc": "1 yr",
    },  # noqa
    "computer peripherals": {
        "value": 83,
        "label": "Computer Peripherals",
        "ml_label": "computer peripherals",
        "roc": "1 yr",
    },  # noqa
    "computerised lens meter": {
        "value": 84,
        "label": "Computerised Lens Meter",
        "ml_label": "computerised lens meter",
        "roc": "1 yr",
    },  # noqa
    "condolences": {
        "value": 85,
        "label": "Condolences",
        "ml_label": "condolences",
        "roc": "Revenue",
    },  # noqa
    "console": {
        "value": 86,
        "label": "Console",
        "ml_label": "console",
        "roc": "3 yrs",
    },  # noqa
    "construction equipment": {
        "value": 87,
        "label": "Construction Equipment",
        "ml_label": "construction equipment",
        "roc": "3 yrs",
    },  # noqa
    "consumables": {
        "value": 88,
        "label": "Consumables",
        "ml_label": "consumables",
        "roc": "Revenue",
    },  # noqa
    "container office": {
        "value": 89,
        "label": "Container Office",
        "ml_label": "container office",
        "roc": "No Claim",
    },  # noqa
    "control panel": {
        "value": 90,
        "label": "Control Panel",
        "ml_label": "control panel",
        "roc": "3 yrs",
    },  # noqa
    "conveyor system": {
        "value": 91,
        "label": "Conveyor System",
        "ml_label": "conveyor system",
        "roc": "3 yrs",
    },  # noqa
    "coolant tank": {
        "value": 92,
        "label": "Coolant Tank",
        "ml_label": "coolant tank",
        "roc": "3 yrs",
    },  # noqa
    "cooler box": {
        "value": 93,
        "label": "Cooler Box",
        "ml_label": "cooler box",
        "roc": "3 yrs",
    },  # noqa
    "cooling tower": {
        "value": 94,
        "label": "Cooling Tower",
        "ml_label": "cooling tower",
        "roc": "3 yrs",
    },  # noqa
    "copier charge": {
        "value": 95,
        "label": "Copier Charge",
        "ml_label": "copier charge",
        "roc": "Revenue",
    },  # noqa
    "crane": {
        "value": 96,
        "label": "Crane",
        "ml_label": "crane",
        "roc": "3 yrs",
    },  # noqa
    "curtain": {
        "value": 97,
        "label": "Curtain",
        "ml_label": "curtain",
        "roc": "3 yrs",
    },  # noqa
    "customised fencing system": {
        "value": 98,
        "label": "Customised Fencing System",
        "ml_label": "customised fencing system",
        "roc": "Tba",
    },  # noqa
    "data acquisition system": {
        "value": 99,
        "label": "Data Acquisition System",
        "ml_label": "data acquisition system",
        "roc": "1 yr",
    },  # noqa
    "data logger": {
        "value": 100,
        "label": "Data Logger",
        "ml_label": "data logger",
        "roc": "1 yr",
    },  # noqa
    "data processing equipment": {
        "value": 101,
        "label": "Data Processing Equipment",
        "ml_label": "data processing equipment",
        "roc": "1 yr",
    },  # noqa
    "data storage": {
        "value": 102,
        "label": "Data Storage",
        "ml_label": "data storage",
        "roc": "1 yr",
    },  # noqa
    "datatrace reader": {
        "value": 103,
        "label": "Datatrace Reader",
        "ml_label": "datatrace reader",
        "roc": "3 yrs",
    },  # noqa
    "decorations": {
        "value": 104,
        "label": "Decorations",
        "ml_label": "decorations",
        "roc": "Revenue",
    },  # noqa
    "decorative lighting": {
        "value": 105,
        "label": "Decorative Lighting",
        "ml_label": "decorative lighting",
        "roc": "Tba",
    },  # noqa
    "decorative structures": {
        "value": 106,
        "label": "Decorative Structures",
        "ml_label": "decorative structures",
        "roc": "Tba",
    },  # noqa
    "demountable partition": {
        "value": 107,
        "label": "Demountable Partition",
        "ml_label": "demountable partition",
        "roc": "3 yrs",
    },  # noqa
    "die": {"value": 108, "label": "Die", "ml_label": "die", "roc": "3 yrs"},
    # noqa
    "digital inspection system": {
        "value": 109,
        "label": "Digital Inspection System",
        "ml_label": "digital inspection system",
        "roc": "3 yrs",
    },  # noqa
    "digital surveillance system": {
        "value": 110,
        "label": "Digital Surveillance System",
        "ml_label": "digital surveillance system",
        "roc": "1 yr",
    },  # noqa
    "digital time zone": {
        "value": 111,
        "label": "Digital Time Zone",
        "ml_label": "digital time zone",
        "roc": "1 yr",
    },  # noqa
    "digital voting system": {
        "value": 112,
        "label": "Digital Voting System",
        "ml_label": "digital voting system",
        "roc": "1 yr",
    },  # noqa
    "dispenser": {
        "value": 113,
        "label": "Dispenser",
        "ml_label": "dispenser",
        "roc": "3 yrs",
    },  # noqa
    "disposable syringes": {
        "value": 114,
        "label": "Disposable Syringes",
        "ml_label": "disposable syringes",
        "roc": "Revenue",
    },  # noqa
    "dmx control equipment": {
        "value": 115,
        "label": "Dmx Control Equipment",
        "ml_label": "dmx control equipment",
        "roc": "1 yr",
    },  # noqa
    "docking system": {
        "value": 116,
        "label": "Docking System",
        "ml_label": "docking system",
        "roc": "3 yrs",
    },  # noqa
    "door": {"value": 117, "label": "Door", "ml_label": "door", "roc": "S14Q"},
    # noqa
    "door bell": {
        "value": 118,
        "label": "Door Bell",
        "ml_label": "door bell",
        "roc": "3 yrs",
    },  # noqa
    "drum": {"value": 119, "label": "Drum", "ml_label": "drum", "roc": "3 yrs"},
    # noqa
    "drumlifter": {
        "value": 120,
        "label": "Drumlifter",
        "ml_label": "drumlifter",
        "roc": "3 yrs",
    },  # noqa
    "dustbin": {
        "value": 121,
        "label": "Dustbin",
        "ml_label": "dustbin",
        "roc": "3 yrs",
    },  # noqa
    "electrical equipment": {
        "value": 122,
        "label": "Electrical Equipment",
        "ml_label": "electrical equipment",
        "roc": "3 yrs",
    },  # noqa
    "electrical switchgear": {
        "value": 123,
        "label": "Electrical Switchgear",
        "ml_label": "electrical switchgear",
        "roc": "3 yrs",
    },  # noqa
    "electrical works": {
        "value": 124,
        "label": "Electrical Works",
        "ml_label": "electrical works",
        "roc": "S14Q",
    },  # noqa
    "electricity supply installation": {
        "value": 125,
        "label": "Electricity Supply Installation",
        "ml_label": "electricity supply installation",
        "roc": "S14Q",
    },  # noqa
    "electromechanical surgical unit": {
        "value": 126,
        "label": "Electromechanical Surgical Unit",
        "ml_label": "electromechanical surgical unit",
        "roc": "3 yrs",
    },  # noqa
    "electronic labelling system": {
        "value": 127,
        "label": "Electronic Labelling System",
        "ml_label": "electronic labelling system",
        "roc": "3 yrs",
    },  # noqa
    "emergency call system": {
        "value": 128,
        "label": "Emergency Call System",
        "ml_label": "emergency call system",
        "roc": "3 yrs",
    },  # noqa
    "employee meals": {
        "value": 129,
        "label": "Employee Meals",
        "ml_label": "employee meals",
        "roc": "Revenue",
    },  # noqa
    "energy monitoring system": {
        "value": 130,
        "label": "Energy Monitoring System",
        "ml_label": "energy monitoring system",
        "roc": "3 yrs",
    },  # noqa
    "equipment parts": {
        "value": 131,
        "label": "Equipment Parts",
        "ml_label": "equipment parts",
        "roc": "3 yrs",
    },  # noqa
    "escalator": {
        "value": 132,
        "label": "Escalator",
        "ml_label": "escalator",
        "roc": "3 yrs",
    },  # noqa
    "e-scooter": {
        "value": 133,
        "label": "E-Scooter",
        "ml_label": "e-scooter",
        "roc": "3 yrs",
    },  # noqa
    "exercise machine": {
        "value": 134,
        "label": "Exercise Machine",
        "ml_label": "exercise machine",
        "roc": "3 yrs",
    },  # noqa
    "extended warranty": {
        "value": 135,
        "label": "Extended Warranty",
        "ml_label": "extended warranty",
        "roc": "Revenue",
    },  # noqa
    "facsimile machine": {
        "value": 136,
        "label": "Facsimile Machine",
        "ml_label": "facsimile machine",
        "roc": "1 yr",
    },  # noqa
    "fire alarm and fire fighting installation": {
        "value": 137,
        "label": "Fire Alarm And Fire Fighting Installation",
        "ml_label": "fire alarm and fire fighting installation",
        "roc": "3 yrs",
    },  # noqa
    "fire certificate": {
        "value": 138,
        "label": "Fire Certificate",
        "ml_label": "fire certificate",
        "roc": "Revenue",
    },  # noqa
    "fire extinguisher": {
        "value": 139,
        "label": "Fire Extinguisher",
        "ml_label": "fire extinguisher",
        "roc": "3 yrs",
    },  # noqa
    "fire truck": {
        "value": 140,
        "label": "Fire Truck",
        "ml_label": "fire truck",
        "roc": "3 yrs",
    },  # noqa
    "flooring works": {
        "value": 141,
        "label": "Flooring Works",
        "ml_label": "flooring works",
        "roc": "S14Q",
    },  # noqa
    "forced air cooling system": {
        "value": 142,
        "label": "Forced Air Cooling System",
        "ml_label": "forced air cooling system",
        "roc": "3 yrs",
    },  # noqa
    "forklift": {
        "value": 143,
        "label": "Forklift",
        "ml_label": "forklift",
        "roc": "3 yrs",
    },  # noqa
    "freight container": {
        "value": 144,
        "label": "Freight Container",
        "ml_label": "freight container",
        "roc": "3 yrs",
    },  # noqa
    "freight, installation cost": {
        "value": 145,
        "label": "Freight, Installation Cost",
        "ml_label": "freight, installation cost",
        "roc": "Tba",
    },  # noqa
    "fuel tank system": {
        "value": 146,
        "label": "Fuel Tank System",
        "ml_label": "fuel tank system",
        "roc": "3 yrs",
    },  # noqa
    "furnace": {
        "value": 147,
        "label": "Furnace",
        "ml_label": "furnace",
        "roc": "3 yrs",
    },  # noqa
    "furniture": {
        "value": 148,
        "label": "Furniture",
        "ml_label": "furniture",
        "roc": "3 yrs",
    },  # noqa
    "galileo g-assembly": {
        "value": 149,
        "label": "Galileo G-Assembly",
        "ml_label": "galileo g-assembly",
        "roc": "3 yrs",
    },  # noqa
    "galileo injector": {
        "value": 150,
        "label": "Galileo Injector",
        "ml_label": "galileo injector",
        "roc": "3 yrs",
    },  # noqa
    "game table": {
        "value": 151,
        "label": "Game Table",
        "ml_label": "game table",
        "roc": "3 yrs",
    },  # noqa
    "gas detector": {
        "value": 152,
        "label": "Gas Detector",
        "ml_label": "gas detector",
        "roc": "3 yrs",
    },  # noqa
    "gas monitoring system": {
        "value": 153,
        "label": "Gas Monitoring System",
        "ml_label": "gas monitoring system",
        "roc": "3 yrs",
    },  # noqa
    "gas supply equipment": {
        "value": 154,
        "label": "Gas Supply Equipment",
        "ml_label": "gas supply equipment",
        "roc": "3 yrs",
    },  # noqa
    "gel dispenser with software": {
        "value": 155,
        "label": "Gel Dispenser With Software",
        "ml_label": "gel dispenser with software",
        "roc": "1 yr",
    },  # noqa
    "general lighting": {
        "value": 156,
        "label": "General Lighting",
        "ml_label": "general lighting",
        "roc": "S14Q",
    },  # noqa
    "generator": {
        "value": 157,
        "label": "Generator",
        "ml_label": "generator",
        "roc": "3 yrs",
    },  # noqa
    "gifts": {
        "value": 158,
        "label": "Gifts",
        "ml_label": "gifts",
        "roc": "Revenue",
    },  # noqa
    "glue vision system": {
        "value": 159,
        "label": "Glue Vision System",
        "ml_label": "glue vision system",
        "roc": "3 yrs",
    },  # noqa
    "golden unit": {
        "value": 160,
        "label": "Golden Unit",
        "ml_label": "golden unit",
        "roc": "3 yrs",
    },  # noqa
    "golf equipment": {
        "value": 161,
        "label": "Golf Equipment",
        "ml_label": "golf equipment",
        "roc": "3 yrs",
    },  # noqa
    "gondola": {
        "value": 162,
        "label": "Gondola",
        "ml_label": "gondola",
        "roc": "3 yrs",
    },  # noqa
    "gps devices": {
        "value": 163,
        "label": "Gps Devices",
        "ml_label": "gps devices",
        "roc": "3 yrs",
    },  # noqa
    "graphic terminal": {
        "value": 164,
        "label": "Graphic Terminal",
        "ml_label": "graphic terminal",
        "roc": "1 yr",
    },  # noqa
    "green mark certification": {
        "value": 165,
        "label": "Green Mark Certification",
        "ml_label": "green mark certification",
        "roc": "Revenue",
    },  # noqa
    "grid magnet": {
        "value": 166,
        "label": "Grid Magnet",
        "ml_label": "grid magnet",
        "roc": "3 yrs",
    },  # noqa
    "handpunch terminal": {
        "value": 167,
        "label": "Handpunch Terminal",
        "ml_label": "handpunch terminal",
        "roc": "1 yr",
    },  # noqa
    "hands free cpr system": {
        "value": 168,
        "label": "Hands Free Cpr System",
        "ml_label": "hands free cpr system",
        "roc": "3 yrs",
    },  # noqa
    "hangar": {
        "value": 169,
        "label": "Hangar",
        "ml_label": "hangar",
        "roc": "3 yrs",
    },  # noqa
    "harddisk duplicator": {
        "value": 170,
        "label": "Harddisk Duplicator",
        "ml_label": "harddisk duplicator",
        "roc": "1 yr",
    },  # noqa
    "hearing aid fitting software": {
        "value": 171,
        "label": "Hearing Aid Fitting Software",
        "ml_label": "hearing aid fitting software",
        "roc": "1 yr",
    },  # noqa
    "hoist": {
        "value": 172,
        "label": "Hoist",
        "ml_label": "hoist",
        "roc": "3 yrs",
    },  # noqa
    "hospital bed": {
        "value": 173,
        "label": "Hospital Bed",
        "ml_label": "hospital bed",
        "roc": "3 yrs",
    },  # noqa
    "human milk bank": {
        "value": 174,
        "label": "Human Milk Bank",
        "ml_label": "human milk bank",
        "roc": "3 yrs",
    },  # noqa
    "hydraulic dock leveller": {
        "value": 175,
        "label": "Hydraulic Dock Leveller",
        "ml_label": "hydraulic dock leveller",
        "roc": "3 yrs",
    },  # noqa
    "hydraulic hooklift container": {
        "value": 176,
        "label": "Hydraulic Hooklift Container",
        "ml_label": "hydraulic hooklift container",
        "roc": "3 yrs",
    },  # noqa
    "image management system": {
        "value": 177,
        "label": "Image Management System",
        "ml_label": "image management system",
        "roc": "1 yr",
    },  # noqa
    "imaging module system": {
        "value": 178,
        "label": "Imaging Module System",
        "ml_label": "imaging module system",
        "roc": "1 yr",
    },  # noqa
    "immovable partition": {
        "value": 179,
        "label": "Immovable Partition",
        "ml_label": "immovable partition",
        "roc": "S14Q",
    },  # noqa
    "incubator": {
        "value": 180,
        "label": "Incubator",
        "ml_label": "incubator",
        "roc": "1 yr",
    },  # noqa
    "industrial freezer": {
        "value": 181,
        "label": "Industrial Freezer",
        "ml_label": "industrial freezer",
        "roc": "3 yrs",
    },  # noqa
    "industrial oven": {
        "value": 182,
        "label": "Industrial Oven",
        "ml_label": "industrial oven",
        "roc": "3 yrs",
    },  # noqa
    "infant care equipment": {
        "value": 183,
        "label": "Infant Care Equipment",
        "ml_label": "infant care equipment",
        "roc": "3 yrs",
    },  # noqa
    "inspection equipment": {
        "value": 184,
        "label": "Inspection Equipment",
        "ml_label": "inspection equipment",
        "roc": "3 yrs",
    },  # noqa
    "inspection light": {
        "value": 185,
        "label": "Inspection Light",
        "ml_label": "inspection light",
        "roc": "3 yrs",
    },  # noqa
    "insurance for renovation works": {
        "value": 186,
        "label": "Insurance For Renovation Works",
        "ml_label": "insurance for renovation works",
        "roc": "S14Q",
    },  # noqa
    "intercom system": {
        "value": 187,
        "label": "Intercom System",
        "ml_label": "intercom system",
        "roc": "3 yrs",
    },  # noqa
    "internet charges": {
        "value": 188,
        "label": "Internet Charges",
        "ml_label": "internet charges",
        "roc": "Revenue",
    },  # noqa
    "interventional neuroradiology x-ray system": {
        "value": 189,
        "label": "Interventional Neuroradiology X-Ray System",
        "ml_label": "interventional neuroradiology x-ray system",
        "roc": "1 yr",
    },  # noqa
    "inverters": {
        "value": 190,
        "label": "Inverters",
        "ml_label": "inverters",
        "roc": "3 yrs",
    },  # noqa
    "ip telephony system": {
        "value": 191,
        "label": "Ip Telephony System",
        "ml_label": "ip telephony system",
        "roc": "1 yr",
    },  # noqa
    "ip television": {
        "value": 192,
        "label": "Ip Television",
        "ml_label": "ip television",
        "roc": "1 yr",
    },  # noqa
    "jig": {"value": 193, "label": "Jig", "ml_label": "jig", "roc": "3 yrs"},
    # noqa
    "kettlebell": {
        "value": 194,
        "label": "Kettlebell",
        "ml_label": "kettlebell",
        "roc": "3 yrs",
    },  # noqa
    "kitchen equipment": {
        "value": 195,
        "label": "Kitchen Equipment",
        "ml_label": "kitchen equipment",
        "roc": "3 yrs",
    },  # noqa
    "laboratory equipment": {
        "value": 196,
        "label": "Laboratory Equipment",
        "ml_label": "laboratory equipment",
        "roc": "3 yrs",
    },  # noqa
    "ladder": {
        "value": 197,
        "label": "Ladder",
        "ml_label": "ladder",
        "roc": "3 yrs",
    },  # noqa
    "landscaping": {
        "value": 198,
        "label": "Landscaping",
        "ml_label": "landscaping",
        "roc": "No Claim",
    },  # noqa
    "learning kits": {
        "value": 199,
        "label": "Learning Kits",
        "ml_label": "learning kits",
        "roc": "3 yrs",
    },  # noqa
    "legal agreement": {
        "value": 200,
        "label": "Legal Agreement",
        "ml_label": "legal agreement",
        "roc": "Tba",
    },  # noqa
    "lens production machine": {
        "value": 201,
        "label": "Lens Production Machine",
        "ml_label": "lens production machine",
        "roc": "3 yrs",
    },  # noqa
    "lift": {"value": 202, "label": "Lift", "ml_label": "lift", "roc": "3 yrs"},
    # noqa
    "lift shaft": {
        "value": 203,
        "label": "Lift Shaft",
        "ml_label": "lift shaft",
        "roc": "No Claim",
    },  # noqa
    "lifting magnet": {
        "value": 204,
        "label": "Lifting Magnet",
        "ml_label": "lifting magnet",
        "roc": "3 yrs",
    },  # noqa
    "light box": {
        "value": 205,
        "label": "Light Box",
        "ml_label": "light box",
        "roc": "3 yrs",
    },  # noqa
    "light source": {
        "value": 206,
        "label": "Light Source",
        "ml_label": "light source",
        "roc": "3 yrs",
    },  # noqa
    "lightcure system": {
        "value": 207,
        "label": "Lightcure System",
        "ml_label": "lightcure system",
        "roc": "3 yrs",
    },  # noqa
    "lighting tower": {
        "value": 208,
        "label": "Lighting Tower",
        "ml_label": "lighting tower",
        "roc": "3 yrs",
    },  # noqa
    "lightning protector": {
        "value": 209,
        "label": "Lightning Protector",
        "ml_label": "lightning protector",
        "roc": "3 yrs",
    },  # noqa
    "loader": {
        "value": 210,
        "label": "Loader",
        "ml_label": "loader",
        "roc": "3 yrs",
    },  # noqa
    "logistic support robot": {
        "value": 211,
        "label": "Logistic Support Robot",
        "ml_label": "logistic support robot",
        "roc": "1 yr",
    },  # noqa
    "lorry": {
        "value": 212,
        "label": "Lorry",
        "ml_label": "lorry",
        "roc": "3 yrs",
    },  # noqa
    "machine with software": {
        "value": 213,
        "label": "Machine With Software",
        "ml_label": "machine with software",
        "roc": "1 yr",
    },  # noqa
    "machinery": {
        "value": 214,
        "label": "Machinery",
        "ml_label": "machinery",
        "roc": "3 yrs",
    },  # noqa
    "magnetic door holder": {
        "value": 215,
        "label": "Magnetic Door Holder",
        "ml_label": "magnetic door holder",
        "roc": "3 yrs",
    },  # noqa
    "magnifier lamp": {
        "value": 216,
        "label": "Magnifier Lamp",
        "ml_label": "magnifier lamp",
        "roc": "3 yrs",
    },  # noqa
    "maintenance charge": {
        "value": 217,
        "label": "Maintenance Charge",
        "ml_label": "maintenance charge",
        "roc": "Revenue",
    },  # noqa
    "maintenance of equipment": {
        "value": 218,
        "label": "Maintenance Of Equipment",
        "ml_label": "maintenance of equipment",
        "roc": "Revenue",
    },  # noqa
    "mammography system": {
        "value": 219,
        "label": "Mammography System",
        "ml_label": "mammography system",
        "roc": "1 yr",
    },  # noqa
    "mannequin": {
        "value": 220,
        "label": "Mannequin",
        "ml_label": "mannequin",
        "roc": "3 yrs",
    },  # noqa
    "marking machine": {
        "value": 221,
        "label": "Marking Machine",
        "ml_label": "marking machine",
        "roc": "3 yrs",
    },  # noqa
    "measuring equipment": {
        "value": 222,
        "label": "Measuring Equipment",
        "ml_label": "measuring equipment",
        "roc": "3 yrs",
    },  # noqa
    "measuring equipment with software": {
        "value": 223,
        "label": "Measuring Equipment With Software",
        "ml_label": "measuring equipment with software",
        "roc": "1 yr",
    },  # noqa
    "medical diagnostic tool": {
        "value": 224,
        "label": "Medical Diagnostic Tool",
        "ml_label": "medical diagnostic tool",
        "roc": "3 yrs",
    },  # noqa
    "medical equipment": {
        "value": 225,
        "label": "Medical Equipment",
        "ml_label": "medical equipment",
        "roc": "3 yrs",
    },  # noqa
    "medical training solution": {
        "value": 226,
        "label": "Medical Training Solution",
        "ml_label": "medical training solution",
        "roc": "3 yrs",
    },  # noqa
    "medical training solution with software": {
        "value": 227,
        "label": "Medical Training Solution With Software",
        "ml_label": "medical training solution with software",
        "roc": "1 yr",
    },  # noqa
    "metal detector": {
        "value": 228,
        "label": "Metal Detector",
        "ml_label": "metal detector",
        "roc": "3 yrs",
    },  # noqa
    "microprocessor": {
        "value": 229,
        "label": "Microprocessor",
        "ml_label": "microprocessor",
        "roc": "1 yr",
    },  # noqa
    "microscope": {
        "value": 230,
        "label": "Microscope",
        "ml_label": "microscope",
        "roc": "3 yrs",
    },  # noqa
    "mirror": {
        "value": 231,
        "label": "Mirror",
        "ml_label": "mirror",
        "roc": "3 yrs",
    },  # noqa
    "mobile phone": {
        "value": 232,
        "label": "Mobile Phone",
        "ml_label": "mobile phone",
        "roc": "1 yr",
    },  # noqa
    "monitoring surveillance system": {
        "value": 233,
        "label": "Monitoring Surveillance System",
        "ml_label": "monitoring surveillance system",
        "roc": "1 yr",
    },  # noqa
    "motor cycle": {
        "value": 234,
        "label": "Motor Cycle",
        "ml_label": "motor cycle",
        "roc": "3 yrs",
    },  # noqa
    "mould": {
        "value": 235,
        "label": "Mould",
        "ml_label": "mould",
        "roc": "3 yrs",
    },  # noqa
    "moving fee": {
        "value": 236,
        "label": "Moving Fee",
        "ml_label": "moving fee",
        "roc": "Tba",
    },  # noqa
    "mower": {
        "value": 237,
        "label": "Mower",
        "ml_label": "mower",
        "roc": "3 yrs",
    },  # noqa
    "multi purpose kiosk": {
        "value": 238,
        "label": "Multi Purpose Kiosk",
        "ml_label": "multi purpose kiosk",
        "roc": "1 yr",
    },  # noqa
    "mural": {
        "value": 239,
        "label": "Mural",
        "ml_label": "mural",
        "roc": "Tba",
    },  # noqa
    "navigation light": {
        "value": 240,
        "label": "Navigation Light",
        "ml_label": "navigation light",
        "roc": "3 yrs",
    },  # noqa
    "negative pressure control system": {
        "value": 241,
        "label": "Negative Pressure Control System",
        "ml_label": "negative pressure control system",
        "roc": "3 yrs",
    },  # noqa
    "nerve monitor machine": {
        "value": 242,
        "label": "Nerve Monitor Machine",
        "ml_label": "nerve monitor machine",
        "roc": "1 yr",
    },  # noqa
    "network communication equipment": {
        "value": 243,
        "label": "Network Communication Equipment",
        "ml_label": "network communication equipment",
        "roc": "1 yr",
    },  # noqa
    "nibp monitor with printer": {
        "value": 244,
        "label": "Nibp Monitor With Printer",
        "ml_label": "nibp monitor with printer",
        "roc": "1 yr",
    },  # noqa
    "office equipment": {
        "value": 245,
        "label": "Office Equipment",
        "ml_label": "office equipment",
        "roc": "3 yrs",
    },  # noqa
    "ophthalmic equipment": {
        "value": 246,
        "label": "Ophthalmic Equipment",
        "ml_label": "ophthalmic equipment",
        "roc": "3 yrs",
    },  # noqa
    "ophthalmic laser": {
        "value": 247,
        "label": "Ophthalmic Laser",
        "ml_label": "ophthalmic laser",
        "roc": "1 yr",
    },  # noqa
    "optometry equipment": {
        "value": 248,
        "label": "Optometry Equipment",
        "ml_label": "optometry equipment",
        "roc": "3 yrs",
    },  # noqa
    "other vehicle": {
        "value": 249,
        "label": "Other Vehicle",
        "ml_label": "other vehicle",
        "roc": "Tba",
    },  # noqa
    "pager": {
        "value": 250,
        "label": "Pager",
        "ml_label": "pager",
        "roc": "3 yrs",
    },  # noqa
    "painting works": {
        "value": 251,
        "label": "Painting Works",
        "ml_label": "painting works",
        "roc": "S14Q",
    },  # noqa
    "paintings": {
        "value": 252,
        "label": "Paintings",
        "ml_label": "paintings",
        "roc": "Tba",
    },  # noqa
    "pallet": {
        "value": 253,
        "label": "Pallet",
        "ml_label": "pallet",
        "roc": "3 yrs",
    },  # noqa
    "pallet magazine loader": {
        "value": 254,
        "label": "Pallet Magazine Loader",
        "ml_label": "pallet magazine loader",
        "roc": "3 yrs",
    },  # noqa
    "pallet truck": {
        "value": 255,
        "label": "Pallet Truck",
        "ml_label": "pallet truck",
        "roc": "3 yrs",
    },  # noqa
    "paperless recorder": {
        "value": 256,
        "label": "Paperless Recorder",
        "ml_label": "paperless recorder",
        "roc": "1 yr",
    },  # noqa
    "patient communication system": {
        "value": 257,
        "label": "Patient Communication System",
        "ml_label": "patient communication system",
        "roc": "3 yrs",
    },  # noqa
    "patient handling equipment": {
        "value": 258,
        "label": "Patient Handling Equipment",
        "ml_label": "patient handling equipment",
        "roc": "3 yrs",
    },  # noqa
    "pest control": {
        "value": 259,
        "label": "Pest Control",
        "ml_label": "pest control",
        "roc": "Revenue",
    },  # noqa
    "phone appointment system": {
        "value": 260,
        "label": "Phone Appointment System",
        "ml_label": "phone appointment system",
        "roc": "1 yr",
    },  # noqa
    "physiotherapy equipment": {
        "value": 261,
        "label": "Physiotherapy Equipment",
        "ml_label": "physiotherapy equipment",
        "roc": "3 yrs",
    },  # noqa
    "pick up": {
        "value": 262,
        "label": "Pick Up",
        "ml_label": "pick up",
        "roc": "3 yrs",
    },  # noqa
    "pictures": {
        "value": 263,
        "label": "Pictures",
        "ml_label": "pictures",
        "roc": "Tba",
    },  # noqa
    "piping system": {
        "value": 264,
        "label": "Piping System",
        "ml_label": "piping system",
        "roc": "Tba",
    },  # noqa
    "plasma surface treatment system": {
        "value": 265,
        "label": "Plasma Surface Treatment System",
        "ml_label": "plasma surface treatment system",
        "roc": "3 yrs",
    },  # noqa
    "portable light": {
        "value": 266,
        "label": "Portable Light",
        "ml_label": "portable light",
        "roc": "3 yrs",
    },  # noqa
    "portable ramps": {
        "value": 267,
        "label": "Portable Ramps",
        "ml_label": "portable ramps",
        "roc": "3 yrs",
    },  # noqa
    "portable toilet": {
        "value": 268,
        "label": "Portable Toilet",
        "ml_label": "portable toilet",
        "roc": "3 yrs",
    },  # noqa
    "pos terminal system": {
        "value": 269,
        "label": "Pos Terminal System",
        "ml_label": "pos terminal system",
        "roc": "1 yr",
    },  # noqa
    "powder blending plant": {
        "value": 270,
        "label": "Powder Blending Plant",
        "ml_label": "powder blending plant",
        "roc": "3 yrs",
    },  # noqa
    "power automation": {
        "value": 271,
        "label": "Power Automation",
        "ml_label": "power automation",
        "roc": "1 yr",
    },  # noqa
    "power magnetization unit": {
        "value": 272,
        "label": "Power Magnetization Unit",
        "ml_label": "power magnetization unit",
        "roc": "3 yrs",
    },  # noqa
    "power supply": {
        "value": 273,
        "label": "Power Supply",
        "ml_label": "power supply",
        "roc": "3 yrs",
    },  # noqa
    "preliminaries": {
        "value": 274,
        "label": "Preliminaries",
        "ml_label": "preliminaries",
        "roc": "S14Q",
    },  # noqa
    "prime mover": {
        "value": 275,
        "label": "Prime Mover",
        "ml_label": "prime mover",
        "roc": "3 yrs",
    },  # noqa
    "printer": {
        "value": 276,
        "label": "Printer",
        "ml_label": "printer",
        "roc": "1 yr",
    },  # noqa
    "private car": {
        "value": 277,
        "label": "Private Car",
        "ml_label": "private car",
        "roc": "No Claim",
    },  # noqa
    "probe": {
        "value": 278,
        "label": "Probe",
        "ml_label": "probe",
        "roc": "1 yr",
    },  # noqa
    "probe card": {
        "value": 279,
        "label": "Probe Card",
        "ml_label": "probe card",
        "roc": "1 yr",
    },  # noqa
    "production line": {
        "value": 280,
        "label": "Production Line",
        "ml_label": "production line",
        "roc": "3 yrs",
    },  # noqa
    "production tools": {
        "value": 281,
        "label": "Production Tools",
        "ml_label": "production tools",
        "roc": "3 yrs",
    },  # noqa
    "professional fees": {
        "value": 282,
        "label": "Professional Fees",
        "ml_label": "professional fees",
        "roc": "No Claim",
    },  # noqa
    "programming equipment": {
        "value": 283,
        "label": "Programming Equipment",
        "ml_label": "programming equipment",
        "roc": "1 yr",
    },  # noqa
    "prototype equipment": {
        "value": 284,
        "label": "Prototype Equipment",
        "ml_label": "prototype equipment",
        "roc": "3 yrs",
    },  # noqa
    "pump": {"value": 285, "label": "Pump", "ml_label": "pump", "roc": "3 yrs"},
    # noqa
    "pvc flexible pipe": {
        "value": 286,
        "label": "Pvc Flexible Pipe",
        "ml_label": "pvc flexible pipe",
        "roc": "Revenue",
    },  # noqa
    "quarantine cage": {
        "value": 287,
        "label": "Quarantine Cage",
        "ml_label": "quarantine cage",
        "roc": "3 yrs",
    },  # noqa
    "queuing system": {
        "value": 288,
        "label": "Queuing System",
        "ml_label": "queuing system",
        "roc": "1 yr",
    },  # noqa
    "rack": {"value": 289, "label": "Rack", "ml_label": "rack", "roc": "3 yrs"},
    # noqa
    "radar vision system": {
        "value": 290,
        "label": "Radar Vision System",
        "ml_label": "radar vision system",
        "roc": "1 yr",
    },  # noqa
    "radiodetection equipment": {
        "value": 291,
        "label": "Radiodetection Equipment",
        "ml_label": "radiodetection equipment",
        "roc": "3 yrs",
    },  # noqa
    "ramp": {
        "value": 292,
        "label": "Ramp",
        "ml_label": "ramp",
        "roc": "No Claim",
    },  # noqa
    "rechargeable battery": {
        "value": 293,
        "label": "Rechargeable Battery",
        "ml_label": "rechargeable battery",
        "roc": "3 yrs",
    },  # noqa
    "refrigerant": {
        "value": 294,
        "label": "Refrigerant",
        "ml_label": "refrigerant",
        "roc": "Revenue",
    },  # noqa
    "refuse disposal": {
        "value": 295,
        "label": "Refuse Disposal",
        "ml_label": "refuse disposal",
        "roc": "Revenue",
    },  # noqa
    "reinstatement costs": {
        "value": 296,
        "label": "Reinstatement Costs",
        "ml_label": "reinstatement costs",
        "roc": "Revenue",
    },  # noqa
    "religious altar": {
        "value": 297,
        "label": "Religious Altar",
        "ml_label": "religious altar",
        "roc": "Tba",
    },  # noqa
    "relocation of plant and machinery": {
        "value": 298,
        "label": "Relocation Of Plant And Machinery",
        "ml_label": "relocation of plant and machinery",
        "roc": "3 yrs",
    },  # noqa
    "removal fee": {
        "value": 299,
        "label": "Removal Fee",
        "ml_label": "removal fee",
        "roc": "Revenue",
    },  # noqa
    "renovation works": {
        "value": 300,
        "label": "Renovation Works",
        "ml_label": "renovation works",
        "roc": "S14Q",
    },  # noqa
    "rental of equipment": {
        "value": 301,
        "label": "Rental Of Equipment",
        "ml_label": "rental of equipment",
        "roc": "Revenue",
    },  # noqa
    "re-painting works": {
        "value": 302,
        "label": "Re-Painting Works",
        "ml_label": "re-painting works",
        "roc": "Revenue",
    },  # noqa
    "repairs": {
        "value": 303,
        "label": "Repairs",
        "ml_label": "repairs",
        "roc": "Revenue",
    },  # noqa
    "repeater lights": {
        "value": 304,
        "label": "Repeater Lights",
        "ml_label": "repeater lights",
        "roc": "3 yrs",
    },  # noqa
    "retarder proofer": {
        "value": 305,
        "label": "Retarder Proofer",
        "ml_label": "retarder proofer",
        "roc": "1 yr",
    },  # noqa
    "robotic cleaner": {
        "value": 306,
        "label": "Robotic Cleaner",
        "ml_label": "robotic cleaner",
        "roc": "1 yr",
    },  # noqa
    "robotic hospital bed": {
        "value": 307,
        "label": "Robotic Hospital Bed",
        "ml_label": "robotic hospital bed",
        "roc": "1 yr",
    },  # noqa
    "robotic machine": {
        "value": 308,
        "label": "Robotic Machine",
        "ml_label": "robotic machine",
        "roc": "1 yr",
    },  # noqa
    "robotic pallet": {
        "value": 309,
        "label": "Robotic Pallet",
        "ml_label": "robotic pallet",
        "roc": "1 yr",
    },  # noqa
    "robotic process automation": {
        "value": 310,
        "label": "Robotic Process Automation",
        "ml_label": "robotic process automation",
        "roc": "1 yr",
    },  # noqa
    "robotic tooling": {
        "value": 311,
        "label": "Robotic Tooling",
        "ml_label": "robotic tooling",
        "roc": "1 yr",
    },  # noqa
    "roller shutter": {
        "value": 312,
        "label": "Roller Shutter",
        "ml_label": "roller shutter",
        "roc": "S14Q",
    },  # noqa
    "roller stand": {
        "value": 313,
        "label": "Roller Stand",
        "ml_label": "roller stand",
        "roc": "3 yrs",
    },  # noqa
    "safe box": {
        "value": 314,
        "label": "Safe Box",
        "ml_label": "safe box",
        "roc": "3 yrs",
    },  # noqa
    "safety barrier": {
        "value": 315,
        "label": "Safety Barrier",
        "ml_label": "safety barrier",
        "roc": "3 yrs",
    },  # noqa
    "safety equipment": {
        "value": 316,
        "label": "Safety Equipment",
        "ml_label": "safety equipment",
        "roc": "3 yrs",
    },  # noqa
    "sanitary fittings": {
        "value": 317,
        "label": "Sanitary Fittings",
        "ml_label": "sanitary fittings",
        "roc": "S14Q",
    },  # noqa
    "scaffold": {
        "value": 318,
        "label": "Scaffold",
        "ml_label": "scaffold",
        "roc": "3 yrs",
    },  # noqa
    "scanclimber": {
        "value": 319,
        "label": "Scanclimber",
        "ml_label": "scanclimber",
        "roc": "3 yrs",
    },  # noqa
    "scanner": {
        "value": 320,
        "label": "Scanner",
        "ml_label": "scanner",
        "roc": "1 yr",
    },  # noqa
    "scientific equipment with software": {
        "value": 321,
        "label": "Scientific Equipment With Software",
        "ml_label": "scientific equipment with software",
        "roc": "1 yr",
    },  # noqa
    "scope": {
        "value": 322,
        "label": "Scope",
        "ml_label": "scope",
        "roc": "3 yrs",
    },  # noqa
    "sculpture": {
        "value": 323,
        "label": "Sculpture",
        "ml_label": "sculpture",
        "roc": "Tba",
    },  # noqa
    "seabed survey equipment": {
        "value": 324,
        "label": "Seabed Survey Equipment",
        "ml_label": "seabed survey equipment",
        "roc": "1 yr",
    },  # noqa
    "security film": {
        "value": 325,
        "label": "Security Film",
        "ml_label": "security film",
        "roc": "Tba",
    },  # noqa
    "security system": {
        "value": 326,
        "label": "Security System",
        "ml_label": "security system",
        "roc": "3 yrs",
    },  # noqa
    "self dumping bucket": {
        "value": 327,
        "label": "Self Dumping Bucket",
        "ml_label": "self dumping bucket",
        "roc": "3 yrs",
    },  # noqa
    "servingware": {
        "value": 328,
        "label": "Servingware",
        "ml_label": "servingware",
        "roc": "3 yrs",
    },  # noqa
    "showcase": {
        "value": 329,
        "label": "Showcase",
        "ml_label": "showcase",
        "roc": "3 yrs",
    },  # noqa
    "shower panels": {
        "value": 330,
        "label": "Shower Panels",
        "ml_label": "shower panels",
        "roc": "S14Q",
    },  # noqa
    "side post parasol": {
        "value": 331,
        "label": "Side Post Parasol",
        "ml_label": "side post parasol",
        "roc": "3 yrs",
    },  # noqa
    "signage": {
        "value": 332,
        "label": "Signage",
        "ml_label": "signage",
        "roc": "3 yrs",
    },  # noqa
    "silo": {"value": 333, "label": "Silo", "ml_label": "silo", "roc": "3 yrs"},
    # noqa
    "simulation kit": {
        "value": 334,
        "label": "Simulation Kit",
        "ml_label": "simulation kit",
        "roc": "3 yrs",
    },  # noqa
    "simulator": {
        "value": 335,
        "label": "Simulator",
        "ml_label": "simulator",
        "roc": "3 yrs",
    },  # noqa
    "slit lamp": {
        "value": 336,
        "label": "Slit Lamp",
        "ml_label": "slit lamp",
        "roc": "3 yrs",
    },  # noqa
    "small tools": {
        "value": 337,
        "label": "Small Tools",
        "ml_label": "small tools",
        "roc": "Renewal Basis",
    },  # noqa
    "software": {
        "value": 338,
        "label": "Software",
        "ml_label": "software",
        "roc": "1 yr",
    },  # noqa
    "software-defined vehicle": {
        "value": 339,
        "label": "Software-Defined Vehicle",
        "ml_label": "software-defined vehicle",
        "roc": "1 yr",
    },  # noqa
    "solar marine hazard light": {
        "value": 340,
        "label": "Solar Marine Hazard Light",
        "ml_label": "solar marine hazard light",
        "roc": "3 yrs",
    },  # noqa
    "solar panel system": {
        "value": 341,
        "label": "Solar Panel System",
        "ml_label": "solar panel system",
        "roc": "3 yrs",
    },  # noqa
    "stacker": {
        "value": 342,
        "label": "Stacker",
        "ml_label": "stacker",
        "roc": "3 yrs",
    },  # noqa
    "stand alone lamp": {
        "value": 343,
        "label": "Stand Alone Lamp",
        "ml_label": "stand alone lamp",
        "roc": "3 yrs",
    },  # noqa
    "stationery": {
        "value": 344,
        "label": "Stationery",
        "ml_label": "stationery",
        "roc": "Revenue",
    },  # noqa
    "storage equipment": {
        "value": 345,
        "label": "Storage Equipment",
        "ml_label": "storage equipment",
        "roc": "3 yrs",
    },  # noqa
    "storage fee": {
        "value": 346,
        "label": "Storage Fee",
        "ml_label": "storage fee",
        "roc": "Revenue",
    },  # noqa
    "strobe light": {
        "value": 347,
        "label": "Strobe Light",
        "ml_label": "strobe light",
        "roc": "3 yrs",
    },  # noqa
    "strolley bag": {
        "value": 348,
        "label": "Strolley Bag",
        "ml_label": "strolley bag",
        "roc": "3 yrs",
    },  # noqa
    "super insulated vacuum line": {
        "value": 349,
        "label": "Super Insulated Vacuum Line",
        "ml_label": "super insulated vacuum line",
        "roc": "3 yrs",
    },  # noqa
    "surgical robot": {
        "value": 350,
        "label": "Surgical Robot",
        "ml_label": "surgical robot",
        "roc": "1 yr",
    },  # noqa
    "survey equipment": {
        "value": 351,
        "label": "Survey Equipment",
        "ml_label": "survey equipment",
        "roc": "1 yr",
    },  # noqa
    "tableware": {
        "value": 352,
        "label": "Tableware",
        "ml_label": "tableware",
        "roc": "3 yrs",
    },  # noqa
    "tape library": {
        "value": 353,
        "label": "Tape Library",
        "ml_label": "tape library",
        "roc": "1 yr",
    },  # noqa
    "taxi": {"value": 354, "label": "Taxi", "ml_label": "taxi", "roc": "3 yrs"},
    # noqa
    "telephone charges": {
        "value": 355,
        "label": "Telephone Charges",
        "ml_label": "telephone charges",
        "roc": "Revenue",
    },  # noqa
    "telephone system": {
        "value": 356,
        "label": "Telephone System",
        "ml_label": "telephone system",
        "roc": "3 yrs",
    },  # noqa
    "telescope": {
        "value": 357,
        "label": "Telescope",
        "ml_label": "telescope",
        "roc": "3 yrs",
    },  # noqa
    "temperature monitoring system": {
        "value": 358,
        "label": "Temperature Monitoring System",
        "ml_label": "temperature monitoring system",
        "roc": "1 yr",
    },  # noqa
    "test card": {
        "value": 359,
        "label": "Test Card",
        "ml_label": "test card",
        "roc": "3 yrs",
    },  # noqa
    "test equipment": {
        "value": 360,
        "label": "Test Equipment",
        "ml_label": "test equipment",
        "roc": "3 yrs",
    },  # noqa
    "test equipment with software": {
        "value": 361,
        "label": "Test Equipment With Software",
        "ml_label": "test equipment with software",
        "roc": "1 yr",
    },  # noqa
    "thermal imager": {
        "value": 362,
        "label": "Thermal Imager",
        "ml_label": "thermal imager",
        "roc": "1 yr",
    },  # noqa
    "thermal temperature scanning system": {
        "value": 363,
        "label": "Thermal Temperature Scanning System",
        "ml_label": "thermal temperature scanning system",
        "roc": "3 yrs",
    },  # noqa
    "tiling works": {
        "value": 364,
        "label": "Tiling Works",
        "ml_label": "tiling works",
        "roc": "S14Q",
    },  # noqa
    "time recorder": {
        "value": 365,
        "label": "Time Recorder",
        "ml_label": "time recorder",
        "roc": "3 yrs",
    },  # noqa
    "toiletries dispenser": {
        "value": 366,
        "label": "Toiletries Dispenser",
        "ml_label": "toiletries dispenser",
        "roc": "3 yrs",
    },  # noqa
    "toolings": {
        "value": 367,
        "label": "Toolings",
        "ml_label": "toolings",
        "roc": "Replacement Basis",
    },  # noqa
    "tracker": {
        "value": 368,
        "label": "Tracker",
        "ml_label": "tracker",
        "roc": "3 yrs",
    },  # noqa
    "traffic alert and collision avoidance system": {
        "value": 369,
        "label": "Traffic Alert And Collision Avoidance System",
        "ml_label": "traffic alert and collision avoidance system",
        "roc": "3 yrs",
    },  # noqa
    "traffic light": {
        "value": 370,
        "label": "Traffic Light",
        "ml_label": "traffic light",
        "roc": "No Claim",
    },  # noqa
    "trailer": {
        "value": 371,
        "label": "Trailer",
        "ml_label": "trailer",
        "roc": "3 yrs",
    },  # noqa
    "transferred assets": {
        "value": 372,
        "label": "Transferred Assets",
        "ml_label": "transferred assets",
        "roc": "Tba",
    },  # noqa
    "transformer": {
        "value": 373,
        "label": "Transformer",
        "ml_label": "transformer",
        "roc": "3 yrs",
    },  # noqa
    "tricycle": {
        "value": 374,
        "label": "Tricycle",
        "ml_label": "tricycle",
        "roc": "3 yrs",
    },  # noqa
    "trolley": {
        "value": 375,
        "label": "Trolley",
        "ml_label": "trolley",
        "roc": "3 yrs",
    },  # noqa
    "truck": {
        "value": 376,
        "label": "Truck",
        "ml_label": "truck",
        "roc": "3 yrs",
    },  # noqa
    "ultrasound machine": {
        "value": 377,
        "label": "Ultrasound Machine",
        "ml_label": "ultrasound machine",
        "roc": "1 yr",
    },  # noqa
    "ups": {"value": 378, "label": "Ups", "ml_label": "ups", "roc": "3 yrs"},
    # noqa
    "utilities meter": {
        "value": 379,
        "label": "Utilities Meter",
        "ml_label": "utilities meter",
        "roc": "3 yrs",
    },  # noqa
    "utility charges": {
        "value": 380,
        "label": "Utility Charges",
        "ml_label": "utility charges",
        "roc": "Revenue",
    },  # noqa
    "van": {"value": 381, "label": "Van", "ml_label": "van", "roc": "3 yrs"},
    # noqa
    "vault": {
        "value": 382,
        "label": "Vault",
        "ml_label": "vault",
        "roc": "3 yrs",
    },  # noqa
    "vehicle camera system": {
        "value": 383,
        "label": "Vehicle Camera System",
        "ml_label": "vehicle camera system",
        "roc": "Tba",
    },  # noqa
    "vehicle inspection system": {
        "value": 384,
        "label": "Vehicle Inspection System",
        "ml_label": "vehicle inspection system",
        "roc": "3 yrs",
    },  # noqa
    "video conference system": {
        "value": 385,
        "label": "Video Conference System",
        "ml_label": "video conference system",
        "roc": "1 yr",
    },  # noqa
    "video inspection probe": {
        "value": 386,
        "label": "Video Inspection Probe",
        "ml_label": "video inspection probe",
        "roc": "1 yr",
    },  # noqa
    "visitor management system": {
        "value": 387,
        "label": "Visitor Management System",
        "ml_label": "visitor management system",
        "roc": "1 yr",
    },  # noqa
    "vital signs monitor": {
        "value": 388,
        "label": "Vital Signs Monitor",
        "ml_label": "vital signs monitor",
        "roc": "1 yr",
    },  # noqa
    "walkie talkie": {
        "value": 389,
        "label": "Walkie Talkie",
        "ml_label": "walkie talkie",
        "roc": "3 yrs",
    },  # noqa
    "wall works": {
        "value": 390,
        "label": "Wall Works",
        "ml_label": "wall works",
        "roc": "S14Q",
    },  # noqa
    "wallpaper": {
        "value": 391,
        "label": "Wallpaper",
        "ml_label": "wallpaper",
        "roc": "S14Q",
    },  # noqa
    "waste disposal unit": {
        "value": 392,
        "label": "Waste Disposal Unit",
        "ml_label": "waste disposal unit",
        "roc": "3 yrs",
    },  # noqa
    "waste pump system": {
        "value": 393,
        "label": "Waste Pump System",
        "ml_label": "waste pump system",
        "roc": "3 yrs",
    },  # noqa
    "waste treatment system": {
        "value": 394,
        "label": "Waste Treatment System",
        "ml_label": "waste treatment system",
        "roc": "3 yrs",
    },  # noqa
    "water chiller system": {
        "value": 395,
        "label": "Water Chiller System",
        "ml_label": "water chiller system",
        "roc": "3 yrs",
    },  # noqa
    "water purifier": {
        "value": 396,
        "label": "Water Purifier",
        "ml_label": "water purifier",
        "roc": "3 yrs",
    },  # noqa
    "water system": {
        "value": 397,
        "label": "Water System",
        "ml_label": "water system",
        "roc": "S14Q",
    },  # noqa
    "water treatment system": {
        "value": 398,
        "label": "Water Treatment System",
        "ml_label": "water treatment system",
        "roc": "3 yrs",
    },  # noqa
    "website": {
        "value": 399,
        "label": "Website",
        "ml_label": "website",
        "roc": "1 yr",
    },  # noqa
    "weight height measuring machine with bmi system": {
        "value": 400,
        "label": "Weight Height Measuring Machine With Bmi System",
        "ml_label": "weight height measuring machine with bmi system",
        "roc": "1 yr",
    },  # noqa
    "wheelchair": {
        "value": 401,
        "label": "Wheelchair",
        "ml_label": "wheelchair",
        "roc": "3 yrs",
    },  # noqa
    "whiteboard": {
        "value": 402,
        "label": "Whiteboard",
        "ml_label": "whiteboard",
        "roc": "3 yrs",
    },  # noqa
    "wireless ultrasonic measuring station": {
        "value": 403,
        "label": "Wireless Ultrasonic Measuring Station",
        "ml_label": "wireless ultrasonic measuring station",
        "roc": "1 yr",
    },  # noqa
    "wireless urinary flow meter": {
        "value": 404,
        "label": "Wireless Urinary Flow Meter",
        "ml_label": "wireless urinary flow meter",
        "roc": "1 yr",
    },  # noqa
    "wiring harness design": {
        "value": 405,
        "label": "Wiring Harness Design",
        "ml_label": "wiring harness design",
        "roc": "3 yrs",
    },  # noqa
    "work assist vehicle": {
        "value": 406,
        "label": "Work Assist Vehicle",
        "ml_label": "work assist vehicle",
        "roc": "3 yrs",
    },  # noqa
    "workshop equipment": {
        "value": 407,
        "label": "Workshop Equipment",
        "ml_label": "workshop equipment",
        "roc": "3 yrs",
    },  # noqa
    "x-ray system": {
        "value": 408,
        "label": "X-Ray System",
        "ml_label": "x-ray system",
        "roc": "3 yrs",
    },  # noqa
    "electrical door operating system": {
        "value": 409,
        "label": "Electrical Door Operating System",
        "ml_label": "electrical door operating system",
        "roc": "3 yrs",
    },  # noqa
    "business events expenditure": {
        "value": 410,
        "label": "Business Events Expenditure",
        "ml_label": "business events expenditure",
        "roc": "Revenue",
    },  # noqa
    "interactive white board": {
        "value": 411,
        "label": "Interactive White Board",
        "ml_label": "interactive white board",
        "roc": "1 yr",
    },  # noqa
    "apportionment": {
        "value": 412,
        "label": "Apportionment",
        "ml_label": "apportionment",
        "roc": "Tba",
    },  # noqa
    "other": {
        "value": 413,
        "label": "Other",
        "ml_label": ".other",
        "roc": "No Claim",
    },  # noqa
}


AI_TAX_CLASSES_V2 = {
    "air supply equipment": {
        "value": 1,
        "label": "Air Supply Equipment",
        "ml_label": "air supply equipment",
        "roc": "3 yrs",
    },
    "aircraft computerised system": {
        "value": 2,
        "label": "Aircraft Computerised System",
        "ml_label": "aircraft computerised system",
        "roc": "1 yr",
    },
    "aircraft equipment": {
        "value": 3,
        "label": "Aircraft Equipment",
        "ml_label": "aircraft equipment",
        "roc": "3 yrs",
    },
    "antique": {"value": 4, "label": "Antique", "ml_label": "antique", "roc": "tba"},
    "audio equipment": {
        "value": 5,
        "label": "Audio Equipment",
        "ml_label": "audio equipment",
        "roc": "3 yrs",
    },
    "audio visual equipment": {
        "value": 6,
        "label": "Audio Visual Equipment",
        "ml_label": "audio visual equipment",
        "roc": "3 yrs",
    },
    "automated equipment": {
        "value": 7,
        "label": "Automated Equipment",
        "ml_label": "automated equipment",
        "roc": "1 yr",
    },
    "automated guided vehicle": {
        "value": 8,
        "label": "Automated Guided Vehicle",
        "ml_label": "automated guided vehicle",
        "roc": "1 yr",
    },
    "automated machinery": {
        "value": 9,
        "label": "Automated Machinery",
        "ml_label": "automated machinery",
        "roc": "1 yr",
    },
    "automated medical device": {
        "value": 10,
        "label": "Automated Medical Device",
        "ml_label": "automated medical device",
        "roc": "1 yr",
    },
    "bicycle": {"value": 11, "label": "Bicycle", "ml_label": "bicycle", "roc": "3 yrs"},
    "building automation system": {
        "value": 12,
        "label": "Building Automation System",
        "ml_label": "building automation system",
        "roc": "1 yr",
    },
    "building structure": {
        "value": 13,
        "label": "Building Structure",
        "ml_label": "building structure",
        "roc": "No Claim",
    },
    "cable": {"value": 14, "label": "Cable", "ml_label": "cable", "roc": "tba"},
    "cleaning equipment": {
        "value": 15,
        "label": "Cleaning Equipment",
        "ml_label": "cleaning equipment",
        "roc": "3 yrs",
    },
    "computer aided equipment": {
        "value": 16,
        "label": "Computer Aided Equipment",
        "ml_label": "computer aided equipment",
        "roc": "1 yr",
    },
    "computer network equipment": {
        "value": 17,
        "label": "Computer Network Equipment",
        "ml_label": "computer network equipment",
        "roc": "1 yr",
    },
    "construction equipment": {
        "value": 18,
        "label": "Construction Equipment",
        "ml_label": "construction equipment",
        "roc": "3 yrs",
    },
    "consumables": {
        "value": 19,
        "label": "Consumables",
        "ml_label": "consumables",
        "roc": "Revenue",
    },
    "container office": {
        "value": 20,
        "label": "Container Office",
        "ml_label": "container office",
        "roc": "No Claim",
    },
    "customised fencing system": {
        "value": 21,
        "label": "Customised Fencing System",
        "ml_label": "customised fencing system",
        "roc": "tba",
    },
    "data handling equipment": {
        "value": 22,
        "label": "Data Handling Equipment",
        "ml_label": "data handling equipment",
        "roc": "3 yrs",
    },
    "data processing and it equipment": {
        "value": 23,
        "label": "Data Processing And It Equipment",
        "ml_label": "data processing and it equipment",
        "roc": "1 yr",
    },
    "decorative lighting": {
        "value": 24,
        "label": "Decorative Lighting",
        "ml_label": "decorative lighting",
        "roc": "tba",
    },
    "decorative structures": {
        "value": 25,
        "label": "Decorative Structures",
        "ml_label": "decorative structures",
        "roc": "tba",
    },
    "electrical equipment": {
        "value": 26,
        "label": "Electrical Equipment",
        "ml_label": "electrical equipment",
        "roc": "3 yrs",
    },
    "electricity supply installation": {
        "value": 27,
        "label": "Electricity Supply Installation",
        "ml_label": "electricity supply installation",
        "roc": "S14Q",
    },
    "electronic equipment": {
        "value": 28,
        "label": "Electronic Equipment",
        "ml_label": "electronic equipment",
        "roc": "3 yrs",
    },
    "equipment parts": {
        "value": 29,
        "label": "Equipment Parts",
        "ml_label": "equipment parts",
        "roc": "3 yrs",
    },
    "equipment with software": {
        "value": 30,
        "label": "Equipment With Software",
        "ml_label": "equipment with software",
        "roc": "1 yr",
    },
    "exercise equipment": {
        "value": 31,
        "label": "Exercise Equipment",
        "ml_label": "exercise equipment",
        "roc": "3 yrs",
    },
    "fire alarm and fire fighting installation": {
        "value": 32,
        "label": "Fire Alarm And Fire Fighting Installation",
        "ml_label": "fire alarm and fire fighting installation",
        "roc": "3 yrs",
    },
    "freight container": {
        "value": 33,
        "label": "Freight Container",
        "ml_label": "freight container",
        "roc": "3 yrs",
    },
    "freight, installation cost": {
        "value": 34,
        "label": "Freight, Installation Cost",
        "ml_label": "freight, installation cost",
        "roc": "tba",
    },
    "furnishings": {
        "value": 35,
        "label": "Furnishings",
        "ml_label": "furnishings",
        "roc": "3 yrs",
    },
    "furniture and fixtures": {
        "value": 36,
        "label": "Furniture And Fixtures",
        "ml_label": "furniture and fixtures",
        "roc": "3 yrs",
    },
    "gas supply equipment": {
        "value": 37,
        "label": "Gas Supply Equipment",
        "ml_label": "gas supply equipment",
        "roc": "3 yrs",
    },
    "general lighting": {
        "value": 38,
        "label": "General Lighting",
        "ml_label": "general lighting",
        "roc": "S14Q",
    },
    "generator": {
        "value": 39,
        "label": "Generator",
        "ml_label": "generator",
        "roc": "3 yrs",
    },
    "green mark certification": {
        "value": 40,
        "label": "Green Mark Certification",
        "ml_label": "green mark certification",
        "roc": "No Claim",
    },
    "hangar": {"value": 41, "label": "Hangar", "ml_label": "hangar", "roc": "3 yrs"},
    "image processing equipment": {
        "value": 42,
        "label": "Image Processing Equipment",
        "ml_label": "image processing equipment",
        "roc": "1 yr",
    },
    "industrial processing equipment": {
        "value": 43,
        "label": "Industrial Processing Equipment",
        "ml_label": "industrial processing equipment",
        "roc": "3 yrs",
    },
    "kitchen equipment": {
        "value": 44,
        "label": "Kitchen Equipment",
        "ml_label": "kitchen equipment",
        "roc": "3 yrs",
    },
    "landscaping": {
        "value": 45,
        "label": "Landscaping",
        "ml_label": "landscaping",
        "roc": "No Claim",
    },
    "laser equipment": {
        "value": 46,
        "label": "Laser Equipment",
        "ml_label": "laser equipment",
        "roc": "1 yr",
    },
    "learning kits": {
        "value": 47,
        "label": "Learning Kits",
        "ml_label": "learning kits",
        "roc": "3 yrs",
    },
    "legal agreement": {
        "value": 48,
        "label": "Legal Agreement",
        "ml_label": "legal agreement",
        "roc": "tba",
    },
    "lift shaft": {
        "value": 49,
        "label": "Lift Shaft",
        "ml_label": "lift shaft",
        "roc": "tba",
    },
    "machinery": {
        "value": 50,
        "label": "Machinery",
        "ml_label": "machinery",
        "roc": "3 yrs",
    },
    "mannequin": {
        "value": 51,
        "label": "Mannequin",
        "ml_label": "mannequin",
        "roc": "3 yrs",
    },
    "material handling equipment": {
        "value": 52,
        "label": "Material Handling Equipment",
        "ml_label": "material handling equipment",
        "roc": "3 yrs",
    },
    "measuring equipment": {
        "value": 53,
        "label": "Measuring Equipment",
        "ml_label": "measuring equipment",
        "roc": "3 yrs",
    },
    "medical equipment": {
        "value": 54,
        "label": "Medical Equipment",
        "ml_label": "medical equipment",
        "roc": "3 yrs",
    },
    "motor cycle": {
        "value": 55,
        "label": "Motor Cycle",
        "ml_label": "motor cycle",
        "roc": "3 yrs",
    },
    "motor vehicle": {
        "value": 56,
        "label": "Motor Vehicle",
        "ml_label": "motor vehicle",
        "roc": "tba",
    },
    "moving fee": {
        "value": 57,
        "label": "Moving Fee",
        "ml_label": "moving fee",
        "roc": "tba",
    },
    "mural": {"value": 58, "label": "Mural", "ml_label": "mural", "roc": "tba"},
    "navigation devices": {
        "value": 59,
        "label": "Navigation Devices",
        "ml_label": "navigation devices",
        "roc": "3 yrs",
    },
    "network communication equipment": {
        "value": 60,
        "label": "Network Communication Equipment",
        "ml_label": "network communication equipment",
        "roc": "1 yr",
    },
    "office equipment": {
        "value": 61,
        "label": "Office Equipment",
        "ml_label": "office equipment",
        "roc": "3 yrs",
    },
    "other vehicle": {
        "value": 62,
        "label": "Other Vehicle",
        "ml_label": "other vehicle",
        "roc": "tba",
    },
    "painting works": {
        "value": 63,
        "label": "Painting Works",
        "ml_label": "painting works",
        "roc": "S14Q",
    },
    "paintings": {
        "value": 64,
        "label": "Paintings",
        "ml_label": "paintings",
        "roc": "tba",
    },
    "passenger handling equipment": {
        "value": 65,
        "label": "Passenger Handling Equipment",
        "ml_label": "passenger handling equipment",
        "roc": "3 yrs",
    },
    "pictures": {
        "value": 66,
        "label": "Pictures",
        "ml_label": "pictures",
        "roc": "tba",
    },
    "piping system": {
        "value": 67,
        "label": "Piping System",
        "ml_label": "piping system",
        "roc": "tba",
    },
    "portable toilet": {
        "value": 68,
        "label": "Portable Toilet",
        "ml_label": "portable toilet",
        "roc": "tba",
    },
    "private car": {
        "value": 69,
        "label": "Private Car",
        "ml_label": "private car",
        "roc": "No Claim",
    },
    "production tools": {
        "value": 70,
        "label": "Production Tools",
        "ml_label": "production tools",
        "roc": "3 yrs",
    },
    "professional fees": {
        "value": 71,
        "label": "Professional Fees",
        "ml_label": "professional fees",
        "roc": "No Claim",
    },
    "prototype equipment": {
        "value": 72,
        "label": "Prototype Equipment",
        "ml_label": "prototype equipment",
        "roc": "3 yrs",
    },
    "religious altar": {
        "value": 73,
        "label": "Religious Altar",
        "ml_label": "religious altar",
        "roc": "tba",
    },
    "relocation of plant and machinery": {
        "value": 74,
        "label": "Relocation Of Plant And Machinery",
        "ml_label": "relocation of plant and machinery",
        "roc": "3 yrs",
    },
    "renovation": {
        "value": 75,
        "label": "Renovation",
        "ml_label": "renovation",
        "roc": "S14Q",
    },
    "revenue expenditure": {
        "value": 76,
        "label": "Revenue Expenditure",
        "ml_label": "revenue expenditure",
        "roc": "Revenue",
    },
    "robot": {"value": 77, "label": "Robot", "ml_label": "robot", "roc": "1 yr"},
    "safety equipment": {
        "value": 78,
        "label": "Safety Equipment",
        "ml_label": "safety equipment",
        "roc": "3 yrs",
    },
    "sanitary fittings": {
        "value": 79,
        "label": "Sanitary Fittings",
        "ml_label": "sanitary fittings",
        "roc": "S14Q",
    },
    "sculpture": {
        "value": 80,
        "label": "Sculpture",
        "ml_label": "sculpture",
        "roc": "tba",
    },
    "security film": {
        "value": 81,
        "label": "Security Film",
        "ml_label": "security film",
        "roc": "tba",
    },
    "security/alarm system": {
        "value": 82,
        "label": "Security/Alarm System",
        "ml_label": "security/alarm system",
        "roc": "3 yrs",
    },
    "servingware": {
        "value": 83,
        "label": "Servingware",
        "ml_label": "servingware",
        "roc": "3 yrs",
    },
    "software": {
        "value": 84,
        "label": "Software",
        "ml_label": "software",
        "roc": "1 yr",
    },
    "storage equipment": {
        "value": 85,
        "label": "Storage Equipment",
        "ml_label": "storage equipment",
        "roc": "3 yrs",
    },
    "taxi": {"value": 86, "label": "Taxi", "ml_label": "taxi", "roc": "3 yrs"},
    "telecommunication equipment": {
        "value": 87,
        "label": "Telecommunication Equipment",
        "ml_label": "telecommunication equipment",
        "roc": "3 yrs",
    },
    "toolings": {
        "value": 88,
        "label": "Toolings",
        "ml_label": "toolings",
        "roc": "Replacement basis",
    },
    "transferred assets": {
        "value": 89,
        "label": "Transferred Assets",
        "ml_label": "transferred assets",
        "roc": "tba",
    },
    "transport equipment": {
        "value": 90,
        "label": "Transport Equipment",
        "ml_label": "transport equipment",
        "roc": "3 yrs",
    },
    "vault": {"value": 91, "label": "Vault", "ml_label": "vault", "roc": "3 yrs"},
    "vehicle camera system": {
        "value": 92,
        "label": "Vehicle Camera System",
        "ml_label": "vehicle camera system",
        "roc": "tba",
    },
    "vehicle inspection system": {
        "value": 93,
        "label": "Vehicle Inspection System",
        "ml_label": "vehicle inspection system",
        "roc": "3yrs",
    },
    "warranty": {
        "value": 94,
        "label": "Warranty",
        "ml_label": "warranty",
        "roc": "tba",
    },
    "water purification equipment": {
        "value": 95,
        "label": "Water Purification Equipment",
        "ml_label": "water purification equipment",
        "roc": "3 yrs",
    },
    "water transportation equipment": {
        "value": 96,
        "label": "Water Transportation Equipment",
        "ml_label": "water transportation equipment",
        "roc": "3 yrs",
    },
    "website": {"value": 97, "label": "Website", "ml_label": "website", "roc": "1 yr"},
    "workshop equipment": {
        "value": 98,
        "label": "Workshop Equipment",
        "ml_label": "workshop equipment",
        "roc": "3 yrs",
    },
    "other": {"value": 99, "label": "Other", "ml_label": "other", "roc": "No Claim"},
}

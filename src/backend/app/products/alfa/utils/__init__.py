from backend.lib.utils.formatters import present_dollar_field


def get_employee_wage(reduction_list, employee_name):
    obj = list(
        filter(
            lambda x: x.get("Employee name") == employee_name.replace("\n", " "),
            reduction_list,
        )
    )
    if obj:
        return present_dollar_field(obj[0].get("Reduction", ""))
    return ""


def add_schedule_a_data(cd, newvals, worksheet_extra_data):
    schedule_a_worksheet = cd.get("schedule_a_worksheet")

    if len(schedule_a_worksheet) <= 8:
        for i, obj in enumerate(schedule_a_worksheet[:-1]):
            newvals.update(
                {
                    "Employees NameRow"
                    + str((i + 1)): (
                        schedule_a_worksheet[i]["Employee name"].replace("\n", " ")
                    ),  # noqa
                    "Employee IdentifierRow"
                    + str((i + 1)): (schedule_a_worksheet[i]["SSN (last 4 digits)"]),
                    "Cash CompensationRow"
                    + str((i + 1)): (schedule_a_worksheet[i]["Adjusted covered wages"]),
                    "Average FTERow" + str((i + 1)): schedule_a_worksheet[i]["FTE"],
                    "Salary  Hourly Wage ReductionRow"
                    + str((i + 1)): (
                        get_employee_wage(
                            cd.get("reduction", []),
                            schedule_a_worksheet[i]["Employee name"],
                        )
                    ),
                }
            )

        newvals.update(
            {
                "Box 1": present_dollar_field(cd.get("box_1", "")),
                "Box 2": present_dollar_field(cd.get("box_2", "")).strip("$"),
                "Box 3": present_dollar_field(cd.get("box_3", "")),
            }
        )

    else:
        for i, obj in enumerate(schedule_a_worksheet[:8]):
            newvals.update(
                {
                    "Employees NameRow"
                    + str((i + 1)): (
                        schedule_a_worksheet[i]["Employee name"].replace("\n", " ")
                    ),  # noqa
                    "Employee IdentifierRow"
                    + str((i + 1)): (schedule_a_worksheet[i]["SSN (last 4 digits)"]),
                    "Cash CompensationRow"
                    + str((i + 1)): (schedule_a_worksheet[i]["Adjusted covered wages"]),
                    "Average FTERow" + str((i + 1)): schedule_a_worksheet[i]["FTE"],
                    "Salary  Hourly Wage ReductionRow"
                    + str((i + 1)): (
                        get_employee_wage(
                            cd.get("reduction", []),
                            schedule_a_worksheet[i]["Employee name"],
                        )
                    ),
                }
            )

        for i in range(8, len(schedule_a_worksheet)):
            worksheet_extra_data.append(
                [
                    schedule_a_worksheet[i]["Employee name"].replace("\n", " "),
                    schedule_a_worksheet[i]["SSN (last 4 digits)"],
                    schedule_a_worksheet[i]["Adjusted covered wages"],
                    schedule_a_worksheet[i]["FTE"],
                    get_employee_wage(
                        cd.get("reduction", []),
                        schedule_a_worksheet[i]["Employee name"],
                    ),
                ]
            )

        worksheet_extra_data[-1][-3] = present_dollar_field(cd.get("box_1", ""))
        worksheet_extra_data[-1][-2] = present_dollar_field(cd.get("box_2", "")).strip(
            "$"
        )
        worksheet_extra_data[-1][-1] = present_dollar_field(cd.get("box_3", ""))


def add_schedule_a_more_than_100k_data(
    cd, newvals, worksheet_more_than_100k_extra_data
):
    schedule_a_worksheet_more_than_100k = cd.get("schedule_a_worksheet_more_than_100k")

    if len(schedule_a_worksheet_more_than_100k) <= 5:
        for i, obj in enumerate(schedule_a_worksheet_more_than_100k[:-1]):
            emp_100k = {
                "Employees NameRow"
                + str((i + 1))
                + "_2": (
                    schedule_a_worksheet_more_than_100k[i]["Employee name"].replace(
                        "\n", " "
                    )
                ),
                "Employee IdentifierRow"
                + str((i + 1))
                + "_2": (schedule_a_worksheet_more_than_100k[i]["SSN (last 4 digits)"]),
                "Cash CompensationRow"
                + str((i + 1))
                + "_2": (
                    schedule_a_worksheet_more_than_100k[i]["Adjusted covered wages"]
                ),
                "Average FTERow"
                + str((i + 1))
                + "_2": (schedule_a_worksheet_more_than_100k[i]["FTE"]),
            }
            newvals.update(emp_100k)

        newvals["Box 4"] = present_dollar_field(cd.get("box_4", ""))
        newvals["Box 5"] = present_dollar_field(cd.get("box_5", "")).strip("$")

    elif len(schedule_a_worksheet_more_than_100k) > 5:
        for i, obj in enumerate(schedule_a_worksheet_more_than_100k[:-5]):
            emp_100k = {
                "Employees NameRow"
                + str((i + 1))
                + "_2": (
                    schedule_a_worksheet_more_than_100k[i]["Employee name"].replace(
                        "\n", " "
                    )
                ),
                "Employee IdentifierRow"
                + str((i + 1))
                + "_2": (schedule_a_worksheet_more_than_100k[i]["SSN (last 4 digits)"]),
                "Cash CompensationRow"
                + str((i + 1))
                + "_2": (
                    schedule_a_worksheet_more_than_100k[i]["Adjusted covered wages"]
                ),
                "Average FTERow"
                + str((i + 1))
                + "_2": (schedule_a_worksheet_more_than_100k[i]["FTE"]),
            }
            newvals.update(emp_100k)

        for i in range(5, len(schedule_a_worksheet_more_than_100k)):
            worksheet_more_than_100k_extra_data.append(
                [
                    schedule_a_worksheet_more_than_100k[i]["Employee name"].replace(
                        "\n", " "
                    ),  # noqa
                    schedule_a_worksheet_more_than_100k[i][
                        "SSN (last 4 digits)"
                    ],  # noqa
                    schedule_a_worksheet_more_than_100k[i][
                        "Adjusted covered wages"
                    ],  # noqa
                    schedule_a_worksheet_more_than_100k[i]["FTE"],
                ]
            )

        worksheet_more_than_100k_extra_data[-1][-2] = present_dollar_field(
            cd.get("box_3", "")
        )
        worksheet_more_than_100k_extra_data[-1][-1] = present_dollar_field(
            cd.get("box_4", "")
        ).strip("$")

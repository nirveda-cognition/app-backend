from django.apps import AppConfig


class KpmgConfig(AppConfig):
    name = "backend.app.products.kpmg"

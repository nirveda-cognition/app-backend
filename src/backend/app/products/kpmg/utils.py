import json
import logging
import sys

from backend.app.ai_services.constants import AIServicesService
from backend.app.ai_services.tasks import start_ai_text_extraction
from backend.app.document.models import Document
from backend.app.organization.exceptions import OrganizationCapabilityError
from backend.app.task.utils import convert_table_to_df

logger = logging.getLogger("backend")


def classify_as_tax(doc_pk):
    document = Document.objects.get(pk=doc_pk)
    if not document.organization.has_capability(
        service=AIServicesService.DOCUMENT_EXTRACTOR, capability="tax_classifier"
    ):
        raise OrganizationCapabilityError(
            organization=document.organization,
            service=AIServicesService.DOCUMENT_EXTRACTOR,
            capability="tax_classifier",
        )

    document.started_processing()
    document.save()

    result = document.task.results.latest()

    # This is a temporary fix - creating a new task is disassociating the task
    # results from the document, because of wonky modeling.
    # document.task = Task.objects.create(
    #     ai_uuid=str(UUID.uuid4()),
    #     organization=document.organization
    # )
    # document.task.request = {
    #     'task_type': Task.TYPE_TAX_CLASSIFICATION,
    #     'task_detail': 'TAX_CLASSIFICATION'  # TODO: Ref from constant.
    # }
    # document.save()

    tables = []
    for table in result.new_data["data"]["extracted_data"]:
        if table["type"] == "collection" and table["subtype"] == "table":
            tables.append(table["items"])

    # Convert all tables in sheet to dfs
    dfs = []
    for table in tables:
        dfs.append(convert_table_to_df(table))

    # we either need to concatenate the results for all tables in the doc
    # or we need to create a mechanism to separate them out but keep track of
    # them

    # find which column is description based on basic match
    desc_list = []
    for df in dfs:
        if df is None:
            desc_list.append([])
            continue
        idx = 0
        found = False
        for col in df.columns:
            if "desc" in col.lower():
                found = True
                break
            idx += 1
        if found:
            desc_list.append(df.iloc[:, idx].to_list())
        else:
            desc_list.append([])

    # set up list to send to ai services
    # We can pass table index to task via final_table_list
    final_table_list = []
    for desc in desc_list:
        idx = 1
        desc_col = []
        for item in desc:
            desc_col.append({"id": idx, "value": item})
            idx += 1
        final_table_list.append(desc_col)
    logger.info("task payload >>>>>>>>> :{}".format(final_table_list))

    with open("task_payload.json", "w+") as f:
        f.write(json.dumps(final_table_list))

    logger.info(
        "payload size in bytes >>>>> : {}".format(sys.getsizeof(final_table_list))
    )
    start_ai_text_extraction.delay(
        document.pk, final_table_list, capability="tax_classifier"
    )

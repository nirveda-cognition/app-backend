from rest_framework import decorators, mixins, response, viewsets

from backend.app.ai_services.api import client
from backend.app.authentication.exceptions import OrganizationPermissionError
from backend.lib.rest_framework_utils.permissions import IsAdminOrSuperuser

from .models import Organization, OrganizationType
from .serializers import OrganizationSerializer, OrganizationTypeSerializer


class OrganizationTypeViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organization-types/
    """

    lookup_field = "pk"
    permission_classes = [IsAdminOrSuperuser]
    serializer_class = OrganizationTypeSerializer

    def get_queryset(self):
        return OrganizationType.objects.all()


class OrganizationViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """
    ViewSet to handle requests to the following endpoints:

    (1) GET /organizations/
    (2) POST /organizations/
    (3) GET /organizations/<pk>/
    (4) PATCH /organizations/<pk>/revoke-api-key/
    (5) PATCH /organizations/<pk>/generate-api-key/
    (6) PATCH /organizations/<pk>/generate-client-key/
    """

    lookup_field = "pk"
    permission_classes = [IsAdminOrSuperuser]
    serializer_class = OrganizationSerializer
    search_fields = ["name", "slug", "type__slug"]

    def get_queryset(self):
        return Organization.objects.all()

    def get_object(self):
        organization = super(OrganizationViewSet, self).get_object()
        # TODO: We should be applying this more systematically.
        if not self.request.user.has_organization_permission(organization):
            raise OrganizationPermissionError()
        return organization

    def perform_create(self, serializer):
        instance = serializer.save()
        client.generate_api_key(instance)

    @decorators.action(detail=True, methods=["PATCH"], url_path="revoke-api-key")
    def revoke_api_key(self, request, *args, **kwargs):
        instance = self.get_object()
        client.revoke_api_key(instance)
        return response.Response(self.serializer_class(instance).data, status=201)

    @decorators.action(detail=True, methods=["PATCH"], url_path="generate-api-key")
    def generate_api_key(self, request, *args, **kwargs):
        instance = self.get_object()
        client.generate_api_key(instance)
        return response.Response(self.serializer_class(instance).data, status=201)

from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from .models import Organization


class OrganizationNestedMixin(object):
    """
    A mixin for views that extend off of an organization's detail endpoint.
    """

    @property
    def organization_lookup_field(self):
        raise NotImplementedError()

    @cached_property
    def organization(self):
        params = {
            self.organization_lookup_field[0]: (
                self.kwargs[self.organization_lookup_field[1]]
            )
        }
        obj = get_object_or_404(Organization.objects.all(), **params)
        obj.raise_no_access(self.request.user)
        return obj

from django.db import models


class OrganizationTypeManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)


class OrganizationQuerier(object):
    def active(self):
        # pylint: disable=no-member
        return self.filter(is_active=True)

    def inactive(self):
        # pylint: disable=no-member
        return self.filter(is_active=False)


class OrganizationQuery(OrganizationQuerier, models.query.QuerySet):
    pass


class OrganizationManager(OrganizationQuerier, models.Manager):
    queryset_class = OrganizationQuery

    def get_queryset(self):
        return self.queryset_class(self.model)

import copy
import logging
import traceback
import uuid as UUID

import pandas as pd
import requests
from django.conf import settings
from django.utils import timezone

from backend.app.ai_services.constants import AIServicesService
from backend.app.collection.models import TrackedOperation
from backend.app.document.models import Document
from backend.app.products.alfa.tasks import start_loan_extraction
from backend.app.task.models import Chunks, Task, TaskResult
from backend.app.task.utils import (
    convert_df_to_table,
    merge_tax_data,
    normalize_table_headers,
)
from backend.conf.constants import API_USER_PROCESSING_ERROR
from backend.lib.aws.nv_s3_bucket import NVs3bucket
from backend.lib.celery_utils import retriable_task

from .api import client
from .api.exceptions import AIServicesHttpError, AttemptThresholdExceededError

logger = logging.getLogger("celery.task")

COMPLETED = "completed"
PENDING = "pending"


@retriable_task(name="backend.app.ai_services.tasks.process_document")
def process_document(doc_id):
    document = Document.objects.get(pk=doc_id)
    document.organization.validate_for_processing()

    if document.organization.type.slug == "alfa":
        # logger.info("Starting ALFA extraction flow.")
        start_loan_extraction.delay(document.pk)
        return True

    # TODO: Instead check if the capability is tax_classifier and if it is,
    # raise an exception if the extension if wrong.
    elif document.organization.has_capability(
        AIServicesService.DOCUMENT_EXTRACTOR, "tax_classifier"
    ) and document.type.lower() in ["xls", "xlsx", "csv"]:
        logger.info("Starting XLS extraction flow.")
        document.task.ai_uuid = str(UUID.uuid4())
        document.task.save()
        start_xls_extraction.delay(document.pk)
        return True

    else:
        logger.info("Starting normal AI extraction flow.")
        start_ai_extraction.delay(document.pk)
        return True


@retriable_task(bind=True, name="backend.app.ai_services.tasks.watch_for_results")
def watch_for_results(self, doc_id, preprocess=False, chunk_id=None):
    # check whether given task is for chunk or not
    # if yes then show chunk details otherwise display task details
    document = Document.objects.get(pk=doc_id)
    logger.info(
         "Waiting for AI results for AI task %s." % document.task.ai_uuid,
         extra={
             'document': document.pk,
             'ai_uuid': document.task.ai_uuid,
         }
     )
    try:
        logger.info(" document task id %s" % document.task.ai_uuid )
        if chunk_id is None:
            task_results = client.await_task_results(
                document.task.ai_uuid, api_key=document.organization.api_key
            )
        else:
            chunk_objs = Chunks.objects.filter(task_id=document.task.id, status=PENDING)
            for obj in chunk_objs:
                task_results = client.await_task_results(
                    obj.ai_uuid, api_key=document.organization.api_key
                )
                obj.ai_response = task_results
                obj.status = COMPLETED
                obj.save()

    except AIServicesHttpError as e:
        # TODO: Are there other circumstances that warrant a retry?
        if e.error is not None:
            logger.error(
                 'Timeout Error: Error fetching task results from AI services '
                 '- retrying...', extra={
                     'document': document.pk,
                     'error': str(e.error)
                 }
             )
            raise self.retry(exc=e, countdown=30, max_retries=2)

        logger.error(
             "HTTP Error: Task results could not be retrieved from "
             "AI Services.\n%s" % e
         )
        # TODO: Better classification of errors.
        callback_if_applicable.delay(document.task.pk, API_USER_PROCESSING_ERROR)
        doc_error = "Task Results Could Not be retrieved from AI Services."
        doc_error += str(traceback.format_exc())

        document.failed_processing(
            error_type=Task.ERROR_AI_TASK_RESULTS, error=doc_error
        )
        document.save()
        document.task.save()
    except AttemptThresholdExceededError as e:
        logger.error(
            "Attempt Threshold Exceeded Error: Task results could not be "
            "retrieved from AI Services.\n%s" % e
        )
        # TODO: Better classification of errors.
        callback_if_applicable.delay(document.task.pk, API_USER_PROCESSING_ERROR)
        document.failed_processing(
            error_type=Task.ERROR_AI_TASK_RESULTS,
            error="Task Results Could Not be retrieved from AI Services.",
        )
        document.save()
        document.task.save()
    else:
        if chunk_id is not None:
            chunk_obj = Chunks.objects.get(id=chunk_id)
            # logger.info("Chunk number %s completed processing." % chunk_obj.sequence_number)
        else:
            # logger.info("Document %s completed processing." % document.pk)
            print("Document %s completed processing." % document.pk)
        # check if it's a chunk object then save it's response in table
        check = True
        if chunk_id is not None:
            check = False
            # check if the current chunk is the last chunk for the given task then
            # concat the ai_response of all the chunks
            last_chunked_obj = Chunks.objects.filter(task_id=chunk_obj.task_id).last()
            if last_chunked_obj.sequence_number == chunk_obj.sequence_number:
                all_obj_data = (
                    Chunks.objects.filter(task_id=chunk_obj.task_id, status=COMPLETED)
                    .order_by("sequence_number")
                    .values_list("ai_response", flat=True)
                )
                task_results = []

                for i in range(all_obj_data.count()):
                    if i == 0:
                        new_push_data = all_obj_data[i]
                        extract_data = all_obj_data[i].get("data").get("extracted_data")
                        new_push_data.get("data").update(
                            {"extracted_data": [extract_data]}
                        )
                        task_results.append(new_push_data)
                        # logger.info("************ extracted data %s" % task_results)

                    else:
                        task_results[0].get("data").get("extracted_data")[0].extend(
                            all_obj_data[i].get("data").get("extracted_data")
                        )

                task_results = task_results[0]
                check = True

        # logger.info(" task results >>>>>> %s" %task_results)
        if check:
            if preprocess == "tax_classifier":
                task_results = merge_tax_data(document.id, task_results)
            normalize_table_headers(task_results)
            new_data = copy.deepcopy(task_results)
            # logger.info("Creating TaskResult for document %s." % document.pk)
            tr = TaskResult(
                document=document,
                task=document.task,
                data=new_data,
                new_data=new_data,
            )
            tr.save(track=True, operation=TrackedOperation.NEW, user=document.owner)
            document.completed_processing()
            document.save()
            document.task.save()
            callback_if_applicable.delay(document.task.pk)
            return True


@retriable_task(
    bind=True, name="backend.app.ai_services.tasks.start_ai_text_extraction"
)
def start_ai_text_extraction(self, doc_id, data, capability="tax_classifier"):
    # logger.info("Starting XLS extraction for document %s." % doc_id)
    document = Document.objects.get(pk=doc_id)

    # logger.info("Document %s started processing." % document.pk)
    #document.started_processing()  # commented prev
    #document.save()   # commented prev
    #document.task.save()   # commented prev
    max_allowed_size = 150000
    # max_allowed_size = config(name='MAX_SIZE',
    # required=[Environments.DEV, Environments.PROD])
    if len(data[0]) > max_allowed_size:
        sequence_number = 0
        chunk_size = 150000
        # chunk_size = config(name='CHUNK_SIZE',
        # required=[Environments.DEV, Environments.PROD])
        Chunks.objects.filter(task_id=document.task.id).delete()
        data = data[0]
        for i in range(0, len(data), chunk_size):
            # fmt: off
            chunked_data = data[i : i + chunk_size]  # noqa
            # fmt: on
            sequence_number += 1
            try:
                response = client.create_task(
                    {
                        "items": chunked_data,
                        "capability": capability,
                    },
                    api_key=document.organization.api_key,
                )
            except AIServicesHttpError as e:
                # TODO: Are there other circumstances that warrant a retry?
                if e.error is not None:
                    # logger.error(
                    #     'Error: Error submitting chunked task to AI services '
                    #     '- retrying...', extra={
                    #         'document': document.pk,
                    #         'chunk_number':sequence_number,
                    #         'error': str(e.error)
                    #     }
                    # )
                    raise self.retry(exc=e, countdown=30, max_retries=2)
                # logger.error(
                #     "There was an error submitting the task to AI Services.",
                #     extra={
                #         'document': document.pk,
                #         'chunk_number':sequence_number,
                #         'error': str(e)
                #     }
                # )
                document.failed_processing(error_type=Task.ERROR_AI_RESPONSE, error=e)
                document.save()
                document.task.save()
                return False
            else:
                # logger.info("Successfully posted task to AI Services for chunk number %s." % sequence_number)
                # logger.info(" AI >>>>>>> %s." % response )

                # create Chunk object
                chunk_obj = Chunks.objects.create(
                    ai_uuid=response["task_id"],
                    task_id=document.task.id,
                    chunked_payload=chunked_data,
                    status=PENDING,
                    ai_response=response,
                    sequence_number=sequence_number,
                )
                chunk_id = chunk_obj.id
                document.task.ai_uuid = response["task_id"]
                document.task.request.update(items=data, capability=capability)
                document.task.response = response
                document.task.save()
                watch_for_results.delay(
                    document.pk, preprocess="tax_classifier", chunk_id=chunk_id
                )
                # return True
    else:
        try:
            response = client.create_task(
                {
                    "items": data,
                    "capability": capability,
                },
                api_key=document.organization.api_key,
            )
        except AIServicesHttpError as e:
            # TODO: Are there other circumstances that warrant a retry?
            if e.error is not None:
                # logger.error(
                #     'Error: Error submitting task to AI services '
                #     '- retrying...', extra={
                #         'document': document.pk,
                #         'error': str(e.error)
                #     }
                # )
                raise self.retry(exc=e, countdown=30, max_retries=2)
            # logger.error(
            #     "There was an error submitting the task to AI Services.",
            #     extra={
            #         'document': document.pk,
            #         'error': str(e)
            #     }
            # )
            document.failed_processing(error_type=Task.ERROR_AI_RESPONSE, error=e)
            document.save()
            document.task.save()
            return False
        else:
            # logger.info("Successfully posted task to AI Services.")
            document.task.ai_uuid = response["task_id"]
            document.task.request.update(items=data, capability=capability)
            document.task.response = response
            document.task.save()
            watch_for_results.delay(document.pk, preprocess="tax_classifier")
            return True


@retriable_task(bind=True, name="backend.app.ai_services.tasks.start_ai_extraction")
def start_ai_extraction(self, doc_id):
    # logger.info("Starting AI extraction for document %s." % doc_id)
    document = Document.objects.get(pk=doc_id)

    # logger.info("Document %s started processing." % document.pk)
    print("DEBUG : In AI started processing")
    document.started_processing()
    print("DEBUG : In AI saving")
    document.save()
    print("DEBUG : In AI task saving")
    document.task.save()

    # KPMG specific: If running the app on their infra we have to upload files
    # to both s3 and azure s3 for intitial extraction and azure so they can see
    # it in app. see 'nv-operations' load balancer rules in aws us-east-1
    get_url = document.storage_path
    #if settings.AI_STORAGE_PROXY is not None:
    #    # generate a get_url for s3
    #    s3upload = NVs3bucket()
    #    get_url = s3upload.generate_get_presigned_url(
    #        settings.NC_AWS_BUCKET_NAME, document.aws_document_key, document.name
    #    )

    # Initialize the request body assuming that the Task has no request data.
    raw_body = {"files": [{"location": get_url}]}
    if "files" in document.task.request and len(document.task.request["files"]) == 2:
        raw_body = document.task.request
    if (
        "property_search" in document.task.request
        and document.task.request["property_search"] is not None
    ):
        raw_body["files"].append(
            {"location": document.task.request["property_search"]["get_url"]}
        )

    # KPMG: If we are in Azure this will send a request to the Platform API url
    # which will be proxied to AI Services on our side.
    # logger.info("Posting Task to AI Services", extra=raw_body)
    try:
        response = client.create_task(raw_body, api_key=document.organization.api_key)
    except AIServicesHttpError as e:
        # TODO: Are there other circumstances that warrant a retry?
        if e.error is not None:
            # logger.error(
            #     'Error: Error submitting task to AI services '
            #     '- retrying...', extra={
            #         'document': document.pk,
            #         'error': str(e.error)
            #     }
            # )
            raise self.retry(exc=e, countdown=30, max_retries=2)

        # logger.error(
        #     "There was an error submitting the task to AI Services.\n%s" % e)
        # document.failed_processing(
        #     error_type=Task.ERROR_AI_RESPONSE,
        #     error=e
        # )
        document.save()
        document.task.save()

        callback_if_applicable.delay(document.task.pk, API_USER_PROCESSING_ERROR)
        return False
    else:
        # logger.info("Successfully posted task to AI Services.")
        document.task.ai_uuid = response["task_id"]
        document.task.request["files"] = raw_body["files"]
        document.task.response = response
        document.task.save()

        watch_for_results.delay(document.pk)
        return True


@retriable_task(name="backend.app.ai_services.tasks.start_xls_extraction")
def start_xls_extraction(doc_id):
    document = Document.objects.get(pk=doc_id)
    # logger.info("Starting XLS extraction for document %s." % document.pk)

    if document.type.lower() not in ["xls", "xlsx", "csv"]:
        raise Exception("Invalid document type %s for XLS extraction." % document.type)

    # logger.info("Document %s started processing." % document.pk)
    document.started_processing()
    document.save()
    document.task.save()

    task_results = {
        "meta": {},
        "data": {
            "capability": "xls/xlsx",
            "result_type": "xls/xlsx",
            "extracted_data": [],
        },
    }

    if document.type.lower() in ["xls", "xlsx"]:
        for i, sheet in enumerate(pd.ExcelFile(document.storage_path).sheet_names):
            df = pd.read_excel(document.storage_path, sheet_name=sheet)
            vals = convert_df_to_table(df)
            vals.update({"regions": [{"page": i}], "label": str(sheet), "index": i})
            task_results["data"]["extracted_data"].append(vals)
    else:
        df = pd.read_csv(document.storage_path)
        vals = convert_df_to_table(df)
        vals.update({"regions": [{"page": 1}], "label": str(document.name), "index": 1})
        task_results["data"]["extracted_data"].append(vals)

    # logger.info("Document %s completed processing." % document.pk)
    document.completed_processing()
    document.save()
    document.task.save()

    task_results.update(
        status="SUCCESS",  # TODO: Reference as constant.
        service=AIServicesService.DOCUMENT_EXTRACTOR,
        task_id=document.task.uuid,
    )
    tr = TaskResult(
        document=document,
        task=document.task,
        uuid=UUID.uuid4(),
        data={"": ""},
        new_data=task_results,
    )
    tr.save(track=True, operation=TrackedOperation.NEW, user=document.owner)
    callback_if_applicable.delay(document.task.pk)
    return True


@retriable_task(name="backend.app.ai_services.tasks.callback_if_applicable")
def callback_if_applicable(task_id, error_response=None):
    """
    :param task: A nc_core.models.Task object
    :param task_results: A dictionary of task results
    :return: None

    The task provided is used to obtain a CallbackRequest (from
    nc_core.models). If one exists, a request is made to it's
    callback_url attribute and the CallbackRequest is updated
    upon request completion.
    """
    task = Task.objects.get(pk=task_id)
    if len(task.callback_requests.all()) == 0:
        # logger.info(
        #     "Task %s does not have any callback requests - returning."
        #     % task.pk
        # )
        return True

    # TODO: Change this so that a Task only has 1 Task Result.
    results = task.results.latest()

    task_results = results.new_data
    if error_response is not None:
        task_results = error_response

    callback_data = copy.deepcopy(task_results)
    callback_data.pop("service", None)

    # I have no idea what is going on here - but this has code smell ALL over
    # it.
    if "data" in callback_data:
        callback_data["data"].pop("capability", None)
        callback_data["data"].update(
            {
                "result_id": results.pk,
                "request": task.client_request,
                "result": callback_data["data"],
            }
        )

    # TODO: Change this so that a Task only has 1 callback request.
    callback_request = task.callback_requests.first()
    callback_request.request_data = callback_data
    callback_request.time_sent = timezone.now()

    try:
        response = requests.post(callback_request.callback, json=callback_data)
    except requests.exceptions.ConnectionError as e:
        # TODO: We might want to retry here.
        # logger.error(
        #     "There was a Connection Error making a callback request at %s."
        #     % callback_request.callback, extra={
        #         'task': task.pk,
        #         'callback_request': callback_request.pk,
        #         'exception': str(e)
        #     }
        # )
        callback_request.callback_processing_error = str(e)
        callback_request.save()
        return False
    except requests.exceptions.Timeout as e:
        # TODO: We might want to retry here.
        # logger.error(
        #     "There was a Timeout Error making a callback request at %s."
        #     % callback_request.callback, extra={
        #         'task': task.pk,
        #         'callback_request': callback_request.pk,
        #         'exception': str(e)
        #     }
        # )
        callback_request.callback_processing_error = str(e)
        callback_request.save()
        return False
    except requests.exceptions.RequestException as e:
        # logger.error(
        #     "There was a Unknown Error making a callback request at %s."
        #     % callback_request.callback, extra={
        #         'task': task.pk,
        #         'callback_request': callback_request.pk,
        #         'exception': str(e)
        #     }
        # )
        callback_request.callback_processing_error = str(e)
        callback_request.save()
        return False
    else:
        callback_request.status_code = response.status_code
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            # logger.error(
            #     "There was a Client Error making a callback request at %s."
            #     % callback_request.callback, extra={
            #         'task': task.pk,
            #         'callback_request': callback_request.pk,
            #         'exception': str(e)
            #     }
            # )
            callback_request.callback_processing_error = str(e)
            callback_request.save()
            return False
        else:
            try:
                callback_request.response_data = response.json()
            except ValueError:
                callback_request.response_data = response.text
            callback_request.save()
            return True

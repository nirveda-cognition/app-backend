import numpy
import pandas as pd

from backend.app.task.models import TaskResult


def normalize_document_results_into_table(documents):
    table_df_array = []
    for document in documents:
        if document.task is None:
            continue
        try:
            result = document.task.results.latest()
        except TaskResult.DoesNotExist:
            continue

        document_df_array = []
        json_extracted_data = pd.json_normalize(
            result.new_data["data"], record_path=["extracted_data"]
        )

        if not json_extracted_data[
            json_extracted_data["type"].str.match("field")
        ].empty:
            first_level_data_all = json_extracted_data[
                json_extracted_data["type"].str.match("field")
            ][["label", "value", "regions"]]

            page_series = pd.json_normalize(
                first_level_data_all["regions"].map(lambda x: x[0])
            )["page"]

            first_level_data_extract = pd.concat(
                [first_level_data_all[["label", "value"]], page_series], axis=1
            )

            first_level_grouped = first_level_data_extract.groupby("page")

            first_level_df_groups = []

            for name, group in first_level_grouped:
                page_num = name
                temp_grp_df = group[["label", "value"]].T
                group_df = temp_grp_df[1:]

                group_df.columns = numpy.array(
                    [
                        column_name.upper()
                        if isinstance(column_name, str)
                        else str(column_name)
                        for column_name in temp_grp_df.iloc[0].values.flatten()
                    ]
                )

                group_df.insert(0, "Document Name", document.name)
                group_df.insert(1, "Page", page_num)
                first_level_df_groups.append(group_df)

            first_level_data = pd.concat(first_level_df_groups)

            document_df_array.append(first_level_data)

        if (json_extracted_data.get("type") is not None) and (
            json_extracted_data.get("subtype") is not None
        ):
            table_data_list = json_extracted_data[
                json_extracted_data["type"].str.match("collection")
                & json_extracted_data["subtype"].str.match("table")
            ]

            if not table_data_list.empty:
                for table_data in table_data_list.iterrows():
                    page_number = table_data[1]["regions"][0]["page"]
                    table_label = table_data[1]["label"]
                    table_index = table_data[1]["index"]

                    for table_data_item in table_data[1]["items"]:
                        if table_data_item["subtype"] == "header_row":
                            table_header_elements_df = pd.json_normalize(
                                table_data_item["elements"]
                            )
                            table_header_df = table_header_elements_df.sort_values(
                                by=["column_index"]
                            )[["value"]]
                            table_header_list = numpy.array(
                                [
                                    column_name.upper()
                                    if isinstance(column_name, str)
                                    and (len(column_name.strip()) > 0)
                                    else f"pg-{page_number}-{table_label}"
                                    f"-{table_index}-hdr-{index}".upper()
                                    for index, column_name in enumerate(
                                        table_header_df.values.flatten()
                                    )
                                ]
                            )

                    table_df = pd.DataFrame()

                    for table_data_item in table_data[1]["items"]:
                        if table_data_item["subtype"] == "data_row":
                            table_one_element_df = pd.json_normalize(
                                table_data_item["elements"]
                            )
                            if "value" in table_one_element_df.columns:
                                table_one_row_df = table_one_element_df.sort_values(
                                    by=["column_index"]
                                )[["column_index", "value"]]
                                flattened_one_cell_df = (
                                    table_one_row_df.groupby("column_index")["value"]
                                    .apply(lambda df: df.reset_index(drop=True))
                                    .unstack()
                                )
                                row_df = flattened_one_cell_df.T
                                table_df = table_df.append(row_df)

                    table_df.columns = table_header_list

                    table_df.insert(0, "Document Name", document.name)
                    table_df.insert(1, "Page", page_number)
                    table_df.insert(2, "Table Index", table_index)
                    document_df_array.append(table_df)
        table_df_array.append(pd.concat(document_df_array))

    temp_df = pd.concat(table_df_array)
    temp_df.sort_index(axis=1, inplace=True)
    final_df = temp_df.sort_values(by=temp_df.columns[0], ascending=True)

    final_df.insert(0, "DOCUMENT NAME", final_df.pop("Document Name"))
    if "Page" in final_df.columns:
        final_df.insert(1, "PAGE", final_df.pop("Page"))
    if "Table Index" in final_df.columns:
        final_df.insert(2, "TABLE INDEX", final_df.pop("Table Index"))
    return final_df

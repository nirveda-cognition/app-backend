from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions


class AIServicesErrorCodes(object):
    HTTP_ERROR = "ai_services_http_error"
    INVALID_HTTP_AUTHORIZATION = "invalid_http_authorization"
    MISSING_HTTP_AUTHORIZATION = "missing_http_authorization"


class InvalidHttpAuthorization(exceptions.ParseError):
    """
    Raised when the HTTP_AUTHORIZATION request header is invalid or malformed.
    """

    default_detail = _("HTTP_AUTHORIZATION is invalid or malformed.")
    default_code = AIServicesErrorCodes.INVALID_HTTP_AUTHORIZATION


class MissingHttpAuthorization(exceptions.PermissionDenied):
    """
    Raised when the HTTP_AUTHORIZATION request header is missing.
    """

    default_detail = _("HTTP_AUTHORIZATION is required.")
    default_code = AIServicesErrorCodes.MISSING_HTTP_AUTHORIZATION

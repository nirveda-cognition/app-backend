from django.urls import path

from .views import (
    CapabilitiesView,
    CapabilityView,
    NormalizeDocumentDataView,
    ServicesView,
)

app_name = "ai_services"

urlpatterns = [
    path("services/", ServicesView.as_view()),
    path("services/<str:service>/capabilities/", CapabilitiesView.as_view()),
    path(
        "services/<str:service>/capabilities/<str:capability>/",
        CapabilityView.as_view(),
    ),
    path("normalize-document-data/", NormalizeDocumentDataView.as_view()),
]

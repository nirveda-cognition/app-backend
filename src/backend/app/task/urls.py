from django.urls import include, path

from .views import (
    DocumentTaskResultViewSet,
    DocumentTaskViewSet,
    OrganizationTaskResultViewSet,
    OrganizationTaskTaskResultViewSet,
    OrganizationTaskViewSet,
)

app_name = "task"

document_urlpatterns = [
    path(
        "",
        DocumentTaskViewSet.as_view(
            {
                "get": "retrieve",
            }
        ),
    ),
    path(
        "results/",
        include(
            [
                path(
                    "",
                    DocumentTaskResultViewSet.as_view(
                        {
                            "get": "list",
                        }
                    ),
                ),
                path(
                    "latest/",
                    DocumentTaskResultViewSet.as_view(
                        {
                            "get": "latest",
                        }
                    ),
                ),
                path(
                    "<int:pk>/",
                    DocumentTaskResultViewSet.as_view(
                        {
                            "get": "retrieve",
                            "patch": "partial_update",
                        }
                    ),
                ),
            ]
        ),
    ),
]

organization_urlpatterns = [
    path(
        "results/",
        OrganizationTaskResultViewSet.as_view(
            {
                "get": "list",
            }
        ),
    ),
    path(
        "results/<int:pk>/",
        OrganizationTaskResultViewSet.as_view(
            {
                "get": "retrieve",
                "patch": "partial_update",
            }
        ),
    ),
    path(
        "<int:pk>/",
        OrganizationTaskViewSet.as_view(
            {
                "get": "retrieve",
            }
        ),
    ),
    path(
        "",
        OrganizationTaskViewSet.as_view(
            {
                "get": "list",
            }
        ),
    ),
    path(
        "<int:task_pk>/",
        include(
            [
                path(
                    "results/",
                    include(
                        [
                            path(
                                "",
                                OrganizationTaskTaskResultViewSet.as_view(
                                    {
                                        "get": "list",
                                    }
                                ),
                            ),
                            path(
                                "latest/",
                                OrganizationTaskTaskResultViewSet.as_view(
                                    {
                                        "get": "latest",
                                    }
                                ),
                            ),
                            path(
                                "<int:pk>/",
                                OrganizationTaskTaskResultViewSet.as_view(
                                    {
                                        "get": "retrieve",
                                        "patch": "partial_update",
                                    }
                                ),
                            ),
                        ]
                    ),
                )
            ]
        ),
    ),
]

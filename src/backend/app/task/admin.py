from django.contrib import admin
from django.template.defaultfilters import truncatechars

from .models import CallbackRequest, Chunks, Task, TaskResult


class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "uuid",
        "organization",
        "shortened_request",
        "shortened_response",
        "created_at",
        "updated_at",
    )

    def shortened_request(self, obj):
        return truncatechars(str(obj.request), 100)

    def shortened_response(self, obj):
        return truncatechars(str(obj.response), 100)

    search_fields = ("ai_uuid", "uuid")


class TaskResultAdmin(admin.ModelAdmin):
    list_display = (
        "document",
        "id",
        "task",
        "uuid",
        "shortened_new_data",
        "created_at",
    )

    def shortened_new_data(self, obj):
        return truncatechars(str(obj.new_data), 100)


class CallbackRequestAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "callback",
        "request_data",
        "response_data",
        "status_code",
        "callback_processing_error",
        "time_sent",
    )


class ChunkAdmin(admin.ModelAdmin):
    list_display = ("task", "sequence_number")

    # search_fields = ('task')


admin.site.register(Task, TaskAdmin)
admin.site.register(TaskResult, TaskResultAdmin)
admin.site.register(CallbackRequest, CallbackRequestAdmin)
admin.site.register(Chunks, ChunkAdmin)

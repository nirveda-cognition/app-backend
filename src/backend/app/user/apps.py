from django.apps import AppConfig


class UserConfig(AppConfig):
    name = "backend.app.user"
    verbose_name = "User"

    def ready(self):
        from backend.app.user.signals import send_email, user_signed_up  # noqa

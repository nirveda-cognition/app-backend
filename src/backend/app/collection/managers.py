from django.db import models


class CollectionQuerier(object):
    def new(self):
        # pylint: disable=no-member
        return self.filter(collection_state=self.model.COLLECTION_STATE_NEW)

    def ready(self):
        # pylint: disable=no-member
        return self.filter(collection_state=self.model.COLLECTION_STATE_READY)

    def completed(self):
        # pylint: disable=no-member
        return self.filter(collection_state=self.model.COLLECTION_STATE_COMPLETED)

    def active(self):
        # pylint: disable=no-member
        return self.filter(state=self.model.STATE_ACTIVE)

    def inactive(self):
        # pylint: disable=no-member
        return self.filter(state=self.model.STATE_TRASH)


class CollectionQuery(CollectionQuerier, models.query.QuerySet):
    pass


class CollectionManager(CollectionQuerier, models.Manager):
    queryset_class = CollectionQuery

    def get_queryset(self):
        return self.queryset_class(self.model)

    def create_loan(self, **kwargs):
        return self.create(schema=self.model.SCHEMA_LOAN, **kwargs)

    def create_invoice(self, **kwargs):
        return self.create(schema=self.model.SCHEMA_INVOICE, **kwargs)

    def create_expense(self, **kwargs):
        return self.create(schema=self.model.SCHEMA_EXPENSE, **kwargs)

    def create_workitem(self, **kwargs):
        return self.create(schema=self.model.SCHEMA_WORKITEM, **kwargs)

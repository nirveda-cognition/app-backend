from django.apps import AppConfig


class CollectionConfig(AppConfig):
    name = "backend.app.collection"
    verbose_name = "Collection"

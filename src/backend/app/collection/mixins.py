from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from backend.app.organization.mixins import OrganizationNestedMixin


class CollectionNestedMixin(OrganizationNestedMixin):
    """
    A mixin for views that extend off of a collections's detail endpoint:
    """

    @property
    def collection_lookup_field(self):
        raise NotImplementedError()

    @cached_property
    def collection(self):
        params = {
            self.collection_lookup_field[0]: (
                self.kwargs[self.collection_lookup_field[1]]
            )
        }
        return get_object_or_404(self.organization.collections.all(), **params)

# Generated by Django 3.1.13 on 2022-01-31 10:07

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('document', '0001_initial'),
        ('collection', '0002_auto_20220131_1007'),
        ('organization', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='collectionpointer',
            name='documents',
            field=models.ManyToManyField(blank=True, to='document.Document'),
        ),
        migrations.AddField(
            model_name='collection',
            name='assigned_users',
            field=models.ManyToManyField(blank=True, related_name='assigned_collections', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='collection',
            name='collection_pointer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='collection.collectionpointer'),
        ),
        migrations.AddField(
            model_name='collection',
            name='collections',
            field=models.ManyToManyField(blank=True, related_name='parents', to='collection.Collection'),
        ),
        migrations.AddField(
            model_name='collection',
            name='created_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='created_collections', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='collection',
            name='organization',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='collections', to='organization.organization'),
        ),
        migrations.AddField(
            model_name='collection',
            name='owner',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='owned_collections', to=settings.AUTH_USER_MODEL),
        ),
    ]

from django.core.management.base import BaseCommand

from backend.app.collection.models import Collection
from backend.app.document.models import Document


class Command(BaseCommand):
    help = "clears doc relations (for all docs), deletes all collecitons"

    """
    This is legacy code but we are keeping it for history. Do not use.
    """

    def add_arguments(self, parser):
        # add logic here to handle a specific conversion
        pass

    def handle(self, *args, **options):
        """
        Run this to clear all collections.
        Warning, this will delete all collections!!
        """
        d = Document.objects.all()
        for doc in d:
            doc.collection_list.set([])
            doc.save()

        Collection.objects.all().delete()

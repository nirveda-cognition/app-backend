import os
from logging.config import dictConfig

import django
from celery import Celery
from celery.schedules import crontab
from celery.signals import setup_logging

from backend.conf import Environments, config

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend.conf.settings.prod")
django.setup()

app = Celery("backend")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.conf.update(
    {
        "worker_hijack_root_logger": False,
        "task_default_queue": config(
            name="CELERY_TASK_QUEUE", required=[Environments.DEV, Environments.PROD]
        ),
    }
)
app.autodiscover_tasks()

app.conf.beat_schedule = {
    "watch-for-old-trash": {
        "task": "backend.app.document.tasks.watch_for_old_trash",
        "schedule": crontab(minute=0, hour=0),
    },
    "manage-failed-tasks": {
        "task": "backend.app.document.tasks.manage_failed_tasks",
        "schedule": crontab(minute="*/5"),
    },
}


# TODO: It would be nice if we can figure out how to send logs from celery.task
# to both the Django Server and the Celery STDOUT.
@setup_logging.connect
def config_loggers(*args, **kwargs):
    from django.conf import settings

    dictConfig(settings.LOGGING)


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))

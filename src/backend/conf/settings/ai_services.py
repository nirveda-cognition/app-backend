import os

from backend.conf import Environments, config

AI_STORAGE_PROXY = config("AI_STORAGE_PROXY", default=None)
AI_SERVICES_API_KEY = config(
    name="AI_SERVICES_API_KEY", required=[Environments.PROD, Environments.DEV]
)
AI_SERVICES_API_URL = config(
    name="AI_SERVICES_API_URL", required=[Environments.PROD, Environments.DEV]
)

if not AI_SERVICES_API_URL.endswith("v1/") and not AI_SERVICES_API_URL.endswith("v1"):
    AI_SERVICES_API_URL = os.path.join(AI_SERVICES_API_URL, "v1") + "/"

import logging
import sys

from request_logging import middleware

from backend.lib.logging.middleware import NirvedaLoggingMiddleware

# Toggle JSON Logging for Base Django App Logger
JSON_LOGGING = False


# TODO: It would be nice if we can figure out how to send logs from celery.task
# to both the Django Server and the Celery STDOUT.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "[%(name)s] %(levelname)s [%(module)s:L%(lineno)d; %(funcName)s()]: %(message)s",  # noqa
        },
        "simple": {
            "format": "[%(name)s] %(levelname)s: %(message)s",
        },
        "json": {
            "()": "backend.lib.logging.formatters.JsonLogFormatter",
            "logger_name": "backend",
        },
        "dynamic": {
            "()": "backend.lib.logging.formatters.DynamicExtraArgumentFormatter",  # noqa
        },
        "django.server": {
            "()": "django.utils.log.ServerFormatter",
            "format": "[{server_time}] {message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {
            "level": logging.DEBUG,
            "class": "logging.StreamHandler",
            "stream": sys.stdout,
            "formatter": "verbose",
        },
        "console.simple": {
            "level": logging.DEBUG,
            "class": "logging.StreamHandler",
            "stream": sys.stdout,
            "formatter": "simple",
        },
        "backend.handler": {
            "level": logging.DEBUG,
            "class": "logging.StreamHandler",
            "stream": sys.stdout,
            "formatter": "json" if JSON_LOGGING else "dynamic",
        },
        "django.server": {
            "level": logging.INFO,
            "class": "logging.StreamHandler",
            "formatter": "django.server",
        },
        "django.request": {
            "level": logging.INFO,
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "gunicorn": {
            "level": logging.DEBUG,
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "celery": {
            "level": logging.INFO,
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "celery.task": {
            "level": logging.INFO,
            "class": "logging.StreamHandler",
            "formatter": "dynamic",
        },
        "null": {
            "class": "logging.NullHandler",
        },
    },
    "root": {"handlers": ["console"], "level": logging.INFO},
    "loggers": {
        "django": {
            "handlers": ["console.simple"],
            "level": logging.INFO,
            "propagate": True,
        },
        "django.server": {
            "handlers": ["django.server"],
            "level": logging.INFO,
            "propagate": False,
        },
        "django.request": {
            "handlers": ["django.request"],
            "level": logging.INFO,
            "propagate": False,
        },
        "requests": {"level": logging.WARNING},
        "backend": {
            "handlers": ["backend.handler"],
            "level": logging.INFO,
            "propagate": False,
        },
        "celery": {
            "handlers": ["celery"],
            "level": "INFO",
            "propagate": False,
        },
        "celery.task": {
            "handlers": ["celery.task"],
            "level": "INFO",
            "propagate": False,
        },
        "": {
            "level": logging.DEBUG,
            "handlers": ["console"],
            "propagate": False,
        },
    },
}

LOGGING_EXEMPTED_URLS = {
    "/v1/login": ["post"],
    "/v1/signup/": ["post"],
    "/v1/organizations": ["get"],
    "/v1/groups": ["get", "post"],
    "/v1/auth/": ["get", "patch"],
    "/v1/users": ["get", "post"],
    "/v1/roles": ["get", "post"],
    "/v1/invite-user/": ["post"],
    "/v1/reset-password'/": ["post"],
    "/v1/forgot-password/": ["post"],
    "/v1/change-password/": ["post"],
    "/v1/update-password/": ["patch"],
}

# Customize LoggingMiddleware
middleware.LoggingMiddleware = NirvedaLoggingMiddleware

"""
Settings configuration file for test environment.
"""
import logging

from backend.conf import Environments

from .base import *  # noqa
from .base import LOGGING, REST_FRAMEWORK

DEBUG = True
ENVIRONMENT = Environments.TEST
ALLOWED_HOSTS = ["testserver"]
TIME_ZONE = "UTC"

EMAIL_ENABLED = False
CELERY_ENABLED = False
RATELIMIT_ENABLE = False

# Disable logging in tests
LOGGING["loggers"] = {  # noqa
    "": {
        "handlers": ["null"],
        "level": logging.DEBUG,
        "propagate": False,
    },
}
del LOGGING["root"]

# Turn off throttling for tests.
REST_FRAMEWORK["DEFAULT_THROTTLE_CLASSES"] = []
REST_FRAMEWORK["DEFAULT_THROTTLE_RATES"] = {}

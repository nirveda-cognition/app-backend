from backend.conf import Environments, config

from .aws import AWS_REGION

CELERY_ENABLED = True

CELERYD_HIJACK_ROOT_LOGGER = False
CELERY_SERVICE = config("CELERY_SERVICE", default="redis")

if CELERY_SERVICE == "redis":
    BROKER_TRANSPORT = "redis"
    # Works for celery in docker NOTE: need to bind 'redis' to 127.0.0.1 in
    # hosts to run locally.
    BROKER_HOST = "redis"
    BROKER_PORT = 6379  # Maps to redis port.
    BROKER_VHOST = "0"  # Maps to database number.

# NOTE: In order to run celery locally, `redis` needs to be bound to
# 127.0.0.1 in hosts.
CELERY_BROKER_URL = config(
    name="REDIS_URL",
    required=[Environments.PROD],
    default={Environments.DEV: "redis://localhost:6379/0"},
)
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_RESULT_SERIALIZER = "json"
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_BACKEND = "django-db"
CELERY_RESULT_EXPIRES = 1209600
CELERY_BROKER_TRANSPORT_OPTIONS = {
    "region": AWS_REGION,
    "polling_interval": 1,
    "visibility_timeout": 30,
    "is_secure": True,
}
CELERY_TASK_DEFAULT_QUEUE = config(
    name="CELERY_TASK_QUEUE",
    required=[Environments.PROD],
    default={Environments.DEV: "nvd-dev-CHANGEME"},
)

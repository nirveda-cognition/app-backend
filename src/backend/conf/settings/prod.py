"""
Settings configuration file for production environment.
"""
import sentry_sdk
from decouple import config
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration

from .aws import *  # noqa
from .base import *  # noqa
from .celery import *  # noqa

DEBUG = False
EMAIL_ENABLED = True

SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False

ALLOWED_HOSTS = ["*"]

# TODO: Audit whether or not all of these needed for development.
CORS_ORIGIN_WHITELIST = [
    "http://app-ncp.s3-website-us-east-1.amazonaws.com",
    "http://nirveda-dev2.s3-website-us-east-1.amazonaws.com",
    "https://kpmg-azure1.nirvedacognition.ai",
    NC_APP_FRONTEND_URL,
]

if NC_APP_FRONTEND_URL_2 is not None:
    CORS_ORIGIN_WHITELIST.append(NC_APP_FRONTEND_URL_2)

if NC_APP_FRONTEND_URL_3 is not None:
    CORS_ORIGIN_WHITELIST.append(NC_APP_FRONTEND_URL_3)

SENTRY_DSN = config(
    "SENTRY_DSN", default="https://a93c26c415844f11952074304c92d1ee@sentry.io/1845208"
)

sentry_sdk.init(
    dsn=SENTRY_DSN,
    integrations=[DjangoIntegration(), CeleryIntegration()],
    environment=ENVIRONMENT,  # noqa
)

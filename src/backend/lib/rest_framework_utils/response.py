from django.utils.translation import gettext_lazy as _
from rest_framework.response import Response


# TODO: This will be deprecated.
class NCPResponse:
    def __init__(
        self,
        data=None,
        user_action="",
        access_token="",
        category="",
        source="",
        detail="",
        code=200,
    ):

        self.access_token = access_token
        self.category = category
        self.source = source
        self.user_action = user_action
        self.data = data
        self.code = code
        self.detail = detail

    def response_400(self):
        try:
            self.detail = self.detail.args[0]["detail"][0]
        # TODO: Fix this blind exception catch.
        except Exception:
            pass
        return Response(
            data={
                "message": {
                    "description": "BAD REQUEST",
                    "user_action": self.user_action,
                    "detail": _(str(self.detail)),
                },
                "category": self.category,
                "source": self.source,
                "message_new": _(str(self.detail)),
                "error": True,
                "code": 400,
                "data": {},
            },
            status=400,
        )

    def response_401(self):
        return Response(
            data={
                "message": {
                    "description": _(str(self.detail)),
                    "user_action": _(
                        "Please login again to get a valid token."
                    ),  # noqa
                    "detail": _(str(self.detail)),
                },
                "message_new": _(str(self.detail)),
                "error": True,
                "code": 401,
                "data": {},
            },
            status=401,
        )

    def response_403(self):
        return Response(
            data={
                "message": {
                    "description": _(
                        "THIS REQUEST IS FORBIDDEN FOR YOUR ACCOUNT"
                    ),  # noqa
                    "user_action": _(
                        "You do not have permission to make this request."
                    ),  # noqa
                    "detail": _(str(self.detail)),
                },
                "category": self.category,
                "source": self.source,
                "message_new": _(str(self.detail)),
                "error": True,
                "code": 403,
                "data": {},
            },
            status=403,
        )

    def response_404(self):
        return Response(
            data={
                "message": {
                    "description": _("RECORD NOT FOUND"),
                    "user_action": _("No record was found in the system."),
                    "detail": _(str(self.detail)),
                },
                "access_token": self.access_token,
                "category": self.category,
                "source": self.source,
                "message_new": _(str(self.detail)),
                "error": True,
                "code": 404,
                "data": {},
            },
            status=404,
        )

    def response_500(self):
        return Response(
            data={
                "message": {
                    "description": _("INTERNAL SERVER ERROR"),
                    "user_action": _(
                        "There was an error processing the request."
                    ),  # noqa
                    "detail": _(str(self.detail)),
                },
                "category": self.category,
                "source": self.source,
                "message_new": _(str(self.detail)),
                "error": True,
                "code": 500,
                "data": {},
            },
            status=500,
        )

    def response(self, drf_data={}):
        if self.code == 400:
            return self.response_400()
        if self.code == 401:
            return self.response_401()
        if self.code == 403:
            return self.response_403()
        if self.code == 404:
            return self.response_404()
        if self.code == 500:
            return self.response_500()

        self.data.update(drf_data)
        return Response(data=self.data, status=self.code)


# TODO: This will be deprecated.
class NCResponse(object):
    def __init__(
        self,
        statusCode,  # TODO: Fix camelCase.
        message="",
        access_token=None,
        category=None,
        user_name=None,
        refresh_token=None,
        payload=[],
    ):
        self.statusCode = statusCode
        # self.message = message
        self.access_token = access_token if access_token else ""
        self.refresh_token = refresh_token if refresh_token else ""
        self.category = category if category else ""
        self.user_name = user_name if user_name else ""
        self.payload = payload
        self.message = {"description": message}

    def ncresponse(self, drf_data={}):
        response_body_dict = {}

        if self.message:
            response_body_dict.update({"message": self.message})
        if self.access_token:
            response_body_dict.update({"access_token": self.access_token})
        if self.refresh_token:
            response_body_dict.update({"refresh_token": self.refresh_token})
        if self.category:
            response_body_dict.update({"category": self.category})
        if self.user_name:
            response_body_dict.update({"user_name": self.user_name})
        if self.payload:
            response_body_dict.update({"payload": self.payload})

        response_body_dict.update(drf_data)
        return Response(data=response_body_dict, status=self.statusCode)

from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions, serializers
from schema import Schema, SchemaError


class SchemaField(serializers.DictField):
    """
    An extension of Django REST Framework's :obj:`serializers.DictField` that
    allows us to specify a :obj:`schema.Schema` for the data structure and
    validates the data structure against this schema.

    Parameters:
    ----------
    schema: :obj:`schema.Schema`
        The :obj:`schema.Schema` that will be used to validate the field value.
    pass_through_error: :obj:`bool` (optional)
        If True, the error from the :obj:`schema.Schema` validation will be
        directly passed through to the DRF response.  Otherwise, a more
        general, standardized error message will be returned.

        Default: False
    """

    default_error_messages = {
        "invalid": _("This value does not conform to the required schema.")
    }

    def __init__(self, *args, **kwargs):
        self._schema = kwargs.pop("schema")
        assert isinstance(
            self._schema, Schema
        ), "The schema must be an instance of schema.Schema"
        self._pass_through_error = kwargs.pop("pass_through_error", False)
        super().__init__(*args, **kwargs)

    def fail(self, code, **kwargs):
        schema_error = kwargs.pop("e", None)
        if schema_error is None:
            return super().fail(code, **kwargs)
        raise exceptions.ValidationError(str(schema_error), code=code)

    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        try:
            self._schema.validate(data)
        except SchemaError as e:
            if self._pass_through_error:
                self.fail("invalid", value=data, e=e)
            else:
                self.fail("invalid", value=data)
        else:
            return data

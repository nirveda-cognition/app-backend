import re

import regex


def dollar(value):
    value = str(value).replace(",", "")
    value = re.sub("[^0-9,.]", "", value)
    try:
        value = float(value)
    except ValueError:
        return "$0.00"

    return "${:,.2f}".format(value)


def decimal(value):
    value = str(value).replace(",", "")
    value = re.sub("[^0-9,.]", "", value)
    try:
        value = float(value)
    except ValueError:
        return "0.00"

    return "{:.2f}".format(value)


def integer(value):
    value = str(value).replace(",", "")
    value = re.sub("[^0-9,.]", "", value)

    try:
        return str(round(float(value)))
    except ValueError:
        return 0


def present_dollar_field(field):
    if not field:
        field = 0.00
    field = str(field)
    field = field.replace(",", "").replace("$", "")
    field = regex.sub(r"^(\d+\.\d{,2})\d*$", r"\1", str(field))
    field = regex.sub(r"(?<!\..*)(\d)(?=(?:\d{3})+(?:\.|$))", "1,", field)
    return " $" + field


def format_date_string(date_string):
    # TODO: There are much better/simpler ways to do this with python date
    # utilities.
    date_list = date_string.split("/")
    if len(date_list) == 3:
        year = date_list[-1].strip()
        if len(year) == 2:
            if 90 <= int(year) <= 99:
                year = "19" + year
            if 10 <= int(year) <= 30:
                year = "20" + year
            return "/".join([date_list[0], date_list[1], year])
    return date_string


def format_phone_no(phone_string):
    # TODO: There are external packages for this.
    phone_list = phone_string.split("-")
    area_code = phone_list[0].strip("(").strip(")").strip()
    space_length = int((9 - len(area_code)) / 2)
    phone_list[0] = " " * space_length + area_code + " " * space_length
    return "-".join(phone_list)

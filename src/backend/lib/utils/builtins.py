import ast
import re

from .dateutils import ensure_datetime


def concat(arrays):
    """
    Concatenates an array of arrays into a 1 dimensional array.
    """
    concatenated = arrays[0]
    if not isinstance(concatenated, list):
        raise ValueError(
            "Each element in the concatenation must be an instance " "of `list`."
        )
    if len(arrays) > 1:
        for array in arrays[1:]:
            if not isinstance(array, list):
                raise ValueError(
                    "Each element in the concatenation must be an instance "
                    "of `list`."
                )
            concatenated += array
    return concatenated


def is_datetime(value):
    """
    Returns whether or not the value is a valid :obj:`datetime.datetime`
    instance, a valid :obj:`datetime.date` instance, or a string that can be
    converted to either.
    """
    try:
        ensure_datetime(value)
    except ValueError:
        return False
    else:
        return True


def is_integer(value):
    """
    Returns whether or not the value can be converted to an integer without
    data loss.  Note that this excludes float values, which can be converted
    to an integer value but at the expense of the floating points.

    Example:
    -------
    >>> is_integer("5")
    >>> True
    >>> is_integer(5.1)
    >>> False
    >>> is_integer(5)
    >>> True
    """
    try:
        int(value)
    except ValueError:
        return False
    else:
        if "%s" % value == "%s" % int(value):
            return True
        return False


def is_float(value):
    """
    Returns whether or not the value can be converted to a float without
    data loss.  Note that this excludes integer values, which are treated
    differently than pure float values.

    Example:
    -------
    >>> is_float("5")
    >>> False
    >>> is_float(5.1)
    >>> True
    >>> is_float(5)
    >>> False
    >>> is_float("5.15")
    >>> True
    """
    try:
        float(value)
    except ValueError:
        return False
    else:
        if "%s" % value == "%s" % float(value):
            return True
        return False


def find_in_dict(data, keys):
    """
    Finds the value in a potentially nested dictionary by a key or set of
    nested keys.

    Parameters:
    ----------
    data: :obj:`dict`
        The dictionary for which we want to find the value indexed by the
        potentially nested keys.
    keys: :obj:`list`, :obj:`tuple`, :obj:`str`
        Either an iterable of nested keys or a single key for which we want
        to locate the associated value in the dictionary for.

    Example:
    -------
    >>> data = {'foo': {'bar': 'banana'}}
    >>> find_in_dict(data, 'foo')
    >>> {'bar': 'banana'}
    >>> find_in_dict(data, ['foo', 'bar'])
    >>> 'banana'
    """
    if hasattr(keys, "__iter__") and not isinstance(keys, str):
        if len(keys) == 1:
            return data[keys[0]]
        current = data[keys[0]]
        for key in keys[1:]:
            current = current[key]
        return current
    return data[keys]


def place_in_dict(data, keys, value):
    """
    Places a value in a potentially nested dictionary at the location defined
    by a set potentially nested keys.

    Parameters:
    ----------
    data: :obj:`dict`
        The dictionary for which the value will be stored in.
    keys: :obj:`list`, :obj:`tuple`, :obj:`str`
        Either an iterable of nested keys or a single key that defines the
        location in the dictionary for which the value should be stored.
    value:
        The value to store in the dictionary.

    Example:
    -------
    >>> data = {'foo': {'bar': 'banana'}}
    >>> place_in_dict(data, 'foo', 'bar')
    >>> data
    >>> {'foo': 'bar'}
    >>> place_in_dict(data, ['foo', 'bar'], 'bar')
    >>> data
    >>> {'foo': {'bar': 'bar'}}
    """
    if hasattr(keys, "__iter__") and not isinstance(keys, str):
        if len(keys) == 1:
            data[keys[0]] = value
            return
        current = data[keys[0]]
        for key in keys[1:-1]:
            current = current[key]
        current[keys[-1]] = value
    else:
        data[keys] = value


def find_string_formatted_arguments(value):
    """
    Finds arguments in a string that are meant to be string formatted.

    Example:
    -------
    >>> value = "{argument1}, the fox jumped over the {argument2}"
    >>> find_string_formatted_arguments(value)
    >>> ["argument1", "argument2"]
    """
    regex = "[^{\}]+(?=})"  # noqa
    matches = re.findall(regex, value)
    if matches is None:
        return []

    arguments = []
    index = 0
    while True:
        try:
            arguments.append(matches[index])
        except IndexError:
            break
        else:
            index += 1
    return arguments


def ensure_iterable(value, strict=False, cast=list):
    """
    Ensures that the provided value is an iterable, either raising a ValueError
    (if `strict = True`) or returning the value as the first element in an
    iterable (if `strict = False`).
    """
    if not hasattr(value, "__iter__") or isinstance(value, type):
        if strict:
            raise ValueError("Value %s is not an iterable." % value)
        return cast([value])
    return value


def filtered_dict(value, treat_as_null=None):
    """
    Recursively filters out all empty, null or blank values from a dictionary.
    If the dictionary has nested iterables or sub-dictionaries, those will be
    filtered as well.

    Use with caution, for simple situations.

    Args:
        treat_as_null (optional)
            The values that should be treated as null and thus filtered out of
            the dictionary.  This parameter can be provided as a list, tuple
            or any other iterable.
    """
    treat_as_null = treat_as_null or ("", [], {}, (), None)
    if not hasattr(treat_as_null, "__iter__"):
        raise ValueError("The parameter `treat_as_null` must be an iterable.")

    def handler(val):
        if isinstance(val, dict):
            new_dict = {}
            for k, vi in val.items():
                vf = handler(vi)
                if vf not in treat_as_null:
                    new_dict[k] = vf
            return new_dict
        elif (
            hasattr(val, "__iter__")
            and not isinstance(val, str)
            and val.__class__.__name__ != "__proxy__"
        ):
            new_iterable = []
            for vi in val:
                vf = handler(vi)
                if vf not in treat_as_null:
                    new_iterable.append(vf)
            return new_iterable
        else:
            return val

    new_dict = {}
    for k, v in value.items():
        vi = handler(v)
        if vi not in treat_as_null:
            new_dict[k] = vi
    return new_dict


def parse_bytes(field):
    """
    Convert string represented in Python byte-string literal syntax into a
    decoded character string. Other field types returned unchanged.
    """
    result = field
    try:
        result = ast.literal_eval(field)
    finally:
        return result.decode() if isinstance(result, bytes) else field

from django.conf import settings
from request_logging import middleware


def is_url_exempted(url, method):
    for key, value in settings.LOGGING_EXEMPTED_URLS.items():
        if url.find(key) != -1:
            if method in value:
                return True
    return False


class NirvedaLoggingMiddleware(middleware.LoggingMiddleware):
    def _should_log_route(self, request):
        path = request.path
        method = request.method.lower()

        if is_url_exempted(path, method):
            return middleware.NO_LOGGING_ATTR, None

        return super()._should_log_route(request)

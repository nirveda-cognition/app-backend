import logging
from urllib import parse as urlparse

from botocore.exceptions import ClientError
from django.conf import settings

from .utils import get_boto_client

logger = logging.getLogger("backend")


class NVs3bucket(object):
    def __init__(self, s3_region="us-east-1"):
        self.client = get_boto_client(
            service="s3", role_arn=settings.AWS_ROLE_ARN, region=s3_region
        )

    def generate_put_presigned_url(
        self, bucket, key, expiration=1800, content_type=None
    ):
        print("DEBUG : In bucket ", bucket)
        print("DEBUG : In key ", key)
        # TODO: We should probably let the calling method catch the exception
        # and handle it appropriately - the exception being thrown if there
        # is an error should be the default.
        try:
            logger.info("INSIDE NVs3bucket")
            return self.client.generate_presigned_url(
                ClientMethod="put_object",
                ExpiresIn=expiration,
                Params={
                    "Bucket": bucket,
                    "Body": "",
                    "Key": str(key) + "/" + str(key),
                    "ContentType": content_type or "application/octet-stream",
                },
            )
            #return self.client.generate_presigned_post(Bucket="bucket",
            #                                         Key=str(key),
            #                                         )
        except ClientError as e:
            logger.error("generate_put_presigned_url failed")
            logger.exception(e)
            return None

    def generate_get_presigned_url(self, bucket, key, file_name="", expiration=1800):
        logger.info("generate_get_presigned_url called")
        # TODO: We should probably let the calling method catch the exception
        # and handle it appropriately - the exception being thrown if there
        # is an error should be the default.
        try:
            return self.client.generate_presigned_url(
                ClientMethod="get_object",
                ExpiresIn=expiration,
                Params={
                    "Bucket": bucket,
                    "Key": str(key) + "/" + str(key),
                    "ResponseContentDisposition": (
                        "attachment; filename={}".format(urlparse.quote(file_name))
                    ),
                },
            )
        except ClientError as e:
            logger.error("generate_get_presigned_url failed")
            logger.exception(e)
            return None

    def generate_head_presigned_url(self, bucket, key, file_name="", expiration=1800):
        logger.info("generate_head_presigned_url called")
        # TODO: We should probably let the calling method catch the exception
        # and handle it appropriately - the exception being thrown if there
        # is an error should be the default.
        try:
            return self.client.generate_presigned_url(
                ClientMethod="head_object",
                ExpiresIn=expiration,
                Params={
                    "Bucket": bucket,
                    "Key": str(key) + "/" + str(key),
                },
            )
        except ClientError as e:
            logger.error("generate_get_presigned_url failed")
            logger.exception(e)
            return None

    def file_delete(self, bucket, obj_name):
        logger.info("file_delete called")
        # TODO: We should probably let the calling method catch the exception
        # and handle it appropriately - the exception being thrown if there
        # is an error should be the default.
        try:
            return self.client.delete_object(
                Bucket=bucket, Key=str(obj_name) + "/" + str(obj_name)
            )
        except ClientError as e:
            logger.error("Failed to delete the document from the S3 bucket.")
            logger.exception(e)
            return None

    def file_fetch(self, bucket, obj_name):
        logger.info("file_fetch called")
        # TODO: We should probably let the calling method catch the exception
        # and handle it appropriately - the exception being thrown if there
        # is an error should be the default.
        try:
            fetch_obj = self.client.get_object(
                Bucket=bucket, Key=str(obj_name) + "/" + str(obj_name)
            )
        except ClientError as e:
            logger.error("Failed to retrieve the document from the S3 bucket.")
            logger.exception(e)
            return None
        else:
            return fetch_obj["Body"].read()

import base64
import collections
import logging

import boto3
from botocore.exceptions import ClientError
from django.conf import settings

logger = logging.getLogger("backend")


AWSRoleCredentials = collections.namedtuple(
    "AWSRoleCredentials", ["access_key_id", "secret_access_key", "session_token"]
)


class AWSClientErrorCodes(object):
    # Secrets Manager can't decrypt the protected secret text using the
    # provided KMS key.
    DECRYPTION_FAILURE = "DecryptionFailureException"
    # An error occurred on the server side.
    INTERNAL_SERVER_ERROR = "InternalServiceErrorException"
    # You provided an invalid value for a parameter.
    INVALID_PARAMETER_ERROR = "InvalidParameterException"
    # You provided a parameter value that is not valid for the current
    # state of the resource.
    INVALID_REQUEST_ERROR = "InvalidRequestException"
    # We can't find the resource that you asked for.
    RESOURCE_NOT_FOUND_ERROR = "ResourceNotFoundException"


# TODO: Some of this logic could be cleaned up more, and we can probably just
# subclass boto3.client.
class Boto3Session(boto3.session.Session):
    """
    An extension of boto3.Session that will allow default configuration for
    the Session configuration and instantiate the boto3.Session with the
    AWS role credentials.
    """

    def __init__(
        self,
        role_arn=None,
        access_key_id=None,
        secret_access_key=None,
        region=None,
        role_session_name="AssumeRoleSession2",
    ):
        self.credentials = get_boto_s3_credentials(
            role_arn=role_arn or settings.AWS_ROLE_ARN,
            access_key_id=access_key_id or settings.AWS_ACCESS_KEY_ID,
            secret_access_key=(secret_access_key or settings.AWS_SECRET_ACCESS_KEY),
            role_session_name=role_session_name,
        )
        super().__init__(
            region_name=region or settings.AWS_REGION,
            aws_access_key_id=self.credentials.access_key_id,
            aws_secret_access_key=self.credentials.secret_access_key,
            aws_session_token=self.credentials.session_token,
        )

    @property
    def session_token(self):
        return self.credentials.session_token

    @property
    def access_key_id(self):
        return self.credentials.access_key_id

    @property
    def secret_access_key(self):
        return self.credentials.secret_access_key


def get_boto_s3_credentials(
    role_arn=None,
    access_key_id=None,
    secret_access_key=None,
    role_session_name="AssumeRoleSession1",
):
    
    sess = boto3.session.Session(
        aws_access_key_id=access_key_id or settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=(secret_access_key or settings.AWS_SECRET_ACCESS_KEY),
    )
    sts_connection = sess.client("sts")
    assume_role_object = sts_connection.assume_role(
        RoleArn=role_arn or settings.AWS_ROLE_ARN, RoleSessionName=role_session_name
    )
    return AWSRoleCredentials(
        access_key_id=assume_role_object["Credentials"]["AccessKeyId"],
        secret_access_key=assume_role_object["Credentials"]["SecretAccessKey"],
        session_token=assume_role_object["Credentials"]["SessionToken"],
    )


def get_boto_client(
    service, role_arn=None, region=None, access_key_id=None, secret_access_key=None
):

    print("DEBUG : In get_boto_client")
    role_arn = role_arn or settings.AWS_ROLE_ARN
    if role_arn is not None:
        print("DEBUG : In IF in get_boto_client")
        credentials = get_boto_s3_credentials(
            role_arn=role_arn,
            access_key_id=access_key_id,
            secret_access_key=secret_access_key,
        )
        return boto3.client(
            service,
            region_name=region or settings.AWS_REGION,
            aws_access_key_id=credentials.access_key_id,
            aws_secret_access_key=credentials.secret_access_key,
            aws_session_token=credentials.session_token,
        )
    print("DEBUG : In get_boto_client role arn is none.")

    return boto3.client(
        service,
        region_name=region or settings.AWS_REGION,
        aws_access_key_id=access_key_id or settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=(secret_access_key or settings.AWS_SECRET_ACCESS_KEY),
    )


def get_secret(secret_name):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name=settings.AWS_SECRETS_MANAGER,
        region_name=settings.AWS_SECRET_REGION,
    )
    try:
        response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        # Wouldn't it be smarter to just ignore the specific error we are
        # looking for?
        if e.response["Error"]["Code"] in (
            AWSClientErrorCodes.DECRYPTION_FAILURE,
            AWSClientErrorCodes.INTERNAL_SERVER_ERROR,
            AWSClientErrorCodes.INVALID_PARAMETER_ERROR,
            AWSClientErrorCodes.INVALID_REQUEST_ERROR,
            AWSClientErrorCodes.RESOURCE_NOT_FOUND_ERROR,
        ):
            raise e
        else:
            pass
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these
        # fields will be populated.
        if "SecretString" in response:
            return response["SecretString"]
        return base64.b64decode(response["SecretBinary"])

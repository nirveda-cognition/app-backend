import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class NumberValidator(object):
    """
    Validator to ensure that the password contains a certain number of
    digits, provided by the `min_digits` parameter.

    This validator is set in the AUTH_PASSWORD_VALIDATORS configuration in
    the project settings.
    """

    def __init__(self, min_digits=0):
        self.min_digits = min_digits

    def validate(self, password, user=None):
        if not len(re.findall("\d", password)) >= self.min_digits:  # noqa
            raise ValidationError(
                _(
                    "The password must contain at least %(min_digits)d "
                    "digit(s), 0-9."
                ),
                code="password_no_number",
                params={"min_digits": self.min_digits},
            )

    def get_help_text(self):
        return _(
            "Your password must contain at least %(min_digits)d digit(s), 0-9."
            % {"min_digits": self.min_digits}
        )


class UppercaseValidator(object):
    """
    Validator to ensure that the password has at least 1 uppercase letter,
    A-Z.

    This validator is set in the AUTH_PASSWORD_VALIDATORS configuration in
    the project settings.
    """

    def validate(self, password, user=None):
        if not re.findall("[A-Z]", password):
            raise ValidationError(
                _("The password must contain at least 1 uppercase " "letter, A-Z."),
                code="password_no_upper",
            )

    def get_help_text(self):
        return _("Your password must contain at least 1 uppercase letter, A-Z.")


class LowercaseValidator(object):
    """
    Validator to ensure that the password contains at least 1 lowercase
    letter.

    This validator is set in the AUTH_PASSWORD_VALIDATORS configuration in
    the project settings.
    """

    def validate(self, password, user=None):
        if not re.findall("[a-z]", password):
            raise ValidationError(
                _("The password must contain at least 1 lowercase " "letter, a-z."),
                code="password_no_lower",
            )

    def get_help_text(self):
        return _("Your password must contain at least 1 lowercase letter, a-z.")


class SymbolValidator(object):
    """
    Validator to ensure that the password contains at least 1 special
    symbol.

    This validator is set in the AUTH_PASSWORD_VALIDATORS configuration in
    the project settings.
    """

    def validate(self, password, user=None):
        if not re.findall("[!@#$%^&*]", password):
            raise ValidationError(
                _("The password must contain at least 1 symbol: " + "!@#$%^&*"),
                code="password_no_symbol",
            )

    def get_help_text(self):
        return _("Your password must contain at least 1 symbol: " + "!@#$%^&*")

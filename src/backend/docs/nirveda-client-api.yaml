swagger: "2.0"
info:
  title: "Nirveda Cognitive Platform API"
  version: "1.0.0"
host: api.nirvedacognition.ai
basePath: /v1
consumes:
- application/json
produces:
- application/json
schemes:
- https
security:
- Bearer Token: []

x-tagGroups:
- name: Endpoints
  tags:
  - Authentication
  - Task
- name: Schema
  tags:
  - Data Element
  - Region Element
- name: Upgrading from Legacy API
  tags:
  - Breaking changes
  - Additions

tags:
- name: Authentication
  description: |
    To use the Nirveda Cognitive Platform API, you need an API Key and a Client Secret. To access protected endpoints, you need an Oauth2 Bearer Token for Authorization.

- name: Task
  description: |
    ## Task execution flow

    Executing a task consists of the following steps:
      - Obtain an [Authorization Token](#operation/token).
      - Create a new [Task](#operation/create_task).
      - Upload file(s) using the signed URL(s), method and headers in the task creation response. Simply create a `PUT` request with the file content as the body of the request to the signed url returned in the previous step.
      - Retrieve [Task Results](#operation/get_task).

    ## Data Elements and Regions

    The data extracted by a task is represented using nested
    Data Elements. The top-level result contains a `result_type` and an array of
    `extracted_data`, which are Data Elements of type `field` or `collection`.
    A collection contains a list of Data Elements of type `item`, each of which
    contains a similar list of elements (fields and collections).

    Each Data Element also contains a list of Region Elements corresponding to
    location of the Data Element within the document.

- name: Data Element
  description: |
    ```
      {
        "index": integer,
        "type": enum,  # "field", "item", or "collection"
        "property_id": string,
        "label": string,
        "value": string,
        "items": [ DataElement ],
        "elements": [ DataElement ],
        "regions": [ RegionElement ]
      }
    ```
    The Data Element is the general format for any data extracted from the
    document, and can be one of three types:

      - `field`: A single data field containing a label and a value.
        ```
          {
            "index": 17,
            "type": "field",
            "property_id": "company_website",
            "label": "Company Website",
            "value": "https://www.nirvedacognition.ai",
            "regions": [ ... ]
          }
        ```

      - `item`: A compound item containing many elements (fields or collections).
        ```
          {
            "index": 23,
            "type": "item",
            "elements": [
              {
                "index": 17,
                "type": "field",
                ...
              },
              ...
            ],
            "regions": [ ... ]
          }
        ```

      - `collection`: A labeled group of homogeneous items.
        ```
          {
            "index": 47,
            "type": "collection",
            "property_id": "employees",
            "label": "Employees",
            "elements": [
              {
                "index": 23,
                "type": "item",
                ...
              },
              ...
            ],
            "regions": [ ... ]
          }
        ```

    For example, a `collection` element labeled "Employees" may contain a
    number of `item` elements, each representing a single employee and
    containing name, position, supervisor, and other fields. Perhaps position
    is itself a collection in which each item contains start date, title, and
    salary.

    In other words, an `item` is like an object or struct that has named
    properties (even though the properties are given as a list), whereas a
    `collection` is like an array of items of similar type.

- name: Region Element
  description: |
    ```
      {
        "x": number,
        "y": number,
        "width": number,
        "height": number,
        "page": integer,
        "type": enum  # "label", "value", "header", or "bounding"
      }
    ```
    The Region Element specifies where on a page and within a document a
    particular Data Element is located. X, Y, width, and height values are
    given in *relative* dimensions, i.e. 0 to 1 where 1 is the full width
    or height of the page.

    The Region Element has a `type` property which gives further information
    about how to interpret each region. The region may apply specifically to
    the "label" or the "value" (i.e. for a data element of type "field") or
    it may be a "bounding" region containing the entire element together
    (label and value for a "field" element, or table header and rows for a
    "collection" element). A "header" region is similar to "label" but for an
    individual field within an item in a collection, where the header region
    may be separated from the value region and/or may apply to multple value
    regions. In this case, the header region is returned but is not included
    within the bounding region for the "field" element. (It would however be
    included in the bounding region for the parent "collection" element.)
    Note in this case it is possible to have both a "value" and "bounding"
    region which are otherwise identical.

- name: Breaking changes
  description: |
    The following are breaking changes from previous Nirveda API offerings:
      * API key must be provided to `/token` endpoint in `api_key` body parameter or in `X-API-Key` header. `key` query string parameter is no longer supported.
      * `/document-extractor` endpoint has been removed. Use `/task` endpoint to create document extraction tasks.
      * `id` property of data elements has been renamed to `index` in order to avoid common confusion. The index is a unique positional identifier within the result set, and has no other semantic or identifying meaning.

- name: Additions
  description: |
    The following are additions to the API specification:
      * JSON body is supported in all POST requests including `/token` (which still supports form-encoded body in compliance with Oauth2).
      * A unique `document_id` is returned in the task create response for each file to be uploaded.
      * `extractor_id` is no longer a parameter of task create request.

paths:

  "/token":
    post:
      tags:
      - Authentication
      operationId: token
      summary: Get authorization token
      description: |
        Returns a temporary authorization token (OAuth 2.0 Bearer Token).

        Nirveda Cognitive Platform API utilizes an OAuth 2.0 Client Credentials
        flow, in which tokens are obtained via `POST` request to the `/v1/token`
        endpoint. You must include the parameters `api_key=YOUR_API_KEY`,
        `client_secret=YOUR_CLIENT_SECRET` and `grant_type=client_credentials`
        in the JSON or form-encoded request body. Optionally, your API key may
        be specified in the `X-API-Key` header.
      consumes:
      - application/json
      - application/x-www-form-urlencoded
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/tokenResponse"
        401:
          description: Unauthorized
          schema:
            $ref: "#/definitions/error"
      parameters:
      - in: header
        name: X-API-Key
        description: "Client API key"
        type: string
      - in: body
        name: body
        schema:
          $ref: "#/definitions/tokenRequest"
      security:
      - API Key in header: []
      - API Key in body: []

  "/generate-secret":
    get:
      tags:
      - Authentication
      summary: Generate client secret
      description: "Request a temporary secret reset link via email."
      operationId: generate_secret
      responses:
        200:
          description: OK
        401:
          description: Unauthorized
          schema:
            $ref: "#/definitions/error"
      parameters:
      - in: query
        name: api_key
        description: "Client API key"
        type: string
      - in: header
        name: X-API-Key
        description: "Client API key"
        type: string
      security:
      - API Key in header: []
      - API Key in query: []

  "/task":
    post:
      tags:
      - Task
      summary: Create task
      description: "Create a new task."
      operationId: create_task
      responses:
        201:
          description: Created
          schema:
            $ref: "#/definitions/taskCreateResponse"
        400:
          description: Bad Request
          schema:
            $ref: "#/definitions/error"
        401:
          description: Unauthorized
          schema:
            $ref: "#/definitions/error"
      parameters:
      - in: header
        name: X-API-Key
        description: "Client API key"
        type: string
      - in: header
        name: Authorization
        description: "Format: `Bearer AUTHORIZATION_TOKEN`"
        type: string
      - in: body
        name: body
        schema:
          $ref: "#/definitions/taskCreateRequest"

  "/task/{id}":
    get:
      tags:
      - Task
      summary: Get status or result
      description: "Get the status or result of a task."
      operationId: get_task
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/taskResult"
        401:
          description: Unauthorized
          schema:
            $ref: "#/definitions/error"
        404:
          description: Not Found
          schema:
            $ref: "#/definitions/error"
      parameters:
      - in: header
        name: X-API-Key
        description: "Client API key"
        type: string
      - in: header
        name: Authorization
        description: "Format: `Bearer AUTHORIZATION_TOKEN`"
        type: string
      - in: path
        name: id
        description: "Task ID"
        type: string
        required: true

definitions:

  error:
    type: object
    properties:
      code:
        type: integer
      message:
        type: string
      details:
        type: array
    required:
    - code
    - message

  tokenRequest:
    type: object
    properties:
      api_key:
        type: string
        description: "Client API key"
      client_secret:
        type: string
        description: "Client secret"
      grant_type:
        type: string
        description: "OAuth 2.0 grant type"
        enum: [ client_credentials ]
    required:
    - client_secret
    - grant_type

  tokenResponse:
    type: object
    properties:
      access_token:
        type: string
      token_type:
        type: string
        enum: [ Bearer ]
      expires_in:
        type: number
    required:
    - access_token
    - token_type
    - expires_in

  taskCreateRequest:
    type: object
    properties:
      files:
        description: "Source documents"
        type: array
        items:
          type: object
          properties:
            name:
              description: |
                File name (required).

                May be any valid string excluding newlines.
                Leading or trailing whitespace will be stripped.
              type: string
            content_type:
              description: |
                MIME type (required).

                Must match HTTP Content-Type header in subsequent upload.
                Example: "application/pdf"
              type: string
          required:
          - name
          - content_type
        minItems: 1
      callback_url:
        description: |
          Result callback URL (optional).

          Task status and result can normally be obtained via the [Get Task](#operation/get_task) endpoint.
          If you would like to also receive a callback `POST` request when the task completes, include a `callback_url` here.
        type: string
    required:
    - files

  taskCreateResponse:
    type: object
    properties:
      task_id:
        description: "Unique ID of the created task"
        type: string
      status:
        description: "Current task status"
        type: string
        enum: [ AWAITING_FILES ]
      files:
        description: "Signed URLs for uploading required files"
        type: array
        items:
          type: object
          properties:
            document_id:
              description: "Unique ID of this document"
              type: string
            name:
              description: "Name of this file"
              type: string
            upload_url:
              description: "Signed URL to use for uploading"
              type: string
            method:
              description: "HTTP method to use for uploading"
              type: string
              enum: [ PUT ]
            headers:
              description: "HTTP headers to use for uploading"
              type: string
            expires:
              description: "Date and time the signed URL expires (ISO 8601 format)"
              type: string
          required:
          - document_id
          - name
          - upload_url
          - method
          - headers
          - expires
    required:
    - task_id
    - status
    - files

  taskResult:
    type: object
    properties:
      task_id:
        description: "Task ID"
        type: string
      status:
        description: "Current task status"
        type: string
        enum: [ AWAITING_FILES, PENDING, RETRY, SUCCESS, FAILURE ]
      files:
        description: "Signed URLs for uploading required files, if still awaiting files."
        type: array
        items:
          type: object
          properties:
            document_id:
              description: "Unique ID of this document"
              type: string
            name:
              description: "Name of this file"
              type: string
            upload_url:
              description: "Signed URL to use for uploading"
              type: string
            method:
              description: "HTTP method to use for uploading"
              type: string
              enum: [ PUT ]
            headers:
              description: "HTTP headers to use for uploading"
              type: string
            expires:
              description: "Date and time the signed URL expires (ISO 8601 format)"
              type: string
          required:
          - document_id
          - name
          - upload_url
          - method
          - headers
          - expires
      data:
        description: "Data returned from a successful task"
        type: object
        properties:
          request:
            description: "The original request sent to create this task"
            $ref: "#/definitions/taskCreateRequest"
          result:
            description: "Task result"
            type: object
            properties:
              result_type:
                description: "Result type"
                type: string
              extracted_data:
                description: "Extracted data"
                type: array
                items:
                  oneOf:
                  - $ref: "#/definitions/field"
                  - $ref: "#/definitions/collection"
            required:
            - result_type
            - extracted_data
        required:
        - request
        - result
      errors:
        description: "Errors returned from a failed task"
        type: array
        items:
          $ref: "#/definitions/taskError"
      meta:
        description: "Task metadata"
        type: object
    required:
    - task_id
    - status

  taskError:
    type: object
    properties:
      status:
        description: "HTTP status code, if applicable"
        type: string
      code:
        description: "An application-specific error code, if applicable"
        type: string
      title:
        type: string
      detail:
        type: string

  field:
    type: object
    properties:
      index:
        description: "Unique index (sequential integer) of this element within the document"
        type: integer
      type:
        description: "Type of this element"
        type: string
        enum: [ field ]
      property_id:
        description: "Property ID (token) of this element in the document schema"
        type: string
      label:
        description: "Label associated with this field"
        type: string
      value:
        description: "Value of this field"
        type: string
      regions:
        description: "Regions of this element within the document"
        type: array
        items:
          $ref: "#/definitions/regionElement"
    required:
    - index
    - type
    - property_id
    - label
    - value
    - regions

  item:
    type: object
    properties:
      index:
        description: "Unique index (sequential integer) of this element within the document"
        type: integer
      type:
        description: "Type of this element"
        type: string
        enum: [ item ]
      elements:
        description: "Elements (fields or collections) of this item"
        type: array
        items:
          oneOf:
          - $ref: "#/definitions/field"
          - $ref: "#/definitions/collection"
      regions:
        description: "Regions of this element within the document"
        type: array
        items:
          $ref: "#/definitions/regionElement"
    required:
    - index
    - type
    - elements
    - regions

  collection:
    type: object
    properties:
      index:
        description: "Unique index (sequential integer) of this element within the document"
        type: integer
      type:
        description: "Type of this element"
        type: string
        enum: [ collection ]
      property_id:
        description: "Property ID (token) of this element in the document schema"
        type: string
      label:
        description: "Label associated with this collection"
        type: string
      items:
        description: "Items in this collection"
        type: array
        items:
          $ref: "#/definitions/item"
      regions:
        description: "Regions of this element within the document"
        type: array
        items:
          $ref: "#/definitions/regionElement"
    required:
    - index
    - type
    - property_id
    - label
    - items
    - regions

  dataElement:
    type: object
    oneOf:
    - $ref: "#/definitions/field"
    - $ref: "#/definitions/item"
    - $ref: "#/definitions/collection"
    discriminator:
      propertyName: type

  regionElement:
    type: object
    properties:
      x:
        description: "Relative left edge position"
        type: number
        minimum: 0
        maximum: 1
      y:
        description: "Relative top edge position"
        type: number
        minimum: 0
        maximum: 1
      width:
        description: "Relative width"
        type: number
        minimum: 0
        maximum: 1
      height:
        description: "Relative height"
        type: number
        minimum: 0
        maximum: 1
      page:
        description: "Page number"
        type: integer
        minimum: 1
      type:
        description: "Region type"
        type: string
        enum: [ label, value, header, bounding ]
    required:
    - x
    - y
    - width
    - height
    - page
    - type

securityDefinitions:

  API Key in header:
    description: |
      Access to some API endpoints is granted using an API key in the
      `X-API-Key` HTTP header, such as obtaining authorization tokens or
      generating a new client secret.
    type: "apiKey"
    name: "X-API-Key"
    in: "header"

  API Key in query:
    description: |
      For some GET requests, the API key may be provided in the `api_key` query
      string parameter rather than in a header.
    type: "apiKey"
    name: "api_key"
    in: "query"

  API Key in body:
    description: |
      For some POST requests requiring body parameters, the API key may be
      provided in the `api_key` body parameter rather than in a header.
    type: "apiKey"
    name: "api_key"
    in: "formData"

  Bearer Token:
    description: |
      Access protected API endpoints by providing an OAuth 2.0 Bearer Token
      in the Authorization request header as `Authorization: Bearer AUTHORIZATION_TOKEN`.
    tokenUrl: "https://api.nirvedacognition.ai/v1/token"
    flow: "application"
    type: "oauth2"
